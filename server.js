// TODO: NOTE for Igor or Maxim
// TODO: we have duplicate code at 'lead-11' and 'leads' cases
// TODO: duplications should be merged to some function

/* global leadsCollection */
/* global agentsCollection */
/* global conciergesCollection */

/** @typedef {LeadCollection} leadsCollection */
/** @typedef {AgentCollection} agentsCollection */
/** @typedef {ConciergeCollection} conciergesCollection */

require('./websocket/global');

const socket = require('socket.io');
const redis = require('redis');

/**
 * At first we create server application which assign & run Rollbar.
 * Also it has special configuration object which will be used in other
 * features.
 */

const app = require('./websocket/app');

const server = app.server;
const config = app.config;
const log = require('./websocket/logger');

/**
 * Starts socket io.
 * Add event listeners for different channels events.
 */

const io = socket(server);

server.listen(config.port);

log.info(`Server start on port ${config.port}`);

/**
 * Start redis & subscribe to the `system` channel where we listen
 * events fired by initial command, then we get data and put it to the collections.
 */

const daemonRedisClient = redis.createClient(config.redis);

daemonRedisClient.on('error', err => {
    log.info(`Redis error ${err}`);
});

log.info('Initialize Redis ON MESSAGE connection listener');

daemonRedisClient.on('message', (channel, message) => {
    require('./websocket/cache/redis')(message);
});

daemonRedisClient.subscribe('system');

log.info('Subscribe Redis client to the SYSTEM channel');

/**
 * Here we load artisan command exec and run commands
 * which send messages to the `system` channel.
 */

const artisan = require('./websocket/artisan');

artisan.initial();

/**
 * Here we require RedisEmitter to listen & fire all events.
 */

const emitter = require('./websocket/emitter');

log.info('Initialize IO connection');

io.on('connection', socket => {
    const socketId = socket.id.toString().replace('-', '');
    const sub = redis.createClient(config.redis);

    let currentConciergeId = null;
    let currentAgentId = null;

    sub.on('error', err => {
        log.info(`Redis error ${err}`);
    });

    log.info('Subscribes to redis messages');

    sub.on('message', (channel, message) => {
        const json = JSON.parse(message);
        const event = json.event;

        /** @typedef {object} data */
        const data = json.data;
        let sender = null;

        if (typeof data.sender !== 'undefined') {
            sender = data.sender;
        }

        delete data.sender;
        delete data.socket;

        const subscribeConciergesLeadsCh = (/subscribe|concierges|leads/).test(channel);
        if (subscribeConciergesLeadsCh) {
            require(`./websocket/channels/${channel}`)(emitter);
        }

        const agentCh = (/agent-(\d+)/i).test(channel);
        if (agentCh) {
            require('./websocket/channels/agent')(emitter);
        }

        const conciergeCh = (/concierge-(\d+)/i).test(channel);
        if (conciergeCh) {
            require('./websocket/channels/concierge')(emitter);
        }

        const leadChRegExp = /lead-([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/i;
        const leadCh = leadChRegExp.test(channel);
        if (leadCh) {
            const match = channel.match(leadChRegExp);
            const index = 1;
            let leadId = null;

            if (typeof match[index] !== 'undefined') {
                leadId = match[index];
            }

            require('./websocket/channels/lead')(emitter, leadId);
        }

        if (!(subscribeConciergesLeadsCh || agentCh || conciergeCh || leadCh)) {
            log.warn(`Unexpected channel: ${channel}`);
        }

        log.info(`Listen ${channel} event: ${event}`);

        emitter.emit(event, {
            sub,
            socket,
            socketId,
            currentConciergeId,
            currentAgentId,
            data,
            sender,
        });
        emitter.removeAllListeners();
    });

    /**
     * Here we add some messages listeners which executes
     * artisan commands.
     */

    socket.on('subscribeAdminConcierges', conciergeId => {
        sub.unsubscribe();
        sub.subscribe('concierges');
        sub.subscribe(`concierge-${parseInt(conciergeId, 10)}`);

        currentConciergeId = conciergeId;

        socket.json.send('conciergeAll', conciergesCollection.collection);
    });

    socket.on('conciergeCreate', (conciergeId, token, name, email, role) => {
        artisan.run('concierge:create', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            role: parseInt(role, 10),
            name,
            email,
            token,
        });
    });

    socket.on('conciergeDelete', (conciergeId, token, id) => {
        artisan.run('concierge:delete', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            token,
            id,
        });
    });

    socket.on('conciergeUpdate', (conciergeId, token, id, name, email, role) => {
        artisan.run('concierge:update', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            role: parseInt(role, 10),
            token,
            id,
            name,
            email,
        });
    });

    socket.on('conciergeSwitchDistribution', (conciergeId, token, id, isPaused) => {
        artisan.run('concierge:switchDistribution', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            token,
            id,
            isPaused,
        });
    });

    socket.on('agentPersonaUpdate', (agentId, token, agentPersona) => {
        artisan.run('agent:persona:update', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            data: agentPersona,
            token,
        });
    });

    socket.on('agentNotificationUpdate', (agentId, token, agentNotification) => {
        artisan.run('agent:notification:update', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            data: agentNotification,
            token,
        });
    });

    socket.on('conciergeSetAway', (conciergeId, token, away) => {
        conciergesCollection.setAway(conciergeId, away);
        if (away) {
            artisan.run('lead:reassign', {
                cid: parseInt(conciergeId, 10),
                sid: socketId,
                token,
            });
        } else {
            artisan.run('subscribe:messaging', {
                cid: parseInt(conciergeId, 10),
                sid: socketId,
                token,
            });
        }
    });

    socket.on('conciergeSetLastAfk', (conciergeId, token, lastAfkActivityIn) => {
        const currentConcierge = conciergesCollection.getItemOrCreate(conciergeId);
        const pub = redis.createClient(config.redis);

        pub.on('error', err => {
            log.info(`Redis error ${err}`);
        });

        currentConcierge.updateDeviceActivity(lastAfkActivityIn);

        pub.publish(`concierge-${parseInt(conciergeId, 10)}`, JSON.stringify({
            event: 'App\\Events\\Concierge\\conciergeSetLastAfk',
            data: {
                lastAfkActivity: lastAfkActivityIn,
                sender: socketId,
            },
        }));

        pub.quit();
    });

    socket.on('subscribeAdminLeads', (
        conciergeId,
        token,
        perPage,
        page,
        search,
        filterByStatus,
        filterByConcierge,
        isArchived,
        filterByAgent,
        filterByTag,
        filterByRepTier,
        filterCreatedAtFrom,
        filterCreatedAtTo
    ) => {
        sub.unsubscribe();
        sub.subscribe('subscribe');
        sub.subscribe('concierges');
        sub.subscribe(`concierge-${parseInt(conciergeId, 10)}`);

        currentConciergeId = conciergeId;
        const filterByAgentCleaned = filterByAgent === null ? null : parseInt(filterByAgent, 10);
        const filterByRepTierCleaned = filterByRepTier === null ? null : parseInt(filterByRepTier, 10);
        const filterByConciergeCleaned = filterByConcierge === null ? null : parseInt(filterByConcierge, 10);

        const data = leadsCollection.getOrderedLeadsBlock(
            perPage,
            page,
            search,
            filterByStatus,
            filterByConciergeCleaned,
            isArchived,
            null,
            null,
            filterByAgentCleaned,
            filterByTag,
            filterByRepTierCleaned,
            filterCreatedAtFrom,
            filterCreatedAtTo
        );

        for (const index in leadsCollection.collection) {
            sub.subscribe(`lead-${leadsCollection.collection[index].id}`);
        }

        socket.json.send('leadBlock', {
            leads: data.leads,
            totalPages: data.totalPages,
        });
        socket.json.send('conciergeAll', conciergesCollection.collection);

        socket.json.send('agentAll', agentsCollection.collection);
    });

    socket.on('resubscribeAdminLeads', (
        perPage,
        page,
        search,
        filterByStatus,
        filterByConcierge,
        isArchived,
        orderColumn,
        orderDirection,
        filterByAgent,
        filterByTag,
        filterByRepTier,
        filterCreatedAtFrom,
        filterCreatedAtTo
    ) => {
        const filterByAgentCleaned = filterByAgent === null ? null : parseInt(filterByAgent, 10);
        const filterByRepTierCleaned = filterByRepTier === null ? null : parseInt(filterByRepTier, 10);
        const filterByConciergeCleaned = filterByConcierge === null ? null : parseInt(filterByConcierge, 10);

        const data = leadsCollection.getOrderedLeadsBlock(
            perPage,
            page,
            search,
            filterByStatus,
            filterByConciergeCleaned,
            isArchived,
            orderColumn,
            orderDirection,
            filterByAgentCleaned,
            filterByTag,
            filterByRepTierCleaned,
            filterCreatedAtFrom,
            filterCreatedAtTo
        );

        socket.json.send('leadBlock', {
            leads: data.leads,
            totalPages: data.totalPages,
        });
    });

    socket.on('subscribePortalLeads', (agentId, token) => {
        sub.unsubscribe();
        sub.subscribe('leads');
        sub.subscribe(`agent-${parseInt(agentId, 10)}`);
        currentAgentId = agentId;

        const currentAgent = agentsCollection.getItemOrCreate(agentId);

        for (const leadIdRelated in currentAgent.leads) {
            sub.subscribe(`lead-${leadIdRelated}`);
        }
        const params = {
            aid: parseInt(agentId, 10),
            sid: socketId,
            token,
        };

        artisan.run('lead:get-portal', params);
        socket.json.send('agentGet', agentsCollection.getItem(agentId));
    });

    socket.on('profileUpdate', (agentId, token, agent) => {
        artisan.run('agent:update', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            data: agent,
            token,
        });
    });

    socket.on('disconnectGmailViking', (agentId, token) => {
        artisan.run('viking:disconnect:google', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            token,
        });
    });

    socket.on('subscribeProfile', agentId => {
        sub.unsubscribe();
        sub.subscribe(`agent-${parseInt(agentId, 10)}`);

        currentAgentId = agentId;

        socket.json.send('agentGet', agentsCollection.getItem(agentId));
    });

    socket.on('subscribeLead', (agentId, token, leadId) => {
        sub.unsubscribe();
        sub.subscribe(`agent-${parseInt(agentId, 10)}`);
        sub.subscribe(`lead-${leadId}`);

        currentAgentId = agentId;

        socket.json.send('agentGet', agentsCollection.getItem(agentId));

        artisan.run('lead:get', {
            aid: parseInt(agentId, 10),
            lid: leadId,
            sid: socketId,
            token,
        });
    });

    socket.on('leadUpdate', (conciergeId, token, data) => {
        artisan.run('lead:update', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            data,
            token,
        });
    });

    socket.on('leadUpdateByAgent', (agentId, token, data) => {
        artisan.run('lead:update:by-agent', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            data,
            token,
        });
    });

    socket.on('leadUpdateStatus', (conciergeId, token, leadId, action, note, date, timezone) => {
        artisan.run('lead:update:status', {
            cid: parseInt(conciergeId, 10),
            lid: leadId,
            sid: socketId,
            note,
            action,
            token,
            date,
            timezone,
        });
    });

    socket.on('leadRefer', (agentId, token, leadId) => {
        artisan.run('lead:refer', {
            aid: parseInt(agentId, 10),
            lid: leadId,
            sid: socketId,
            token,
        });
    });

    socket.on('subscribeSettings', agentId => {
        sub.unsubscribe();
        sub.subscribe(`agent-${parseInt(agentId, 10)}`);
        // TODO: remove this channel
        sub.subscribe(`settings-${parseInt(agentId, 10)}`);

        currentAgentId = agentId;

        socket.json.send('agentGet', agentsCollection.getItem(agentId));
    });

    socket.on('generatePhoneNumber', (agentId, token, areaCode) => {
        artisan.run('twilio:generate:phone', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            areaCode,
            token,
        });
    });

    socket.on('updateTutorial', (agentId, token, data, step) => {
        artisan.run('agent:update:tutorial', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            data,
            step,
            token,
        });
    });

    socket.on('leadSendTo', (agentId, token, leadId, emails) => {
        artisan.run('agent:send:lead', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            leadId,
            emails,
            token,
        });
    });

    socket.on('subscribeSetup', (agentId, token) => {
        sub.unsubscribe();
        sub.subscribe(`agent-${parseInt(agentId, 10)}`);

        currentAgentId = agentId;

        artisan.run('agent:get:setup', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            token,
        });
    });

    socket.on('subscribeAgentPersona', (conciergeId, token, agentId) => {
        sub.subscribe(`agent-${agentId}`);

        currentConciergeId = conciergeId;

        artisan.run('agent:persona:get', {
            cid: parseInt(conciergeId, 10),
            aid: parseInt(agentId, 10),
            sid: socketId,
            token,
        });
    });

    socket.on('resubscribeAgentPersona', (conciergeId, token, previousAgentId, agentId) => {
        sub.unsubscribe(`agent-${previousAgentId}`);
        sub.subscribe(`agent-${agentId}`);

        artisan.run('agent:persona:get', {
            cid: parseInt(conciergeId, 10),
            aid: parseInt(agentId, 10),
            sid: socketId,
            token,
        });
    });

    socket.on('subscribeMessaging', (conciergeId, token, leadId) => {
        sub.unsubscribe();
        sub.subscribe('subscribe');
        sub.subscribe('leads');
        sub.subscribe('concierges');
        sub.subscribe(`concierge-${parseInt(conciergeId, 10)}`);
        currentConciergeId = conciergeId;

        const shouldSendInitRequest = conciergesCollection.connectDevice(conciergeId, socketId);
        const currentConcierge = conciergesCollection.getItemOrCreate(conciergeId);

        if (leadId) {
            sub.subscribe(`lead-${leadId}`);
        }

        for (const leadIdRelated in currentConcierge.leads) {
            if (leadId !== leadIdRelated) {
                sub.subscribe(`lead-${leadIdRelated}`);
            }
        }

        artisan.run('subscribe:messaging', {
            cid: parseInt(conciergeId, 10),
            lid: leadId,
            sid: socketId,
            shouldSendInitRequest,
            token,
        });
    });

    socket.on('messagingCall', (conciergeId, token, leadId, callSid, phone) => {
        artisan.run('lead:call', {
            cid: parseInt(conciergeId, 10),
            lid: leadId,
            csid: callSid,
            sid: socketId,
            phone,
            token,
        });
    });

    socket.on('messagingCallEnd', (conciergeId, token, leadId, callSid) => {
        artisan.run('lead:call:end', {
            cid: parseInt(conciergeId, 10),
            lid: leadId,
            csid: callSid,
            sid: socketId,
            token,
        });
    });

    socket.on('messagingCallNotes', (conciergeId, token, leadId, callId, notes) => {
        artisan.run('lead:call:notes', {
            cid: parseInt(conciergeId, 10),
            lid: leadId,
            sid: socketId,
            callId: parseInt(callId, 10),
            notes,
            token,
        });
    });

    socket.on('leadAll', (conciergeId, token) => {
        artisan.run('lead:all', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            token,
        });
    });

    socket.on('leadSendEmails', (agentId, token, leadId, emails) => {
        artisan.run('lead:send:emails', {
            aid: parseInt(agentId, 10),
            lid: leadId,
            sid: socketId,
            token,
            emails,
        });
    });

    socket.on('leadUpdateRep', (conciergeId, token, leadId, repId) => {
        artisan.run('lead:update:rep', {
            cid: parseInt(conciergeId, 10),
            rid: parseInt(repId, 10),
            lid: leadId,
            sid: socketId,
            token,
        });
    });

    socket.on('leadUnassignRep', (conciergeId, token, leadId) => {
        artisan.run('lead:unassign:rep', {
            cid: parseInt(conciergeId, 10),
            lid: leadId,
            sid: socketId,
            token,
        });
    });

    socket.on('messageCreate', (conciergeId, token, data) => {
        artisan.run('message:create', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            token,
            data,
        });
    });

    socket.on('messageReadAll', (conciergeId, token, leadId) => {
        artisan.run('message:read:all', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            lid: leadId,
            token,
        });
    });

    socket.on('messageUpdateStatus', (conciergeId, token, leadId, twilioMessageSid, status) => {
        artisan.run('message:update:status', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            leadId,
            twilioMessageSid,
            status,
            token,
        });
    });

    socket.on('emailCreate', (conciergeId, token, data) => {
        artisan.run('email:create', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            token,
            data,
        });
    });

    socket.on('emailReadAll', (conciergeId, token, leadId) => {
        artisan.run('email:read:all', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            lid: leadId,
            token,
        });
    });

    socket.on('agentPasswordUpdate', (agentId, token, currentPassword, newPassword, senderSessionId) => {
        artisan.run('agent:password:update', {
            aid: parseInt(agentId, 10),
            sid: socketId,
            session: senderSessionId,
            password: currentPassword,
            newPassword,
            token,
        });
    });

    socket.on('noteCreate', (conciergeId, token, data, sendToAgent) => {
        artisan.run('note:create', {
            cid: parseInt(conciergeId, 10),
            sid: socketId,
            sendToAgent: !!sendToAgent,
            data,
            token,
        });
    });

    socket.on('stopTimer', leadId => {
        leadsCollection.collection[leadId].stopTimer();
    });

    socket.on('disconnect', () => {
        sub.quit();
        conciergesCollection.disconnectDevice(socketId);
    });

    socket.on('leadValidatePhone', (conciergeId, token, phone, leadId) => {
        artisan.run('twilio:validate:phone', {
            cid: parseInt(conciergeId, 10),
            lid: leadId,
            sid: socketId,
            phone,
            token,
        });
    });
});
