<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'PortalController@getIndex')->name('index_portal');

Route::group(['middleware' => 'guest:portal'], function () {
    Route::get('/create-account', 'PortalController@getIndex')->name('create_account_portal');
    //Portal registration routes
    Route::group(['prefix' => 'register-portal'], function () {
        Route::post('/register', 'Auth\RegisterController@registerPortalUser')->name('portal_user_register');
        Route::group(['prefix' => 'google'], function () {
            Route::get('/', 'Auth\LoginPortalController@redirectToRegisterPortalProvider')->name('portal_google_auth');
            Route::get('/callback', 'Auth\LoginPortalController@handleRegisterPortalProviderCallback')->name('portal_google_auth_register_callback');
        });
    });
    //Portal login and forgot password routes
    Route::group(['prefix' => 'login-portal'], function () {
        Route::get('/forgot-password', 'PortalController@getIndex')->name('forgot_password_portal');
        Route::get('/reset-password', 'PortalController@resetPortalPasswordView')->name('reset_password_portal');
        Route::post('/login', 'Auth\LoginPortalController@loginPortalUser')->name('portal_user_login');
        Route::group(['prefix' => 'google'], function () {
            Route::get('/', 'Auth\LoginPortalController@redirectToLoginPortalProvider')->name('portal_google_auth_login');
            Route::get('/callback', 'Auth\LoginPortalController@handlePortalLoginProviderCallback')->name('portal_google_auth_login_callback');
        });
    });
    // Restore password
    Route::group(['prefix' => 'portal'], function () {
        Route::post('/send-new-password', 'Auth\ForgotPasswordController@sendNewPassword')->name('portal_send_new_password');
        Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@portalPasswordReset')->name('password.reset');
        Route::get('/reset-form', 'Auth\ResetPasswordController@portalPasswordResetForm')->name('portal_password_reset_form');
        Route::post('/set-new-password', 'Auth\ResetPasswordController@portalSetNewPassword')->name('set_new_password');
    });
});

// Emails (in browser view)
Route::get('/emails/{uuid}/view', 'EmailController@viewInBrowser')->name('portal_emails');

Route::group(['middleware' => 'auth:portal'], function () {
    Route::get('/logout', 'Auth\LoginPortalController@logout')->name('logout_portal');
    Route::group(['prefix' => 'portal'], function () {
        // Main auth page
        Route::get('/', function() {
            return redirect('/portal/myleads/all');
        })->name('portal_user');
        // Leads lists
        Route::get('/myleads/{tab?}', 'PortalController@getContent')->name('portal_myleads');
        // Lead view/edit
        Route::get('/lead/{id}', 'PortalController@getContent')->name('portal_lead');
        // Profile
        Route::get('/profile/{tab?}', 'PortalController@getContent')->name('profile_portal');
        // Billing
        Route::get('/billing', 'PortalController@getContent')->name('billing_portal');
        // Lead source
        Route::get('/lead-source', 'PortalController@getContent')->name('lead_source_portal');
        // Settings
        Route::get('/settings/{tab?}', 'PortalController@getContent')->name('settings_portal');
        // Support
        Route::get('/support', 'PortalController@getContent')->name('support_portal');
        // Setup (finish registration)
        Route::get('/setup', 'PortalController@getContent')->name('portal_setup');
        Route::get('/agent-email-token', 'GoogleController@getGoogleAuthCode')->name('agent_email_token');
    });

    Route::group(['prefix' => 'google'], function () {
        Route::get('/viking-google-auth', 'GoogleController@vikingGoogleAuth')->name('viking_google_auth');
        Route::get('/process-viking-google-response', 'GoogleController@vikingGoogleAuthProcessing')->name('viking_google_response');
    });
});

Route::group(['prefix' => 'console'], function () {
    Route::group(['middleware' => 'guest:web'], function () {
        Route::get('/', 'ConsoleController@getIndex')->name('index');
        // Login
        Route::group(['prefix' => 'login'], function () {
            // Google authorization
            Route::group(['prefix' => 'google'], function () {
                Route::get('/', 'Auth\LoginConsoleController@redirectToProvider')->name('google_auth');
                Route::get('/callback', 'Auth\LoginConsoleController@handleProviderCallback')->name('google_auth_callback');
            });
        });
    });
    Route::group(['middleware' => ['auth:web', 'checkConciergeSuspended']], function () {
        // Logout
        Route::get('/logout', 'Auth\LoginConsoleController@logout')->name('logout');
        // Permissions
        Route::get('/permission', 'ConsoleController@getIndex')->name('permission');
        Route::group(['prefix' => 'leads'], function () {
            Route::get('/', 'ConsoleController@getIndex')->name('leads');
            Route::get('/active', 'ConsoleController@getIndex')->name('leads_active');
            Route::get('/archive', 'ConsoleController@getIndex')->name('leads_archive');
        });
        // Messaging
        Route::get('/messaging/{id?}', 'ConsoleController@getIndex')->name('messaging');
    });
});
