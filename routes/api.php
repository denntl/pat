<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function () {
    Route::group(['prefix' => 'leads', 'namespace' => 'Leads', 'middleware' => 'authClient'], function () {
        Route::post('/', 'LeadsController@createLead');
    });

    Route::group(['prefix' => 'twilio', 'namespace' => 'Twilio'], function () {
        Route::group(['prefix' => 'messages', 'middleware' => 'twilioAccess'], function () {
            Route::post('/receive', 'WebhookController@receive');
            Route::post('/status-updated', 'WebhookController@statusUpdated')
                ->name('twilio_message_status_updated_hook');
        });

        Route::group(['prefix' => 'call'], function () {
            Route::post('/', 'WebhookController@call');
            Route::post('/status-updated', 'WebhookController@callStatusUpdated')
                ->name('twilio_call_status_updated_hook');
        });

    });

    Route::group(['prefix' => 'sendGrid', 'namespace' => 'SendGrid', 'middleware' => 'sendGridCheck'], function () {
        Route::post('/letterStatus', 'WebHookController@sendGridEmailEvent');
        Route::post('/inboundParse', 'WebHookController@sendGridParseInbound');
    });

});