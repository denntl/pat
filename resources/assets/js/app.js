import Vue from 'vue';
import axios from 'axios';
import VueFloatLabel from 'vue-float-label';
import VueMask from 'v-mask';
import Rollbar from 'vue-rollbar';
import VueWebsocket from 'vue-websocket';
import router from './router';
import store from './store';

window.jQuery = require('jquery');

window.$ = window.jQuery;

require('./validator');
require('./directives/loader')(Vue);
require('./components/loader')(Vue);

Vue.prototype.$http = axios.create();
Vue.prototype.$role = {
    SUSPENDED: 0,
    TIER1: 1,
    TIER2: 2,
    ADMIN: 3,
};

Vue.use(VueFloatLabel);
Vue.use(VueMask);

if (laravelEnv === 'production' || laravelEnv === 'demo' || laravelEnv === 'dev') {
    Vue.use(Rollbar, {
        accessToken: rollBarAccessToken,
        captureUncaught: true,
        captureUnhandledRejections: true,
        payload: {
            environment: laravelEnv,
        },
    });
}

Vue.use(VueWebsocket, `${window.location.protocol === 'http:' ? 'ws' : 'wss' }://${ location.hostname }:8890`);

new Vue({
    router,
    store,
    data: {
        token: document.getElementById('token').value,
        role: parseInt(document.getElementById('role').value, 10),
        isConcierge: document.getElementById('isConcierge').value,
        id: parseInt(document.getElementById('id').value, 10),
        referralNetworkUrl: document.getElementById('referralNetworkUrl').value,
    },
}).$mount('#app');
