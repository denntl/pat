import Concierge from '../objects/concierge';

/**
 * @class ConciergeCollection
 *
 * @property {Concierge[]} concierges
 * @property {String} search
 * @property {Array} computedList
 * @property {Boolean} createMode
 * @property {Concierge} createConcierge
 * @property {Number|null} waitForSyncId,
 * @property {String|null} orderColumn,
 * @property {Boolean|null} orderDirectionUp
 */
export default class ConciergeCollection {
    /**
     * @constructor
     */
    constructor() {
        this.concierges = [];
        this.search = '';
        this.computedList = [];
        this.createMode = false;
        this.createConcierge = new Concierge(null, '', '', '0');
        this.waitForSyncId = null;
        this.orderColumn = null;
        this.orderDirectionUp = null;
    }

    /**
     * Update concierge's list.
     *
     * @returns {void}
     */
    cleanCollection() {
        this.concierges = [];
        this.waitForSyncId = null;

        this.updateCollection();
    }

    /**
     * Update concierge's list.
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getConcierges();
    }

    /**
     * Prepare add concierge.
     *
     * @returns {void}
     */
    prepareAddConcierge() {
        this.createMode = true;
        this.createConcierge = new Concierge(null, '', '', '0');
        this.createConcierge.stateEdit = true;

        this.updateCollection();
    }

    /**
     * Add concierge.
     *
     * @param {Concierge} concierge - Concierge object
     *
     * @returns {void}
     */
    addConcierge(concierge) {
        let id = concierge.id;
        if (id === null) {
            let maxId = 0;

            for (const index in this.concierges) {
                if (maxId < index) {
                    maxId = index;
                }
            }

            id = maxId + 1000;

            this.waitForSyncId = id;
        }

        this.concierges[id] = concierge;
        this.createMode = false;
        this.createConcierge = new Concierge(null, '', '', '0');
    }

    /**
     * Sync concierge.
     *
     * @param {number} id - Concierge id
     *
     * @returns {void}
     */
    syncConcierge(id) {
        if (this.waitForSyncId === null) {
            return;
        }

        this.concierges[id] = this.concierges[this.waitForSyncId];

        delete this.concierges[this.waitForSyncId];

        this.concierges[id].id = id;
        this.waitForSyncId = null;
        this.createMode = false;
        this.createConcierge = new Concierge(null, '', '', '0');

        this.updateCollection();
    }

    /**
     * Delete concierge.
     *
     * @param {Number} id - Concierge id
     *
     * @returns {void}
     */
    deleteConcierge(id) {
        delete this.concierges[id];

        this.updateCollection();
    }

    /**
     * Switch lead distribution.
     *
     * @param {Number} id - Concierge id
     * @param {Boolean} isPaused - Is paused lead distribution
     *
     * @returns {void}
     */
    switchDistribution(id, isPaused) {
        if (this.concierges[id]) {
            this.concierges[id].isPaused = isPaused;
            this.updateCollection();
        }
    }

    /**
     * Return Concierge by id
     *
     * @param {Number} id - Concierge id
     *
     * @returns {Concierge|null} - Concierge object
     */
    getConcierge(id) {
        if (id === null) {
            return this.createConcierge;
        }

        const newConcierge = new Concierge(null, 'invalid', 'invalid', '0');

        return typeof this.concierges[id] == 'undefined' ? newConcierge : this.concierges[id];
    }

    /**
     * Return filtered 'concierges' array
     *
     * @returns {Array} - All concierges in collection
     */
    getConcierges() {
        const ret = [];
        const searchWord = this.search.toLowerCase();

        for (const index in this.concierges) {
            const concierge = this.concierges[index];

            if (this.search !== '') {
                const name = concierge.name ? concierge.name.toLowerCase() : '';
                const email = concierge.email ? concierge.email.toLowerCase() : '';
                const nameIncludeSearch = name.includes(searchWord);
                const emailIncludeSearch = email.includes(searchWord);

                if (!(nameIncludeSearch || emailIncludeSearch)) {
                    continue;
                }
            }

            ret.unshift(concierge);
        }

        if (this.createMode) {
            ret.unshift(this.createConcierge);
        }

        if (this.orderColumn !== null) {
            return this.sortRet(ret);
        }

        return ret;
    }

    /**
     * Sort concierges by column.
     *
     * @param {Array} ret - Concierges array.
     *
     * @returns {Array} - return sorted concierges array.
     */
    sortRet(ret) {
        const rolesNames = {
            0: 'Suspended',
            1: 'Tier 1',
            2: 'Tier 2',
            3: 'Admin',
        };

        return ret.sort((fst, snd) => {
            let orderType = null;

            const aName = fst.name ? fst.name.toLowerCase() : '';
            const bName = snd.name ? snd.name.toLowerCase() : '';

            const direction = this.orderDirectionUp;

            const aEmail = fst.email ? fst.email.toLowerCase() : '';
            const bEmail = snd.email ? snd.email.toLowerCase() : '';

            const aRole = rolesNames[fst.role].toLowerCase();
            const bRole = rolesNames[snd.role].toLowerCase();

            switch (this.orderColumn) {
                case 'name':
                    orderType = ConciergeCollection.comparisonOrderValues(aName, bName, direction);
                    break;
                case 'email':
                    orderType = ConciergeCollection.comparisonOrderValues(aEmail, bEmail, direction);
                    break;
                case 'permissions':
                    orderType = ConciergeCollection.comparisonOrderValues(aRole, bRole, direction);
                    break;
                default:
                    break;
            }

            return orderType !== null ? orderType : 0;
        });
    }

    /**
     * Comparison order values.
     *
     * @param {string} first - First order param.
     * @param {string} second - Second order param.
     * @param {boolean} orderDirectionUp - Is order direction up.
     *
     * @returns {null|number} - Return order type.
     */
    static comparisonOrderValues(first, second, orderDirectionUp) {
        const orderUp = 1;
        const orderDown = -1;

        if (first < second) {
            return orderDirectionUp ? orderDown : orderUp;
        }

        if (first > second) {
            return orderDirectionUp ? orderUp : orderDown;
        }

        return null;
    }
}
