import Note from '../objects/note';

/**
 * @class NoteCollection
 *
 * @property {Note[]} notes
 * @property {Note[]} computedList
 * @property {boolean} createMode
 * @property {Note} createdNote
 * @property {number|null} syncId
 */
export default class NoteCollection {
    /**
     * @constructor
     */
    constructor() {
        this.notes = [];
        this.computedList = [];
        this.createMode = false;
        this.createdNote = new Note();
        this.syncId = null;
    }

    /**
     * Clean note's collection.
     *
     * @returns {void}
     */
    cleanCollection() {
        this.notes = [];
        this.syncId = null;
        this.updateCollection();
    }

    /**
     * Update note's collection.
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getNotesList();
    }

    /**
     * Prepare to add note.
     *
     * @returns {void}
     */
    prepareAddNote() {
        this.createMode = true;
        this.createdNote = new Note();
        this.updateCollection();
    }

    /**
     * Add note.
     *
     * @param {Note} note - Note object
     *
     * @returns {void}
     */
    addNote(note) {
        let id = note.id;
        if (id === null) {
            let maxId = 0;

            for (const index in this.notes) {
                if (maxId < index) {
                    maxId = index;
                }
            }

            id = maxId + 1000;
            this.syncId = id;
        }

        this.notes[id] = note;
        this.createMode = false;
        this.createdNote = new Note();
    }

    /**
     * Synchronize note.
     *
     * @param {number} id - Note id
     *
     * @returns {void}
     */
    syncNote(id) {
        if (this.syncId === null) {
            return;
        }

        this.notes[id] = this.notes[this.syncId];

        delete this.notes[this.syncId];

        this.notes[id].id = id;
        this.syncId = null;
        this.createMode = false;
        this.createdNote = new Note();

        this.updateCollection();
    }

    /**
     * Delete note from collection.
     *
     * @param {number} id - Note id
     *
     * @returns {void}
     */
    deleteNote(id) {
        delete this.notes[id];

        this.updateCollection();
    }

    /**
     * Get note by id.
     *
     * @param {number} id - Note id
     *
     * @returns {Note|null} - Note object
     */
    getNote(id) {
        if (id === null) {
            return this.createdNote;
        }

        return typeof this.notes[id] == 'undefined' ? new Note() : this.notes[id];
    }

    /**
     * Get notes list.
     *
     * @returns {Array} - Note list
     */
    getNotesList() {
        const temp = [];

        for (const index in this.notes) {
            const note = this.notes[index];

            temp.unshift(note);
        }

        return this.createMode ? temp.unshift(this.createdNote) : temp;
    }
}
