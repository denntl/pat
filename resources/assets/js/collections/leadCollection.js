import moment from 'moment';
import Lead from '../objects/lead';
import { isSearchSuccess } from '../helpers/lead';
import { sortRet } from '../helpers/lead';

/**
 * @class LeadCollection
 *
 * @property {Lead[]} leads
 * @property {String} search
 * @property {String|null} orderColumn
 */
export default class LeadCollection {
    /**
     * @constructor
     */
    constructor() {
        this.leads = [];
        this.search = '';
        this.filterStatus = '-';
        this.filterRep = '-';
        this.filterAgent = '-';
        this.filterTag = '-';
        this.filterRepTier = '-';
        this.filterCreatedAtFrom = '';
        this.filterCreatedAtTo = '';
        this.filterSource = [];
        this.filterType = '-';
        this.orderColumn = null;
        this.orderDirectionUp = true;
        this.subPage = null;
        this.subPageStatuses = [];
        this.computedList = [];
    }

    /**
     * Update lead's list
     *
     * @returns {void}
     */
    cleanCollection() {
        this.leads = [];
        this.waitForSyncId = null;
        this.updateCollection();
    }

    /**
     * Update lead's list
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getLeads();
    }

    /**
     * Update timers
     *
     * @returns {void}
     */
    updateTimers() {
        for (const index in this.leads) {
            const lead = this.leads[index];

            if (lead.finalTimeForResponse) {
                const time = lead.finalTimeForResponse - new Date().getTime();
                const fiveMin = 300000;

                lead.showTimer = time <= fiveMin && !this.showTimer;
                lead.timeLeftForResponse = time > 0 ? moment.utc(time).format('mm:ss') : '00:00';
            }
        }
    }

    /**
     * Add lead
     *
     * @param {Lead} lead - Lead object
     *
     * @returns {void}
     */
    addLead(lead) {
        let id = lead.id;
        if (id === null) {
            let maxId = 0;

            for (const index in this.leads) {
                if (maxId < index) {
                    maxId = index;
                }
            }

            id = maxId + 1000;
        }
        this.leads[id] = lead;
    }

    /**
     * Delete lead
     *
     * @param {Number} id - Lead id
     *
     * @returns {void}
     */
    deleteLead(id) {
        delete this.leads[id];
        this.updateCollection();
    }

    /**
     * Update lead
     *
     * @param {Object} data - Lead
     *
     * @returns {void}
     */
    updateLead(data) {
        if (data === null) {
            return;
        }

        if (typeof this.leads[data.id] === 'undefined') {
            return;
        }

        this.leads[data.id].addDataRecursively(data);
        this.updateCollection();
    }

    /**
     * Return lead by id
     *
     * @param {Number} id - Lead id
     *
     * @returns {Lead|null} - Lead object
     */
    getLead(id) {
        return typeof this.leads[id] === 'undefined' ? new Lead() : this.leads[id];
    }

    /**
     * Check filter status and filter rep.
     *
     * @param {Object} lead - Lead object.
     *
     * @returns {Boolean} - Result of check.
     */
    checkStatusRep(lead) {
        const checkFilterStatus = this.filterStatus !== '-' && this.filterStatus !== lead.status;
        const checkFilterRep = this.filterRep !== '-' && this.filterRep !== lead.conciergeId;

        return checkFilterStatus || checkFilterRep;
    }

    /**
     * Check sub page and lead status.
     *
     * @param {String} status - Lead status.
     *
     * @returns {Boolean} - Result of check.
     */
    checkLeadStatus(status) {
        let continueThis = true;

        for (const indexStatus in this.subPageStatuses) {
            if (this.subPageStatuses[indexStatus] === status) {
                continueThis = false;
                break;
            }
        }

        return continueThis;
    }

    /**
     * Check filter source.
     *
     * @param {String} leadSourceName - Lead source name.
     *
     * @returns {Boolean} - Result of check.
     */
    checkFilterSource(leadSourceName) {
        let continueThis = true;

        if (this.filterSource[0] === 'All') {
            return false;
        }

        for (const index in this.filterSource) {
            if (this.filterSource[index] === leadSourceName) {
                continueThis = false;
                break;
            }
        }

        return continueThis;
    }

    /**
     * Get leads function. Return filtered 'leads' array
     *
     * @returns {Array|Number} - All leads inside collection
     */
    getLeads() {
        const ret = [];

        for (const index in this.leads) {
            const lead = this.leads[index];

            if (!isSearchSuccess(lead, this.search) || this.checkStatusRep(lead)) {
                continue;
            }

            const checkLength = Object.keys(this.filterSource).length !== 0;
            const checkSourceId = lead.sourceId === null;
            const checkLeadSource = typeof lead.source === 'undefined';
            const checkFilterSource = this.checkFilterSource(lead.source.name);

            if (checkLength && (checkSourceId || checkLeadSource || checkFilterSource)) {
                continue;
            }

            const checkFilter = this.filterType === '-' || this.filterType === 'All';
            const checkLeadType = this.filterType !== lead.leadType;
            const checkSubPage = this.subPage !== null;
            const checkLeadStatus = this.checkLeadStatus(lead.status);

            if (!checkFilter && checkLeadType || checkSubPage && checkLeadStatus) {
                continue;
            }

            ret.unshift(lead);
        }

        return this.orderColumn !== null ? sortRet(ret, this) : ret;
    }

    /**
     * Check on empty leads.
     *
     * @returns {boolean} - return empty leads array or not
     */
    isEmpty() {
        return !Object.keys(this.leads).length;
    }
}
