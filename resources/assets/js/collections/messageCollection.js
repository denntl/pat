import Message from '../objects/message';

/**
 * @class MessageCollection
 *
 * @property {Message[]} messages
 * @property {Message[]} computedList
 * @property {number} unread
 * @property {Array} unreadMessagesIds
 * @property {boolean} createMode
 * @property {Message} createdMessage
 * @property {number|null} syncId
 */
export default class MessageCollection {
    /**
     * @constructor
     */
    constructor() {
        this.messages = [];
        this.computedList = [];
        this.unread = 0;
        this.unreadMessagesIds = [];
        this.createMode = false;
        this.createdMessage = new Message();
        this.syncId = null;
    }

    /**
     * Clean message's collection.
     *
     * @returns {void}
     */
    cleanCollection() {
        this.messages = [];
        this.syncId = null;
        this.unread = 0;
        this.updateCollection();
    }

    /**
     * Update message's collection.
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getMessagesList();
    }

    /**
     * Prepare to add message.
     *
     * @returns {void}
     */
    prepareAddMessage() {
        this.createMode = true;
        this.createdMessage = new Message();
        this.updateCollection();
    }

    /**
     * Set Message status.
     *
     * @param {Number} messageId - Email id
     * @param {Number} twilioMessageStatus - Status
     *
     * @returns {void}
     */
    setMessageStatus(messageId, twilioMessageStatus) {
        if (typeof this.messages[messageId] != 'undefined') {
            this.messages[messageId].twilioMessageStatus = twilioMessageStatus;
        }
    }

    /**
     * Add message.
     *
     * @param {Message} message - Message object
     *
     * @returns {void}
     */
    addMessage(message) {
        let id = message.id;
        if (id === null) {
            let maxId = 0;

            for (const index in this.messages) {
                if (maxId < index) {
                    maxId = index;
                }
            }

            id = maxId + 1000;
            this.syncId = id;
        }

        if (!message.isRead && !this.unreadMessagesIds.includes(id) && message.incoming) {
            this.unreadMessagesIds.push(id);
            this.unread++;
        }

        this.messages[id] = message;
        this.createMode = false;
        this.createdMessage = new Message();
    }

    /**
     * Synchronize message.
     *
     * @param {number} id - Messasge id
     *
     * @returns {void}
     */
    syncMessage(id) {
        if (this.syncId === null) {
            return;
        }

        this.messages[id] = this.messages[this.syncId];

        delete this.messages[this.syncId];

        this.messages[id].id = id;
        this.syncId = null;
        this.createMode = false;
        this.createdMessage = new Message();

        this.updateCollection();
    }

    /**
     * Delete message from collection.
     *
     * @param {number} id - Message id
     *
     * @returns {void}
     */
    deleteMessage(id) {
        delete this.messages[id];

        this.updateCollection();
    }

    /**
     * Get message by id.
     *
     * @param {number} id - Message id
     *
     * @returns {Message|null} - Message object
     */
    getMessage(id) {
        if (id === null) {
            return this.createdMessage;
        }

        return typeof this.messages[id] == 'undefined' ? new Message() : this.messages[id];
    }

    /**
     * Get messages list.
     *
     * @returns {Array} - Message list
     */
    getMessagesList() {
        const temp = [];

        for (const index in this.messages) {
            const message = this.messages[index];

            temp.unshift(message);
        }

        if (this.createMode) {
            temp.unshift(this.createdMessage);
        }

        temp.reverse();

        return temp;
    }

    /**
     * Set emails to "isRead".
     *
     * @param {Array} data - read emails id
     *
     * @returns {void}
     */
    readMessages(data) {
        if (typeof data == 'undefined') {
            return;
        }

        data.forEach(messageId => {
            this.messages[messageId].makeRead();
        });

        this.updateCollection();
    }
}
