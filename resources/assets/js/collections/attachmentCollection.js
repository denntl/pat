/**
 * @class AttachmentCollection
 *
 * @property {Attachment[]} attachments
 * @property {Attachment[]} computedList
 */
export default class AttachmentCollection {
    /**
     * @constructor
     */
    constructor() {
        this.attachments = [];
        this.computedList = [];
    }

    /**
     * Update attachments collection.
     * @returns {void}
     */
    cleanCollection() {
        this.attachments = [];
        this.updateCollection();
    }

    /**
     * Update attachments collection.
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getAttachmentsList();
    }

    /**
     * Add Attachment.
     *
     * @param {Attachment} attachment - Attachment object
     *
     * @returns {void}
     */
    addEmail(attachment) {
        this.attachments[attachment.id] = attachment;
    }

    /**
     * Get attachments list.
     *
     * @returns {Array} - Attachment list
     */
    getAttachmentsList() {
        const temp = [];

        for (const index in this.attachments) {
            const attachment = this.attachments[index];

            temp.unshift(attachment);
        }

        temp.reverse();

        return temp;
    }
}
