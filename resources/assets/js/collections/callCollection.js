import Call from '../objects/call';

/**
 * @class CallCollection
 *
 * @property {Call[]} calls
 * @property {Call[]} computedList
 * @property {boolean} createMode
 * @property {Call} createdCall
 * @property {number|null} syncId
 */
export default class CallCollection {
    /**
     * @constructor
     */
    constructor() {
        this.calls = [];
        this.computedList = [];
        this.createMode = false;
        this.createdCalls = new Call();
        this.syncId = null;
    }

    /**
     * Clean call's collection.
     *
     * @returns {void}
     */
    cleanCollection() {
        this.calls = [];
        this.syncId = null;
        this.updateCollection();
    }

    /**
     * Update call's collection.
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getCallsList();
    }

    /**
     * Prepare to add call.
     *
     * @returns {void}
     */
    prepareAddCall() {
        this.createMode = true;
        this.createdCall = new Call();
        this.updateCollection();
    }

    /**
     * Add call.
     *
     * @param {Call} call - Call object
     *
     * @returns {void}
     */
    addCall(call) {
        let id = call.id;
        if (id === null) {
            let maxId = 0;

            for (const index in this.calls) {
                if (maxId < index) {
                    maxId = index;
                }
            }

            id = maxId + 1000;
            this.syncId = id;
        }

        this.calls[id] = call;
        this.createMode = false;
        this.createdCall = new Call();
    }

    /**
     * Synchronize call.
     *
     * @param {number} id - Call id
     *
     * @returns {void}
     */
    syncCall(id) {
        if (this.syncId === null) {
            return;
        }

        this.calls[id] = this.calls[this.syncId];

        delete this.calls[this.syncId];

        this.calls[id].id = id;
        this.syncId = null;
        this.createMode = false;
        this.createdCall = new Call();

        this.updateCollection();
    }

    /**
     * Delete call from collection.
     *
     * @param {number} id - Call id
     *
     * @returns {void}
     */
    deleteCall(id) {
        delete this.calls[id];

        this.updateCollection();
    }

    /**
     * Get call by id.
     *
     * @param {number} id - Call id
     *
     * @returns {Call|null} - Call object
     */
    getCall(id) {
        if (id === null) {
            return this.createdCall;
        }

        return typeof this.call[id] == 'undefined' ? new Call() : this.calls[id];
    }

    /**
     * Get calls list.
     *
     * @returns {Array} - Call list
     */
    getCallsList() {
        const temp = [];

        for (const index in this.calls) {
            const call = this.calls[index];

            temp.unshift(call);
        }

        if (this.createMode) {
            temp.unshift(this.createdCall);
        }

        return temp;
    }
}
