import Email from '../objects/email';

/**
 * @class EmailCollection
 *
 * @property {Email[]} emails
 * @property {Email[]} computedList
 * @property {number} unread
 * @property {Array} unreadEmailsIds
 * @property {boolean} createMode
 * @property {Email} createdMessage
 * @property {number|null} syncId
 *
 */
export default class EmailCollection {
    /**
     * @constructor
     */
    constructor() {
        this.emails = [];
        this.unread = 0;
        this.unreadEmailsIds = [];
        this.computedList = [];
        this.createMode = false;
        this.createdEmail = new Email();
        this.syncId = null;
    }

    /**
     * Update emails collection.
     *
     * @returns {void}
     */
    cleanCollection() {
        this.emails = [];
        this.syncId = null;
        this.unread = 0;
        this.updateCollection();
    }

    /**
     * Update emails collection.
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getEmailsList();
    }

    /**
     * Add Email.
     *
     * @param {Email} email - Email object
     *
     * @returns {void}
     */
    addEmail(email) {
        let id = email.id;
        if (id === null) {
            let maxId = 0;

            for (const index in this.emails) {
                if (maxId < index) {
                    maxId = index;
                }
            }

            id = maxId + 1000;
            this.syncId = id;
        }

        if (!email.isRead && !this.unreadEmailsIds.includes(id) && email.incoming) {
            this.unreadEmailsIds.push(id);
            this.unread++;
        }

        this.emails[id] = email;
    }

    /**
     * Set Email status.
     *
     * @param {Number} emailId - Email id
     * @param {Number} statusId - Status id
     *
     * @returns {void}
     */
    setEmailStatus(emailId, statusId) {
        if (typeof this.emails[emailId] !== 'undefined') {
            this.emails[emailId].statusId = statusId;
        }
    }

    /**
     * Get emails list.
     *
     * @returns {Array} - Email list
     */
    getEmailsList() {
        const temp = [];

        for (const index in this.emails) {
            const email = this.emails[index];

            temp.unshift(email);
        }

        if (this.createMode) {
            temp.unshift(this.createdEmail);
        }

        temp.reverse();

        return temp;
    }

    /**
     * Get emails list.
     *
     * @param {Array} data - Read emails id
     *
     * @returns {void}
     */
    readEmails(data) {
        if (typeof data == 'undefined') {
            return;
        }

        data.forEach(emailId => {
            this.emails[emailId].makeRead();
        });

        this.updateCollection();
    }
}
