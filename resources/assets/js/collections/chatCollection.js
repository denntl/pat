import Call from '../objects/call';

/**
 * @class ChatCollection
 *
 * @property {Array} objects - Collection of Email, Call and Message objects
 * @property {Array} computedList
 */
export default class ChatCollection {
    /**
     * @constructor
     */
    constructor() {
        this.objects = [];
        this.computedList = [];
    }

    /**
     * Add objects to array.
     *
     * @param {object} data - Objects array
     * @param {Email|Message|Call} ObjectClass - Instance of class
     *
     * @returns {void}
     */
    addObject(data, ObjectClass) {
        if (Array.isArray(data)) {
            data.forEach(element => {
                const newObject = new ObjectClass();

                newObject.addDataRecursively(element);
                this.objects.push(newObject);
            });
        } else {
            const newObject = new ObjectClass();

            newObject.addDataRecursively(data);
            this.objects.push(newObject);
        }
    }

    /**
     * Update object in collection.
     *
     * @param {object} data - Email, message or call data
     * @param {Email|Message|Call} objectClass - Class instance
     *
     * @returns {void}
     */
    updateObject(data, objectClass) {
        if (data === null) {
            return;
        }

        const self = this;

        // This line is important. If delete this line instanceof below will not be working.
        const objectInstance = objectClass;

        this.objects.forEach((element, key) => {
            if (element instanceof objectInstance && element.id === data.id) {
                self.objects[key].addDataRecursively(data);
            }
        });

        this.updateCollection();
    }

    /**
     * Get object by instance and id.
     *
     * @param {Number} objectId - Object id
     * @param {Email|Message|Call} objectClass - Class instance
     *
     * @returns {Email|Message|Call} - Email, Message or Call object
     */
    getObject(objectId, objectClass) {
        const self = this;
        let object = null;

        // This line is important. If delete this line instanceof below will not be working.
        const objectInstance = objectClass;

        this.objects.forEach((element, key) => {
            if (element instanceof objectInstance && element.id === parseInt(objectId, 10)) {
                object = self.objects[key];
            }
        });

        return object;
    }

    /**
     * Get Objects array.
     *
     * @returns {Array} - Array of calls, emails and messages objects
     */
    getObjects() {
        const temp = [];

        for (const index in this.objects) {
            const object = this.objects[index];

            temp.push(object);
        }

        temp.sort((fst, snd) => {
            const orderUp = -1;
            const orderDown = 1;

            if (fst.createdAtObject.getTime() < snd.createdAtObject.getTime()) {
                return orderUp;
            }

            if (fst.createdAtObject.getTime() > snd.createdAtObject.getTime()) {
                return orderDown;
            }

            if (fst.id < snd.id) {
                return orderUp;
            }

            if (fst.id > snd.id) {
                return orderDown;
            }

            if (fst instanceof Call) {
                return orderUp;
            } else if (snd instanceof Call) {
                return orderDown;
            } else if (fst.text < snd.text) {
                return orderUp;
            } else if (fst.text > snd.text) {
                return orderDown;
            }

            return 0;
        });

        return temp;
    }
    /**
     * Update chat collection.
     *
     * @returns {void}
     */
    updateCollection() {
        this.computedList = this.getObjects();
    }

    /**
     * Clean chat collection.
     *
     * @returns {void}
     */
    cleanCollection() {
        this.objects = [];
        this.computedList = [];
        this.updateCollection();
    }
}
