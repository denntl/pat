/* global jQuery $ */

import 'jquery-validation';
import { isEmail, isDomain, isCalendly } from '../helpers/link';
import { isDashes, isNational, isAreaCode } from '../helpers/phone';

jQuery.validator.addMethod('patEmail', function (value, element) {
    return this.optional(element) || isEmail(value);
}, 'Please enter valid email');

jQuery.validator.addMethod('patDomain', function (value, element) {
    return this.optional(element) || isDomain(value);
}, 'Please enter valid domain');

jQuery.validator.addMethod('patCalendlyLink', function (value, element) {
    return this.optional(element) || isCalendly(value);
}, 'Please enter valid calendly Link');

jQuery.validator.addMethod('patPhone', function (value, element) {
    return this.optional(element) || isDashes(value);
}, 'Please enter valid phone');

jQuery.validator.addMethod('patPhone2', function (value, element) {
    return this.optional(element) || isNational(value);
}, 'Please enter valid phone');

jQuery.validator.addMethod('areaCode', function (value, element) {
    return this.optional(element) || isAreaCode(value);
}, 'Please enter valid area code');

jQuery.validator.addMethod('notEqual', function (value, element, param) {
    return this.optional(element) || value !== $(param).val();
}, 'Coincides with current password');

jQuery.validator.addMethod('uniqueAgentEmail', function (value, element) {
    return this.optional(element) || $(element).attr('emailDuplicate') !== value;
}, 'Agent with this email exists');
