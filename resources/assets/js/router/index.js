/* global resetData */

import Vue from 'vue';
import Router from 'vue-router';
import laroute from './laroute';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            name: 'loginPortal',
            path: laroute.route('index_portal'),
            component: require('../components/portal/LoginPortal.vue'),
        },
        {
            name: 'createAccountPortal',
            path: laroute.route('create_account_portal'),
            component: require('../components/portal/CreateAccountPortal.vue'),
        },
        {
            name: 'forgotPasswordPortal',
            path: laroute.route('forgot_password_portal'),
            component: require('../components/portal/ForgotPasswordPortal.vue'),
        },
        {
            name: 'resetPasswordPortal',
            path: laroute.route('password.reset', { token: resetData.token }),
            component: require('../components/portal/ResetPasswordPortal.vue'),
        },
        {
            name: 'portalSetup',
            path: laroute.route('portal_setup'),
            component: require('../components/portal/setup/Layout.vue'),
        },
        {
            name: 'portalMainPage',
            path: laroute.route('portal_myleads', { tab: ':tab' }),
            component: require('../components/portal/myleads/Layout.vue'),
        },
        {
            name: 'portalLeadPage',
            path: laroute.route('portal_lead', { id: ':id' }),
            component: require('../components/portal/lead/LeadPage.vue'),
        },
        {
            name: 'portalProfileLayout',
            path: laroute.route('profile_portal', { tab: ':tab' }),
            component: require('../components/portal/profile/Layout.vue'),
        },
        {
            name: 'portalBilling',
            path: laroute.route('billing_portal'),
            component: require('../components/portal/billing/Layout.vue'),
        },
        {
            name: 'portalSettingsLayout',
            path: laroute.route('settings_portal', { tab: ':tab' }),
            component: require('../components/portal/settings/Layout.vue'),
        },
        {
            name: 'login',
            path: laroute.route('index'),
            component: require('../components/Login.vue'),
        },
        {
            name: 'permission',
            path: laroute.route('permission'),
            component: require('../components/console/admin/Permission.vue'),
        },
        {
            name: 'leads',
            path: laroute.route('leads'),
            redirect: laroute.route('leads_active'),
        },
        {
            name: 'leads_active',
            path: laroute.route('leads_active'),
            param: 'active',
            component: require('../components/console/admin/Leads.vue'),
        },
        {
            name: 'leads_archive',
            path: laroute.route('leads_archive'),
            param: 'archive',
            component: require('../components/console/admin/Leads.vue'),
        },
        {
            name: 'chat',
            path: laroute.route('messaging', { id: ':id?' }),
            component: require('../components/console/Messaging.vue'),
        },
        {
            name: 'lead_sources',
            path: laroute.route('lead_source_portal'),
            component: require('../components/portal/leadsources/Layout.vue'),
        },
    ],
});
