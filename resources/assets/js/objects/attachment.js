/**
 * @class Attachment
 * @property {String} id
 * @property {String|null} leadId
 * @property {String} location
 * @property {String} content
 * @property {null|String} createdAt - Created date
 * @property {null|String} updatedAt - Updated date
 */
export default class Attachment {
    /**
     * @constructor
     */
    constructor() {
        this.id = null;
        this.leadId = null;
        this.location = '';
        this.content = '';
        this.createdAt = null;
        this.updatedAt = null;
    }

    /**
     * Add data recursively.
     *
     * @param {{}} data - Data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                this[property] = data[property];
            }
        }
    }
}
