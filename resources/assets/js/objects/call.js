import moment from 'moment';
import Note from '../objects/note';
import NoteCollection from '../collections/noteCollection';

/**
 * @class Call
 *
 * @property {Number} id
 * @property {Number} agentId
 * @property {Number} conciergeId
 * @property {Boolean} isForwarded
 * @property {String} leadId
 * @property {String} phone
 * @property {String} twilioCallSid
 * @property {NoteCollection} notes - Note list
 * @property {null|String} createdAt - Created date for chat records
 * @property {null|String} finishedAt - Finished date for chat records
 * @property {null|String} originalCreatedAt - Original created date
 * @property {null|String} originalFinishedAt - Original finished date
 * @property {null|Date} createdAtObject - Created date Object
 * @property {Number} uid - Unique id for object
 */
export default class Call {
    /**
     * @constructor
     */
    constructor() {
        const randomOffset = Math.floor(Math.random() * 10000);

        this.id = null;
        this.agentId = null;
        this.conciergeId = null;
        this.isForwarded = null;
        this.leadId = null;
        this.phone = null;
        this.twilioCallSid = null;
        this.notes = new NoteCollection();
        this.createdAt = null;
        this.finishedAt = null;
        this.originalCreatedAt = null;
        this.originalFinishedAt = null;
        this.createdAtObject = null;
        this.uid = Date.now() + randomOffset;
    }

    /**
     * Add data recursively.
     *
     * @param {{}} data - Data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                if (property === 'notes' && Array.isArray(data[property])) {
                    data.notes.forEach(elem => {
                        const note = new Note();

                        note.addDataRecursively(elem);
                        note.createdAt = moment(note.createdAt).format('MM/DD/YY');

                        this[property].addNote(note);
                    });
                    this[property].updateCollection();
                } else {
                    this[property] = data[property];
                }
            }
        }

        if (data.createdAt) {
            this.originalCreatedAt = data.createdAt;
            this.createdAt = moment(data.createdAt).format('MMM D, h:mm A');
            this.createdAtObject = data.createdAtObject ? data.createdAtObject : new Date(data.createdAt);
        }

        if (data.finishedAt) {
            this.originalFinishedAt = data.finishedAt;
            this.finishedAt = moment(data.finishedAt).format('MMM D, h:mm A');
        }
    }
}
