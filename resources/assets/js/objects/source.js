/**
 * @class Source
 *
 * @property {null|Number} id - Source ID
 * @property {null|String} name - Source name
 */
export default class Source {
    /**
     * @constructor
     *
     * @param {Number} id - Lead source id
     * @param {String} name - Source name
     */
    constructor(id = null, name = '') {
        this.id = id || null;
        this.name = name || '';
    }

    /**
     * Add data recursively.
     *
     * @param {{}} data - Source data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                this[property] = data[property];
            }
        }
    }
}
