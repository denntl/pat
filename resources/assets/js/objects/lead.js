import Agent from '../objects/agent';
import NoteCollection from '../collections/noteCollection';
import AttachmentCollection from '../collections/attachmentCollection';
import MessageCollection from '../collections/messageCollection';
import EmailCollection from '../collections/emailCollection';
import CallCollection from '../collections/callCollection';
import Source from '../objects/source';

/**
 * @class Lead
 *
 * @property {null|number|String} id
 * @property {null|String} address
 * @property {null|String} agentId
 * @property {null|Agent} agent
 * @property {null|String} bathrooms
 * @property {null|String} bedrooms
 * @property {null|String} conciergeId
 * @property {null|String} currentRep
 * @property {null|String} email
 * @property {null|String} emailSource
 * @property {null|String} firstName
 * @property {null|String} lastName
 * @property {null|String} leadType
 * @property {null|String} mortgage
 * @property {null|String} originalEmail
 * @property {null|String} phone
 * @property {null|String} phoneSource
 * @property {null|String} phoneCarrierType
 * @property {null|String} priceRange
 * @property {null|String} status
 * @property {null|String} tag
 * @property {null|String} timeFrame
 * @property {null|String} updatedAt
 * @property {null|String} createdAt
 * @property {null|String} agentAssignedAt
 * @property {null|Number} sourceId
 * @property {null|boolean} fromViking
 * @property {null|Source} source
 * @property {NoteCollection} notes - Note list
 * @property {MessageCollection} messages - Message list
 * @property {EmailCollection} emails - Email list
 * @property {CallCollection} calls - Call list
 * @property {AttachmentCollection} attachments - Attachment list
 * @property {null|number} finalTimeForResponse
 * @property {null|number} timeLeftForResponse
 * @property {null|String} lastMessage
 * @property {boolean} showTimer
 * @property {boolean} isPaused - Paused SMS automation or not
 * @property {integer} smsFlowStep - Next SMS Flow Step
 * @property {string|null} opportunityId - referred lead id
 */
export default class Lead {
    /**
     * @constructor
     */
    constructor() {
        this.id = null;
        this.agentId = null;
        // TODO remove following initialization to prevent confusion
        this.agent = new Agent();
        this.bathrooms = '';
        this.bedrooms = '';
        this.conciergeId = null;
        this.currentRep = '';
        this.email = '';
        this.emailSource = '';
        this.firstName = '';
        this.lastName = '';
        this.leadType = '';
        this.mortgage = '';
        this.originalEmail = '';
        this.phone = '';
        this.phoneSource = '';
        this.phoneCarrierType = '';
        this.priceRange = '';
        this.status = '';
        this.tag = '';
        this.icon = '';
        this.timeFrame = '';
        this.updatedAt = '';
        this.createdAt = '';
        this.agentAssignedAt = '';
        this.city = '';
        this.state = '';
        this.street = '';
        this.postalcode = '';
        this.sourceId = null;
        this.fromViking = false;
        // TODO remove following initialization to prevent confusion
        this.source = new Source();
        this.notes = new NoteCollection();
        this.messages = new MessageCollection();
        this.emails = new EmailCollection();
        this.calls = new CallCollection();
        this.attachments = new AttachmentCollection();
        this.finalTimeForResponse = null;
        this.timeLeftForResponse = null;
        this.lastMessage = '';
        this.showTimer = false;
        this.isPaused = false;
        this.smsFlowStep = null;
        this.opportunityId = null;
    }

    /**
     * Return activity based on 'updatedAt' value
     *
     * @returns {string} - Updated date
     */
    activity() {
        const dateDifference = new Date().getTime() - new Date(this.updatedAt).getTime();

        const seconds = dateDifference / 1000 + new Date().getTimezoneOffset() * 60;
        const minute = 60;
        const hour = minute * minute;
        const day = hour * 24;
        const month = day * 30;

        if (seconds < 1) {
            return 'second ago';
        } else if (seconds < minute) {
            return `${Math.floor(seconds)} sec ago`;
        } else if (seconds < hour) {
            return `${Math.floor(seconds / minute)} min ago`;
        } else if (seconds < day) {
            return `${Math.floor(seconds / hour)} hr ago`;
        } else if (seconds < month) {
            return `${Math.floor(seconds / day)} day ago`;
        }

        return this.updatedAt;
    }

    /**
     * Return agent full name
     *
     * @returns {string} - Agent full name
     */
    getAgentName() {
        const firstName = this.agent.firstName === null ? '' : this.agent.firstName;
        const lastName = this.agent.lastName === null ? '' : this.agent.lastName;

        return this.agentId === null ? 'Unassigned' : `${firstName} ${lastName}`;
    }

    /**
     * Return Lead`s short name.
     *
     * @returns {string} - Lead`s shout name
     */
    getShortName() {
        const firstName = this.firstName;

        let ret = '';

        const lastName = this.lastName;

        if (firstName !== null) {
            ret += firstName[0];
        }

        if (lastName !== null) {
            ret += ` ${lastName[0]}`;
        }

        return ret;
    }

    /**
     * Add data recursively.
     *
     * @param {{}} data - Lead data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                if (this[property] !== null && typeof this[property] === 'object') {
                    if (typeof this[property].addDataRecursively == 'undefined') {
                        this[property] = data[property];
                    } else {
                        this[property].addDataRecursively(data[property]);
                    }
                } else {
                    this[property] = data[property];
                }
            }
        }
    }

    /**
     * Returns count unread items (for now: emails + messages)
     *
     * @returns {number} count of unread items
     */
    countUnreadItems() {
        return this.emails.unread + this.messages.unread;
    }
}
