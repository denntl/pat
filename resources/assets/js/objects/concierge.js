import isArray from 'lodash/fp/isArray';
import isObject from 'lodash/fp/isObject';
import NoteCollection from '../collections/noteCollection';
import { isEmail } from '../helpers/link';

/**
 * @class Concierge
 *
 * @property {null|number|String} id
 * @property {null|String} name
 * @property {null|String} email
 * @property {null|number|String} permission
 * @property {null|String} conciergeRole
 * @property {NoteCollection} notes
 * @property {Boolean} stateEdit
 * @property {Boolean} isDeleted
 */
export default class Concierge {
    /**
     * @constructor
     *
     * @param {null|number|String} id - Concierge id
     * @param {null|String} name - Concierge full name
     * @param {null|String} email - Concierge email
     * @param {null|number|String} role - Concierge role
     * @param {null|String} conciergeRole - Concierge role as a concierge
     * @param {null|String} company - Concierge company
     * @param {null|String} team - Team to which concierge belongs
     * @param {null|String} leader - Leader of the team to which concierge belongs
     * @param {Array} notes - Concierge notes
     * @param {Boolean} isDeleted - Is concierge marked as deleted
     * @param {Boolean} isPaused - Is concierge marked as paused
     */
    constructor(
        id = null,
        name = '',
        email = '',
        role = '',
        conciergeRole = '',
        company = '',
        team = '',
        leader = '',
        notes = new NoteCollection(),
        isDeleted = false,
        isPaused = false
    ) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.role = role;
        this.conciergeRole = conciergeRole;
        this.company = company;
        this.team = team;
        this.leader = leader;
        this.notes = typeof notes === typeof NoteCollection ? notes : new NoteCollection();
        this.stateEdit = false;
        this.isDeleted = isDeleted;
        this.isPaused = isPaused;
    }

    /**
     * Return permission string by code
     *
     * @returns {string} - Concierge permission or 'n/a'
     */
    getPermission() {
        switch (this.role.toString()) {
            case '0':
                return 'Suspended';
            case '1':
                return 'Tier 1';
            case '2':
                return 'Tier 2';
            case '3':
                return 'Admin';
            default:
                return 'n/a';
        }
    }

    /**
     * Return Concierge's name for delete message
     *
     * @returns {string} - Concierge name or email
     */
    getName() {
        return `${this.name !== '' ? this.name : this.email}`;
    }

    /**
     * Check allow to create this Concierge or not
     *
     * @returns {boolean} - Allow to create object status
     */
    allowToCreate() {
        if (this.id !== null) {
            return false;
        }

        if (this.name === '') {
            return false;
        }

        return isEmail(this.email);
    }

    /**
     * TODO: Move this method in separate object and make it inherited for other objects
     * Add data recursively.
     *
     * @param {{}} data - Concierge data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const key in data) {
            if (!this.hasOwnProperty(key)) {
                continue;
            }

            if (isObject(data[key]) && !isArray(data[key])) {
                this[key].addDataRecursively(data[key]);
                break;
            } else {
                this[key] = data[key];
            }
        }
    }

    /**
     * Get short name.
     *
     * @returns {string} short name value
     */
    getShortName() {
        this.name = this.name === null ? '' : this.name;

        const regExp = new RegExp([' ', '_', '-'].join('|'), 'g');
        const tmp = this.name.trim().split(regExp);

        if (tmp.length <= 1) {
            return tmp[0][0];
        }

        const lastNameIndex = tmp.length - 1;
        const shortName = tmp[0].substr(0, 1) + tmp[lastNameIndex].substr(0, 1);

        return shortName.toUpperCase();
    }

    /**
     * Validate concierge email
     *
     * @returns {boolean} flag
     */
    validateEmail() {
        return isEmail(this.email);
    }

    /**
     * Validate concierge name
     *
     * @returns {boolean} flag
     */
    validateName() {
        return !(this.name === null || !this.name.trim());
    }
}
