import moment from 'moment';

/**
 * @class Message
 *
 * @property {Number|String} id
 * @property {Number|String} conciergeId
 * @property {null|Number} incoming - Message incoming
 * @property {null|Boolean} isRead - Message is read or not
 * @property {null|Number} isAutomatic - Message is automatic or not
 * @property {null|String} text - Message text
 * @property {null|Number} leadId - Lead id
 * @property {null|String} createdAt - Created date
 * @property {null|Date} createdAtObject - Created date object
 * @property {null|String} twilioMessageStatus - Twilio message status
 * @property {Number} uid - Unique id for object
 */
export default class Message {
    /**
     * @constructor
     *
     * @param {null|number|String} id - Message id
     * @param {null|Number} incoming - Message incoming
     * @param {null|Boolean} isRead - Message is read or not
     * @param {null|Number} isAutomatic - Message is automatic or not
     * @param {null|String} text - Message text
     * @param {null|Number} conciergeId - Author id
     * @param {null|Number} leadId - Lead id
     * @param {null|String} createdAt - Created date
     * @param {null|String} twilioMessageStatus - Twilio message status
     */
    constructor(
        id = null,
        incoming = null,
        isRead = false,
        isAutomatic = null,
        text = '',
        conciergeId = null,
        leadId = null,
        createdAt = '',
        twilioMessageStatus = null
    ) {
        const randomOffset = Math.floor(Math.random() * 10000);

        this.id = id;
        this.incoming = incoming;
        this.isRead = isRead;
        this.isAutomatic = isAutomatic;
        this.text = text;
        this.conciergeId = conciergeId;
        this.leadId = leadId;
        this.createdAt = createdAt;
        this.twilioMessageStatus = twilioMessageStatus;
        this.createdAtObject = this.createdAt ? new Date(this.createdAt) : this.createdAt;
        this.uid = Date.now() + randomOffset;
    }

    /**
     * Add data recursively.
     *
     * @param {{}} data - Data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                this[property] = data[property];
            }
        }

        this.createdAt = moment(data.createdAt).format('MMM D, h:mm A');
        this.createdAtObject = new Date(data.createdAt);
    }

    /**
     * Makes message read.
     *
     * @returns {void}
     */
    makeRead() {
        this.isRead = true;
    }
}
