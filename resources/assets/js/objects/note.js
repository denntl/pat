import moment from 'moment';

/**
 * @class Note
 *
 * @property {Number} id
 * @property {String} type
 * @property {String} note
 * @property {Number} conciergeId
 * @property {Number} leadId
 * @property {String} createdAt
 * @property {Number} callId
 */
export default class Note {
    /**
     * @constructor
     *
     * @param {Number} id - Note id
     * @param {String} type - Note type
     * @param {String} note - Note text
     * @param {Number} conciergeId - Author id
     * @param {Number} leadId - Lead id
     * @param {String} createdAt - Created date
     * @param {Number} callId - Call id if exists
     */
    constructor(id = null, type = '', note = '', conciergeId = null, leadId = null, createdAt = '', callId = null) {
        this.id = id;
        this.type = type;
        this.note = note;
        this.conciergeId = conciergeId;
        this.leadId = leadId;
        this.createdAt = createdAt;
        this.callId = callId;
    }

    /**
     * Get concierge name for current note.
     *
     * @param {ConciergeCollection} collection - ConciergeCollection
     *
     * @returns {string} - Concierge name
     */
    getConciergeName(collection) {
        for (const index in collection.concierges) {
            if (collection.concierges[index].id === this.conciergeId) {
                return collection.concierges[index].name;
            }
        }

        return '';
    }

    /**
     * Add data recursively.
     *
     * @param {object} data - Data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                this[property] = data[property];
            }
        }

        if (data.createdAt !== null) {
            this.createdAt = moment(data.createdAt).format('MMM D, hh:mm A');
        }
    }
}
