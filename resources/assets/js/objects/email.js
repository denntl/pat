import moment from 'moment';

/**
 * @class Email
 * @property {Number} id
 * @property {Number} incoming
 * @property {Boolean} isRead
 * @property {String} from
 * @property {String} to
 * @property {String} subject
 * @property {String|Null} senderFullName
 * @property {String} text
 * @property {Number} agentId
 * @property {Number|Null} conciergeId
 * @property {String} leadId
 * @property {null|String} createdAt - Created date
 * @property {null|Date} createdAtObject - Created date object
 * @property {Number} statusId
 * @property {Number} uid - Unique id for object
 */
export default class Email {
    /**
     * @constructor
     */
    constructor() {
        const randomOffset = Math.floor(Math.random() * 10000);

        this.id = null;
        this.incoming = null;
        this.isRead = false;
        this.from = '';
        this.to = '';
        this.subject = '';
        this.senderFullName = '';
        this.text = '';
        this.agentId = null;
        this.conciergeId = null;
        this.leadId = null;
        this.createdAt = null;
        this.createdAtObject = null;
        this.statusId = null;
        this.uid = Date.now() + randomOffset;
    }

    /**
     * Add data recursively.
     *
     * @param {{}} data - Data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                this[property] = data[property];
            }
        }

        this.createdAt = moment(data.createdAt).format('MMM D, h:mm A');
        this.createdAtObject = new Date(data.createdAt);
    }

    /**
     * Makes email read.
     *
     * @returns {void}
     */
    makeRead() {
        this.isRead = true;
    }
}
