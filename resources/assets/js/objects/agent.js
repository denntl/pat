/**
 * @class Agent
 *
 * @property {null|Number} id - Agent ID
 * @property {null|String} firstName
 * @property {null|String} lastName
 * @property {string} phone - Phone number
 * @property {string} twilioNumber - Twilio number
 * @property {string} email - Email address
 * @property {string} website - Website address
 * @property {string} calendly - Calendly address
 * @property {string} city - City
 * @property {string} state - State
 * @property {string} company - Company
 * @property {string} personaTeamName - Persona team name
 * @property {string} personaName - Persona name
 * @property {string} personaTitle - Persona Title
 * @property {boolean} gmailConnected - Gmail connected
 * @property {string} sendGridEmail - Persona sendGrid email
 * @property {string} notificationPhone - Agent notification phone
 * @property {string} notificationEmail - Agent notification email
 * @property {string} callForwardingNumber - Call Forwarding Number
 * @property {string} extraNoteForRep - Extra Note For Rep (Concierge)
 * @property {string} emailForwarding - Forwarding email
 * @property {object} agentOnBoarding - Agent on boarding
 * @property {boolean} vikingGmailParse - Parse or not gmail emails
 */
export default class Agent {
    /**
     * @constructor
     */
    constructor() {
        this.id = null;
        this.firstName = null;
        this.lastName = null;
        this.phone = '';
        this.twilioNumber = '';
        this.email = '';
        this.website = '';
        this.calendly = '';
        this.city = '';
        this.state = '';
        this.company = '';
        this.personaTeamName = '';
        this.personaName = '';
        this.personaTitle = '';
        this.gmailConnected = false;
        this.sendGridEmail = '';
        this.notificationPhone = '';
        this.notificationEmail = '';
        this.callForwardingNumber = '';
        this.extraNoteForRep = '';
        this.emailForwarding = '';
        this.agentOnBoarding = null;
        this.vikingGmailParse = true;
    }

    /**
     * Add data recursively.
     *
     * @param {{}} data - Agent data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const property in data) {
            if (this.hasOwnProperty(property)) {
                this[property] = data[property];
            }
        }
    }

    /**
     * Return agent city and state.
     *
     * @returns {string} - Agent city and state
     */
    getCityAndState() {
        return (this.city ? this.city : '') + (this.state ? `, ${this.state}` : '');
    }

    /* eslint-disable */
    /**
     * Return CamelCase string.
     *
     * @param {String} str - String non formated
     * @returns {string} - String formated
     */
    camelize(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
            return index == 0 ? letter.toUpperCase() : letter.toLowerCase();
        }).replace(/\s+/g, '');
    }
    /* eslint-enable */

    /**
     * Return full name with lower case.
     *
     * @returns {string} - Full name
     */
    getFullName() {
        const firstName = this.firstName ? this.camelize(this.firstName) : '';
        const lastName = this.lastName ? this.camelize(this.lastName) : '';
        return `${firstName} ${lastName}`;
    }
}
