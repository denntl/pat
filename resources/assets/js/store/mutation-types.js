/**
 * Here we store all mutation types (names).
 * After adding first type you could remove this file
 *
 * @example export const MUTATION_TYPE = 'MUTATION_TYPE'
 * @link https://github.com/vuejs/vuex/blob/dev/examples/shopping-cart/store/mutation-types.js
 */

// Agents mutation types
export const ADD_AGENT = 'ADD_AGENT';
export const UPDATE_AGENT = 'UPDATE_AGENT';
export const DELETE_AGENT = 'DELETE_AGENT';

// Concierges mutation types
export const ADD_CONCIERGE = 'ADD_CONCIERGE';
export const UPDATE_CONCIERGE = 'UPDATE_CONCIERGE';
export const DELETE_CONCIERGE = 'DELETE_CONCIERGE';

// Leads mutation types
export const SET_ACTIVE_LEAD = 'SET_ACTIVE_LEAD';
export const ADD_LEAD = 'ADD_LEAD';
export const DELETE_LEAD = 'DELETE_LEAD';
export const UPDATE_LEAD = 'UPDATE_LEAD';
export const READ_MESSAGES = 'READ_MESSAGES';
export const READ_EMAILS = 'READ_EMAILS';
export const UPDATE_LEADS_LIST = 'UPDATE_LEADS_LIST';
export const UPDATE_LEADS_ACTIVITY = 'UPDATE_LEADS_ACTIVITY';
export const UPDATE_LEADS_TIMERS = 'UPDATE_LEADS_TIMERS';

// Messages mutation types
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const UPDATE_MESSAGE_STATUS = 'UPDATE_MESSAGE_STATUS';

// Emails mutation types
export const ADD_EMAIL = 'ADD_EMAIL';
export const UPDATE_EMAIL_STATUS = 'UPDATE_EMAIL_STATUS';

// Notes mutation types
export const ADD_NOTE = 'ADD_NOTE';

// Calls mutation types
export const ADD_CALL = 'ADD_CALL';
