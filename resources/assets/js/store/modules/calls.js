import Vue from 'vue';
import * as types from '../mutation-types';

const state = {
    all: [],
};

const getters = {};

const actions = {
    addCall({ commit }, call) {
        commit(types.ADD_CALL, { call });
    },
};

const mutations = {
    [types.ADD_CALL](state, { call }) {
        Vue.set(state.all, call.id, call);
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
