import Vue from 'vue';
import * as types from '../mutation-types';

const state = {
    all: [],
};

const getters = {};

const actions = {
    addNote({ commit }, note) {
        commit(types.ADD_NOTE, { note });
    },
};

const mutations = {
    [types.ADD_NOTE](state, { note }) {
        Vue.set(state.all, note.id, note);
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
