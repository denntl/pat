import Vue from 'vue';
import moment from 'moment';
import * as types from '../mutation-types';
import { isSearchSuccess } from '../../helpers/lead';
import { sortRet } from '../../helpers/lead';
import { setLeadActivity } from '../../helpers/lead';

const state = {
    search: '',
    filterStatus: '-',
    filterRep: '-',
    filterSource: [],
    filterType: '-',
    orderColumn: null,
    orderDirectionUp: true,
    subPage: null,
    subPageStatuses: [],
    activeLeadId: null,
    all: {},
    filteredLeadsList: [],
};

/**
 * Check filter status and filter rep.
 *
 * @param {Object} lead - Lead object.
 *
 * @returns {Boolean} - Result of check.
 */
function checkStatusRep(lead) {
    const checkFilterStatus = state.filterStatus !== '-' && state.filterStatus !== lead.status;
    const checkFilterRep = state.filterRep !== '-' && state.filterRep !== lead.conciergeId;

    return checkFilterStatus || checkFilterRep;
}

/**
 * Check filter source.
 *
 * @param {String} leadSourceName - Lead source name.
 *
 * @returns {Boolean} - Result of check.
 */
function checkFilterSource(leadSourceName) {
    let continueThis = true;

    if (state.filterSource[0] === 'All') {
        return false;
    }

    for (const index in state.filterSource) {
        if (state.filterSource[index] === leadSourceName) {
            continueThis = false;
            break;
        }
    }

    return continueThis;
}

/**
 * Check sub page and lead status.
 *
 * @param {String} status - Lead status.
 *
 * @returns {Boolean} - Result of check.
 */
function checkLeadStatus(status) {
    let continueThis = true;

    for (const indexStatus in state.subPageStatuses) {
        if (state.subPageStatuses[indexStatus] === status) {
            continueThis = false;
            break;
        }
    }

    return continueThis;
}

/**
 * Get sorted/filtered leads
 *
 * @param {Object} state - Lead module state
 *
 * @returns {void}
 */
function getLeads(state) {
    if (!Object.keys(state.all).length) {
        state.filteredLeadsList = [];

        return;
    }

    const ret = [];

    for (const index in state.all) {
        const lead = state.all[index];

        if (!isSearchSuccess(lead, state.search) || checkStatusRep(lead)) {
            continue;
        }

        const checkLength = Object.keys(state.filterSource).length !== 0;
        const checkSourceId = lead.sourceId === null;
        const checkLeadSource = typeof lead.source == 'undefined';

        if (checkLength && (checkSourceId || checkLeadSource || checkFilterSource(lead.source.name))) {
            continue;
        }

        const checkFilter = state.filterType === '-' || state.filterType === 'all';
        const checkLeadType = state.filterType !== lead.leadType;
        const checkSubPage = state.subPage !== null;

        if (!checkFilter && checkLeadType || checkSubPage && checkLeadStatus(lead.status)) {
            continue;
        }

        ret.unshift(lead);
    }

    state.filteredLeadsList = state.orderColumn === null ? ret : sortRet(ret, state);
}

const getters = {
    activeLead: state => state.activeLeadId ? state.all[state.activeLeadId] : {},
    activeLeadMessages: (state, getters, rootState) => {
        const messages = rootState.messages.all;
        const activeLead = state.activeLeadId ? state.all[state.activeLeadId] : {};

        if (activeLead) {
            return activeLead.messages ? activeLead.messages.map(id => messages[id]) : [];
        }

        return [];
    },
    activeLeadEmails: (state, getters, rootState) => {
        const emails = rootState.emails.all;
        const activeLead = state.activeLeadId ? state.all[state.activeLeadId] : {};

        if (activeLead) {
            return activeLead.emails ? activeLead.emails.map(id => emails[id]) : [];
        }

        return [];
    },
    getLeads: state => state.filteredLeadsList,
    getLeadsByAgentId: (state, getters, rootState) => agentId => {
        const leads = rootState.leads.all;
        const agent = rootState.agents.all[agentId];

        if (agent) {
            return agent.leads ? agent.leads.map(id => leads[id]) : [];
        }

        return [];
    },
};

const actions = {
    setActiveLead({ commit }, id) {
        commit(types.SET_ACTIVE_LEAD, { id });
    },
    addLead({ commit }, lead) {
        commit(types.ADD_LEAD, { lead });
    },
    deleteLead({ commit }, lead) {
        commit(types.DELETE_LEAD, { lead });
    },
    updateLead({ commit }, data) {
        commit(types.UPDATE_LEAD, data);
    },
    updateLeadsList({ commit }, data) {
        commit(types.UPDATE_LEADS_LIST, data);
    },
    updateLeadsActivity({ commit }) {
        commit(types.UPDATE_LEADS_ACTIVITY);
    },
    updateLeadsTimers({ commit }) {
        commit(types.UPDATE_LEADS_TIMERS);
    },
};

const mutations = {
    [types.SET_ACTIVE_LEAD](state, { id }) {
        state.activeLeadId = id;
    },

    [types.ADD_LEAD](state, { lead }) {
        lead.messages = [];
        lead.emails = [];
        lead.calls = [];
        lead.notes = [];
        lead.attachments = [];
        lead.unreadMessages = 0;
        lead.unreadEmails = 0;
        lead.unreadItems = 0;
        lead.activity = '';

        Vue.set(state.all, lead.id, lead);
    },

    [types.DELETE_LEAD](state, { lead }) {
        delete state.all[lead.id];
    },

    [types.UPDATE_LEAD](state, data) {
        const lead = state.all[data.leadId];

        for (const property in data.lead) {
            if (lead.hasOwnProperty(property)) {
                Vue.set(lead, property, data.lead[property]);
            }
        }
    },

    [types.UPDATE_LEADS_LIST](state, data) {
        for (const property in data) {
            if (state.hasOwnProperty(property)) {
                Vue.set(state, property, data[property]);
            }
        }

        getLeads(state);
    },

    [types.UPDATE_LEADS_ACTIVITY](state) {
        state.filteredLeadsList.forEach(lead => {
            setLeadActivity(lead);
        });
    },

    [types.UPDATE_LEADS_TIMERS](state) {
        state.filteredLeadsList.forEach(lead => {
            if (lead.finalTimeForResponse) {
                const time = lead.finalTimeForResponse - new Date().getTime();

                // 5 minutes = 300000 ms
                lead.showTimer = time <= 300000 && !this.showTimer;
                lead.timeLeftForResponse = time > 0 ? moment.utc(time).format('mm:ss') : '00:00';
            }
        });
    },

    [types.ADD_MESSAGE](state, { message }) {
        // Add message to the lead it belongs to
        const lead = state.all[message.leadId];

        if (!lead.messages.some(id => id === message.id)) {
            if (message.incoming && !message.isRead) {
                lead.unreadMessages++;
                lead.unreadItems++;
            }

            lead.messages.push(message.id);
        }
    },

    [types.ADD_EMAIL](state, { email }) {
        // Add email to the lead it belongs to
        const lead = state.all[email.leadId];

        if (!lead.emails.some(id => id === email.id)) {
            if (email.incoming && !email.isRead) {
                lead.unreadEmails++;
                lead.unreadItems++;
            }

            lead.emails.push(email.id);
        }
    },

    [types.READ_MESSAGES](state, { leadId }) {
        const lead = state.all[leadId];

        lead.unreadItems -= lead.unreadMessages;
        lead.unreadMessages = 0;
    },

    [types.READ_EMAILS](state, { leadId }) {
        const lead = state.all[leadId];

        lead.unreadItems -= lead.unreadEmails;
        lead.unreadEmails = 0;
    },

    [types.ADD_NOTE](state, { note }) {
        // Add note to the lead it belongs to
        const lead = state.all[note.leadId];

        if (!lead.notes.some(id => id === note.id)) {
            lead.notes.push(note.id);
        }
    },

    [types.ADD_CALL](state, { call }) {
        // Add call to the lead it belongs to
        const lead = state.all[call.leadId];

        if (!lead.calls.some(id => id === call.id)) {
            lead.calls.push(call.id);
        }
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
