import Vue from 'vue';
import * as types from '../mutation-types';

const state = {
    all: [],
};

const getters = {};

const actions = {
    addEmail({ commit }, email) {
        commit(types.ADD_EMAIL, { email });
    },
    updateEmailStatus({ commit }, data) {
        commit(types.UPDATE_EMAIL_STATUS, data);
    },
    readEmails({ commit }, leadId) {
        commit(types.READ_EMAILS, { leadId });
    },
};

const mutations = {
    [types.ADD_EMAIL](state, { email }) {
        Vue.set(state.all, email.id, email);
    },

    [types.UPDATE_EMAIL_STATUS](state, data) {
        const email = state.all[data.emailId];

        if (email) {
            Vue.set(email, 'status', data.status);
        }
    },

    [types.READ_EMAILS](state, { leadId }) {
        state.all.forEach(email => {
            if (email.leadId === leadId) {
                email.isRead = true;
            }
        });
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
