import Vue from 'vue';
import * as types from '../mutation-types';

const state = {
    all: {},
};

const getters = {
    allConcierges: state => state.all,
};

const actions = {
    addConcierge({ commit }, concierge) {
        commit(types.ADD_CONCIERGE, { concierge });
    },
    updateConcierge({ commit }, data) {
        commit(types.UPDATE_CONCIERGE, data);
    },
    deleteConcierge({ commit }, conciergeId) {
        commit(types.DELETE_CONCIERGE, conciergeId);
    },
};

const mutations = {
    [types.ADD_CONCIERGE](state, { concierge }) {
        concierge.leads = [];

        Vue.set(state.all, concierge.id, concierge);
    },

    [types.UPDATE_CONCIERGE](state, data) {
        const concierge = state.all[data.id];

        concierge.name = data.name;
        concierge.email = data.email;
        concierge.permission = `${data.permission }`;
    },

    [types.DELETE_CONCIERGE](state, conciergeId) {
        delete state.all[conciergeId];
    },

    [types.ADD_LEAD](state, { lead }) {
        // Add lead to the agent it belongs to
        const concierge = state.all[lead.conciergeId];

        if (concierge && !concierge.leads.some(id => id === concierge.id)) {
            concierge.leads.push(lead.id);
        }
    },

    [types.DELETE_LEAD](state, { lead }) {
        const concierge = state.all[lead.conciergeId];

        // Remove concierge-lead relation
        if (concierge) {
            concierge.leads.splice(concierge.leads.indexOf(lead.id), 1);
        }
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
