import Vue from 'vue';
import * as types from '../mutation-types';

const state = {
    all: {},
};

const getters = {
    getAgentById: state => id => state.all[id] ? state.all[id] : {},
};

const actions = {
    addAgent({ commit }, agent) {
        commit(types.ADD_AGENT, { agent });
    },
    updateAgent({ commit }, data) {
        commit(types.UPDATE_AGENT, data);
    },
    deleteAgent({ commit }, agentId) {
        commit(types.DELETE_AGENT, agentId);
    },
};

const mutations = {
    [types.ADD_AGENT](state, { agent }) {
        agent.leads = [];

        Vue.set(state.all, agent.id, agent);
    },

    [types.UPDATE_AGENT](state, data) {
        const agent = state.all[data.agentId];

        for (const property in data.agent) {
            if (agent.hasOwnProperty(property)) {
                Vue.set(agent, property, data.agent[property]);
            }
        }
    },

    [types.DELETE_AGENT](state, agentId) {
        delete state.all[agentId];
    },

    [types.ADD_LEAD](state, { lead }) {
        // Add lead to the agent it belongs to
        const agent = state.all[lead.agentId];

        if (agent && !agent.leads.some(id => id === agent.id)) {
            agent.leads.push(lead.id);
        }
    },

    [types.DELETE_LEAD](state, { lead }) {
        const agent = state.all[lead.agentId];

        // Remove agent-lead relation
        if (agent) {
            agent.leads.splice(agent.leads.indexOf(lead.id), 1);
        }
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
