import Vue from 'vue';
import * as types from '../mutation-types';

const state = {
    all: [],
};

const getters = {};

const actions = {
    addMessage({ commit }, message) {
        commit(types.ADD_MESSAGE, { message });
    },
    updateMessageStatus({ commit }, data) {
        commit(types.UPDATE_MESSAGE_STATUS, data);
    },
    readMessages({ commit }, leadId) {
        commit(types.READ_MESSAGES, { leadId });
    },
};

const mutations = {
    [types.ADD_MESSAGE](state, { message }) {
        Vue.set(state.all, message.id, message);
    },

    [types.UPDATE_MESSAGE_STATUS](state, data) {
        const message = state.all[data.messageId];

        if (message) {
            Vue.set(message, 'twilioMessageStatus', data.twilioMessageStatus);
        }
    },

    [types.READ_MESSAGES](state, { leadId }) {
        state.all.forEach(message => {
            if (message.leadId === leadId) {
                Vue.set(message, 'isRead', true);
            }
        });
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};
