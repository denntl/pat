/* global laravelEnv */

import Vue from 'vue';
import Vuex from 'vuex';
import agents from './modules/agents';
import concierges from './modules/concierges';
import messages from './modules/messages';
import emails from './modules/emails';
import notes from './modules/notes';
import leads from './modules/leads';

Vue.use(Vuex);

const strict = laravelEnv !== 'production';

export default new Vuex.Store({
    strict,
    modules: {
        agents,
        concierges,
        leads,
        messages,
        emails,
        notes,
    },
});
