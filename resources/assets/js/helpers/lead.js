/**
 * Get lowercase string
 *
 * @param {string} stringValue - string value for function
 *
 * @returns {string} - lowercase result
 */
export function getLowerCase(stringValue) {
    return stringValue && typeof stringValue == 'string' ? stringValue.toLowerCase() : '';
}

/**
 * Comparison order values.
 *
 * @param {string|number} first - First order param.
 * @param {string|number} second - Second order param.
 * @param {boolean} orderDirectionUp - Is order direction up.
 *
 * @returns {null|number} - Return order type.
 */
function comparisonOrderValues(first, second, orderDirectionUp) {
    const orderUp = 1;
    const orderDown = -1;

    if (first < second) {
        return orderDirectionUp ? orderDown : orderUp;
    }

    if (first > second) {
        return orderDirectionUp ? orderUp : orderDown;
    }

    return null;
}

/**
 * Check is search success.
 *
 * @param {Object} lead - Lead object.
 * @param {String} searchKey - Search key.
 *
 * @returns {Boolean} - Return is search success.
 */
export function isSearchSuccess(lead, searchKey) {
    const firstName = getLowerCase(lead.firstName).trim();
    const lastName = getLowerCase(lead.lastName).trim();
    const fullName = `${firstName } ${ lastName}`;

    return searchKey !== '' ? fullName.includes(searchKey.toLowerCase()) : true;
}

/**
 * Sort leads by column.
 *
 * @param {object} ret - Leads array.
 * @param {object} state - Leads module state.
 *
 * @returns {Array|Number} - Sorted array of leads
 */
export function sortRet(ret, state) {
    return ret.sort((fst, snd) => {
        let orderType = null;

        const rightOrder = ['Urgent', 'Awaiting Response', 'Action Required', 'Ongoing Chat', 'Automation', 'Archive'];

        const aName = getLowerCase(fst.firstName) + getLowerCase(fst.lastName);
        const bName = getLowerCase(snd.firstName) + getLowerCase(snd.lastName);

        const aDate = new Date(fst.updatedAt).getTime();
        const bDate = new Date(snd.updatedAt).getTime();

        const aTypeLower = getLowerCase(fst.leadType);
        const bTypeLower = getLowerCase(snd.leadType);

        const aSourceLower = getLowerCase(fst.source.name);
        const bSourceLower = getLowerCase(snd.source.name);

        const aStatusLower = getLowerCase(fst.status);
        const bStatusLower = getLowerCase(snd.status);

        const aOrderIndex = rightOrder.indexOf(fst.tag);
        const bOrderIndex = rightOrder.indexOf(snd.tag);

        switch (state.orderColumn) {
            case 'name':
                orderType = comparisonOrderValues(aName, bName, state.orderDirectionUp);
                break;
            case 'activity':
                orderType = comparisonOrderValues(aDate, bDate, state.orderDirectionUp);
                break;
            case 'type':
                orderType = comparisonOrderValues(aTypeLower, bTypeLower, state.orderDirectionUp);
                break;
            case 'source':
                orderType = comparisonOrderValues(aSourceLower, bSourceLower, state.orderDirectionUp);
                break;
            case 'status':
                orderType = comparisonOrderValues(aStatusLower, bStatusLower, state.orderDirectionUp);
                break;
            case 'tag':
                orderType = comparisonOrderValues(aOrderIndex, bOrderIndex, state.orderDirectionUp);
                break;
            default:
                break;
        }

        return orderType !== null ? orderType : 0;
    });
}

/**
 * Set lead activity based on 'updatedAt' value
 *
 * @param {Object} lead - Lead object
 * @returns {void}
 */
export function setLeadActivity(lead) {
    const dateDifference = new Date().getTime() - new Date(lead.updatedAt).getTime();

    const seconds = dateDifference / 1000 + new Date().getTimezoneOffset() * 60;
    const minute = 60;
    const hour = minute * minute;
    const day = hour * 60;
    const month = day * 30;

    if (seconds < 1) {
        lead.activity = 'second ago';
    } else if (seconds < minute) {
        lead.activity = `${Math.floor(seconds) } sec ago`;
    } else if (seconds < hour) {
        lead.activity = `${Math.floor(seconds / minute) } min ago`;
    } else if (seconds < day) {
        lead.activity = `${Math.floor(seconds / hour) } hr ago`;
    } else if (seconds < month) {
        lead.activity = `${Math.floor(seconds / day) } day ago`;
    } else {
        lead.activity = lead.updatedAt;
    }
}
