/**
 * Format string to phone number.
 *
 * @param {string} phone - Phone number
 *
 * @returns {string} - Phone number in dashes format
 */
export function toDashes(phone) {
    if (phone === null) {
        return '';
    }

    let part1;
    let part2;
    let part3;
    let newPhone;
    let oldPhone = phone.replace(/[^\d]/g, '');

    oldPhone = oldPhone.substr(0, 10);
    part1 = oldPhone.substr(0, 4);

    if (part1.length > 3) {
        part1 = part1.substr(0, 3);
        newPhone = part1;
        part2 = oldPhone.substr(3, 3);

        if (part2.length > 0) {
            newPhone += '-';
        }

        if (part2.length === 3) {
            newPhone += part2;
            part3 = oldPhone.substr(6, 4);

            if (part3.length > 0) {
                newPhone += '-';
            }

            newPhone += part3;
        } else {
            newPhone += part2;
        }
    } else if (part1.length <= 3) {
        newPhone = part1;
    }

    return newPhone;
}

/**
 * Format string to phone number with spaces.
 *
 * @param {string} phone - Phone number
 *
 * @returns {string} - Phone number in national format
 */
export function toNational(phone) {
    let part1;
    let part2;
    let part3;
    let newPhone;
    let oldPhone = phone.replace(/[^\d]/g, '');

    oldPhone = oldPhone.substr(0, 10);
    part1 = oldPhone.substr(0, 4);

    if (part1.length > 3) {
        part1 = part1.substr(0, 3);
        newPhone = `(${part1})`;
        part2 = oldPhone.substr(3, 3);

        if (part2.length > 0) {
            newPhone += ' ';
        }

        if (part2.length === 3) {
            newPhone += part2;
            part3 = oldPhone.substr(6, 4);

            if (part3.length > 0) {
                newPhone += '-';
            }

            newPhone += part3;
        } else {
            newPhone += part2;
        }
    } else if (part1.length <= 3) {
        newPhone = part1;
    }

    return newPhone;
}

/**
 * Checks if phone is consist of dashes: XXX-XXX-XXXX
 *
 * @param {string} phone - Check value
 *
 * @returns {boolean} - Valid or not
 */
export function isDashes(phone) {
    const phoneRegEx = /^\d{3}-\d{3}-\d{4}$/;

    return phoneRegEx.test(phone);
}

/**
 * Checks if phone is an international USA phone number: +1XXXXXXXXXX.
 *
 * @param {string} phone - Check value
 *
 * @returns {boolean} - International or not
 */
export function isInternational(phone) {
    const phoneRegEx = /^\+1\d{10}$/;

    return phoneRegEx.test(phone);
}

/**
 * Checks if phone is a national USA phone number: (XXX) XXX-XXXX.
 *
 * @param {string} phone - Check value
 *
 * @returns {boolean} - National or not
 */
export function isNational(phone) {
    const phoneRegEx = /^\(\d{3}\)\s\d{3}-\d{4}$/;

    return phoneRegEx.test(phone);
}

/**
 * Checks if area code format is valid.
 *
 * @param {string} areaCode - Area code value
 *
 * @returns {boolean} - Valid or not
 */
export function isAreaCode(areaCode) {
    const areaCodeRegEx = /^[2-9]\d{2}$/;

    return areaCodeRegEx.test(areaCode);
}
