/**
 * Check format of email address.
 *
 * @param {string} link - Check value
 *
 * @returns {boolean} - Valid or not
 */
export function isEmail(link) {
    const emailRegEx = new RegExp(['^(([^<>()[\\]\\.,;:\\s@"]+(.[^<>()[\\]\\.,;:\\s@\\"]+)*)|(".+"))@((\\[[0-9]]'
    + '[{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$'].join(''));

    return emailRegEx.test(link);
}

/**
 * Check format of domain.
 *
 * @param {string} link - Check value
 *
 * @returns {boolean} - Domain or not
 */
export function isDomain(link) {
    const domainRegEx = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/;

    return domainRegEx.test(link);
}

/**
 * Check format of Calendly link.
 *
 * @param {string} link - Check value
 *
 * @returns {boolean} - Valid or not
 */
export function isCalendly(link) {
    const regEx = /^https:\/\/calendly\.com\/.*$/;

    return regEx.test(link);
}
