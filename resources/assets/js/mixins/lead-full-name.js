export default {
    /**
     * Computed.
     *
     * @link https://vuejs.org/v2/api/#computed
     */
    computed: {
        /**
         * Gets lead full name.
         *
         * @returns {string} - Lead full name
         */
        leadFullName() {
            return `${this.lead.firstName } ${ this.lead.lastName}`;
        },
    },
};
