export default {
    /**
     * Mounted hook of the current mixin.
     *
     * @returns {void}
     */
    mounted() {
        this.mountIScroll();
    },
    methods: {
        /**
         * Refresh IScroll.
         *
         * @returns {void}
         */
        refreshIScroll() {
            setTimeout(() => {
                this.scroll.refresh();
            }, 0);
        },
    },
};
