export default {
    /**
     * Mounted hook of the current mixin.
     *
     * @returns {void}
     */
    mounted() {
        if (typeof this.onMessageWebSocket != 'undefined') {
            this.$socket.on('message', this.onMessageWebSocket);
        }
    },
    /**
     * Before destroy (unmounted) hook of the current mixin.
     *
     * @returns {void}
     */
    beforeDestroy() {
        if (typeof this.onMessageWebSocket != 'undefined') {
            this.$socket.off('message', this.onMessageWebSocket);
        }
    },
    methods: {
        /**
         * Try to initialize websocket connection & subscribe.
         *
         * @param {function} callback - Callback function with socket emit
         *
         * @returns {void}
         */
        tryInitWebSocket(callback) {
            if (this.$socket.io.readyState === 'open') {
                setTimeout(() => {
                    callback();
                }, 100);
            } else {
                setTimeout(() => {
                    this.tryInitWebSocket(callback);
                }, 10);
            }
        },
    },
};
