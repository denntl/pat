/* global $ */
export default {
    methods: {
        /**
         * Initialize Materialize drop-down button.
         *
         * @returns {void}
         */
        initDropDown() {
            $(document).ready(() => {
                $('.dropdown-button').dropdown();
            });
        },
    },
};
