/**
 * Components loader.
 * @param {Vue} Vue - Vue instance
 * @returns {void}
 */
module.exports = Vue => {
    Vue.component('navbar', require('./common/Navbar.vue'));
    Vue.component('modal', require('./common/Modal.vue'));
    Vue.component('gmailButton', require('./common/GmailButton.vue'));
    Vue.component('sourcesRow', require('./common/SourcesRow.vue'));
    Vue.component('spinner', require('./common/Spinner.vue'));
    Vue.component('tooltip', require('./common/Tooltip.vue'));

    // Console info
    Vue.component('concierge-identity', require('./console/messaging/info/ConciergeIdentity.vue'));
    Vue.component('contact-info', require('./console/messaging/info/ContactInfo.vue'));
    Vue.component('lead-content', require('./console/messaging/info/LeadContent.vue'));
    Vue.component('notes-panel', require('./console/messaging/info/NotesPanel.vue'));

    // Console Admin
    Vue.component('layout', require('./console/admin/Layout.vue'));
    Vue.component('navbar-search', require('./console/admin/leads/NavbarSearch.vue'));

    // Console Messaging
    Vue.component('call', require('./console/messaging/Call.vue'));
    Vue.component('channels', require('./console/messaging/Channels.vue'));
    Vue.component('channel-lead', require('./console/messaging/ChannelLead.vue'));
    Vue.component('channel', require('./console/messaging/Channel.vue'));
    Vue.component('chat', require('./console/messaging/Chat.vue'));
    Vue.component('chat-call', require('./console/messaging/chat/ChatCall.vue'));
    Vue.component('chat-message', require('./console/messaging/chat/ChatMessage.vue'));
    Vue.component('chat-tabs', require('./console/messaging/ChatTabs.vue'));
    Vue.component('email-original', require('./console/messaging/EmailOriginal.vue'));
    Vue.component('email', require('./console/messaging/email/Email.vue'));
    Vue.component('lead-history', require('./console/messaging/LeadHistory.vue'));
    Vue.component('lead-info', require('./console/messaging/LeadInfo.vue'));
    Vue.component('message-input', require('./console/messaging/MessageInput.vue'));
    Vue.component('messages', require('./console/messaging/Messages.vue'));
    Vue.component('scripts', require('./console/messaging/Scripts.vue'));

    // Portal Lead Chat
    Vue.component('portalAlienChatMessage', require('./portal/lead/chat/AlienChatMessage.vue'));
    Vue.component('portalChatCallEnd', require('./portal/lead/chat/ChatCallEnd.vue'));
    Vue.component('portalChatEmail', require('./portal/lead/chat/ChatEmail.vue'));
    Vue.component('portalLeadChat', require('./portal/lead/chat/LeadChat.vue'));
    Vue.component('portalOwnChatMessage', require('./portal/lead/chat/OwnChatMessage.vue'));

    // Portal Lead
    Vue.component('portalLeadDetails', require('./portal/lead/LeadDetails.vue'));
    Vue.component('portalLeadNotes', require('./portal/lead/LeadNotes.vue'));
    Vue.component('portalLeadPage', require('./portal/lead/LeadPage.vue'));
    Vue.component('portalSingleLeadCard', require('./portal/lead/SingleLeadCard.vue'));

    // Portal My Leads
    Vue.component('portalCard', require('./portal/myleads/Card.vue'));
    Vue.component('portalLeadsTable', require('./portal/myleads/Table.vue'));

    // Portal Profile
    Vue.component('portalProfileContent', require('./portal/profile/Profile.vue'));
    Vue.component('portalProfilePassword', require('./portal/profile/Password.vue'));

    // Portal Settings
    Vue.component('portalSettingsConnection', require('./portal/settings/Connection.vue'));
    Vue.component('portalSettingsNotification', require('./portal/settings/Notification.vue'));
    Vue.component('portalSettingsPersona', require('./portal/settings/Persona.vue'));

    // Portal Setup
    Vue.component('portalSelectPhoneStep', require('./portal/setup/SelectPhoneNumberStep.vue'));
    Vue.component('portalConciergeIdentity', require('./portal/setup/portalConciergeIdentity.vue'));
    Vue.component('portalLeadSources', require('./portal/setup/portalLeadSources.vue'));

    // Portal
    Vue.component('portalNavbar', require('./portal/PortalNavbar.vue'));
};
