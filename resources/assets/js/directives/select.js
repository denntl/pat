/* global $ */

/**
 * Directive m
 * @type {{bind: module.exports.bind, unbind: module.exports.unbind}}
 */
module.exports = {
    bind(el, binding, vnode) {
        $(() => {
            // Material select initialization
            $(el).material_select();

            // Define caret image instead of standard caret
            const caretImage = '<img src="/assets/images/grey.png" srcset="/assets/images/grey@2x.png 2x, '
                + '/assets/images/grey@3x.png 3x">';

            // Apply new caret to select dropdown
            $('.caret').html(caretImage);
        });

        let arg = binding.arg;

        arg = arg ? `on${arg}` : 'change';

        /* eslint-disable no-param-reassign */
        el[arg] = function () {
            if (binding.expression) {
                // If expression like user.name split it by dot and assign value to user.name
                if (binding.expression.split('.').length > 1) {
                    const elements = binding.expression.split('.');

                    let vnodeContext = vnode.context;

                    elements.forEach((elem, index) => {
                        if (elements.length - 1 === index) {
                            vnodeContext[elem] = el.value;
                        } else {
                            vnodeContext = vnodeContext[elem];
                        }
                    });
                } else if (binding.expression in vnode.context.$data) {
                    vnode.context.$data[binding.expression] = el.value;
                } else if (vnode.context[binding.expression] && vnode.context[binding.expression].length <= 1) {
                    vnode.context[binding.expression](el.value);
                } else {
                    throw new Error(`Directive v-${binding.name} can not take more than 1 argument`);
                }
            } else {
                throw new Error(`Directive v-${binding.name} must take value`);
            }
        };
        /* eslint-enable no-param-reassign */
    },
    unbind(el) {
        $(el).material_select('destroy');
    },
};
