/**
 * This directive used for materialize select to set. Value to model.
 * To use it use v-select instead of v-model.
 *
 * @param {Vue} Vue - Vue instance
 * @returns {void}
 */
module.exports = Vue => {
    Vue.directive('select', require('./select.js'));
};
