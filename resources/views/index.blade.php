<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>{{ $title }}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="stylesheet" href="{{ mix('/assets/css/app.css') }}">
        <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
        <script src="https://media.twiliocdn.com/sdk/js/client/releases/1.4.14/twilio.min.js"></script>
    </head>
    <body{{(isset($portalPage) ? ' class=body-portal-page' : '')}}>
        <div id="app">
            <router-view></router-view>
            <input type="hidden" id="role" value="{{ $role }}">
            <input type="hidden" id="id" value="{{ $id }}">
            <input type="hidden" id="token" value="{{ $token }}">
            <input type="hidden" id="isConcierge" value="{{ $isConcierge ?? false }}">
            <input type="hidden" id="referralNetworkUrl" value="{{ $referralNetworkUrl ?? ''}}">
        </div>
        <script>
            window.Laravel = {
                csrfToken: '{{ csrf_token() }}'
            };

            let googleData = {!! json_encode($googleData) !!};
            let googleError = {!! json_encode($errors->getBags()) !!};
            let googleStaticMapsKey = '{{ $googleStaticMapsKey }}';
            let vueData = {!! isset($vueData) ? json_encode($vueData) : '{}' !!};

            vueData.googleStyles = '{!! $googleStylesData !!}';

            let resetData = {
              token: '{{$resetToken ?? ''}}',
              email: '{{$email ?? ''}}',

            };

            let laraErrors = {!! json_encode($errors->getBags()) !!};
            let sessionId = '{{ $sessionId ?? ''}}';
            let twilioToken = '{{ $twilioToken ?? ''}}';
            let rollBarAccessToken = '{{ $rollBarAccessToken ?? '' }}';
            let laravelEnv = '{{ env('APP_ENV') }}';
            let safeMode = {{ config('app.safe') ? 'true' : 'false' }};
        </script>
        <script src="{{ mix('/assets/js/app.js') }}"></script>
        <script src="{{ asset('/assets/js/materialize.min.js') }}"></script>
    </body>
</html>
