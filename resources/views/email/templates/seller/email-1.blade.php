<span style='font-family: Helvetica, Arial;'>
Hi {{ $lead->firstName ?? 'there' }},

Greetings from {{ $agent->personaTeamName }} - we're glad you found us!

I see you’re looking for a personalized home valuation for the property on
    {{ $lead->leadStreet ?? 'a great street' }}. Good news... I can help!

I'd love to set up an appointment with our team - do you have any time available in
the next few days to meet with one of our listing specialists?

Also, it would be super helpful if you could send back any other details that make
your home unique. This way we can begin prepping the most accurate comparative
market analysis possible to help us figure out the best listing price for your home.

I look forward to connecting with you!

Best,

    {{ $agent->personaName }}
    {{ $agent->personaTeamName }}
    {{ $agent->personaTitle }}
    Phone: {{ $agent->phone }}
</span>