<span style='font-family: Helvetica, Arial;'>
Ahoy, {{ $lead->firstName ?? 'there' }}!

    {{ $agent->personaName }} here from {{ $agent->personaTeamName }} again. Since I haven’t
heard back, I assume my last email must have gotten intercepted by a band of
internet pirates and never reached you (we’ll have to be more careful next time).

The fact of the matter is if you’re going to embark upon an important journey,
you’re going to want to work with an agent who can navigate through sunny skies
& stormy weather (at {{ $agent->personaTeamName }} the only time we ever jump ship
is when we get a low-ball offer). In short: we want to give you a selling experience
that you can really treasure.

So if you’d like to chat, give us a call at {{ $agent->phone }}. If not, we'll start
prepping ourselves to walk the plank.

Best,

    {{ $agent->personaName }}
    {{ $agent->personaTeamName }}
    {{ $agent->personaTitle }}
    Phone: {{ $agent->phone }}
</span>