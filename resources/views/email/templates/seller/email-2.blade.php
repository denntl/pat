<span style='font-family: Helvetica, Arial;'>
 Hi {{ $lead->firstName ?? 'there' }},

You are one tough cookie to get a hold of! I assume you’re busy doing something
productive like researching tips on how to sell your home or sprucing up the paint
job in the master bathroom (or something adult like that) - so I understand how
things like getting back to me can fall behind on the priority list.

I just wanted to let you know that I'm always available if you decide you'd like to
work with {{ $agent->personaTeamName }} to sell your home in {{ $lead->city ?? 'your area' }}.

Please don't hesitate to touch base by responding to this email or giving me a
call at the number below.

Thanks for letting me know either way and have a great day!

Kind regards,

 {{ $agent->personaName }}
 {{ $agent->personaTeamName }}
 {{ $agent->personaTitle }}
 Phone: {{ $agent->phone }}
</span>