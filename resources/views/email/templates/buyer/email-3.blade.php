<span style='font-family: Helvetica, Arial;'>
Ahoy, {{ $lead->firstName ?? 'there' }}!

    {{ $agent->personaName }} here from {{ $agent->personaTeamName }} again. Since I haven’t
heard back, I assume my last email must have gotten lost at sea and never reached you.
Don’t worry, it happens.

The fact of the matter is you’re embarking upon an exciting journey, and we want to start
you off sailing in the right direction toward homeownership ASAP (see what I did there?).
    {{ $agent->personaTeamName }} is happy to be your captain when it comes to finding your
dream home in {{ $lead->city ?? 'your area' }}.

Simply reply to this email to let me know when you’re free to talk more about the
details of your search, or give me a call anytime at the number below.

Thanks again and have a fantastic day!

    {{ $agent->personaName }}
    {{ $agent->personaTeamName }}
    {{ $agent->personaTitle }}
    Phone: {{ $agent->phone }}
</span>