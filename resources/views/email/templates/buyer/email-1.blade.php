<span style='font-family: Helvetica, Arial;'>
Hi {{ $lead->firstName ?? 'there' }},

Greetings from {{ $agent->personaTeamName }} - we’re absolutely THRILLED you found us!

I see that you’re looking to buy a home in {{ $lead->city ?? 'your area' }}. Awesome! Before I connect you to your agent, I have a few quick questions to help us better
understand your wants and needs when it comes to buying a home:

Is there a specific neighborhood you're interested in?
When are you available to meet with an agent?
Are you prequalified or pre-approved for a mortgage yet?
Do you want to add any other details to your search criteria?

Are you free anytime today or this week to talk? If not, you can always email me
back the answers and I'll try giving you a call tomorrow to follow up.

Thanks and I look forward to working with you!
Kind regards,

    {{ $agent->personaName }}
    {{ $agent->personaTeamName }}
    {{ $agent->personaTitle }}
    Phone: {{ $agent->phone }}
</span>