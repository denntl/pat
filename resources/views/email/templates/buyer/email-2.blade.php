<span style='font-family: Helvetica, Arial;'>
Hi {{ $lead->firstName ?? 'there' }},

How are things? I tried calling again a little earlier and wasn't able to reach you
- I assume you’re busy doing something important so it’s totally understandable how
getting back to me falls a little low on the “to-do” list.

I mostly just wanted to let you know that I'm always available if you decide you'd
still like me to introduce you to a great agent - I can tell you,
    {{ $agent->personaTeamName }} is ready to make your home-buying dreams come
true (so you have that going for you... which is nice).

Please don't hesitate to touch base by responding to this email or giving me a call
at the number below.

We’re excited to help you get started on finding your future new home!

Kind regards,

    {{ $agent->personaName }}
    {{ $agent->personaTeamName }}
    {{ $agent->personaTitle }}
    Phone: {{ $agent->phone }}
</span>