<?php

namespace Tests\Feature\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

/**
 * Class EmailControllerTest
 * @package Tests\Feature\Controllers
 */
class EmailControllerTest extends TestCase
{
    /**
     * Test `viewInBrowser` method success.
     *
     * @return void
     */
    public function testViewInBrowserSuccess()
    {
        Storage::fake('emails');

        $disk = Storage::disk('emails');
        $disk->put('test.html', '<p>Test email</p>');

        $resp = $this->get('/emails/test/view');

        $resp->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test viewInBrowser method failed.
     *
     * @return void
     */
    public function testVieInbrowserFailed()
    {
        Storage::fake('emails');

        $resp = $this->get('/emails/test/view');

        $resp->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
