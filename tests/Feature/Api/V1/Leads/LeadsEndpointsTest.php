<?php

namespace Tests\Feature\Api\V1\Leads;

use App\Facades\PhoneNumber;
use App\Facades\Lead as LeadFacade;
use App\Lead;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

/**
 * Class LeadsEndpointsTest.
 * @package Tests\Feature\Api\V1\Leads
 */
class LeadsEndpointsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Test posting lead from Viking
     * @param array $data
     * @dataProvider validDataProvider
     *
     * @return void
     */
    public function testSaveLeadSuccess(array $data)
    {
        $response = $this->json('POST', '/api/v1/leads/', $data);
        $response->assertStatus(200)->assertJson([
            'data' => true,
        ])->assertExactJson([
            'data' => [
                'message' => 'Lead Saved.'
            ],
        ]);

        $lead = Lead::find($data['id']);

        // Check if lead created
        $this->assertTrue(!empty($lead));

        // Check marker fromViking
        $this->assertTrue($lead->fromViking);

        // Check if posted data equal to created object
        $formattedData = LeadFacade::formatVikingLeadData($data);
        foreach ($formattedData as $key => $value) {
            if ($key === 'phone') {
                $formattedPhone = PhoneNumber::getNationalPhone($value);
                $this->assertTrue($lead->phone == $formattedPhone);
            } elseif ($key === 'leadType') {
                $this->assertTrue($lead->$key == strtolower($value));
            } else {
                $this->assertTrue($lead->$key == $value);
            }
        }
    }

    /**
     * Test posting lead with invalid data.
     *
     * @return void
     */
    public function testPostLeadEmptyDataFail()
    {
        $response = $this->json('POST', '/api/v1/leads/', []);
        $response->assertStatus(400)->assertJson([
            'errors' => true,
        ]);
    }

    /**
     * Test posting lead with empty data.
     *
     * @return void
     */
    public function testPostLeadValidationFail()
    {
        $lead = factory(Lead::class)->make();

        $response = $this->json('POST', '/api/v1/leads/', $lead->toArray());
        $response->assertStatus(400)->assertJson([
            'errors' => true,
        ]);
    }

    /**
     * @return array
     */
    public function validDataProvider()
    {
        return [
            'maximalDataSet' =>[
                [
                    "id" => Uuid::uuid1()->toString(),
                    "firstname" => "John",
                    "lastname" => "Snow",
                    "email" => "some@test.com",
                    "phone" => "(415) 555-2671",
                    "city" => "TestCity",
                    "state" => "TestState",
                    "street" => "TestStreet",
                    "postalcode" => "6100",
                    "leadcomment" => "leadcomment",
                    "leadtype" => "Seller",
                    "duplicate" => "true",
                    "duplicate_lead_id" => Uuid::uuid1()->toString(),
                    "phone_carrier_type" => "mobile",
                    "channelwebsite" => "http://test",
                    "gmail_id" => "test",
                    "gmail_thread" => "test",
                    "user_id" => Uuid::uuid1()->toString(),
                    "pat_id" => Uuid::uuid1()->toString(),
                    "mortgage" => "Cash Buyer",
                    "bedrooms" => "1",
                    "bathrooms" => "1",
                    "timeframe" => "Asap",
                    "price" => "$800,000 - $1,000,000"
                ]
            ],
        ];
    }
}
