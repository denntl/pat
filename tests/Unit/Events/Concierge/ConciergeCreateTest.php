<?php

namespace Tests\Unit\Events\Concierge;

use App\Concierge;
use App\Events\Concierge\ConciergeCreate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class ConciergeCreateTest
 *
 * @package Tests\Unit\Events\Concierge
 */
class ConciergeCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test ConciergeCreate event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Concierge $concierge */
        $concierge = factory(Concierge::class)->create();

        $event = new ConciergeCreate(
            $concierge->name,
            $concierge->email,
            $concierge->role,
            $concierge->id
        );
        event($event);

        Event::assertDispatched(ConciergeCreate::class, function ($event) use ($concierge) {
            $sameConciergeName = $event->name === $concierge->name;
            $sameConciergeEmail = $event->email === $concierge->email;
            $sameConciergeRole = $event->role === $concierge->role;
            $sameConciergeId = $event->id === $concierge->id;
            $sameSender = $event->sender === null;

            return $sameConciergeName && $sameConciergeEmail && $sameConciergeRole && $sameConciergeId && $sameSender;
        });

        $broadcastOn = ['system', 'concierges'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
