<?php

namespace Tests\Unit\Events\Agent;

use App\Concierge;
use App\Events\Concierge\ConciergeAll;
use Tests\TestCase;
use Illuminate\Support\Facades\Event;

/**
 * Class ConciergeAllTest
 *
 * @package Tests\Unit\Events\Concierge
 */
class ConciergeAllTest extends TestCase
{
    /**
     * Test ConciergeAll event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        $event = new ConciergeAll();
        event($event);

        $conciergesCount = Concierge::all()->count();

        Event::assertDispatched(ConciergeAll::class, function ($event) use ($conciergesCount) {
            return $conciergesCount === count($event->concierges);
        });

        $broadcastOn = ['system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
