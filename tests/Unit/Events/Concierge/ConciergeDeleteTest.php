<?php

namespace Tests\Unit\Events\Concierge;

use App\Concierge;
use App\Events\Concierge\ConciergeDelete;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class ConciergeDeleteTest
 *
 * @package Tests\Unit\Events\Concierge
 */
class ConciergeDeleteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test ConciergeDelete event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Concierge $concierge */
        $concierge = factory(Concierge::class)->create();

        $event = new ConciergeDelete($concierge->id);
        event($event);

        Event::assertDispatched(ConciergeDelete::class, function ($event) use ($concierge) {
            $sameConciergeId = $event->id === $concierge->id;
            $validSender = $event->sender === null;

            return $sameConciergeId && $validSender;
        });

        $broadcastOn = ['concierges', 'system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
