<?php

namespace Tests\Unit\Events\Concierge;

use App\Concierge;
use App\Events\Concierge\ConciergeUpdate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class ConciergeUpdateTest
 *
 * @package Tests\Unit\Events\Concierge
 */
class ConciergeUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test ConciergeUpdate event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Concierge $concierge */
        $concierge = factory(Concierge::class)->create();

        $event = new ConciergeUpdate(
            $concierge->id,
            $concierge->name,
            $concierge->email,
            $concierge->role
        );
        event($event);

        Event::assertDispatched(ConciergeUpdate::class, function ($event) use ($concierge) {
            $sameConciergeId = $event->id === $concierge->id;
            $sameConciergeName = $event->name === $concierge->name;
            $sameConciergeEmail = $event->email === $concierge->email;
            $sameConciergeRole = $event->role === $concierge->role;
            $sameSender = $event->sender === null;

            return $sameConciergeId && $sameConciergeName && $sameConciergeEmail && $sameConciergeRole && $sameSender;
        });

        $broadcastOn = ['concierges', 'system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
