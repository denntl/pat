<?php

namespace Tests\Unit\Events\Lead;

use App\Concierge;
use App\Lead;
use App\Events\Lead\LeadUpdateRep;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadUpdateRepTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadUpdateRepTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadUpdateRep event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();
        $conciergeNew = factory(Concierge::class)->create();

        $event = new LeadUpdateRep($lead->id, $conciergeNew->id, null);
        event($event);

        Event::assertDispatched(LeadUpdateRep::class, function ($event) use ($lead, $conciergeNew) {
            $validSender = $event->sender === null;
            $validLeadId = $event->leadId === $lead->id;
            $validOldRepId = $event->oldRepId === $lead->conciergeId;
            $validRepId = $event->repId === $conciergeNew->id;

            return  $validSender && $validLeadId && $validOldRepId && $validRepId;
        });
    }

    /**
     * Test LeadUpdateRep event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();
        $conciergeNew = factory(Concierge::class)->create();

        $event = new LeadUpdateRep($lead->id, $conciergeNew->id, null);
        event($event);
        $broadcastOn = ['system', 'leads', 'lead-' . $lead->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}