<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Events\Lead\LeadSendEmails;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadSendEmailsTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadSendEmailsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadSendEmails event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();
        $status = 'success';

        $event = new LeadSendEmails($lead->id);
        event($event);

        Event::assertDispatched(LeadSendEmails::class, function ($event) use ($lead, $status) {
            $validSender = $event->sender === null;
            $sameId = $event->id === $lead->id;
            $sameStatus = $event->status === $status;

            return $validSender && $sameId && $sameStatus;
        });
    }

    /**
     * Test LeadSendEmails event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadSendEmails($lead->id);
        event($event);
        $broadcastOn = ['lead-' . $lead->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}