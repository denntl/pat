<?php

namespace Tests\Unit\Events\Lead;

use App\LeadCall;
use App\Events\Lead\LeadCallStatusUpdate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadCallStatusUpdateTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadCallStatusUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadCallStatusUpdate event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create();

        $event = new LeadCallStatusUpdate($leadCall->leadId, $leadCall->twilioCallSid);
        event($event);

        Event::assertDispatched(LeadCallStatusUpdate::class, function ($event) use ($leadCall) {
            $sameCallId = $event->id === $leadCall->leadId;
            $sameTwilioCallSid = $event->callSid === $leadCall->twilioCallSid;
            $sameCallStatus = $event->status === null;
            $validSender = $event->sender === null;

            return  $sameCallId && $sameTwilioCallSid && $sameCallStatus && $validSender;
        });
    }

    /**
     * Test LeadCallStatusUpdate event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create();

        $event = new LeadCallStatusUpdate($leadCall->leadId, $leadCall->twilioCallSid);
        event($event);
        $broadcastOn = ['lead-' . $leadCall->leadId, 'leads'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}