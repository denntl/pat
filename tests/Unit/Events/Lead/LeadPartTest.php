<?php

namespace Tests\Unit\Events\Lead;

use App\Events\Lead\LeadPart;
use App\Lead;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class LeadPartTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadPartTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadPart event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        $leadsNum = rand(1, 3);

        /** @var Lead $leads */
        $leads = factory(Lead::class, $leadsNum)->create();

        $event = new LeadPart($leads);
        event($event);

        Event::assertDispatched(LeadPart::class, function ($event) use ($leads) {
            $sameLeads = $event->leads == $leads->toArray();

            return $sameLeads;
        });
    }

    /**
     * Test LeadPart event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        $leadsNum = rand(1, 3);

        /** @var Lead $leads */
        $leads = factory(Lead::class, $leadsNum)->create();

        $event = new LeadPart($leads);
        event($event);
        $broadcastOn = ['system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
