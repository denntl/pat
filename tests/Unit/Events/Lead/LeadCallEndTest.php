<?php

namespace Tests\Unit\Events\Lead;

use App\LeadCall;
use App\Events\Lead\LeadCallEnd;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadCallEndTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadCallEndTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadCallEnd event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create();

        $event = new LeadCallEnd(
            $leadCall->leadId,
            $leadCall->phone,
            $leadCall->id
        );
        event($event);

        Event::assertDispatched(LeadCallEnd::class, function ($event) use ($leadCall) {
            $validSender = $event->sender === null;
            $sameLeadId = $event->leadId === $leadCall->leadId;
            $samePhone = $event->phone === $leadCall->phone;
            $sameCallId = $event->leadCallId === $leadCall->id;

            return $validSender && $sameLeadId && $samePhone && $sameCallId;
        });
    }

    /**
     * Test LeadCallEnd event default broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create();

        $event = new LeadCallEnd(
            $leadCall->leadId,
            $leadCall->phone,
            $leadCall->id
        );
        event($event);
        $broadcastOn = $leadCall->leadId ? ['lead-' . $leadCall->leadId, 'leads'] : [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Test LeadCallEnd event empty broadcasting.
     *
     * @return void
     */
    public function testEventEmptyBroadcasting()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create();

        $event = new LeadCallEnd();
        event($event);
        $broadcastOn = [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}