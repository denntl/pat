<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Events\Lead\LeadGet;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadGetTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadGetTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadGet event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        // Get lead from DB because on serialization will get only field listed in factory
        $leadFromDB = Lead::with('source', 'agent')->where('id', $lead->id)->first();

        $event = new LeadGet($lead->id);
        event($event);

        Event::assertDispatched(LeadGet::class, function ($event) use ($leadFromDB) {
            $validSender = $event->sender === null;
            $sameLead = $event->lead === $leadFromDB->toArray();

            return $validSender && $sameLead;
        });
    }

    /**
     * Test LeadGet event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadGet($lead->id);
        event($event);
        $broadcastOn = ['lead-' . $lead->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}