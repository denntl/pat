<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Events\Lead\LeadUnassignRep;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadUnassignRepTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadUnassignRepTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadUnassignRep event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadUnassignRep($lead->id, null);
        event($event);

        Event::assertDispatched(LeadUnassignRep::class, function ($event) use ($lead) {
            $validSender = $event->sender === null;
            $validLeadId = $event->leadId === $lead->id;

            return  $validSender && $validLeadId;
        });
    }

    /**
     * Test LeadUnassignRep event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadUnassignRep($lead->id, null);
        event($event);
        $broadcastOn = ['system', 'lead-' . $lead->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}