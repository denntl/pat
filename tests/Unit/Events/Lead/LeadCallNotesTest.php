<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Events\Lead\LeadCallNotes;
use App\Note;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadCallNotesTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadCallNotesTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadCallNotes event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $note = factory(Note::class)->create([
            'type' => 'Call note'
        ]);

        $event = new LeadCallNotes($note->leadId, $note->note,   $callId = null,
         $conciergeId = null,
         $sender = null);

        event($event);

        Event::assertDispatched(LeadCallNotes::class);
    }

    /**
     * Test LeadCallNotes event default broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        /** @var Lead $lead */
        $note = factory(Note::class)->create([
            'type' => 'Call note'
        ]);

        $event = new LeadCallNotes($note->leadId, $note->note);
        event($event);
        $broadcastOn = ['lead-' . $note->leadId, 'leads'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}