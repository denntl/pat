<?php

namespace Tests\Unit\Events\Lead;

use App\LeadCall;
use App\Events\Lead\LeadCall as CallLead;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadCallTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadCallTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadCall event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create();

        $event = new CallLead($leadCall);
        event($event);

        Event::assertDispatched(CallLead::class, function ($event) use ($leadCall) {
            $validSender = $event->sender === null;
            $sameLeadId = $event->leadId === $leadCall->leadId;
            $sameCall = $event->leadCall === $leadCall->toArray();

            return $validSender && $sameLeadId && $sameCall;
        });
    }

    /**
     * Test LeadCall event default broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create();

        $event = new CallLead($leadCall);
        event($event);

        $broadcastOn = $leadCall->leadId ? ['lead-' . $leadCall->leadId, 'leads'] : [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Test LeadCall event empty broadcasting.
     *
     * @return void
     */
    public function testEventEmptyBroadcasting()
    {
        Event::fake();

        /** @var LeadCall $leadCall */
        $leadCall = factory(LeadCall::class)->create([
            'leadId' => null
        ]);

        $event = new CallLead($leadCall);
        event($event);

        $broadcastOn = [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}