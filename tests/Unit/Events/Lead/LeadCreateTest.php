<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Events\Lead\LeadCreate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadCreateTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadCreate event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadCreate($lead);
        event($event);

        Event::assertDispatched(LeadCreate::class, function ($event) use ($lead) {
            $sameLead = $event->lead === $lead;

            return $sameLead;
        });
    }

    /**
     * Test LeadCreate event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadCreate($lead);
        event($event);
        $broadcastOn = ['system', 'concierges', 'agent-'.$lead->agentId];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}