<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Events\Lead\LeadUpdate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadUpdateTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadUpdate event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadUpdate($lead);
        event($event);

        Event::assertDispatched(LeadUpdate::class, function ($event) use ($lead) {
            $validSender = $event->sender === null;
            $sameId = $event->id === $lead->id;
            $sameLead = $event->lead === $lead;

            return  $validSender && $sameId && $sameLead;
        });
    }

    /**
     * Test LeadUpdate event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadUpdate($lead);
        event($event);
        $broadcastOn = ['system', 'leads', 'lead-' . $lead->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}