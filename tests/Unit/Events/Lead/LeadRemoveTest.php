<?php

namespace Tests\Unit\Events\Lead;

use App\Events\Lead\LeadRemove;
use App\Lead;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class LeadRemoveTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadRemoveTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadRemove event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadRemove($lead);
        event($event);

        Event::assertDispatched(LeadRemove::class, function ($event) use ($lead) {
            $sameLead = $event->lead == $lead;

            return $sameLead;
        });
    }

    /**
     * Test LeadRemove event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadRemove($lead);
        event($event);
        $broadcastOn = ['leads', 'subscribe'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
