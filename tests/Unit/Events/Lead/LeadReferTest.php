<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Events\Lead\LeadRefer;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class LeadReferTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadReferTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadRefer event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadRefer($lead->id);
        event($event);

        Event::assertDispatched(LeadRefer::class, function ($event) use ($lead) {
            $validSender = $event->sender === null;
            $sameLeadId = $event->id === $lead->id;

            return  $validSender && $sameLeadId;
        });
    }

    /**
     * Test LeadRefer event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadRefer($lead->id);
        event($event);
        $broadcastOn = ['lead-' . $lead->id, 'leads'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}