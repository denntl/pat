<?php

namespace Tests\Unit\Events\Lead;

use App\Lead;
use App\Message;
use App\Events\Lead\LeadAll;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;


/**
 * Class LeadAllTest
 *
 * @package Tests\Unit\Events\Lead
 */
class LeadAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test LeadAll event with one lead.
     *
     * @return void
     */
    public function testEventOneLead()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new LeadAll($lead->id, $lead->conciergeId, $lead->agentId);
        event($event);

        Event::assertDispatched(LeadAll::class);
    }

    /**
     * Test LeadAll event with multiple data.
     *
     * @return void
     */
    public function testEventMultipleData()
    {
        Event::fake();
        $lead = factory(Lead::class)->create();

        /** @var Lead $lead */
        $lead2 = factory(Lead::class)->create([
            'tag' => null,
            'agentId' => $lead->agentId,
            'conciergeId' => $lead->conciergeId
        ]);
        $lead3 = factory(Lead::class)->create([
            'tag' => null,
            'agentId' => $lead->agentId,
            'conciergeId' => $lead->conciergeId
        ]);
        factory(Lead::class)->create([
            'tag' => null,
            'agentId' => $lead->agentId,
            'conciergeId' => $lead->conciergeId
        ]);
        factory(Message::class)->create([
            'leadId' =>$lead2->id,
            'incoming' => 1
        ]);
        factory(Message::class)->create([
            'leadId' =>$lead3->id,
            'incoming' => 0
        ]);
        $event = new LeadAll(null, $lead2->conciergeId, $lead2->agentId);
        event($event);

        Event::assertDispatched(LeadAll::class);
    }

    /**
     * Test LeadAll event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        $lead = factory(Lead::class)->create();
        $event = new LeadAll(null, $lead->conciergeId, $lead->agentId);
        $broadcastOn = ['system'] ;

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}