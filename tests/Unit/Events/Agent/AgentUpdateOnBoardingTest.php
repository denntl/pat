<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentUpdateOnBoarding;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgentUpdateOnBoardingTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test agent get event.
     *
     * @return void
     */
    public function testAgentGet()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();

        $sender = null;
        $step = 3;

        event(new AgentUpdateOnBoarding($sender, $agent->id, $step));

        Event::assertDispatched(AgentUpdateOnBoarding::class, function ($event) use ($sender, $agent, $step) {
            $sameSender = $event->sender == $sender;
            $sameAgentId = $event->id == $agent->id;
            $sameStep = $event->step == $step;
            return $sameSender && $sameAgentId && $sameStep;
        });
    }

    /**
     * Test AgentUpdateOnBoarding event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting() {
        Event::fake();

        /** @var Agent $Agent */
        $agent = factory(Agent::class)->create();
        $sender = null;
        $step = 3;
        $event = new AgentUpdateOnBoarding($sender, $agent->id, $step);
        event($event);
        $broadcastOn = ['agent-' . $agent->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
