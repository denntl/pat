<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentGeneratePhone;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class AgentGeneratePhoneTest
 * @package Tests\Unit\Events\Agent
 */
class AgentGeneratePhoneTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test generation of agent phone.
     *
     * @dataProvider agentIdProvider
     *
     * @param bool $hasAgentId
     *
     * @return void
     */
    public function testAgentGeneratePhone(bool $hasAgentId)
    {
        Event::fake();

        $agentId = null;
        if ($hasAgentId) {
            $agentId = factory(Agent::class)->create()->id;
        }

        $event = new AgentGeneratePhone(222, $agentId);
        event($event);

        Event::assertDispatched(AgentGeneratePhone::class, function ($event) {
            return $event->phone;
        });

        $broadcastOn = $hasAgentId ? ['agent-' . $agentId] : [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Agent id provider.
     *
     * @return array
     */
    public function agentIdProvider()
    {
        return [
            [true],
            [false]
        ];
    }
}
