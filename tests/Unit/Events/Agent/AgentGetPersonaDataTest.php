<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentGetPersonaData;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Support\Facades\Event;

/**
 * Class AgentGetPersonaDataTest
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentGetPersonaDataTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test AgentGetPersonaData event.
     *
     * @return void
     */
    public function testAgentGetPersonaData()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();

        $agentPersonaData = [
            'personaName' => $agent->personaName,
            'personaTeamName' => $agent->personaTeamName,
            'personaTitle' => $agent->personaTitle,
        ];

        $event = new AgentGetPersonaData($agent->id);
        event($event);

        Event::assertDispatched(AgentGetPersonaData::class, function ($event) use ($agent, $agentPersonaData) {
            $dataEqualed = $event->agentPersonaData === $agentPersonaData;
            $idEqualed = $event->id === $agent->id;

            return !$event->sender && $dataEqualed && $idEqualed;
        });

        $broadcastOn = ['agent-' . $agent->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
