<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentPersonaUpdate;
use Faker\Factory;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgentPersonaUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * AgentNotificationUpdateTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->faker = Factory::create();
    }

    /**
     * Test agent get event.
     *
     * @return void
     */
    public function testAgentGet()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create([
            'personaTeamName' => null,
            'personaName' => null,
            'personaTitle' => null,
            'sendGridEmail' => null
        ]);

        $sender = null;
        $data = [];

        $data['personaTeamName'] = $this->faker->words(rand(1,3), true);
        $data['personaName'] = $this->faker->words(rand(1,3), true);
        $data['personaTitle'] = $this->faker->words(rand(1,3), true);
        $data['extraNoteForRep'] = null;

        event(new AgentPersonaUpdate($agent->id, $data, $sender));

        Event::assertDispatched(AgentPersonaUpdate::class);

        $agentUpdated = Agent::find($agent->id);

        $this->assertNotNull($agentUpdated->sendGridEmail);
        $this->assertEquals($agentUpdated->personaTeamName, $data['personaTeamName']);
        $this->assertEquals($agentUpdated->personaName, $data['personaName']);
        $this->assertEquals($agentUpdated->personaTitle, $data['personaTitle']);
    }

    /**
     * Test AgentPersonaUpdate event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting() {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create([
            'personaTeamName' => null,
            'personaName' => null,
            'personaTitle' => null,
            'sendGridEmail' => null
        ]);

        $sender = null;
        $data = [];
        $data['personaTeamName'] = $this->faker->words(rand(1,3), true);
        $data['personaName'] = $this->faker->words(rand(1,3), true);
        $data['personaTitle'] = $this->faker->words(rand(1,3), true);
        $data['extraNoteForRep'] = null;

        $event = new AgentPersonaUpdate($agent->id, $data, $sender);
        event($event);
        $broadcastOn = ['agent-' . $agent->id, 'system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
