<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentNotificationUpdate;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class AgentNotificationUpdateTest
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentNotificationUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * AgentNotificationUpdateTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->faker = Factory::create();
    }

    /**
     * Test AgentGetPersonaData event.
     *
     * @return void
     */
    public function testAgentNotificationUpdate()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();

        $data = [];
        $data['notificationPhone'] = $this->faker->unique()->e164PhoneNumber;
        $data['notificationEmail'] = $this->faker->unique()->email;
        $data['callForwardingNumber'] = $this->faker->unique()->e164PhoneNumber;

        $event = new AgentNotificationUpdate($agent->id, $data);
        event($event);

        Event::assertDispatched(AgentNotificationUpdate::class, function ($event) use ($agent, $data) {
            $dataEqualed = $event->agentData === $data;
            $idEqualed = $event->id === $agent->id;

            return !$event->sender && $dataEqualed && $idEqualed;
        });

        $broadcastOn = ['agent-' . $agent->id, 'system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
