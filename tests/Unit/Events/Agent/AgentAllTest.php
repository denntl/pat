<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentAll;
use Tests\TestCase;
use Illuminate\Support\Facades\Event;

/**
 * Class AgentAllTest
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentAllTest extends TestCase
{
    /**
     * Test all agent on null sender.
     *
     * @return void
     */
    public function testAgentAll()
    {
        Event::fake();

        $event = new AgentAll();
        event($event);

        $agentsCount = Agent::count();

        Event::assertDispatched(AgentAll::class, function ($event) use ($agentsCount) {
            return $agentsCount === count($event->agents);
        });

        $broadcastOn = ['system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
