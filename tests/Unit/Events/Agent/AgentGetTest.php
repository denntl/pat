<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentGet;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class AgentGetTest
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentGetTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test AgentGet event.
     *
     * @return void
     */
    public function testAgentGet()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();

        // Get agent from DB because on serialization will get only field listed in factory
        $agentFromDB = Agent::find($agent->id);

        $event = new AgentGet($agent->id);
        event($event);

        Event::assertDispatched(AgentGet::class, function ($event) use ($agentFromDB) {
            return !$event->sender && $event->agent == $agentFromDB->toArray();
        });

        $broadcastOn = ['agent-' . $agent->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
