<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\AgentOnBoarding;
use App\Events\Agent\AgentSetup;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\CreatesApplication;
use Tests\TestCase;

/**
 * Class AgentSetupTest
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentSetupTest extends TestCase
{
    use DatabaseTransactions;
    use CreatesApplication;

    /**
     * Test AgentSetup event.
     *
     * @return void
     */
    public function testAgentNotificationUpdate()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();
        factory(AgentOnBoarding::class)->create([
            'agentId' => $agent->id
        ]);

        $event = new AgentSetup($agent->id);
        event($event);

        Event::assertDispatched(AgentSetup::class, function ($event) use ($agent) {
            return $event->data;
        });

        $broadcastOn = ['agent-' . $agent->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
