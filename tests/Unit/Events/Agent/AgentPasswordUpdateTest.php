<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentPasswordUpdate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class AgentPasswordUpdateTest
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentPasswordUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test agent get event.
     *
     * @return void
     */
    public function testAgentGet()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();

        $event = new AgentPasswordUpdate($agent->id);
        event($event);

        Event::assertDispatched(AgentPasswordUpdate::class, function ($event) use ($agent) {
            return !$event->status && $event->id == $agent->id;
        });

        $broadcastOn = ['agent-' . $agent->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
