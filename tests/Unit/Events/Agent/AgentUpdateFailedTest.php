<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentUpdateFailed;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class AgentUpdateFailed
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentUpdateFailedTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test AgentUpdateFailed event.
     *
     * @return void
     */
    public function testEvent() {
        Event::fake();

        /** @var Agent $Agent */
        $agent = factory(Agent::class)->create();

        $event = new AgentUpdateFailed(null, $agent->id, []);
        event($event);

        Event::assertDispatched(AgentUpdateFailed::class, function ($event) use ($agent) {
            $validSender = $event->sender === null;
            $sameId = $event->id === $agent->id;
            $sameFaildFields = $event->failedFields === [];

            return  $validSender && $sameId && $sameFaildFields;
        });
    }

    /**
     * Test AgentUpdateFailed event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting() {
        Event::fake();

        /** @var Agent $Agent */
        $agent = factory(Agent::class)->create();

        $event = new AgentUpdateFailed(null, $agent->id, []);
        event($event);
        $broadcastOn = ['agent-' . $agent->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

}