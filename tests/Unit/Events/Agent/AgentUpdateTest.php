<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentUpdate;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AgentUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test agent get event.
     *
     * @return void
     */
    public function testAgentGet()
    {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();

        $sender = null;

        event(new AgentUpdate($sender, $agent));

        Event::assertDispatched(AgentUpdate::class, function ($event) use ($sender, $agent) {
            $sameSender = $event->sender == $sender;
            $sameAgentId = $event->id == $agent->id;
            $sameAgent = $event->agent == $agent->toArray();
            return $sameSender && $sameAgentId && $sameAgent;
        });
    }

    /**
     * Test AgentUpdate event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting() {
        Event::fake();

        /** @var Agent $agent */
        $agent = factory(Agent::class)->create();
        $sender = null;
        $event = new AgentUpdate($sender, $agent);
        event($event);
        $broadcastOn = ['agent-' . $agent->id, 'system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
