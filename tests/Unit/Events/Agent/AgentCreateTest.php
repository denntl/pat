<?php

namespace Tests\Unit\Events\Agent;

use App\Agent;
use App\Events\Agent\AgentCreate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class AgentCreateTest
 *
 * @package Tests\Unit\Events\Agent
 */
class AgentCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test AgentCreate event.
     *
     * @return void
     */
    public function testEvent() {
        Event::fake();

        /** @var Agent $Agent */
        $agent = factory(Agent::class)->create();

        $event = new AgentCreate($agent);
        event($event);

        Event::assertDispatched(AgentCreate::class, function ($event) use ($agent) {
            $sameAgent = $event->agent === $agent;

            return $sameAgent;
        });
    }

    /**
     * Test AgentCreate event broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting() {
        Event::fake();

        /** @var Agent $Agent */
        $agent = factory(Agent::class)->create();

        $event = new AgentCreate($agent);
        event($event);
        $broadcastOn = ['system'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

}