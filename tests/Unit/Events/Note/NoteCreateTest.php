<?php

namespace Tests\Unit\Events\Note;

use App\Events\Note\NoteCreate;
use App\Note;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class NoteCreateTest
 *
 * @package Tests\Unit\Events\Note
 */
class NoteCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test NoteCreate event.
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Note $note */
        $note = factory(Note::class)->make();

        $event = new NoteCreate($note);
        event($event);

        Event::assertDispatched(NoteCreate::class);

        $broadcastOn = $note->leadId ? ['lead-' . $note->leadId] : [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}