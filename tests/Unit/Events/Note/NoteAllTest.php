<?php

namespace Tests\Unit\Events\Note;

use App\Events\Note\NoteAll;
use App\Lead;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class NoteAllTest
 *
 * @package Tests\Unit\Events\Note
 */
class NoteAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test NoteAll event.
     *
     * @dataProvider channelsProvider
     *
     * @param bool $broadcast
     * @param array $expected
     *
     * @return void
     */
    public function testEvent(bool $broadcast, array $expected)
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new NoteAll($lead->id, null, $broadcast);
        event($event);

        Event::assertDispatched(NoteAll::class);

        $channel = ['lead-' . $lead->id];
        $broadcastOn = array_merge($channel, $expected);

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Channel provider.
     *
     * @return array
     */
    public function channelsProvider()
    {
        return [
            [false, []],
            [true, ['leads']]
        ];
    }
}