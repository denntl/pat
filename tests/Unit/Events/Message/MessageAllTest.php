<?php

namespace Tests\Unit\Events\Message;

use App\Events\Message\MessageAll;
use App\Lead;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class MessageAllTest
 *
 * @package Tests\Unit\Events\Message
 */
class MessageAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test MessageAll event.
     *
     * @dataProvider channelsProvider
     *
     * @param bool $broadcast
     * @param array $expected
     *
     * @return void
     */
    public function testEvent(bool $broadcast, array $expected)
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $event = new MessageAll($lead->id, null, $broadcast);
        event($event);

        Event::assertDispatched(MessageAll::class);

        $channel = ['lead-' . $lead->id];
        $broadcastOn = array_merge($channel, $expected);

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Channel provider.
     *
     * @return array
     */
    public function channelsProvider()
    {
        return [
            [false, []],
            [true, ['leads']]
        ];
    }
}