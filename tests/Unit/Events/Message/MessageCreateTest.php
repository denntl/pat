<?php

namespace Tests\Unit\Events\Message;

use App\Events\Message\MessageCreate;
use App\Message;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class MessageCreate
 *
 * @package Tests\Unit\Events\Message
 */
class MessageCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test event MessageCreate for incoming messages only.
     *
     * @return void
     */
    public function testEventIncoming()
    {
        Event::fake();

        /** @var Message $message */
        $message = factory(Message::class)->create([
            'incoming' => 1
        ]);

        $event = new MessageCreate($message);
        event($event);

        Event::assertDispatched(MessageCreate::class);

        $broadcastOn = ['system', 'lead-' . $message->leadId, 'leads'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}