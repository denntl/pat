<?php

namespace Tests\Unit\Events\Message;

use App\Events\Message\MessageReadAll;
use App\Lead;
use App\Message;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class MessageReadAllTest
 *
 * @package Tests\Unit\Events\Message
 */
class MessageReadAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test event MessageReadAll.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        factory(Message::class, 10)->create([
            'incoming' => true,
            'leadId' => $lead->id
        ]);

        $event = new MessageReadAll($lead->id);
        event($event);

        Event::assertDispatched(MessageReadAll::class);

        $broadcastOn = ['lead-' . $lead->id];

        $messages = Message::all();

        /** @var Message $message */
        foreach ($messages as $message) {
            if ($message->leadId === $lead->id) {
                $this->assertTrue((bool)$message->isRead);
            }
        }

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Test event MessageReadAll failed.
     *
     * @return void
     */
    public function testEventFailed()
    {
        Event::fake();

        $event = new MessageReadAll('');
        event($event);

        Event::assertDispatched(MessageReadAll::class);

        $broadcastOn = [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}