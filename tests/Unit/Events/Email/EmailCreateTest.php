<?php

namespace Tests\Unit\Events\Email;

use App\Events\Email\EmailCreate;
use App\Email;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class EmailCreateTest
 *
 * @package Tests\Unit\Events\Email
 */
class EmailCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test EmailCreate event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        $email = factory(Email::class)->create();

        $event = new EmailCreate($email);
        event($event);

        Event::assertDispatched(EmailCreate::class, function ($event) use ($email) {
            $sameEmail = $email == $event->email;
            $sameSender = $event->sender === null;

            return $sameEmail && $sameSender;
        });
    }

    /**
     * Test EmailCreate event default broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        $email = factory(Email::class)->create();

        $event = new EmailCreate($email);
        event($event);

        $broadcastOn = ['system', 'lead-'.$email->leadId, 'leads'];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Test EmailCreate event empty broadcasting.
     *
     * @return void
     */
    public function testEventEmptyBroadcasting()
    {
        Event::fake();

        $event = new EmailCreate();
        event($event);

        $broadcastOn = [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
