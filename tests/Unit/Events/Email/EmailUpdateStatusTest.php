<?php

namespace Tests\Unit\Events\Email;

use App\Email;
use App\Events\Email\EmailUpdateStatus;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class EmailUpdateStatusTest
 *
 * @package Tests\Unit\Events\Email
 */
class EmailUpdateStatusTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test EmailUpdateStatus event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        /** @var Email $email */
        $email = factory(Email::class )->create();

        $event = new EmailUpdateStatus($email->id, $email->leadId, $email->statusId);
        event($event);

        Event::assertDispatched(EmailUpdateStatus::class, function ($event) use ($email) {

            $sameEmailId = $email->id == $event->emailId;
            $sameStatusId = $email->statusId == $event->status;
            $sameLeadId = $email->leadId == $event->leadId;

            return $sameEmailId && $sameStatusId && $sameLeadId;
        });
    }

    /**
     * Test EmailUpdateStatus event default broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        /** @var Email $email */
        $email = factory(Email::class )->create();

        $event = new EmailUpdateStatus($email->id, $email->leadId, $email->statusId);

        event($event);

        $broadcastOn = ['lead-' .  $email->leadId];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Test EmailUpdateStatus event empty broadcasting.
     *
     * @return void
     */
    public function testEventEmptyBroadcasting()
    {
        Event::fake();

        $event = new EmailUpdateStatus();
        event($event);

        $broadcastOn = [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
