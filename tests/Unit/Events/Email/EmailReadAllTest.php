<?php

namespace Tests\Unit\Events\Email;

use App\Concierge;
use App\Email;
use App\Events\Email\EmailReadAll;
use App\Lead;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class EmailReadAllTest
 *
 * @package Tests\Unit\Events\Email
 */
class EmailReadAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test EmailReadAll event.
     *
     * @return void
     */
    public function testEvent()
    {
        Event::fake();

        $emailsNum = rand(1, 3);
        $read = 1;

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        /** @var Concierge $concierge */
        $concierge = factory(Concierge::class)->create();

        /** @var Email $emails */
        $emails = factory(Email::class, $emailsNum)->create([
            'leadId' => $lead->id,
            'conciergeId' => $concierge->id,
            'isRead' => $read
        ]);

        $emailsIds = $emails->map(function ($item, $key) {
            return $item->id;
        })->all();

        $event = new EmailReadAll($lead->id, $emailsIds);
        event($event);

        Event::assertDispatched(EmailReadAll::class, function ($event) use ($lead, $emailsIds) {

            $sameEmailIds = $emailsIds && $event->readEmailsIds;
            $sameSender = $event->sender == null;
            $sameLeadId = $event->leadId == $lead->id;

            return $sameEmailIds && $sameSender && $sameLeadId;
        });
    }

    /**
     * Test EmailReadAll event default broadcasting.
     *
     * @return void
     */
    public function testEventBroadcasting()
    {
        Event::fake();

        $lead = factory(Lead::class)->create();

        $event = new EmailReadAll($lead->id);
        event($event);

        $broadcastOn = ['lead-'.$lead->id];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Test EmailReadAll event empty broadcasting.
     *
     * @return void
     */
    public function testEventEmptyBroadcasting()
    {
        Event::fake();

        $event = new EmailReadAll();
        event($event);

        $broadcastOn = [];

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }
}
