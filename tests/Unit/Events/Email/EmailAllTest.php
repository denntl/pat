<?php

namespace Tests\Unit\Events\Email;

use App\Concierge;
use App\Email;
use App\Events\Email\EmailAll;
use App\Lead;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class EmailAllTest
 *
 * @package Tests\Unit\Events\Email
 */
class EmailAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test EmailAll event.
     *
     * @dataProvider channelsProvider
     *
     * @param bool $broadcast
     * @param array $expected
     *
     * @return void
     */
    public function testEvent(bool $broadcast, array $expected)
    {
        Event::fake();

        $emailsNum = rand(1, 3);

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        /** @var Concierge $concierge */
        $concierge = factory(Concierge::class)->create();

        /** @var Email $emails */
        $emails = factory(Email::class, $emailsNum)->create([
            'leadId' => $lead->id,
            'conciergeId' => $concierge->id
        ]);

        $sender = null;

        $event = new EmailAll($lead->id, $sender, $broadcast);
        event($event);

        Event::assertDispatched(EmailAll::class, function ($event) use ($lead, $sender, $emails) {
            $diff = $emails->diff($event->emails);

            $sameEmails = !$diff->all();
            $sameSender = $sender == $event->sender;
            $sameLeadId = $event->leadId == $lead->id;

            return $sameEmails && $sameSender && $sameLeadId;
        });

        $channel = ['lead-' . $lead->id];
        $broadcastOn = array_merge($channel, $expected);

        $this->assertEquals($broadcastOn, $event->broadcastOn());
    }

    /**
     * Channel provider.
     *
     * @return array
     */
    public function channelsProvider()
    {
        return [
            [false, []],
            [true, ['leads']]
        ];
    }
}
