<?php

namespace Tests\Unit\Commands\Lead;

use App\Lead;
use App\Concierge;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LeadUnassignRepTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of lead:unassign:rep command via socket connection
     */
    public function testSocketSuccess()
    {
        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        Artisan::call('lead:unassign:rep', [
            '-S' => base64_encode(json_encode([
                'cid'  => $conciergeData['id'],
                'lid'  => $leadData['id'],
                'sid'  => null,
                'token'=> $concierge->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:unassign:rep command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:unassign:rep', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
