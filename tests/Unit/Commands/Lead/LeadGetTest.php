<?php

namespace Tests\Unit\Commands\Lead;

use App\Lead;
use App\Agent;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadGetTest extends TestCase
{
    /**
     * Test success result of lead:get command via socket connection
     */
    public function testSocketSuccess()
    {
        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        Artisan::call('lead:get', [
            '-S' => base64_encode(json_encode([
                'lid'  => $leadData['id'],
                'sid' => null,
                'aid'  => $agentData['id'],
                'token'=> $agent->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:get command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:get', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
