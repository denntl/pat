<?php

namespace Tests\Unit\Commands\Lead;

use App\Concierge;
use App\Lead;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of lead:update command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create();
        $lead = factory(Lead::class)->create([
            'conciergeId' => $concierge->id
        ]);

        $email = 'igor@pronin.com';
        $lead->email = $email;

        Artisan::call('lead:update', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'sid' => null,
                'token' => $concierge->getToken(),
                'data' => $lead->toArray()
            ]))
        ]);

        $output = Artisan::output();
        $updatedLead = Lead::find($lead->id);

        $this->assertEmpty($output);
        $this->assertEquals($email, $updatedLead->email);
    }

    /**
     * Test failed result of lead:update command via socket connection
     */
    public function testSocketFail()
    {
        $data = [
            'cid' => null,
            'sid' => null,
            'token' => null,
            'data' => []
        ];

        Artisan::call('lead:update', [
            '-S' => base64_encode(json_encode($data))
        ]);

        $output = Artisan::output();

        $this->assertEquals('Wrong token', trim($output));
    }
}
