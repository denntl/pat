<?php

namespace Tests\Unit\Commands\Lead;

use App\Lead;
use App\Agent;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadSendEmailsTest extends TestCase
{
    /**
     * Test success result of lead:send:emails command via socket connection
     */
    public function testSocketSuccess()
    {
        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        Artisan::call('lead:send:emails', [
            '-S' => base64_encode(json_encode([
                'aid'    => $agentData['id'],
                'lid'    => $leadData['id'],
                'sid'    => null,
                'token'  => $agent->getToken(),
                'emails' => 'test@test.com, test2@test.com'
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:send:emails command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:send:emails', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
