<?php

namespace Tests\Unit\Commands\Lead;

use App\Lead;
use App\Concierge;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LeadUpdateRepTest extends TestCase
{
    //use DatabaseTransactions;
    /**
     * Test success result of lead:update:rep command via socket connection
     */
    public function testSocketSuccess()
    {
        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        $concierge = factory(Concierge::class)->create(
            [
                'role' => 3
            ]
        );
        $conciergeData = $concierge->toArray();

        Artisan::call('lead:update:rep', [
            '-S' => base64_encode(json_encode([
                'lid'  => $leadData['id'],
                'cid'  => $conciergeData['id'],
                'token'=> $concierge->getToken(),
                'sid'  => null,
                'rid'  => $conciergeData['id'],
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:update:rep command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:update:rep', []);

        $this->assertNotEmpty(Artisan::output());
    }
}
