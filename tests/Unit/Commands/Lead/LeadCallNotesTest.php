<?php

namespace Tests\Unit\Commands\Lead;

use App\Concierge;
use App\Lead;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadCallNotesTest extends TestCase
{
    /**
     * Test success result of lead:call:notes command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        Artisan::call('lead:call:notes', [
            '-S' => base64_encode(json_encode([
                'lid'   => $leadData['id'],
                'cid'   => $conciergeData['id'],
                'sid'   => null,
                'token' => $concierge->getToken(),
                'notes' => 'test note'
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:call:notes command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:call:notes', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
