<?php

namespace Tests\Unit\Commands\Lead;

use App\Concierge;
use App\Agent;
use App\Lead;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LeadUpdateByAgentTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of lead:update:by-agent command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        Artisan::call('lead:update:by-agent', [
            '-S' => base64_encode(json_encode([
                'cid'  => $conciergeData['id'],
                'aid'  => $agentData['id'],
                'data' => $leadData,
                'token'=> $agent->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:update:by-agent command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:update:by-agent', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
