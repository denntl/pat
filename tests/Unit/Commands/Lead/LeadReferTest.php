<?php

namespace Tests\Unit\Commands\Lead;

use App\Lead;
use App\Agent;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadReferTest extends TestCase
{
    /**
     * Test success result of lead:refer command via socket connection
     */
    public function testSocketSuccess()
    {
        /*
         * Temporary disabled due Viking-demo crash reason
        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        Artisan::call('lead:refer', [
            '-S' => base64_encode(json_encode([
                'lid'  => $leadData['id'],
                'sid' => null,
                'aid'  => $agentData['id'],
                'token'=> $agent->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
        */
    }

    /**
     * Test failed result of lead:refer command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:refer', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
