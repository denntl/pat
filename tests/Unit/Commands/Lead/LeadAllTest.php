<?php

namespace Tests\Unit\Commands\Lead;

use App\Concierge;
use App\Lead;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadAllTest extends TestCase
{
    /**
     * Test success result of lead:all command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);
        $conciergeData = $concierge->toArray();

        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        Artisan::call('lead:all', [
            '-S' => base64_encode(json_encode([
                'lid'   => $leadData['id'],
                'cid'   => $conciergeData['id'],
                'sid'   => null,
                'token' => $concierge->getToken(),
                'admin' => true
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:all command via socket connection
     */
    public function testSocketFail()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);
        $conciergeData = $concierge->toArray();

        Artisan::call('lead:all', [
            '-S' => base64_encode(json_encode([
                'cid'   => $conciergeData['id']
            ]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
