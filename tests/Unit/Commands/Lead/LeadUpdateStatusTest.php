<?php

namespace Tests\Unit\Commands\Lead;

use App\Lead;
use App\Concierge;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LeadUpdateStatusTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of lead:update:status command via socket connection
     */
    public function testSocketSuccess()
    {
        $status = 'New lead';
        $lead = factory(Lead::class)->create([
            'status' => $status
        ]);
        $leadData = $lead->toArray();

        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        Artisan::call('lead:update:status', [
            '-S' => base64_encode(json_encode([
                'cid'  => $conciergeData['id'],
                'lid'  => $leadData['id'],
                'sid'  => null,
                'status'  => $status,
                'note'  => 'test',
                'action'  => 'Call note',
                'token'=> $concierge->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:update:status command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:update:status', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
