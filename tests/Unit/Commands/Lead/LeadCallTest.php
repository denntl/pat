<?php

namespace Tests\Unit\Commands\Lead;

use App\Lead;
use App\Concierge;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadCallTest extends TestCase
{
    /**
     * Test success result of lead:call command via socket connection
     */
    public function testSocketSuccess()
    {
        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        Artisan::call('lead:call', [
            '-S' => base64_encode(json_encode([
                'cid'  => $conciergeData['id'],
                'lid'  => $leadData['id'],
                'sid'  => 'test',
                'csid' => 'test',
                'phone'=> '+12053866015',
                'token'=> $concierge->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:call command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:call', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
