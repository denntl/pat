<?php

namespace Tests\Unit\Commands\Lead;

use App\Concierge;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LeadReassignTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of lead:reassign command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        Artisan::call('lead:reassign', [
            '-S' => base64_encode(json_encode([
                'cid'  => $conciergeData['id'],
                'sid'  => null,
                'token'=> $concierge->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:reassign command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:reassign', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
