<?php

namespace Tests\Unit\Commands\Lead;

use App\Agent;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LeadGetPortalTest extends TestCase
{
    /**
     * Test success result of lead:get-portal command via socket connection
     */
    public function testSocketSuccess()
    {
        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        Artisan::call('lead:get-portal', [
            '-S' => base64_encode(json_encode([
                'sid' => null,
                'aid'  => $agentData['id'],
                'token'=> $agent->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of lead:get-portal command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('lead:get-portal', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
