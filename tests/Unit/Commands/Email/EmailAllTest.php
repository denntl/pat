<?php

namespace Tests\Unit\Commands\Email;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Concierge;
use App\Lead;
use App\Console\Commands\Email\EmailAll;

class EmailAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:all command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create();
        $lead = factory(Lead::class)->create();

        Artisan::call('email:all', [
            '-S' => base64_encode(json_encode([
                    'cid' => $concierge->id,
                    'lid' => $lead->id,
                    'sid' => null,
                    'token' => $concierge->getToken()
           ]))
        ]);

        $output = Artisan::output();
        $this->assertEquals(EmailAll::SUCCESS_MESSAGE, trim($output));
    }

    /**
     * Test failed result of email all command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('email:all', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'token' => null
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(EmailAll::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
