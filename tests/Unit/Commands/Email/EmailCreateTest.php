<?php

namespace Tests\Unit\Commands\Email;

use App\Email;
use App\Concierge;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Email\EmailCreate;

class EmailCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of email create command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create();
        $email = factory(Email::class)->create();
        Artisan::call('email:create', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'sid' => null,
                'token' => $concierge->getToken(),
                'data' => $email->toArray()
            ]))
        ]);

        $output = Artisan::output();
        $this->assertEquals(EmailCreate::SUCCESS_MESSAGE, trim($output));
    }

    /**
     * Test failed result of email create command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('email:create', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'token' => null,
                'data' => []
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(EmailCreate::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
