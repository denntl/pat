<?php

namespace Tests\Unit\Commands\Agent;

use App\Agent;
use App\Concierge;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AgentGetPersonaTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:persona:get command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        Artisan::call('agent:persona:get', [
            '-S' => base64_encode(json_encode([
                'sid'  => null,
                'aid'  => $agentData['id'],
                'cid'  => $conciergeData['id'],
                'token'=> $concierge->getToken()
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of agent:persona:get command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('agent:persona:get', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
