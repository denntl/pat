<?php

namespace Tests\Unit\Commands\Agent;

use App\Agent;
use App\Concierge;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AgentUpdateOnBoardingTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:update:tutorial command via socket connection
     */
    public function testSocketSuccess()
    {
        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        $concierge = factory(Concierge::class)->create();
        $conciergeData = $concierge->toArray();

        Artisan::call('agent:update:tutorial', [
            '-S' => base64_encode(json_encode([
                'sid'   => null,
                'aid'   => $agentData['id'],
                'token' => $agent->getToken(),
                'step'  => 1,
                'data'  => $conciergeData
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of agent:update:tutorial command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('agent:update:tutorial', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
