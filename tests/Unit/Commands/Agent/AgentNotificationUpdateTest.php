<?php

namespace Tests\Unit\Commands\Agent;

use App\Agent;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AgentNotificationUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:notification:update command via socket connection
     */
    public function testSocketSuccess()
    {
        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        Artisan::call('agent:notification:update', [
            '-S' => base64_encode(json_encode([
                'sid'  => null,
                'aid'  => $agentData['id'],
                'token'=> $agent->getToken(),
                'data' => [
                    'notificationPhone'    => '(415) 555-2671',
                    'notificationEmail'    => 'test@test.com',
                    'callForwardingNumber' => '(415) 555-2671'
                ]
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of agent:notification:update command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('agent:notification:update', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
