<?php

namespace Tests\Unit\Commands\Agent;

use App\Agent;
use App\Lead;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AgentSendLeadToTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:send:lead command via socket connection
     */
    public function testSocketSuccess()
    {
        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        $lead = factory(Lead::class)->create();
        $leadData = $lead->toArray();

        Artisan::call('agent:send:lead', [
            '-S' => base64_encode(json_encode([
                'sid'  => null,
                'aid'  => $agentData['id'],
                'leadId'  => $leadData['id'],
                'token'=> $agent->getToken(),
                'emails' => 'test@test.com'
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of agent:send:lead command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('agent:send:lead', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
