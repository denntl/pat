<?php

namespace Tests\Unit\Commands\Agent;

use App\Agent;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AgentPasswordUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:password:update command via socket connection
     */
    public function testSocketSuccess()
    {
        $password = 'test123456789';
        $newPassword = 'test987654321';
        $agent = factory(Agent::class)->create([
            'password' => bcrypt($password)
        ]);
        $agentData = $agent->toArray();

        Artisan::call('agent:password:update', [
            '-S' => base64_encode(json_encode([
                'sid'         => null,
                'session'     => null,
                'aid'         => $agentData['id'],
                'token'       => $agent->getToken(),
                'password'    => $password,
                'newPassword' => $newPassword
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of agent:password:update command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('agent:password:update', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
