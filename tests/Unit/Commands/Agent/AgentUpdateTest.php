<?php

namespace Tests\Unit\Commands\Agent;

use App\Agent;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AgentUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:update command via socket connection
     */
    public function testSocketSuccess()
    {
        $agent = factory(Agent::class)->create();
        $agentData = $agent->toArray();

        Artisan::call('agent:update', [
            '-S' => base64_encode(json_encode([
                'sid'  => null,
                'aid'  => $agentData['id'],
                'token'=> $agent->getToken(),
                'data' => [
                    'personaTeamName' => 'test',
                    'personaName'     => 'test',
                    'personaTitle'    => 'test',
                    'extraNoteForRep' => 'test',
                    'email' => 'test@test.com'
                ]
            ]))
        ]);

        $this->assertEmpty(Artisan::output());
    }

    /**
     * Test failed result of agent:get command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('agent:update', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertNotEmpty(Artisan::output());
    }
}
