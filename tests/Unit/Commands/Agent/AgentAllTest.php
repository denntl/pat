<?php

namespace Tests\Unit\Commands\Agent;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AgentAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of agent:all command via socket connection
     */
    public function testSocketSuccess()
    {
        Artisan::call('agent:all', [
            '-S' => base64_encode(json_encode([]))
        ]);

        $this->assertEmpty(Artisan::output());
    }
}
