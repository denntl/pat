<?php

namespace Tests\Unit\Commands\Message;

use App\Concierge;
use App\Lead;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Message\MessageAll;

class MessageAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of message all command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);
        $lead = factory(Lead::class)->create();

        Artisan::call('message:all', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'sid' => null,
                'lid' => $lead->id,
                'token' => $concierge->getToken()
            ]))
        ]);

        $output = Artisan::output();
        $this->assertEquals(MessageAll::SUCCESS_MESSAGE, trim($output));
    }

    /**
     * Test failed result of message all command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('concierge:delete', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'lid' => null,
                'token' => null
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(MessageAll::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
