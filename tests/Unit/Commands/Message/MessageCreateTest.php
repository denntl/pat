<?php

namespace Tests\Unit\Commands\Message;

use App\Message;
use App\Concierge;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Message\MessageCreate;

class MessageCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of message create command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);
        $message = factory(Message::class)->create();
        Artisan::call('message:create', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'sid' => null,
                'token' => $concierge->getToken(),
                'data' => $message->toArray()
            ]))
        ]);

        $output = Artisan::output();
        $this->assertEquals(MessageCreate::SUCCESS_MESSAGE, trim($output));
    }

    /**
     * Test failed result of message create command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('message:create', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'token' => null,
                'data' => []
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(MessageCreate::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
