<?php

namespace Tests\Unit\Commands\Message;

use App\Message;
use App\Concierge;
use App\Lead;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Message\MessageUpdateStatus;

class MessageUpdateStatusTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of message update status command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);
        $lead = factory(Lead::class)->create();
        $message = factory(Message::class)->create();
        $messageStatus = 'received';

        Artisan::call('message:status:update', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'lid' => $lead->id,
                'sid' => null,
                'token' => $concierge->getToken(),
                'twilioMessageSid' => $message->twilioMessageSid,
                'status' => $messageStatus

            ]))
        ]);

        $updatedMessage = Message::find($message->id);
        $output = Artisan::output();

        $this->assertEquals(MessageUpdateStatus::SUCCESS_MESSAGE, trim($output));
        $this->assertEquals($messageStatus, $updatedMessage->twilioMessageStatus);
    }

    /**
     * Test failed result of message update status command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('message:status:update', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'lid' => null,
                'sid' => null,
                'token' => null,
                'twilioMessageSid' => null,
                'status' => null
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(MessageUpdateStatus::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
