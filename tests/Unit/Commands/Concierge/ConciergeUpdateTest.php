<?php

namespace Tests\Unit\Commands\Concierge;

use App\Concierge;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Concierge\ConciergeUpdate;

class ConciergeUpdateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of concierge update command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);

        $testName = 'test_name';
        $testEmail = 'testemail@test.com';

        Artisan::call('concierge:update', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'sid' => null,
                'token' => $concierge->getToken(),
                'name' => $testName,
                'email' => $testEmail,
                'role' => 3,
                'id' => $concierge->id
            ]))
        ]);

        $output = Artisan::output();
        $updatedConcierge = Concierge::find($concierge->id);

        $this->assertEquals(ConciergeUpdate::SUCCESS_MESSAGE, trim($output));
        $this->assertEquals($testEmail, $updatedConcierge->email);
        $this->assertEquals($testName, $updatedConcierge->name);
    }

    /**
     * Test failed result of concierge update command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('concierge:update', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'token' => null,
                'name' => null,
                'email' => null,
                'role' => null
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(ConciergeUpdate::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
