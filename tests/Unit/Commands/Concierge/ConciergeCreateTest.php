<?php

namespace Tests\Unit\Commands\Concierge;

use App\Concierge;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Concierge\ConciergeCreate;

class ConciergeCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of concierge create command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);

        Artisan::call('concierge:create', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'sid' => null,
                'token' => $concierge->getToken(),
                'name' => 'test_name',
                'email' => 'testemail@test.com',
                'role' => 3
            ]))
        ]);

        $output = Artisan::output();
        $this->assertEquals(ConciergeCreate::SUCCESS_MESSAGE, trim($output));
    }

    /**
     * Test failed result of concierge create command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('concierge:create', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'token' => null,
                'name' => null,
                'email' => null,
                'role' => null
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(ConciergeCreate::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
