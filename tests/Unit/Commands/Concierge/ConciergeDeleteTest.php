<?php

namespace Tests\Unit\Commands\Concierge;

use App\Concierge;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Concierge\ConciergeDelete;

class ConciergeDeleteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of concierge delete command via socket connection
     */
    public function testSocketSuccess()
    {
        $conciergeRequest = factory(Concierge::class)->create([
            'role' => 3
        ]);
        $conciergeForRemove = factory(Concierge::class)->create();

        Artisan::call('concierge:delete', [
            '-S' => base64_encode(json_encode([
                'cid' => $conciergeRequest->id,
                'sid' => null,
                'token' => $conciergeRequest->getToken(),
                'id' => $conciergeForRemove->id,

            ]))
        ]);

        $output = Artisan::output();
        $removedConcierge = Concierge::find($conciergeForRemove->id);

        $this->assertEquals(ConciergeDelete::SUCCESS_MESSAGE, trim($output));
        $this->assertEquals($removedConcierge->isDeleted, true);
    }

    /**
     * Test failed result of concierge delete command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('concierge:delete', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'token' => null,
                'id' => null,
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(ConciergeDelete::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
