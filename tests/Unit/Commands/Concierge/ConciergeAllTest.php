<?php

namespace Tests\Unit\Commands\Concierge;

use App\Concierge;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Console\Commands\Concierge\ConciergeAll;

class ConciergeAllTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test success result of concierge all command via socket connection
     */
    public function testSocketSuccess()
    {
        $concierge = factory(Concierge::class)->create([
            'role' => 3
        ]);

        Artisan::call('concierge:all', [
            '-S' => base64_encode(json_encode([
                'cid' => $concierge->id,
                'sid' => null,
                'token' => $concierge->getToken()
            ]))
        ]);

        $output = Artisan::output();
        $this->assertEquals(ConciergeAll::SUCCESS_MESSAGE, trim($output));
    }

    /**
     * Test failed result of concierge all command via socket connection
     */
    public function testSocketFail()
    {
        Artisan::call('concierge:delete', [
            '-S' => base64_encode(json_encode([
                'cid' => null,
                'sid' => null,
                'token' => null
            ]))
        ]);

        $output = Artisan::output();

        $this->assertEquals(ConciergeAll::WRONG_TOKEN_MESSAGE, trim($output));
    }
}
