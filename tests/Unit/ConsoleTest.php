<?php

namespace Tests\Unit;

use App\Concierge;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * Class ConsoleTest.
 * @package Tests\Unit
 */
class ConsoleTest extends TestCase
{
    /**
     * Test console general access.
     */
    public function testGeneralAccess()
    {
        $response = $this->get('/console/');
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test login access to console `messaging` page.
     */
    public function testMessagingLoginAccess()
    {
        $concierge = new Concierge([
            'email' => 'test@example.com'
        ]);

        $this->be($concierge);

        $response = $this->get('/console/messaging');
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test login access to console `permission` page.
     */
    public function testPermissionLoginAccess()
    {
        $concierge = new Concierge([
            'email' => 'test@example.com'
        ]);

        $this->be($concierge);

        $response = $this->get('/console/permission');
        $response->assertStatus(Response::HTTP_OK);
    }
}
