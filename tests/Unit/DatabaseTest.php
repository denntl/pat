<?php

namespace Tests\Unit;

use App\Concierge;
use Tests\TestCase;

/**
 * Class DatabaseTest.
 * @package Tests\Unit
 */
class DatabaseTest extends TestCase
{
    /**
     * Test client account availability.
     */
    public function testClientAccountAvailability()
    {
        $this->assertDatabaseHas('concierges', [
            'email' => 'blakeweis1986@gmail.com'
        ]);
    }

    /**
     * Test user factory.
     */
    public function testUserFactory()
    {
        $concierge = factory(Concierge::class)->make([
            'email' => 'test@test.com',
            'role' => 1,
        ]);

        $this->assertEquals(1, $concierge->role);
        $this->assertEquals('test@test.com', $concierge->email);
    }
}
