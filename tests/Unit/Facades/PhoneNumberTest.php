<?php

namespace Tests\Unit\Facades;

use App\Facades\PhoneNumber;
use Tests\TestCase;

/**
 * Class TwilioTest
 *
 * @package Tests\Unit\Facades
 */
class PhoneNumberTest extends TestCase
{
    /**
     * Test getFormattedPhone method for success
     *
     * @dataProvider phoneNumbersProvider
     *
     * @param string $phone
     * @param string $expected
     */
    public function testGetFormattedPhoneSuccess(string $phone, string $expected)
    {
        $formatted = PhoneNumber::getNationalPhone($phone);

        $this->assertEquals($expected, $formatted);
    }

    /**
     * Phone numbers provider.
     *
     * @return array
     */
    public function phoneNumbersProvider()
    {
        $expected = '(415) 555-2671';

        return [
            ['4155552671', $expected],
            ['415 555 2671', $expected],
            ['415-555-2671', $expected],
            ['(415) 555-2671', $expected],
            ['+1 (415) 555-2671', $expected],
        ];
    }
}
