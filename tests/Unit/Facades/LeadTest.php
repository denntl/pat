<?php

namespace Tests\Unit\Facades;

use App\Facades\Lead;
use App\Lead as LeadModel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class LeadTest
 *
 * @package Tests\Unit\Facades
 */
class LeadTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test addSource method for success
     *
     * @dataProvider sourcesProvider
     *
     * @param mixed $source
     * @param bool $expected
     */
    public function testAddSource($source, bool $expected)
    {
        $lead = factory(LeadModel::class)->create([
            'channelwebsite' => $source
        ]);

        $result = Lead::addSource($lead);

        $this->assertEquals($expected, $result);
    }

    /**
     * Source provider for addSource test.
     *
     * @return array
     */
    public function sourcesProvider()
    {
        return [
            [false, false],
            [null, false],
            ['', false],
            ['Some source', true]
        ];
    }

    /**
     * Test addSource method for fail
     *
     * @return void
     */
    public function testAddSourceFail()
    {
        $lead = factory(LeadModel::class)->create();

        $result = Lead::addSource($lead);

        $this->assertFalse($result);
    }
}
