<?php

namespace Tests\Unit\Facades;

use App\Email;
use App\Facades\SendGrid;
use App\Lead;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class SendGridTest
 *
 * @package Tests\Unit\Facades
 */
class SendGridTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test send email stopple
     */
    public function testEmailStoppleSuccess()
    {
        $this->enableSafeMode();

        /** @var Email $email */
        $email = factory(Email::class)->create();

        $res = SendGrid::sendLeadEmail($email);

        $this->assertFalse($res);
    }

    /**
     * Test sending email stopple when lead is missed
     */
    public function testStoppleMissedLead()
    {
        $this->enableSafeMode();

        $email = factory(Email::class)->make();

        $res = SendGrid::sendLeadEmail($email);

        $this->assertEmpty($res);
    }

    /**
     * Test sending email with changed email.
     */
    public function testEmailStoppleChangeEmail()
    {
        $this->enableSafeMode();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();

        $lead->update(['email' => 'pronin.igor@ddi-dev.com']);

        $email = factory(Email::class)->create([
            'to' => $lead->email,
            'leadId' => $lead->id
        ]);

        $res = SendGrid::sendLeadEmail($email);

        $this->assertEmpty($res);
    }
}