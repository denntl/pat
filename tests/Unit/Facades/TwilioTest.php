<?php

namespace Tests\Unit\Facades;

use App\Facades\Twilio;
use App\Lead;
use App\Services\TwilioService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Twilio\Rest\Api\V2010\Account\MessageInstance;
use Twilio\Exceptions\RestException;

/**
 * Class TwilioTest
 *
 * @package Tests\Unit\Facades
 */
class TwilioTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test sms sending when SAFE_MODE is enable
     */
    public function testStoppleSuccess()
    {
        $this->enableSafeMode();

        $lead = factory(Lead::class)->create([
            'phone' => '+18888888888'
        ]);

        // TODO: enable it when investigate problem
        // $res = Twilio::sendSms($lead->phone, TwilioService::TEST_FROM_PHONE, 'Wake, up!');

        // $this->assertFalse($res);
    }

    /**
     * Test sending sms stopple when lead is missed
     */
    public function testStoppleMissedLead()
    {
        $this->enableSafeMode();

        $res = Twilio::sendSms('+380977790677', '+15005550006', 'Wake, up!');

        $this->assertFalse($res);
    }

    /**
     * Test sms sending with changed phone
     */
    public function testStoppleChangePhone()
    {
        $this->enableSafeMode();

        /** @var Lead $lead */
        $lead = factory(Lead::class)->create();
        $lead->update(['phone' => '+15005550006']);

        // TODO: enable it when investigate problem with self sending
        // $res = Twilio::sendSms($lead->phone, '+15005550006', 'Wake, up!', false);

        // $this->assertInstanceOf(MessageInstance::class, $res);
    }

    /**
     * Test sms sending when SAFE_MODE is disable
     */
    public function testStoppleFail()
    {
        $this->disableSafeMode();

        // TODO: enable it when investigate problem with self sending
        // $res = Twilio::sendSms('+15005550006', '+15005550006', 'Wake, up!', false);

        // $this->assertInstanceOf(MessageInstance::class, $res);
    }

    /**
     * Test hasSid method.
     *
     * @return void
     */
    public function testHasSid()
    {
        $sid = config('services.twilio.account_sid');

        $this->assertTrue(Twilio::hasSid($sid));
    }

    /**
     * Test that sendSms fail
     *
     * @return void
     */
    public function testSendSmsFail()
    {
        try {
            Twilio::sendSms('+14108675309', '+15005550006', 'Hello!');
        } catch (RestException $exception) {
            $this->assertEquals(400, $exception->getStatusCode());
        }
    }

    /**
     * Test that createNumber fail
     *
     * @return void
     */
//    public function testCreateNumberFail()
//    {
//        try {
//            Twilio::createNumber();
//        } catch (RestException $exception) {
//            $this->assertEquals(403, $exception->getStatusCode());
//        }
//    }
    /**
     * Test that generateNumberByAre fail
     *
     * @return void
     */
//    public function testGenerateNumberByAreaFail()
//    {
//        try {
//            Twilio::generateNumberByArea('205');
//        } catch (RestException $exception) {
//            $this->assertEquals(403, $exception->getStatusCode());
//        }
//    }

    /**
     * Test generate token
     *
     * @return void
     */
    public function testGenerateToken()
    {
        $token = Twilio::generateToken();

        $this->assertEquals('string', gettype($token));
        $this->assertEquals(276, strlen($token));
    }

    /**
     * Test message status checker.
     *
     * @dataProvider statusProvider
     *
     * @param mixed $status
     * @param bool $expected
     */
    public function testCheckMessageStatus($status, bool $expected)
    {
        $checked = Twilio::checkMessageStatus($status);

        $this->assertEquals($expected, $checked);
    }

    /**
     * Status provider for status checker test.
     *
     * @return array
     */
    public function statusProvider()
    {
        return [
            [false, false],
            [null, false],
            ['', false],
            [1, false],
            ['Received', false],
            ['received', true],
            ['sent', true],
            ['sending', true],
            ['failed', true],
            ['queued', true],
            ['delivered', true],
            ['undelivered', true]
        ];
    }
}