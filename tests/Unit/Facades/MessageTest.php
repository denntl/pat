<?php

namespace Tests\Unit\Facades;

use App\Facades\Message as MessageFacade;
use App\Message;
use App\Services\TwilioService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class MessageTest
 *
 * @package Tests\Unit\Facades
 */
class MessageTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test message service updateTwilioStatus method
     *
     * @return void
     */
    public function testUpdateTwilioStatus()
    {
        /** @var Message $message */
        $message = factory(Message::class)->create([
            'incoming' => 1
        ]);

        $status = $message->twilioMessageStatus;
        $newStatus = TwilioService::MESSAGE_FAILED_STATUS;
        if ($status === $newStatus) {
            $newStatus = TwilioService::MESSAGE_UNDELIVERED_STATUS;
        }

        $sid = $message->twilioMessageSid;

        MessageFacade::updateTwilioStatus($sid, $newStatus);

        /** @var Message $updatedMessage */
        $updatedMessage = Message::where('twilioMessageSid', $sid)->first();

        $this->assertEquals($newStatus, $updatedMessage->twilioMessageStatus);
    }
}