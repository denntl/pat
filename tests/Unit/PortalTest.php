<?php

namespace Tests\Unit;

use App\Agent;
use App\AgentOnBoarding;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * Class PortalTest.
 * @package Tests\Unit
 */
class PortalTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test portal general access.
     */
    public function testGeneralAccess()
    {
        $response = $this->get('/');
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test login access to portal `myleads/all` page.
     */
    public function testMyLeadsAllLoginAccess()
    {
        $agent = $this->createAgentAndFinishTutorial();

        $this->be($agent, 'portal');

        $response = $this->get('/portal/myleads/all');
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test login access to portal `lead` page.
     */
    public function testLeadLoginAccess()
    {
        $agent = $this->createAgentAndFinishTutorial();

        $this->be($agent, 'portal');

        $response = $this->get('/portal/lead/1');
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test login access to portal `billing` page.
     */
    public function testBillingLoginAccess()
    {
        $agent = $this->createAgentAndFinishTutorial();

        $this->be($agent, 'portal');

        $response = $this->get('/portal/billing');
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test login access to portal `profile/view` page.
     */
    public function testProfileViewLoginAccess()
    {
        $agent = $this->createAgentAndFinishTutorial();

        $this->be($agent, 'portal');

        $response = $this->get('/portal/profile/view');
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test redirect on setup if user didn't finish onboarding
     */
    public function testSetupRedirect()
    {
        $agent = factory(Agent::class)->create();

        $this->be($agent, 'portal');

        $response = $this->get('/portal');

        $response->assertStatus(Response::HTTP_FOUND);
    }

    /**
     * Test access to onboarding setup page
     */
    public function testAccessTutorial()
    {
        $agent = factory(Agent::class)->create();

        $this->be($agent, 'portal');

        $response = $this->get('/portal/setup');

        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Method to create agent user and onboarding record and finish onboarding.
     * 
     * @return Agent
     */
    private function createAgentAndFinishTutorial()
    {
        $agent = factory(Agent::class)->create();

        factory(AgentOnBoarding::class)->create([
            'agentId' => $agent->id,
            'step' => 4,
        ]);

        return $agent;
    }
}
