<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase
 *
 * @package Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Enables SAFE_MODE.
     *
     * @return void
     */
    protected function enableSafeMode()
    {
        $isSafe = config('app.safe');
        if (!$isSafe) {
            config(['app.safe' => true]);
        }
    }

    /**
     * Disables SAFE_MODE.
     *
     * @return void
     */
    protected function disableSafeMode()
    {
        $isSafe = config('app.safe');
        if ($isSafe) {
            config(['app.safe' => false]);
        }
    }
}
