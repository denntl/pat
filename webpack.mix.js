const {mix} = require('laravel-mix');

mix
    .js('resources/assets/js/app.js', 'public/assets/js')
    .sass('resources/assets/sass/app.scss', 'public/assets/css')
    .copy('resources/assets/images', 'public/assets/images')
    .sourceMaps()
    .copy('node_modules/materialize-css/dist/js/materialize.min.js', 'public/assets/js')
    .version();
