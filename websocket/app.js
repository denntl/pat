const fs = require('fs');
const rollbar = require('rollbar');
const express = require('express')();
const config = require('./config/app');
const logger = require('./logger');

/**
 * Assign rollbar.
 *
 * @typedef {EventEmitter} process
 */
process.on('uncaughtException', error => {
    logger.error(error);

    if (config.isEnv('production') || config.isEnv('demo') || config.isEnv('dev')) {
        rollbar.init(config.rollbar.token);
        rollbar.error(error);
    }

    if (!error.isOperational) {
        setTimeout(() => {
            process.exit(1);
            // TODO: change timeout to callback of 'rollbar.error'
        }, 3000);
    }
});

/**
 * Create socket server app.
 */

const app = {
    config,
};

if (config.isEnv('production') || config.isEnv('demo') || config.isEnv('dev')) {
    const serverSettings = {
        key: fs.readFileSync(config.ssl.key),
        cert: fs.readFileSync(config.ssl.cert),
        ca: fs.readFileSync(config.ssl.ca),
        requestCert: false,
        rejectUnauthorized: false,
    };

    app.server = require('https').createServer(serverSettings, express);
} else {
    app.server = require('http').Server(express);
}

module.exports = app;
