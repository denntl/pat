/**
 * Exports events for `concierge-<id>` channel.
 *
 * @param {RedisEmitter} emitter - Event emitter
 *
 * @returns {void}
 */
module.exports = emitter => {
    emitter.on('App\\Events\\Logout', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('conciergeLogout', args.data);
        }
    });

    emitter.on('App\\Events\\Concierge\\ConciergeSetAway', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('conciergeSetAway', args.data.away);
        }
    });

    emitter.on('App\\Events\\Concierge\\conciergeSetLastAfk', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('conciergeSetLastAfk', args.data.lastAfkActivity);
        }
    });

    emitter.on('App\\Events\\Concierge\\ConciergeSwitchDistribution', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('conciergeSwitchDistribution', args.data);
        }
    });
};
