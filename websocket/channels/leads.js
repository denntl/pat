/* global leadsCollection */
/* global conciergesCollection */

/**
 * Exports events for `leads` channel.
 *
 * @param {RedisEmitter} emitter - Event emitter
 *
 * @returns {void}
 */
module.exports = emitter => {
    emitter.on('App\\Events\\Lead\\LeadUpdateRep', args => {
        const data = args.data;

        if (args.socketId !== args.sender) {
            data.lead.finalTimeForResponse = leadsCollection.setFinalTimeForResponse(data.leadId, data.lead.tag);
            args.socket.json.send('leadUpdateRep', data);
        } else {
            args.socket.json.send('leadUpdateTime', {
                leadId: data.leadId,
                updatedAt: data.updatedAt,
            });
        }
    });

    emitter.on('App\\Events\\Lead\\LeadUpdate', args => {
        const currentConcierge = conciergesCollection.getItemOrCreate(args.currentConciergeId);
        const permission = currentConcierge.getPermission();

        // Prevent sending LeadUpdate to Tier 1/Tier 2.
        // This event already sends from `lead` channel to Tier1/Tier2 that subscribed to the channel of current lead.
        if (permission !== 'Tier 1' && permission !== 'Tier 2' && args.socketId !== args.sender) {
            args.socket.json.send('leadUpdate', args.data.lead);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadAll', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('leadAll', args.data.leads);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadUpdateStatus', args => {
        args.socket.json.send('leadUpdateStatus', args.data);
    });

    emitter.on('App\\Events\\Lead\\LeadRefer', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('leadRefer', args.data.id);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCall', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('leadCall', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCallEnd', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('leadCallEnd', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCallNotes', args => {
        const message = args.socketId !== args.sender ? 'leadCallNotes' : 'leadCallNotesReport';

        args.socket.json.send(message, args.data);
    });

    emitter.on('App\\Events\\Lead\\LeadRemove', args => {
        if (args.socket !== args.sender) {
            args.socket.json.send('leadRemove', args.data.lead);
        }

        if (args.currentConciergeId === args.data.lead.id) {
            args.sub.unsubscribe(`lead-${args.data.lead.id}`);
        }
    });

    emitter.on('App\\Events\\Message\\MessageAll', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('messageAll', args.data.messages);
        }
    });

    emitter.on('App\\Events\\Email\\EmailAll', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('emailAll', args.data.messages);
        }
    });

    emitter.on('App\\Events\\Email\\EmailCreate', args => {
        const message = args.socketId === args.sender ? 'emailCreateReport' : 'emailCreate';
        const data = args.data;

        data.finalTimeForResponse = leadsCollection.setFinalTimeForResponse(data.leadId, data.tag);

        args.socket.json.send(message, data);
    });

    emitter.on('App\\Events\\Message\\MessageCreate', args => {
        const message = args.socketId === args.sender ? 'messageCreateReport' : 'messageCreate';
        const data = args.data;

        data.finalTimeForResponse = leadsCollection.setFinalTimeForResponse(data.leadId, data.tag);

        args.socket.json.send(message, data);
    });

    emitter.on('App\\Events\\Message\\MessageUpdateStatus', args => {
        args.socket.json.send('messageUpdateStatus', args.data);
    });
};
