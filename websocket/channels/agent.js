module.exports = emitter => {
    emitter.on('App\\Events\\Agent\\AgentGet', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('agentGet', args.data.agent);
        }
    });

    emitter.on('App\\Events\\LogoutAgent', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('agentLogout', args.data);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentUpdate', args => {
        args.socket.json.send('agentUpdate', args.data.agent);
    });

    emitter.on('App\\Events\\Agent\\AgentUpdateFailed', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('agentUpdateFailed', args.data);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentPasswordUpdate', args => {
        args.socket.json.send('agentPasswordUpdate', args.data);
    });

    emitter.on('App\\Events\\Agent\\AgentPersonaUpdate', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('agentPersonaUpdate', args.data.agentData);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentGetPersonaData', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('agentGetPersonaData', args.data.agentPersonaData);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentGeneratePhone', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('agentGeneratePhone', args.data.phone);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentUpdateOnBoarding', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('agentUpdateOnBoarding', args.data.step);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentSetup', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('agentSetup', args.data.data);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentNotificationUpdate', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('agentNotificationUpdate', args.data.agentData);
        }
    });

    emitter.on('App\\Events\\Agent\\AgentBilling', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('agentBilling', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCreate', args => {
        const isCurrentAgent = args.currentAgentId === args.data.agentId;

        args.socket.json.send('leadCreate', args.data);
        if (isCurrentAgent) {
            args.sub.subscribe(`lead-${args.data.lead.id}`);
        }
    });
};
