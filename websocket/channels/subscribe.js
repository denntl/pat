/* global conciergesCollection */
/* global leadsCollection */

/**
 * Exports events for `subscribe` channel.
 *
 * @param {RedisEmitter} emitter - Event emitter
 *
 * @returns {void}
 */
module.exports = emitter => {
    emitter.on('App\\Events\\Subscribe\\SubscribeMessaging', args => {
        if (args.socketId === args.sender) {
            const currentConcierge = conciergesCollection.getItemOrCreate(args.currentConciergeId);
            const data = args.data;

            data.concierges = conciergesCollection.collection;
            if (currentConcierge.getPermission() !== 'Admin') {
                data.leads = leadsCollection.setTimeForActiveLeads(data.leads);
            }

            args.socket.json.send('subscribeMessaging', data);
        }
    });

    emitter.on('App\\Events\\Subscribe\\ResubscribeMessagingLead', args => {
        if (args.currentConciergeId === args.data.repId) {
            args.sub.subscribe(`lead-${args.data.leadId}`);
        }
    });
};
