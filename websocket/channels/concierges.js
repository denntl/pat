/* global agentsCollection */
/* global conciergesCollection */

/**
 * Exports events for `concierges` channel.
 *
 * @param {RedisEmitter} emitter - Event emitter
 *
 * @returns {void}
 */
module.exports = emitter => {
    emitter.on('App\\Events\\Concierge\\ConciergeCreate', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('conciergeCreate', args.data);
        } else {
            args.socket.json.send('conciergeCreateReport', args.data.id);
        }
    });

    emitter.on('App\\Events\\Concierge\\ConciergeDelete', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('conciergeDelete', args.data.id);
        }
    });

    emitter.on('App\\Events\\Concierge\\ConciergeUpdate', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('conciergeUpdate', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCreate', args => {
        const sendData = {
            agent: agentsCollection.getItem(args.data.lead.agentId),
            leadId: args.data.lead.id,
        };
        const currentConcierge = conciergesCollection.getItemOrCreate(args.currentConciergeId);
        const permission = currentConcierge.getPermission();
        const isAssignedConcierge = args.currentConciergeId === args.data.lead.conciergeId;

        args.socket.json.send('leadCreate', args.data.lead);
        args.socket.json.send('agentGet', sendData);

        if (isAssignedConcierge || permission !== 'Tier 1' && permission !== 'Tier 2') {
            args.sub.subscribe(`lead-${args.data.lead.id}`);
        }
    });
};
