/* global leadsCollection */
/* global conciergesCollection */

/**
 * Exports events for `lead-<id>` channel.
 *
 * @param {RedisEmitter} emitter - Event Emitter
 * @param {string} leadId - Lead id
 *
 * @returns {void}
 */
module.exports = (emitter, leadId) => {
    emitter.on('App\\Events\\Lead\\LeadPauseSmsFlow', args => {
        args.socket.json.send('leadPauseSmsFlow', args.data);
    });

    emitter.on('App\\Events\\Lead\\LeadUnassignRep', args => {
        const currentConcierge = conciergesCollection.getItemOrCreate(args.currentConciergeId);

        args.socket.json.send('leadUnassignRep', args.data);

        if (currentConcierge.getPermission() !== 'Admin') {
            args.sub.unsubscribe(`lead-${args.data.leadId}`);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadUpdateRep', args => {
        const data = args.data;

        if (args.socketId !== args.sender) {
            data.lead.finalTimeForResponse = leadsCollection.setFinalTimeForResponse(data.leadId, data.lead.tag);

            args.socket.json.send('leadUpdateRep', data);
        } else {
            args.socket.json.send('leadUpdateTime', {
                leadId: data.leadId,
                updatedAt: data.updatedAt,
            });
        }

        const currentConcierge = conciergesCollection.getItemOrCreate(args.currentConciergeId);

        if (currentConcierge.getPermission() !== 'Admin' && args.currentConciergeId !== data.repId) {
            args.sub.unsubscribe(`lead-${data.leadId}`);
        }
    });

    emitter.on('App\\Events\\Message\\MessageAll', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('messageAll', args.data);
        }
    });

    emitter.on('App\\Events\\Message\\MessageCreate', args => {
        const message = args.socketId !== args.sender ? 'messageCreateReport' : 'messageCreate';
        const data = args.data;

        data.finalTimeForResponse = leadsCollection.setFinalTimeForResponse(data.leadId, data.tag);

        args.socket.json.send(message, data);
    });

    emitter.on('App\\Events\\Message\\MessageUpdateStatus', args => {
        args.socket.json.send('messageUpdateStatus', args.data);
    });

    emitter.on('App\\Events\\Message\\MessageReadAll', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('messageReadAll', args.data);
        }
    });

    emitter.on('App\\Events\\Email\\EmailCreate', args => {
        const message = args.socketId === args.sender ? 'emailCreateReport' : 'emailCreate';
        const data = args.data;

        data.finalTimeForResponse = leadsCollection.setFinalTimeForResponse(data.leadId, data.tag);

        args.socket.json.send(message, data);
    });

    emitter.on('App\\Events\\Email\\EmailUpdateStatus', args => {
        args.socket.json.send('emailUpdateStatus', args.data);
    });

    emitter.on('App\\Events\\Email\\EmailReadAll', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('emailReadAll', args.data);
        }
    });

    emitter.on('App\\Events\\Email\\EmailInbound', args => {
        // TODO: update only id in emails collection for current email
        args.socket.json.send('emailCreate', args.data);
    });

    emitter.on('App\\Events\\Email\\EmailAll', args => {
        if (args.socketId === args.sender) {
            // TODO: Use the same data format in the `EmailAll` event in leads channel.
            args.socket.json.send('emailAll', args.data);
        }
    });

    emitter.on('App\\Events\\Call\\AllCalls', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('allCalls', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadGet', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('leadGet', args.data.lead);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadUpdate', args => {
        args.socket.json.send('leadUpdate', args.data.lead);

        const leadUndef = typeof leadsCollection.collection[args.data.lead.id] !== 'undefined';

        if (leadUndef && args.currentConciergeId === leadsCollection.collection[args.data.lead.id].conciergeId) {
            args.sub.unsubscribe(`lead-${args.data.lead.id}`);
            // TODO: here can be bug if 'system' channel process it a bit faster
        }

        if (args.currentConciergeId === args.data.lead.conciergeId) {
            args.sub.subscribe(`lead-${args.data.lead.id}`);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadUpdateStatus', args => {
        // TODO: Use the same data format & logic in the event for channel leads.
        args.socket.json.send('leadUpdateStatus', args.data);
    });

    emitter.on('App\\Events\\Lead\\LeadRefer', args => {
        args.socket.json.send('leadRefer', args.data);

        leadsCollection.collection[args.data.id].status = 'Referred';
    });

    emitter.on('App\\Events\\Lead\\LeadReferFailed', args => {
        args.socket.json.send('leadReferFailed', args.data);
    });

    emitter.on('App\\Events\\Lead\\LeadCall', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('leadCall', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCallEnd', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('leadCallEnd', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCallStatusUpdate', args => {
        if (args.socketId !== args.sender) {
            args.socket.json.send('leadCallStatusUpdate', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadCallNotes', args => {
        const message = args.socketId !== args.sender ? 'leadCallNotes' : 'leadCallNotesReport';

        args.socket.json.send(message, args.data);
    });

    emitter.on('App\\Events\\Note\\NoteAll', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('noteAll', args.data);
        }
    });

    emitter.on('App\\Events\\Note\\NoteCreate', args => {
        const message = args.socketId !== args.sender ? 'noteCreate' : 'noteCreateReport';

        args.socket.json.send(message, args.data);
    });

    emitter.on('App\\Events\\Lead\\LeadPhoneIsValid', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('LeadPhoneIsValid', args.data);
        }
    });

    emitter.on('App\\Events\\Lead\\LeadPhoneIsInvalid', args => {
        if (args.socketId === args.sender) {
            args.socket.json.send('LeadPhoneIsInvalid', args.data);
        }
    });
};
