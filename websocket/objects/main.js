/**
 * @class Main
 */
class Main {
    /**
     * Add data recursively.
     *
     * @param {{}} data - Concierge data object
     *
     * @returns {void}
     */
    addDataRecursively(data) {
        for (const key in data) {
            if (!this.hasOwnProperty(key)) {
                continue;
            }

            this[key] = data[key];
        }
    }
}

module.exports = Main;
