const Main = require('./main');

/**
 * @class Agent
 *
 * @property {!number} id
 * @property {!string} firstName
 * @property {!string} lastName
 * @property {!string} phone
 * @property {!string} email
 * @property {!string} website
 * @property {!string} calendly
 * @property {!string} city
 * @property {!string} state
 * @property {!string} company
 * @property {!string} sendGridEmail
 * @property {!string} twilioNumber
 * @property {!string} personaTeamName
 * @property {!string} personaName
 * @property {!string} personaTitle
 * @property {!string} gmailConnected
 * @property {!string} agentologyUUID
 * @property {!string} areaCode
 * @property {!string} notificationPhone
 * @property {!string} notificationEmail
 * @property {!string} callForwardingNumber
 * @property {!string} tokenEmailScopeProvided
 * @property {!string} callForwardingNumber
 * @property {!string} emailForwarding
 * @property {!string} extraNoteForRep
 * @property {object} agentOnBoarding
 * @property {boolean} vikingGmailParse
 * @property {Boolean[]|object} leads -- key is leadId, value - true. Just cache for relation lead-agent.
 *
 */
class Agent extends Main {
    /**
     * @constructor
     */
    constructor() {
        super();

        this.id = null;
        this.firstName = null;
        this.lastName = null;
        this.phone = null;
        this.email = null;
        this.website = null;
        this.calendly = null;
        this.city = null;
        this.state = null;
        this.company = null;
        this.sendGridEmail = null;
        this.twilioNumber = null;
        this.personaTeamName = null;
        this.personaName = null;
        this.personaTitle = null;
        this.gmailConnected = null;
        this.agentologyUUID = null;
        this.areaCode = null;
        this.notificationPhone = null;
        this.notificationEmail = null;
        this.callForwardingNumber = null;
        this.tokenEmailScopeProvided = null;
        this.emailForwarding = null;
        this.extraNoteForRep = null;
        this.agentOnBoarding = null;
        this.vikingGmailParse = true;
        this.leads = {};
    }

    /**
     * Update lead-agent relation cache (add).
     *
     * @param {Lead} lead - Lead data
     *
     * @returns {void}
     */
    addCacheLeadRelation(lead) {
        this.leads[lead.id] = {
            status: true,
            leadSource: lead.channelwebsite,
        };
    }

    /**
     * Update lead-agent relation cache (remove).
     *
     * @param {string} leadId - Lead id
     *
     * @returns {void}
     */
    removeCacheLeadRelation(leadId) {
        delete this.leads[leadId];
    }
}

module.exports = Agent;
