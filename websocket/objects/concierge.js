const Main = require('./main');

/**
 * @class Concierge
 *
 * @property {null|number|String} id
 * @property {null|String} name
 * @property {null|String} email
 * @property {null|number|String} role
 * @property {null|String} conciergeRole
 * @property {NoteCollection} notes
 * @property {Boolean} stateEdit
 *
 * @property {Boolean[]|object} leads -- key is leadId, value - true. Just cache for relation lead-concierge.
 * @property {number[]|object} devices
 *
 * @example: 'this.devices[SocketID] = new Date().getTime();'
 * 'device' key is SocketID.
 * 'device' value is online timestamp.
 * @property {Boolean} isAway
 * @property {Boolean} isDeleted
 * @property {Boolean} isPaused
 */
class Concierge extends Main {
    /**
     * @constructor
     */
    constructor() {
        super();

        this.id = null;
        this.name = '';
        this.email = '';
        this.role = '';
        this.conciergeRole = '';
        this.company = '';
        this.team = '';

        this.leads = {};
        this.devices = {};
        this.isAway = false;
        this.isDeleted = false;
        this.isPaused = false;
    }

    /**
     * Return permission string by code.
     *
     * @returns {string} - Concierge permission or 'n/a'
     */
    getPermission() {
        switch (this.role.toString()) {
            case '0':
                return 'Suspended';
            case '1':
                return 'Tier 1';
            case '2':
                return 'Tier 2';
            case '3':
                return 'Admin';
            default:
                return 'n/a';
        }
    }

    /**
     * Update device activity.
     *
     * @param {string} time - New device activity timestamp
     *
     * @returns {void}
     */
    updateDeviceActivity(time) {
        for (const index in this.devices) {
            this.devices[index] = time;
        }
    }

    /**
     * Update lead-concierge relation cache (add).
     *
     * @param {string} leadId - Lead id
     *
     * @returns {void}
     */
    addCacheLeadRelation(leadId) {
        this.leads[leadId] = true;
    }

    /**
     * Update lead-concierge relation cache (remove).
     *
     * @param {string} leadId - Lead id
     *
     * @returns {void}
     */
    removeCacheLeadRelation(leadId) {
        delete this.leads[leadId];
    }
}

module.exports = Concierge;
