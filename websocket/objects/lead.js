const artisan = require('../artisan');
const Main = require('./main');

/**
 * @class Lead
 *
 * @property {!number} id
 * @property {!string} firstName
 * @property {!string} lastName
 * @property {!string} phoneCarrierType - Phone carrier type
 * @property {?number} conciergeId
 * @property {?string} status
 * @property {?string} tag
 * @property {?string} icon
 * @property {?number} agentId
 * @property {?string} createdAt - Date time string when lead was created
 * @property {?string} updatedAt - String from server
 * @property {?number} updatedAtTimestamp - Timestamp
 * @property {?string} channelwebsite - Lead source
 * @property {?string} name - First & last name
 * @property {?number} finalTimeForResponse - Timestamp of final time for response
 * @property {?*} timer - Time interval object
 * @property {!string} lastMessage - Last message/email text
 * @property {!string} lastMessage - Last message/email text
 * @property {boolean} isPaused - Paused SMS automation or not
 * @property {integer} smsFlowStep - Next SMS Flow Step
 */
class Lead extends Main {
    /**
     * @constructor
     */
    constructor() {
        super();

        this.id = null;
        this.firstName = null;
        this.lastName = null;
        this.phoneCarrierType = null;
        this.conciergeId = null;
        this.status = null;
        this.tag = null;
        this.icon = null;
        this.agentId = null;
        this.createdAt = null;
        this.updatedAt = null;
        this.updatedAtTimestamp = null;
        this.channelwebsite = null;
        this.finalTimeForResponse = null;
        this.timer = null;
        this.lastMessage = null;
        this.isPaused = false;
        this.smsFlowStep = null;
    }

    /**
     * Update cache.
     *
     * @returns {void}
     */
    updateCache() {
        const time = this.updatedAt.split(/[- :]/);
        const date = new Date(Date.UTC(time[0], time[1] - 1, time[2], time[3], time[4], time[5]));

        const millisecondsAtSecond = 1000;

        this.updatedAtTimestamp = date.getTime() / millisecondsAtSecond;

        this.name = '';
        this.name += this.firstName === null ? '' : this.firstName;
        this.name += this.lastName === null ? '' : this.lastName;
    }

    /**
     * Start timer
     *
     * @returns {void}
     */
    startTimer() {
        if (this.timer) {
            return;
        }
        this.timer = null;

        /* NOTE: Timer feature was disabled by Madhur request
        const timeout = this.finalTimeForResponse - new Date().getTime();

        this.timer = setTimeout(() => {
            this.reassign();
            clearTimeout(this.timer);
            this.finalTimeForResponse = null;
        }, timeout);
        */
    }

    /**
     * Restart timer
     *
     * @returns {void}
     */
    restartTimer() {
        clearTimeout(this.timer);
        this.timer = null;
        this.startTimer();
    }

    /**
     * Stop timer
     *
     * @returns {void}
     */
    stopTimer() {
        this.finalTimeForResponse = null;
        clearTimeout(this.timer);
    }

    /**
     * Reassign lead.
     *
     * @returns {void}
     */
    reassign() {
        artisan.run('lead:reassignSingle', {
            lid: this.id,
        });
    }
}

module.exports = Lead;
