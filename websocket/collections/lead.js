/* global conciergesCollection */
const inherits = require('util').inherits;
const Collection = require('./collection');
const Lead = require('./../objects/lead');

/**
 * @constructor
 *
 */
function LeadCollection() {
    Collection.call(this);
}

inherits(LeadCollection, Collection);

/**
 * Add new lead object to collection by using object as lead data.
 *
 * @param {object} data - Lead source data.
 * @param {Object} agent - Lead source data.
 *
 * @returns {void}
 */
LeadCollection.prototype.addItemData = function (data, agent) {
    if (data === null) {
        return;
    }

    const lead = new Lead();

    lead.id = data.id;
    lead.firstName = data.firstName;
    lead.lastName = data.lastName;
    lead.conciergeId = data.conciergeId;
    lead.status = data.status;
    lead.tag = data.tag;
    lead.icon = data.icon;
    lead.agentId = data.agentId;
    lead.agent = agent;
    lead.updatedAt = data.updatedAt;
    lead.createdAt = data.createdAt;
    lead.channelwebsite = data.channelwebsite;

    lead.updateCache();

    this.addItem(lead);
};

/**
 * Set final time for response for active leads
 *
 * @param {array} leads - Leads data
 *
 * @returns {array} - Leads data
 */
// TODO: write safe code in this function
LeadCollection.prototype.setTimeForActiveLeads = function (leads) {
    let finalTimeForResponse;

    leads.forEach(lead => {
        const activeLead = lead;

        finalTimeForResponse = this.updateFinalTimeForResponse(activeLead.id, activeLead.tag);
        if (finalTimeForResponse) {
            activeLead.finalTimeForResponse = finalTimeForResponse;
            this.collection[activeLead.id].startTimer();
        }
    });

    return leads;
};

/**
 * Update final time for response
 *
 * @param {string} leadId - Lead id
 * @param {string} tag - Lead tag
 *
 * @returns {number} - Timestamp of final time for response
 */
LeadCollection.prototype.updateFinalTimeForResponse = function (leadId, tag) {
    let finalTimeForResponse = this.collection[leadId].finalTimeForResponse;

    if (finalTimeForResponse) {
        return finalTimeForResponse;
    }

    finalTimeForResponse = this.setFinalTimeForResponse(leadId, tag);

    return finalTimeForResponse;
};

/**
 * Set final time for response
 *
 * @param {string} leadId - Lead id
 * @param {string} tag - Lead tag
 *
 * @returns {number|null} - Timestamp of final time for response or null
 */
LeadCollection.prototype.setFinalTimeForResponse = function (leadId, tag) {
    /* NOTE: Timer feature was disabled by Madhur request
    let finalTimeForResponse;

    const currentTime = new Date().getTime();
    const lead = this.collection[leadId];

    if (typeof lead !== 'undefined') {
        if (tag === 'Urgent') {
            // Curren time + 5 min
            finalTimeForResponse = currentTime + 300000;
            lead.finalTimeForResponse = finalTimeForResponse;

            return finalTimeForResponse;
        } else if (tag === 'Awaiting Response') {
            // Current time + 10 min
            finalTimeForResponse = currentTime + 600000;
            lead.finalTimeForResponse = finalTimeForResponse;

            return finalTimeForResponse;
        }

        lead.finalTimeForResponse = null;
    }
    */

    return null;
};

/**
 * Check if we should continue order in LeadCollection.getBlock
 * method or not.
 *
 * @param {string} status - Lead status
 * @param {boolean} isArchived - Archive state
 *
 * @returns {boolean} - Continue status
 */
function orderByStatus(status, isArchived) {
    let res = true;
    let filterByStatus = [
        'New lead',
        'Escalated',
        'Call Requested',
    ];

    if (isArchived) {
        filterByStatus = [
            'Qualified',
            'Unqualified',
            'Referred',
            'Not a lead',
            'No Response Needed',
        ];
    }

    for (const statusKey in filterByStatus) {
        if (filterByStatus[statusKey] === status) {
            res = false;
            break;
        }
    }

    return res;
}

/**
 * Inside helper function to sort leads in correct order by column.
 * Uses in LeadCollection.getBlock method.
 *
 * @param {Lead} fst - First Lead object
 * @param {Lead} snd - Second Lead object
 * @param {string} column - Column name
 * @param {string} direction - Order direction
 *
 * @returns {number} - Direction number
 */
function sort(fst, snd, column, direction) {
    const orderUp = 1;
    const orderDown = -1;
    const agentA = fst.agent;
    const agentB = snd.agent;
    const isAgentA = agentA !== null;
    const isAgentB = agentB !== null;

    const agentFullNameA = isAgentA ? `${agentA.firstName.toLowerCase() } ${agentA.lastName.toLowerCase()}` : '';
    const agentFullNameB = isAgentB ? `${agentB.firstName.toLowerCase() } ${agentB.lastName.toLowerCase()}` : '';

    const createdAtTimestampA = new Date(fst.createdAt).getTime();
    const createdAtTimestampB = new Date(snd.createdAt).getTime();

    const currentRepA = fst.currentRep ? fst.currentRep.toLowerCase() : fst.currentRepEmail.toLowerCase() ;
    const currentRepB = snd.currentRep ? snd.currentRep.toLowerCase() : snd.currentRepEmail.toLowerCase() ;

    const tagsOrder = ['Urgent', 'Awaiting Response', 'Action Required', 'Ongoing Chat', 'Automation', 'Archive'];

    const tagOrderA = tagsOrder.indexOf(fst.tag);
    const tagOrderB = tagsOrder.indexOf(snd.tag);

    switch (column) {
        case 'currentRep':
            if (currentRepA < currentRepB) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (currentRepA > currentRepB) {
                return direction === 'up' ? orderUp : orderDown;
            }
            break;
        case 'agent':
            if (agentFullNameA < agentFullNameB) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (agentFullNameA > agentFullNameB) {
                return direction === 'up' ? orderUp : orderDown;
            }

            break;
        case 'name':
            if (fst.name < snd.name) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (fst.name > snd.name) {
                return direction === 'up' ? orderUp : orderDown;
            }

            break;
        case 'activity':
            if (fst.updatedAtTimestamp < snd.updatedAtTimestamp) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (fst.updatedAtTimestamp > snd.updatedAtTimestamp) {
                return direction === 'up' ? orderUp : orderDown;
            }

            break;
        case 'type':
            if (fst.leadType < snd.leadType) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (fst.leadType > snd.leadType) {
                return direction === 'up' ? orderUp : orderDown;
            }

            break;
        case 'source':
        // TODO: implement it when it will be needs
            break;
        case 'status':
            if (fst.status < snd.status) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (fst.status > snd.status) {
                return direction === 'up' ? orderUp : orderDown;
            }

            break;
        case 'createdAt':
            if (createdAtTimestampA < createdAtTimestampB) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (createdAtTimestampA > createdAtTimestampB) {
                return direction === 'up' ? orderUp : orderDown;
            }

            break;
        case 'tag':
            if (tagOrderA < tagOrderB) {
                return direction === 'up' ? orderDown : orderUp;
            }

            if (tagOrderA > tagOrderB) {
                return direction === 'up' ? orderUp : orderDown;
            }
            break;
        default:
            break;
    }

    return 0;
}

/**
 * Get sorted leads in correct order divided per page.
 *
 * @param {number} perPage - How many lead we should use per page
 * @param {number} page - First page is "1"
 * @param {string} search - Keyword of search
 * @param {null|string} filterByStatus - Filter by status
 * @param {null|number} filterByConcierge - Filter by concierge
 * @param {null|boolean} isArchived - (what 'tab' at admin list)
 * @param {null|string} orderColumn  - (null|'name'|'activity'|'type'|'source'|'status')
 * @param {null|string} orderDirection - (null|'up'|'down'),
 * @param {null|number} filterByAgent - Filter by agent
 * @param {null|string} filterByTag - Filter by tag
 * @param {null|number} filterByRepTier - Filter by rep tier
 * @param {null|string} filterCreatedAtFrom - Filter by created at minimum
 * @param {null|string} filterCreatedAtTo - Filter by created at maximum
 *
 * @return {Object} ordered-filtered-leads, total pages number
 */
LeadCollection.prototype.getOrderedLeadsBlock = function (
    perPage,
    page,
    search,
    filterByStatus,
    filterByConcierge,
    isArchived,
    orderColumn,
    orderDirection,
    filterByAgent,
    filterByTag,
    filterByRepTier,
    filterCreatedAtFrom,
    filterCreatedAtTo
) {
    const orderedLeads = [];
    let totalOrderedLeads = 0;
    const filterCreateAtFromObj = filterCreatedAtFrom === null ? null : new Date(filterCreatedAtFrom);
    const filterCreateAtToObj = filterCreatedAtTo === null ? null : new Date(filterCreatedAtTo);

    for (const id in this.collection) {
        const lead = this.collection[id];
        const leadCreatedAtObj = new Date(lead.createdAt);

        if (search !== '') {
            const firstName = lead.firstName === null ? '' : lead.firstName.toLowerCase().trim();
            const lastName = lead.lastName === null ? '' : lead.lastName.toLowerCase().trim();
            const fullName = `${firstName} ${lastName}`;

            let related = false;
            if (fullName.includes(search.toLowerCase())) {
                related = true;
            }

            if (!related) {
                continue;
            }
        }
        const concierge = conciergesCollection.getItem(lead.conciergeId);
        const conciergeRole = concierge !== null ? concierge.role : null;

        const statusFilter = filterByStatus !== null && filterByStatus !== lead.status;
        const conciergeFilter = filterByConcierge !== null && filterByConcierge !== lead.conciergeId;
        const orderArchivedByStatus = isArchived !== null && orderByStatus(lead.status, isArchived);
        const agentFilter = filterByAgent !== null && filterByAgent !== lead.agentId;
        const tagFilter = filterByTag !== null && filterByTag !== lead.tag;
        const repTierFilter = filterByRepTier !== null && filterByRepTier !== conciergeRole;
        let createdAtFromFilter = false;
        let createdAtToFilter = false;

        if (filterCreateAtFromObj) {
            createdAtFromFilter = leadCreatedAtObj.getTime() < filterCreateAtFromObj.getTime() ;
        }

        if (filterCreateAtToObj) {
            createdAtToFilter = leadCreatedAtObj.getTime() > filterCreateAtToObj.getTime();
        }


        if (statusFilter || conciergeFilter || orderArchivedByStatus
            || agentFilter || tagFilter || repTierFilter || createdAtFromFilter || createdAtToFilter) {
            continue;
        }
        const tempLead = Object.assign({}, lead);
        delete tempLead.timer;

        const repId = tempLead.conciergeId;
        tempLead.currentRep = repId !== null ? conciergesCollection.collection[repId].name : '';
        tempLead.currentRepEmail = repId !== null ? conciergesCollection.collection[repId].email : '';

        orderedLeads.push(tempLead);
        totalOrderedLeads++;
    }

    if (orderColumn !== null) {
        orderedLeads.sort((fst, snd) => sort(fst, snd, orderColumn, orderDirection));
    }

    const to = page * perPage;
    let from = to - perPage;
    const ret = { leads: [] };

    while (from < to) {
        if (orderedLeads[from]) {
            ret.leads.unshift(orderedLeads[from]);
        }

        from++;
    }

    ret.totalPages = Math.ceil(totalOrderedLeads / perPage);

    return ret;
};


module.exports = LeadCollection;
