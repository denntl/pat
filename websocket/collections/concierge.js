/* global agentsCollection */
/* global leadsCollection */

const inherits = require('util').inherits;
const redis = require('redis');
const artisan = require('../artisan').run;
const log = require('../logger');
const Collection = require('./collection');
const Concierge = require('./../objects/concierge');

/**
 * TODO: Move out redis client creation process. Just set redisClient as argument where it's needed.
 *
 * @class ConciergeCollection
 *
 * @property {Concierge[]} concierges
 * @property {String} search
 * @property {Array} computedList
 * @property {Boolean} createMode
 * @property {Concierge} createConcierge
 * @property {Number|null} waitForSyncId
 *
 * @property {Number[]|Boolean|Object} onlineConcierges - List of active concierges
 */
/**
 * @constructor
 *
 * @param {Object} configRedis - Redis config object
 */
function ConciergeCollection(configRedis) {
    Collection.call(this);

    this.configRedis = configRedis;

    const redisClient = redis.createClient(this.configRedis);

    redisClient.on('error', err => {
        log.info(`Redis error ${err}`);
    });

    this.onlineConcierges = JSON.parse(redisClient.get('concierges:online'));

    if (!this.onlineConcierges) {
        this.onlineConcierges = {};
    }

    redisClient.quit();

    // 1 minute
    const timeoutDelay = 1000 * 60;

    setTimeout(() => {
        let changed = false;

        for (const id in this.onlineConcierges) {
            const conciergeActivity = this.onlineConcierges[id];
            const activityIsUndefined = typeof conciergeActivity === 'undefined';
            const activityIsBool = typeof conciergeActivity === 'boolean';
            const activityIsRelevant = new Date().getTime() - conciergeActivity < timeoutDelay;

            if (activityIsUndefined || activityIsBool || activityIsRelevant) {
                return;
            }

            delete this.onlineConcierges[id];

            changed = true;
        }

        if (changed) {
            this.saveToRedisActivity();
        }
    }, timeoutDelay);
}

inherits(ConciergeCollection, Collection);

/**
 * Return item by id or create it.
 *
 * @param {Number} id - Concierge id
 *
 * @returns {Concierge} - Concierge object
 */
ConciergeCollection.prototype.getItemOrCreate = function (id) {
    if (typeof this.collection[id] == 'undefined') {
        this.collection[id] = new Concierge();
        this.collection[id].id = id;
    }

    return this.collection[id];
};

/**
 * Switch lead distribution
 *
 * @param {Number} id - Concierge id
 * @param {Boolean} isPaused - Is paused lead distribution
 *
 * @returns {void}
 */
ConciergeCollection.prototype.switchDistribution = function (id, isPaused) {
    if (typeof this.collection[id] !== 'undefined') {
        this.collection[id].isPaused = isPaused;

        const redisClient = redis.createClient(this.configRedis);

        redisClient.on('error', err => {
            log.info(`Redis error ${err}`);
        });

        if (!this.onlineConcierges) {
            this.onlineConcierges = {};
        }

        this.onlineConcierges[id] = !isPaused;

        const data = JSON.stringify(this.onlineConcierges);

        redisClient.set('concierges:online', data);
        redisClient.quit();
    }
};

/**
 * Add lead-concierge relation cache safely.
 *
 * @param {Lead} lead - Lead id
 *
 * @returns {void}
 */
ConciergeCollection.prototype.addCacheLeadRelationSafe = function (lead) {
    if (lead !== null && lead.conciergeId !== null) {
        const concierge = this.getItem(lead.conciergeId);

        if (concierge !== null) {
            concierge.addCacheLeadRelation(lead.id);
        }
    }
};

/**
 * Remove lead-concierge relation cache safely.
 *
 * @param {Lead} lead - Lead id
 *
 * @returns {void}
 */
ConciergeCollection.prototype.removeCacheLeadRelationSafe = function (lead) {
    if (lead !== null && lead.conciergeId !== null) {
        const concierge = this.getItem(lead.conciergeId);

        if (concierge !== null) {
            concierge.removeCacheLeadRelation(lead.id);
        }
    }
};

/**
 * Lead flow update redis cache.
 *
 * @param {Object} leadData - New lead data
 *
 * @returns {void}
 */
ConciergeCollection.prototype.updateRedisCacheOnLeadEvents = function (leadData) {
    const agent = agentsCollection.getItem(leadData.agentId);

    leadsCollection.addItemData(leadData, agent);

    const lead = leadsCollection.getItem(leadData.id);

    this.addCacheLeadRelationSafe(lead);

    agentsCollection.addCacheLeadRelationSafe(lead);
};

/**
 * Return item by id or create it.
 *
 * @param {number} conciergeId - Concierge id
 * @param {string} socketId - Socket id
 *
 * @returns {boolean} - Should send init request status
 */
ConciergeCollection.prototype.connectDevice = function (conciergeId, socketId) {
    const concierge = this.getItemOrCreate(conciergeId);

    if (concierge.role === 3) {
        return false;
    }

    const time = new Date().getTime();

    let shouldSendInitRequest = true;

    concierge.devices[socketId] = time;
    concierge.isAway = false;
    concierge.updateDeviceActivity(time);

    const redisClient = redis.createClient(this.configRedis);

    redisClient.on('error', err => {
        log.info(`Redis error ${err}`);
    });

    redisClient.publish(`concierge-${parseInt(conciergeId, 10)}`, JSON.stringify({
        event: 'App\\Events\\Concierge\\conciergeSetLastAfk',
        data: {
            lastAfkActivity: time,
            sender: socketId,
        },
    }));

    redisClient.publish(`concierge-${parseInt(conciergeId, 10)}`, JSON.stringify({
        event: 'App\\Events\\Concierge\\ConciergeSetAway',
        data: {
            away: false,
            sender: socketId,
        },
    }));

    if (Object.keys(this.onlineConcierges).length !== 0) {
        shouldSendInitRequest = !this.isAnotherConciergeOnline(conciergeId);
    }

    this.onlineConcierges[conciergeId] = !concierge.isPaused;

    redisClient.quit();

    this.saveToRedisActivity();

    return shouldSendInitRequest;
};

/**
 * Check if another Rep (Concierge) online
 *
 * @param {string} conciergeId - Concierge id
 *
 * @returns {boolean} - True if another Rep online
 */
ConciergeCollection.prototype.isAnotherConciergeOnline = function (conciergeId) {
    for (const index in this.onlineConcierges) {
        if (conciergeId === index) {
            continue;
        }

        if (this.onlineConcierges[index]) {
            return true;
        }
    }

    return false;
};

/**
 * Return item by id or create it.
 *
 * @param {string} socketId - Socket id
 *
 * @returns {void}
 */
ConciergeCollection.prototype.disconnectDevice = function (socketId) {
    const self = this;

    for (const index in this.collection) {
        const concierge = this.collection[index];

        for (const device in concierge.devices) {
            if (device !== socketId) {
                continue;
            }

            delete concierge.devices[device];

            if (Object.keys(concierge.devices).length === 0) {
                concierge.isAway = true;

                this.onlineConcierges[concierge.id] = new Date().getTime();

                // 30 seconds
                const timeoutDelay = 1000 * 30;

                setTimeout(() => {
                    const conciergeActivity = self.onlineConcierges[concierge.id];

                    const activityIsUndefined = typeof conciergeActivity === 'undefined';
                    const activityIsBool = typeof conciergeActivity === 'boolean';
                    const activityIsRelevant = new Date().getTime() - conciergeActivity < timeoutDelay;

                    if (activityIsUndefined || activityIsBool || activityIsRelevant) {
                        return;
                    }

                    delete self.onlineConcierges[concierge.id];

                    // Artisan command lead:reassign calls here
                    self.reassignLeads(concierge.id);
                    self.saveToRedisActivity();
                }, timeoutDelay);

                this.saveToRedisActivity();
            }

            return;
        }
    }
};

/**
 * Reassign concierge leads.
 *
 * @param {number|string} conciergeId - Concierge id
 *
 * @returns {void}
 */
ConciergeCollection.prototype.reassignLeads = function (conciergeId) {
    artisan('lead:reassign', {
        cid: parseInt(conciergeId, 10),
        system: true,
    });
};

/**
 * Set away status to concierge.
 *
 * @param {number|string} conciergeId - Concierge id
 * @param {boolean} isAway - Concierge away state
 *
 * @returns {void}
 */
ConciergeCollection.prototype.setAway = function (conciergeId, isAway) {
    const concierge = this.getItemOrCreate(conciergeId);

    concierge.isAway = isAway;

    this.onlineConcierges[conciergeId] = !isAway;
    this.saveToRedisActivity();
};

/**
 * Save concierges activity to Redis.
 *
 * @returns {void}
 */
ConciergeCollection.prototype.saveToRedisActivity = function () {
    const redisClient = redis.createClient(this.configRedis);
    const data = JSON.stringify(this.onlineConcierges);

    redisClient.on('error', err => {
        log.info(`Redis error ${err}`);
    });

    redisClient.set('concierges:online', data);
    redisClient.quit();
};

module.exports = ConciergeCollection;
