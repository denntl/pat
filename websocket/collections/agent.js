/* global Agent */
const inherits = require('util').inherits;
const Collection = require('./collection');

/**
 * @class AgentCollection
 *
 * @constructor
 */
function AgentCollection() {
    Collection.call(this);
}

inherits(AgentCollection, Collection);

/**
 * Return item by id or create it.
 *
 * @param {Number} id - Agent id
 *
 * @returns {Agent} - Agent object
 */
AgentCollection.prototype.getItemOrCreate = function (id) {
    if (typeof this.collection[id] == 'undefined') {
        this.collection[id] = new Agent();
        this.collection[id].id = id;
    }

    return this.collection[id];
};

/**
 * Add lead-agent relation cache safely.
 *
 * @param {Lead} lead - Lead id
 *
 * @returns {void}
 */
AgentCollection.prototype.addCacheLeadRelationSafe = function (lead) {
    if (lead !== null && lead.agentId !== null) {
        const agent = this.getItem(lead.agentId);

        if (agent !== null) {
            agent.addCacheLeadRelation(lead);
        }
    }
};

/**
 * Remove lead-agent relation cache safely.
 *
 * @param {Lead} lead - Lead id
 *
 * @returns {void}
 */
AgentCollection.prototype.removeCacheLeadRelationSafe = function (lead) {
    if (lead !== null && lead.agentId !== null) {
        const agent = this.getItem(lead.agentId);

        if (agent !== null) {
            agent.removeCacheLeadRelation(lead.id);
        }
    }
};

module.exports = AgentCollection;
