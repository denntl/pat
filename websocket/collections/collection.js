/**
 * @class Collection
 *
 * @property {Array} collection
 */
/**
 * @constructor
 */
function Collection() {
    this.collection = [];
}

Collection.prototype = {};

/**
 * Update item's list.
 *
 * @returns {void}
 */
Collection.prototype.cleanCollection = function () {
    this.collection = [];
};

/**
 * Add item.
 *
 * @param {Object} item - Item object
 *
 * @returns {void}
 */
Collection.prototype.addItem = function (item) {
    this.collection[item.id] = item;
};

/**
 * Delete item.
 *
 * @param {Number} id - Item id
 *
 * @returns {void}
 */
Collection.prototype.deleteItem = function (id) {
    delete this.collection[id];
};

/**
 * Return item by id.
 *
 * @param {Number} id - Item id
 *
 * @returns {item|null} - Item object
 */
Collection.prototype.getItem = function (id) {
    return typeof this.collection[id] == 'undefined' ? null : this.collection[id];
};

module.exports = Collection;

