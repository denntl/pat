/* global __stack */

const Agent = require('./objects/agent');
const Concierge = require('./objects/concierge');

const LeadCollection = require('./collections/lead');
const AgentCollection = require('./collections/agent');
const ConciergeCollection = require('./collections/concierge');

const leads = new LeadCollection();
const agents = new AgentCollection();
// TODO: Move redis outside concierge object & update constructor
const concierges = new ConciergeCollection(require('./app').config.redis);

Object.defineProperty(global, 'Agent', {
    /**
     * Getter for Agent proto.
     *
     * @returns {Agent} - Agent proto
     */
    get() {
        return Agent;
    },
});

Object.defineProperty(global, 'Concierge', {
    /**
     * Getter for Concierge proto.
     *
     * @returns {Concierge} - Concierge proto
     */
    get() {
        return Concierge;
    },
});

Object.defineProperty(global, 'leadsCollection', {
    /**
     * Getter for LeadsCollection object.
     *
     * @return {LeadCollection} - LeadCollection
     */
    get() {
        return leads;
    },
});

Object.defineProperty(global, 'agentsCollection', {
    /**
     * Getter for AgentCollection object.
     *
     * @returns {AgentCollection} - AgentCollection
     */
    get() {
        return agents;
    },
});

Object.defineProperty(global, 'conciergesCollection', {
    /**
     * Getter for ConciergeCollection object.
     *
     * @returns {ConciergeCollection} - ConciergeCollection
     */
    get() {
        return concierges;
    },
});

Object.defineProperty(global, '__stack', {
    /**
     * Get trace stack.
     *
     * @returns {array} - Trace stack
     */
    get() {
        const err = new Error();
        const original = Error.prepareStackTrace;

        Error.prepareStackTrace = function (_, stack) {
            return stack;
        };

        // TODO: Need to find the way to remove arguments.callee. Remove ignore after.
        /* eslint-disable no-caller */

        Error.captureStackTrace(err, arguments.callee);

        /* eslint-enable no-caller */

        const stack = err.stack;

        Error.prepareStackTrace = original;

        return stack;
    },
});

Object.defineProperty(global, '__line', {
    /**
     * Get line number.
     *
     * @returns {number} - Line number
     */
    get() {
        return __stack[1].getLineNumber();
    },
});
