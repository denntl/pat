const EventEmitter = require('events');

/**
 * @class RedisEmitter
 */
class RedisEmitter extends EventEmitter {
}

const emitter = new RedisEmitter();

module.exports = emitter;
