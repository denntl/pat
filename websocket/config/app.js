const dotEnv = require('dotenv').config().parsed;

const redisPort = 6379;

/**
 * Config object.
 *
 * @typedef config {{
 *      name: string,
 *      env: string,
 *      port: number,
 *      rollbar: {
 *          token: string
 *      },
 *      redis: {
 *          host: string,
 *          port: number
 *      },
 *      ssl: {
 *          key: string,
 *          cert: string,
 *          ca: string
 *      },
 *      isEnv: ((env:string)=>boolean)
 * }}
 */
const config = {
    name: dotEnv.APP_NAME || 'Project PAT',
    env: dotEnv.APP_ENV || 'local',
    port: 8890,
    rollbar: {
        token: dotEnv.ROLL_BAR_TOKEN || null,
    },
    redis: {
        host: dotEnv.REDIS_HOST || '127.0.0.1',
        port: dotEnv.REDIS_PORT || redisPort,
        password: dotEnv.REDIS_PASSWORD || null,
    },
    ssl: {
        key: dotEnv.SSL_KEY || null,
        cert: dotEnv.SSL_CERT || null,
        ca: dotEnv.SSL_CA || null,
    },
    /**
     * Check if socket server works on specific env.
     *
     * @param {string} env - Server environment
     *
     * @returns {boolean} - Check status
     */
    isEnv(env) {
        const currentEnv = this.env.replace(/local-.*/i, 'local');

        return currentEnv === env;
    },
};

if (config.redis.password === null || config.redis.password === 'null') {
    delete config.redis.password;
}

module.exports = config;
