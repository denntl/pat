const exec = require('child_process').exec;
const _ = require('lodash');
const log = require('./logger');

/**
 * @class Artisan
 */
class Artisan {
    /**
     * Wrapper for `exec` to run php artisan command for web-socket usage.
     *
     * This wrapper accept to arguments `command` & `params` for it.
     * It parse all params to `JSON` then wrap it into `base64` string.
     * Then execute command. Here is how output is looks like:
     *
     *     `php artisan concierge:get -S=eyJjb25jaWVyZ2VJZCI6IjEiLCJ0b2tlbiI6ImZhOGFhMzgwMzZjMTAzMzBlMjM3Yzgx`
     *
     * @example artisan('concierge:get', {
     *     cid: 1,
     *     token: 'eyJjb25jaWVyZ2VJZCI6IjEiLCJ0b2tlbiI',
     *     isAway: true
     * })
     *
     * @param {string} command - PHP Artisan command
     * @param {{}} params - Parameters object
     *
     * @returns {boolean|string} - Command signature
     */
    static run(command, params) {
        if (!_.isString(command) || !_.isObject(params)) {
            return false;
        }

        const json = JSON.stringify(params);
        const data = Buffer.from(json).toString('base64');
        const executable = `php artisan ${command} -S=${data}`;

        exec(executable);

        log.debug(`...Executed: ${executable}`);

        return executable;
    }

    /**
     * Runs initial commands for `system` channel.
     *
     * @returns {void}
     */
    static initial() {
        log.info('Execute initial commands');

        // TODO: Replace this three commands with one `initial` command
        Artisan.run('concierge:all', {
            cid: null,
            sid: null,
            token: null,
        });
        Artisan.run('agent:all', {
            aid: null,
            sid: null,
            token: null,
        });
        Artisan.run('lead:all', {
            cid: null,
            lid: null,
            sid: null,
            token: null,
            admin: true,
        });
    }
}

module.exports = Artisan;
