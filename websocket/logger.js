const bunyan = require('bunyan');
const config = require('./config/app');

const streams = [];

if (config.isEnv('local')) {
    streams.push({
        level: 'trace',
        stream: process.stdout,
    });
} else {
    streams.push({
        type: 'rotating-file',
        level: 'debug',
        path: `${__dirname}/../storage/logs/web-socket.log`,
        period: '1d',
        count: 3,
    });
}

module.exports = bunyan.createLogger({
    name: config.name,
    streams,
});
