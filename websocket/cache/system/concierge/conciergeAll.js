/* global conciergesCollection */
/* global Concierge */

/**
 * Listener for `ConciergeAll` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {ConciergeCollection} conciergesCollection */
    conciergesCollection.cleanCollection();

    for (const index in data.concierges) {
        const conciergeData = data.concierges[index];
        const concierge = new Concierge();

        concierge.addDataRecursively(conciergeData);
        conciergesCollection.addItem(concierge);
    }
};
