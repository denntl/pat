/* global conciergesCollection */

/**
 * Listener for `ConciergeDelete` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {ConciergeCollection} conciergesCollection */
    const lead = conciergesCollection.collection[data.id];

    if (lead) {
        lead.isDeleted = true;
    }
};
