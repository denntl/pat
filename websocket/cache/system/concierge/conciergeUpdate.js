/* global conciergesCollection */

/**
 * Listener for `ConciergeUpdate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {ConciergeCollection} conciergesCollection */
    const concierge = conciergesCollection.getItemOrCreate(data.id);

    concierge.addDataRecursively(data);
};
