/* global Concierge */
/* global conciergesCollection */

/**
 * Listener for `ConciergeCreate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    const concierge = new Concierge();

    concierge.addDataRecursively(data);

    /** @typedef {ConciergeCollection} conciergesCollection */
    conciergesCollection.addItem(concierge);
};
