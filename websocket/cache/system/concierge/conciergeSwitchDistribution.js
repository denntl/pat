/* global conciergesCollection */

/**
 * Listener for `conciergeSwitchDistribution` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {ConciergeCollection} conciergesCollection */
    conciergesCollection.switchDistribution(data.id, data.isPaused);
};
