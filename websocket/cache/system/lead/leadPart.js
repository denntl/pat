/* global conciergesCollection */

/**
 * Listener for `Leadpart` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    for (const index in data.leads) {
        const dataLead = data.leads[index];

        /** @typedef {ConciergeCollection} conciergesCollection */
        conciergesCollection.updateRedisCacheOnLeadEvents(dataLead);
    }
};
