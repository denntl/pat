/* global leadsCollection */
/* global conciergesCollection */
/* global agentsCollection */

/**
 * Listener for `LeadUpdate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {LeadCollection} leadsCollection */
    const lead = leadsCollection.getItem(data.lead.id);

    if (lead !== null) {
        lead.addDataRecursively(data.lead);
        lead.updateCache();

        /** @typedef {conciergesCollection} conciergesCollection */
        conciergesCollection.removeCacheLeadRelationSafe(lead);

        /** @typedef {AgentCollection} agentsCollection */
        agentsCollection.removeCacheLeadRelationSafe(lead);
    }
    conciergesCollection.updateRedisCacheOnLeadEvents(data.lead);
};
