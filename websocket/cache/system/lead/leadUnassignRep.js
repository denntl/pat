/* global leadsCollection */

/**
 * Listener for `LeadUnassignRep` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {LeadCollection} leadsCollection */
    const lead = leadsCollection.getItem(data.leadId);

    if (lead !== null) {
        lead.conciergeId = null;
        lead.updateCache();

        lead.stopTimer();
    }
};
