/* global leadsCollection */
/* global conciergesCollection */

/**
 * Listener for `LeadUpdateRep` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {LeadCollection} leadsCollection */
    const lead = leadsCollection.getItem(data.leadId);
    let finalTimeForResponse;

    if (lead) {
        /** @typedef {conciergesCollection} conciergesCollection */
        conciergesCollection.removeCacheLeadRelationSafe(lead);

        lead.conciergeId = data.repId;
        lead.updatedAt = data.updatedAt.date;

        finalTimeForResponse = leadsCollection.setFinalTimeForResponse(data.leadId, data.lead.tag);
        lead.updateCache();

        finalTimeForResponse ? lead.restartTimer() : lead.stopTimer();

        conciergesCollection.addCacheLeadRelationSafe(lead);
    }
};
