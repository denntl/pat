/* global leadsCollection */

/**
 * Listener for `LeadUpdateStatus` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    if (data.leadId) {
        /** @typedef {LeadCollection} leadsCollection */
        const lead = leadsCollection.getItem(data.leadId);

        if (lead !== null) {
            lead.status = data.status;
            lead.tag = data.tag;
            lead.icon = data.icon;

            lead.updateCache();

            if (lead.tag === 'Ongoing Chat') {
                lead.stopTimer();
            }
        }
    }
};
