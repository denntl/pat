/* global leadsCollection */

/**
 * Listener for `LeadPauseSmsFlow` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {LeadCollection} leadsCollection */
    const lead = leadsCollection.getItem(data.leadId);

    if (lead) {
        lead.isPaused = data.isPaused;
        lead.smsFlowStep = data.smsFlowStep;
        lead.updateCache();
    }
};
