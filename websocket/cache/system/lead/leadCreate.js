/* global conciergesCollection */

/**
 * Listener for `LeadCreate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {ConciergeCollection} conciergesCollection */
    conciergesCollection.updateRedisCacheOnLeadEvents(data.lead);
};
