/* global leadsCollection */

/**
 * Listener for `EmailCreate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {LeadCollection} leadsCollection */
    const lead = leadsCollection.collection[data.leadId];

    if (lead !== null) {
        lead.tag = data.tag;
        lead.icon = data.icon;
        lead.lastMessage = data.email.text;

        if (data.email.incoming) {
            /** @typedef {LeadCollection} leadsCollection */
            leadsCollection.setFinalTimeForResponse(data.leadId, data.tag);
            lead.restartTimer();
        } else {
            lead.stopTimer();
        }
    }
};
