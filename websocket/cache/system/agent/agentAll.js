/* global agentsCollection */
/* global Agent */

/**
 * Listener for `AgentAll` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {AgentCollection} agentsCollection */
    agentsCollection.cleanCollection();

    for (const index in data.agents) {
        const agentData = data.agents[index];
        const agent = new Agent();

        agent.addDataRecursively(agentData);
        agentsCollection.addItem(agent);
    }
};
