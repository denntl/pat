/* global agentsCollection */

/**
 * Listener for `AgentPersonaUpdate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {AgentCollection} agentsCollection */
    const agent = agentsCollection.getItem(data.id);

    if (agent !== null) {
        agent.personaName = data.agent.personaName;
        agent.personaTeamName = data.agent.personaTeamName;
        agent.personaTitle = data.agent.personaTitle;
        agent.sendGridEmail = data.agent.sendGridEmail;
        agent.twilioNumber = data.agent.twilioNumber;
        agent.extraNoteForRep = data.agent.extraNoteForRep;
    }
};
