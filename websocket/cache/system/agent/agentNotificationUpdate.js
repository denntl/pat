/* global agentsCollection */

/**
 * Listener for `AgentNotificationUpdate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {AgentCollection} agentsCollection */
    const agent = agentsCollection.getItem(data.id);

    if (agent !== null) {
        agent.notificationPhone = data.agentData.notificationPhone;
        agent.notificationEmail = data.agentData.notificationEmail;
        agent.callForwardingNumber = data.agentData.callForwardingNumber;
        // TODO: Additional Email Notifications
    }
};
