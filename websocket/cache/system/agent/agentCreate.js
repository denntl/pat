/* global Agent */
/* global agentsCollection */

/**
 * Listener for `AgentCreate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    const agent = new Agent();

    agent.addDataRecursively(data.agent);

    /** @typedef {AgentCollection} agentsCollection */
    agentsCollection.addItem(agent);
};
