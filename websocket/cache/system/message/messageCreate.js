/* global leadsCollection */

/**
 * Listener for `MessageCreate` event.
 *
 * @param {object} data - Redis data
 *
 * @returns {void}
 */
module.exports = data => {
    /** @typedef {LeadCollection} leadsCollection */
    const lead = leadsCollection.getItem(data.leadId);

    if (lead !== null) {
        lead.tag = data.tag;
        lead.icon = data.icon;
        lead.lastMessage = data.message.text;

        if (data.message.incoming) {
            /** @typedef {LeadCollection} leadsCollection */
            leadsCollection.setFinalTimeForResponse(data.leadId, data.tag);
            lead.restartTimer();
        } else {
            lead.stopTimer();
        }
    }
};
