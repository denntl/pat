const fs = require('fs');
const _ = require('lodash');
const log = require('../logger');

/**
 * @class RedisListener
 */
class RedisListener {
    /**
     * @constructor
     *
     * @param {string} message - Redis JSON data
     *
     * @returns {void}
     */
    constructor(message) {
        const parsed = JSON.parse(message);

        delete parsed.data.sender;
        delete parsed.data.socket;

        this.event = parsed.event;
        this.data = parsed.data;
    }

    /**
     * Listen Redis subscriber for `system` channel.
     *
     * @returns {void}
     */
    listen() {
        const listener = require(this.getPath());

        listener(this.data);

        log.info(`Listen SYSTEM event: ${this.event}`);
    }

    /**
     * Get path for event listener file in camel case.
     *
     * @returns {string} - Event listener path
     */
    getPath() {
        const parts = this.event.split('\\');
        const file = _.camelCase(parts[parts.length - 1]);
        const dir = parts[parts.length - 2];

        const path = `${__dirname}/system/${dir.toLowerCase()}/${file}.js`;

        return fs.existsSync(path) ? path : log.fatal(`Missed listener for event ${this.event} file in ${path}`);
    }
}

/**
 * Runs Redis listener.
 *
 * @param {string} message - Redis JSON data
 *
 * @returns {void}
 */
module.exports = message => {
    const redisListener = new RedisListener(message);

    redisListener.listen();
};
