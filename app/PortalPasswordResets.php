<?php

namespace App;

use App\Models\AbstractModel as Model;
use Carbon\Carbon;

/**
 * Class portalPasswordResets
 *
 * @property string $email
 * @property string $token
 * @property Carbon $createdAt
 *
 * @package App
 */
class PortalPasswordResets extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'portalPasswordResets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'createdAt'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['token'];
}
