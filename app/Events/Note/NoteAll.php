<?php

namespace App\Events\Note;

use App\Note;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NoteAll.
 * @package App\Events\Note
 */
class NoteAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var int
     */
    public $leadId;

    /**
     * @var Collection
     */
    public $notes;

    /**
     * @var bool
     */
    private $broadcastToLeads;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     * @param boolean $broadcastToLeads
     * @param boolean $isAgent
     */
    public function __construct(
        string $leadId = null,
        string $sender = null,
        bool $broadcastToLeads = false,
        bool $isAgent = false
    ) {
        $this->sender = $sender;
        $this->leadId = $leadId;
        if ($isAgent) {
            $this->notes = Note::where('leadId', $leadId)
                ->whereNotIn('type', ['Admin Note'])
                ->orderBy('createdAt', 'DESC')
                ->get();
        } else {
            $this->notes = Note::where('leadId', $leadId)->get();
        }
        $this->broadcastToLeads = $broadcastToLeads;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $ret = ['lead-' . $this->leadId];

        if ($this->broadcastToLeads) {
            $ret[] = 'leads';
        }

        return $ret;
    }
}
