<?php

namespace App\Events\Note;

use App\Facades\SendGrid;
use App\Note;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NoteCreate.
 * @package App\Events\Note
 */
class NoteCreate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var int
     */
    public $id;

    /**
     * @var Note
     */
    public $note;

    /**
     * @var string|null
     */
    public $leadId = null;

    /**
     * @var string
     */
    public $sender;

    /**
     * Create a new event instance.
     *
     * @param Note $note
     * @param string $sender
     */
    public function __construct(Note $note, string $sender = null)
    {
        $this->leadId = $note->leadId;
        $this->note = $note;
        $this->id = $this->note->id;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return $this->leadId === null ? [] : ['lead-' . $this->leadId];
    }
}
