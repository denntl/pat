<?php

namespace App\Events\Call;

use App\LeadCall;
use App\Note;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AllCalls.
 *
 * @package App\Events\Call
 */
class AllCalls implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var int
     */
    public $leadId;

    /**
     * @var Collection
     */
    public $calls;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     */
    public function __construct(
        string $leadId = null,
        string $sender = null
    ) {
        $this->sender = $sender;
        $this->leadId = $leadId;
        $this->calls = LeadCall::with('notes')->where('leadId', $leadId)->get()->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $ret = ['lead-' . $this->leadId];

        return $ret;
    }
}
