<?php

namespace App\Events\Message;

use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Exception;

/**
 * Class MessageReadAll.
 * @package App\Events\Email
 */
class MessageReadAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var array
     */
    public $errors;

    /**
     * @var array
     */
    public $readMessagesIds;

    /**
     * @var boolean
     */
    public $success;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     */
    public function __construct(string $leadId = null, string $sender = null)
    {
        $this->sender = $sender;
        $this->leadId = $leadId;
        $readMessagesIds = [];

        try {
            $messages = Message::where('leadId', $leadId)->where('isRead', false)->get();

            foreach ($messages as $message) {
                $message->isRead = true;
                $message->saveOrFail();
                $readMessagesIds[] = $message->id;
            }

            $this->readMessagesIds = $readMessagesIds;
            $this->success = true;
        } catch (Exception $exception) {
            $this->success = false;
            $this->errors = ['messages' => 'Emails were not read.'];
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return $this->leadId ? ['lead-' . $this->leadId] : [];
    }
}
