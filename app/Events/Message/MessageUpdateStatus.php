<?php

namespace App\Events\Message;

use App\Facades\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

/**
 * Class MessageUpdateStatus.
 * @package App\Events\Message
 */
class MessageUpdateStatus implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var string
     */
    public $twilioMessageSid;

    /**
     * @var string
     */
    public $twilioMessageStatus;

    /**
     * @var string
     */
    public $messageId;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $twilioMessageSid
     * @param string $twilioMessageStatus
     * @param string $messageId
     */
    public function __construct(
        string $leadId = null,
        string $twilioMessageSid = null,
        string $twilioMessageStatus = null,
        string $messageId = null
    ) {
        $this->leadId = $leadId;
        $this->twilioMessageSid = $twilioMessageSid;
        $this->twilioMessageStatus = $twilioMessageStatus;
        $this->messageId = $messageId;

        $updated = Message::updateTwilioStatus($twilioMessageSid, $twilioMessageStatus);
        if (!$updated) {
            info('Failed to update message status');
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['lead-' . $this->leadId];
    }
}
