<?php

namespace App\Events\Message;

use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class MessageCreate.
 * @package App\Events\Message
 */
class MessageCreate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $messageId;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var Message
     */
    public $message;

    /**
     * @var string
     */
    public $tag;

    /**
     * @var string
     */
    public $icon;

    /**
     * Create a new event instance.
     *
     * @param Message $message
     * @param string $sender
     */
    public function __construct(Message $message, string $sender = null)
    {
        $this->sender = $sender;
        $this->leadId = $message->leadId;
        $this->messageId = $message->id;
        $this->message = $message;
        $this->tag = $message->lead->tag;
        $this->icon = $message->lead->icon;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if (!$this->leadId) {
            return [];
        }

        return ['system', 'lead-' . $this->leadId, 'leads'];
    }
}
