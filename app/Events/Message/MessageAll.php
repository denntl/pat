<?php

namespace App\Events\Message;

use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class MessageAll.
 * @package App\Events\Message
 */
class MessageAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var int
     */
    public $leadId;

    /**
     * @var Collection|static[]
     */
    public $messages;

    /**
     * @var bool
     */
    private $broadcastToLeads;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     * @param boolean $broadcastToLeads
     */
    public function __construct(string $leadId = null, string $sender = null, $broadcastToLeads = false)
    {
        $this->sender = $sender;
        $this->leadId = $leadId;
        $this->messages = Message::where('leadId', $leadId)->get();
        $this->broadcastToLeads = $broadcastToLeads;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $ret = ['lead-' . $this->leadId];

        if ($this->broadcastToLeads) {
            $ret[] = 'leads';
        }

        return $ret;
    }
}
