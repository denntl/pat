<?php

namespace App\Events\Lead;

use App\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadUpdate.
 * @package App\Events\Lead
 */
class LeadUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $id; // TODO: change to leadId

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Lead|null
     */
    public $lead = null;

    /**
     * LeadUpdate constructor.
     *
     * @param string $sender
     * @param Lead $lead
     */
    public function __construct(Lead $lead, string $sender = null)
    {
        $this->sender = $sender;
        $this->id = $lead->id;
        $this->lead = $lead;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system', 'leads', 'lead-' . $this->id];
    }
}
