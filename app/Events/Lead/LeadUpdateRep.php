<?php

namespace App\Events\Lead;

use App\Concierge;
use App\Events\Subscribe\ResubscribeMessagingLead;
use App\Facades\Lead as LeadService;
use App\Email;
use App\Lead;
use App\Message;
use App\Note;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadUpdateRep.
 * @package App\Events\Lead
 */
class LeadUpdateRep implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var int
     */
    public $repId;

    /**
     * @var int
     */
    public $oldRepId;

    /**
     * @var int
     */
    public $leadId;

    /**
     * @var Lead
     */
    public $lead;

    /**
     * @var array
     */
    public $messages;

    /**
     * @var array
     */
    public $emails;

    /**
     * @var array
     */
    public $notes;

    /**
     * @var string
     */
    public $updatedAt;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param integer $repId
     * @param integer|null $conciergeId - probably admin
     * @param string $sender
     */
    public function __construct(
        string $leadId = null,
        int $repId = null,
        int $conciergeId = null,
        string $sender = null
    ) {
        $this->sender = $sender;
        $this->repId = $repId;
        $this->leadId = $leadId;

        $this->oldRepId = Lead::find($leadId)->conciergeId;
        /** @var Lead $lead */
        $this->lead = LeadService::updateRep($leadId, $repId);

        $this->messages = Message::where('leadId', $leadId)->get();
        $this->emails = Email::where('leadId', $leadId)->get();
        // TODO: leadCalls $this->leadCalls = LeadCall::whereIn('leadId', $ids)->where('isForwarded', 0)->get();
        $this->notes = Note::where('leadId', $leadId)->get();
        $this->updatedAt = $this->lead->updatedAt;
        $this->lead = $this->lead->toArray();

        if (($conciergeId === null) ||
            (($conciergeId !== null) && (Concierge::find($conciergeId)->role != Concierge::ROLE_ADMIN))
        ) {
            event(new ResubscribeMessagingLead($this->sender, $this->leadId, $repId, $this->oldRepId));
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system', 'leads', 'lead-' . $this->leadId];
    }
}
