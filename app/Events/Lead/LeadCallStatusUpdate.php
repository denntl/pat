<?php

namespace App\Events\Lead;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class LeadCallStatusUpdate
 * @package App\Events
 */
class LeadCallStatusUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $callSid;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $sender;

    /**
     * Create a new event instance.
     *
     * @inheritDoc
     *
     * @param string $leadId
     * @param string $callSid
     * @param string $status
     */
    public function __construct(
        string $leadId = null,
        string $callSid = null,
        string $status = null,
        string $sender = null
    ) {
        $this->id = $leadId;
        $this->callSid = $callSid;
        $this->status = $status;
        $this->sender = $sender;

        info('in call update status event', [
            'id' => $this->id,
            'callSid' => $this->callSid,
            'status' => $this->status,
            'sender' => $this->sender
        ]);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        info('update call statuses broadcasted ' . $this->id);
        return ['lead-' . $this->id, 'leads'];
    }
}
