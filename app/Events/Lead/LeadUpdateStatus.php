<?php

namespace App\Events\Lead;

use App\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadUpdateStatus.
 * @package App\Events\Lead
 */
class LeadUpdateStatus implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var int|null
     */
    public $leadId = null;

    /**
     * @var string|null
     */
    public $note;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $tag;

    /**
     * @var string
     */
    public $lastAction;
    /**
     * @var string
     */
    public $icon;


    /**
     * Create a new event instance.
     *
     * @param Lead $lead
     * @param string $sender
     */
    public function __construct(Lead $lead, string $sender = null)
    {
        if ($lead) {
            $this->status = $lead->status;
            $this->sender = $sender;
            $this->leadId = $lead->id;
            $this->tag = $lead->tag;
            $this->lastAction = $lead->lastAction;
            $this->icon = $lead->icon;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system', 'leads', 'lead-' . $this->leadId];
    }
}
