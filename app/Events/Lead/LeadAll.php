<?php

namespace App\Events\Lead;

use App\Events\Message\MessageAll;
use App\Events\Email\EmailAll;
use App\Events\Note\NoteAll;
use App\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadAll. TODO: we should remove this event. All leads we have at websocket side.
 * @package App\Events\Lead
 */
class LeadAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var array
     */
    public $leads;

    /**
     * Create a new event instance.
     *
     * @param string|null $leadId
     * @param integer|null $conciergeId
     * @param integer|null $agentId
     * @param string|null $sender
     */
    public function __construct(
        string $leadId = null,
        int $conciergeId = null,
        int $agentId = null,
        string $sender = null
    ) {
        $query = Lead::with('source', 'agent');

        if ($agentId !== null) {
            $query->where('agentId', $agentId);
        }
        if ($conciergeId !== null) {
            $query->where('conciergeId', $conciergeId);
        }

        /** @var Lead[] $leads */
        $leads = $query->orderBy('updatedAt', 'asc')->get();

        $this->leads = $leads->toArray();
        $this->sender = $sender;

        $broadcastToLeads = false;

        if (($leadId === null) && ($leads->count() > 0)) {
            $leadId = $leads->first()->id;
            $broadcastToLeads = true;
        }
        if ($leadId !== null) {
            event(new MessageAll($leadId, $sender, $broadcastToLeads));
            event(new EmailAll($leadId, $sender, $broadcastToLeads));
            event(new NoteAll($leadId, $sender, $broadcastToLeads));
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ($this->sender === null) ? ['system'] : ['leads'];
    }
}
