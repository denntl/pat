<?php

namespace App\Events\Lead;

use App\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadSendEmails.
 * @package App\Events\Lead
 */
class LeadSendEmails implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Lead
     */
    private $lead;

    /**
     * @var string
     */
    public $status;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $emails
     * @param string $sender
     */
    public function __construct(string $leadId = null, string $emails = null, string $sender = null)
    {
        $this->id = $leadId;
        $this->sender = $sender;
        $this->lead = Lead::find($leadId);

        // TODO: send email feature

        $this->status = 'success';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['lead-' . $this->id];
    }
}
