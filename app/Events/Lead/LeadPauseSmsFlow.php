<?php

namespace App\Events\Lead;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class LeadPauseSmsFlow implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var boolean
     */
    public $isPaused;

    /**
     * @var integer
     */
    public $smsFlowStep;

    /**
     * LeadPauseSmsFlow constructor.
     *
     * @param string $leadId
     * @param boolean $isPaused
     * @param integer $smsFlowStep
     */
    public function __construct($leadId, $isPaused, $smsFlowStep = null)
    {
        $this->leadId = $leadId;
        $this->isPaused = $isPaused;
        $this->smsFlowStep = $smsFlowStep;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system', 'lead-' . $this->leadId];
    }
}
