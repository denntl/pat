<?php

namespace App\Events\Lead;

use App\Lead;
use App\Facades\Intercom;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadRefer.
 * @package App\Events\Lead
 */
class LeadRefer implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     */
    public function __construct(string $leadId = null, string $sender = null)
    {
        $this->id = $leadId;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['lead-' . $this->id, 'leads'];
    }
}
