<?php

namespace App\Events\Lead;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LeadReferFailed implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $leadId;

    public $sender;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     */
    public function __construct(string $leadId, string $sender = null)
    {
        $this->leadId = $leadId;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['lead-' . $this->leadId, 'leads'];
    }
}
