<?php

namespace App\Events\Lead;

use App\Facades\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadCallNotes.
 *
 * @package App\Events\Lead
 */
class LeadCallNotes implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id; // TODO: rename to leadId

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $notes;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $note
     * @param integer $callId
     * @param integer $conciergeId
     * @param string $sender
     */
    public function __construct(
        string $leadId = null,
        string $note = null,
        int $callId = null,
        int $conciergeId = null,
        string $sender = null
    ) {
        $this->id = $leadId;
        $this->sender = $sender;
        $this->notes = $note;

        $data = compact('note', 'conciergeId', 'leadId', 'callId');
        $data['type'] = 'Call note';

        // TODO: check why call notes sends NoteCreated event
        Lead::createLeadNote($data, $sender);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if (!$this->id) {
            return [];
        }

        return ['lead-' . $this->id, 'leads'];
    }
}
