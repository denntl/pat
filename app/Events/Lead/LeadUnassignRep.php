<?php

namespace App\Events\Lead;

use App\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class LeadUnassignRep.
 * @package App\Events\Lead
 */
class LeadUnassignRep implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var int
     */
    public $leadId;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     */
    public function __construct(string $leadId = null, string $sender = null)
    {
        $this->sender = $sender;
        $this->leadId = $leadId;

        /** @var Lead $lead */
        $lead = Lead::find($leadId);
        $lead->conciergeId = null;
        $lead->save();

        Log::info('Lead ' . $lead->id . ' unassigned from concierge');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system', 'lead-' . $this->leadId];
    }
}
