<?php

namespace App\Events\Lead;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\LeadCall as LeadCallModel;

/**
 * Class LeadCall.
 * @package App\Events\Lead
 */
class LeadCall implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var int
     */
    public $leadId;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var array
     */
    public $leadCall;

    /**
     * Create a new event instance.
     *
     * @param LeadCallModel $leadCall
     * @param string $sender
     */
    public function __construct(
        LeadCallModel $leadCall,
        string $sender = null
    ) {
        $this->sender = $sender;
        $this->leadId = $leadCall->leadId;
        $this->leadCall = $leadCall->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if (!$this->leadId) {
            return [];
        }

        return ['lead-' . $this->leadId, 'leads'];
    }
}
