<?php

namespace App\Events\Lead;

use App\Events\Message\MessageAll as MessageAllEvents;
use App\Events\Email\EmailAll as EmailAllEvents;
use App\Events\Note\NoteAll as NoteAllEvents;
use App\Events\Call\AllCalls as CallAllEvents;
use App\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadGet.
 * @package App\Events\Lead
 */
class LeadGet implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Lead
     */
    public $lead;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param boolean $isAgent
     * @param string $sender
     */
    public function __construct(string $leadId = null, bool $isAgent = false, string $sender = null)
    {
        $this->id = $leadId;
        $this->sender = $sender;
        $lead = Lead::with('source', 'agent')->where('id', $leadId)->first();
        if ($lead) {
            $this->lead = $lead->toArray();
            $broadcastToLeads = false;

            event(new MessageAllEvents($leadId, $sender));
            event(new EmailAllEvents($leadId, $sender));
            event(new NoteAllEvents($leadId, $sender, $broadcastToLeads, $isAgent));
            event(new CallAllEvents($leadId, $sender));
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['lead-' . $this->id];
    }
}
