<?php

namespace App\Events\Lead;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadCallEnd.
 * @package App\Events\Lead
 */
class LeadCallEnd implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $leadId;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var integer
     */
    public $leadCallId;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $finishedAt;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $phone
     * @param integer $leadCallId
     * @param string $sender
     * @param string $finishedAt
     */
    public function __construct(
        string $leadId = null,
        string $phone = null,
        $leadCallId = null,
        string $sender = null,
        string $finishedAt = null
    ) {
        $this->leadId = $leadId;
        $this->sender = $sender;
        $this->phone = $phone;
        $this->leadCallId = $leadCallId;
        $this->finishedAt = $finishedAt;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if (!$this->leadId) {
            return [];
        }

        return ['lead-' . $this->leadId, 'leads'];
    }
}
