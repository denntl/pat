<?php

namespace App\Events\Lead;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadPhoneIsInvalid.
 * @package App\Events\Lead
 */
class LeadPhoneIsInvalid implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $leadId;

    /**
     * Create a new event instance.
     *
     * @param string $isValid
     * @param integer $conciergeId
     * @param string $sender
     */
    public function __construct(
        string $leadId = null,
        string $sender = null
    ) {
        $this->leadId = $leadId;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['lead-' . $this->leadId];
    }
}
