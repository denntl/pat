<?php

namespace App\Events\Lead;

use App\Attachment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * TODO: this event should be refactored
 *
 * Class LeadGet.
 * @package App\Events\Lead
 */
class LeadGetOriginalEmail implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Attachment
     */
    public $emails;

    /**
     * Create a new event instance.
     *
     * @param string $sender
     * @param string $leadId
     */
    public function __construct(string $sender = null, string $leadId = null)
    {
        $this->id = $leadId;
        $this->sender = $sender;
        $this->emails = Attachment::where('leadId', $leadId)->get();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['lead-original-email-' . $this->id];
    }
}
