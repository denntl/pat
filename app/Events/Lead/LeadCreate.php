<?php

namespace App\Events\Lead;

use App\Lead;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LeadCreate.
 * @package App\Events\Lead
 */
class LeadCreate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Lead
     */
    public $lead;

    /**
     * @var integer
     */
    public $agentId;

    /**
     * @var object
     */
    public $agent;

    /**
     * @var object
     */
    public $source;

    /**
     * Create a new event instance.
     *
     * @param Lead $lead
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
        $this->agentId = $lead->agentId;
        $this->agent = $lead->agent;
        $this->source = $lead->source;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system', 'concierges', 'agent-' . $this->agentId];
    }
}
