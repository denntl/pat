<?php

namespace App\Events\Concierge;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ConciergeDelete.
 * @package App\Events\Concierge
 */
class ConciergeDelete implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var number
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * Create a new event instance.
     *
     * @param int $id
     * @param string $sender
     */
    public function __construct(int $id = null, string $sender = null)
    {
        $this->id = $id;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['concierges', 'system'];
    }
}
