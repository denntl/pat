<?php

namespace App\Events\Concierge;

use App\Concierge;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ConciergeCreate.
 * @package App\Events\Concierge
 */
class ConciergeCreate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var int
     */
    public $role;

    /**
     * @var string
     */
    public $sender;

    /**
     * Create a new event instance.
     *
     * @param string $name
     * @param string $email
     * @param int $role
     * @param int $conciergeId
     * @param string $sender
     */
    public function __construct(
        string $name = null,
        string $email = null,
        int $role = null,
        int $conciergeId = null,
        string $sender = null
    ) {
        $this->name = $name;
        $this->email = $email;
        $this->role = $role;
        $this->id = $conciergeId;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system', 'concierges'];
    }
}
