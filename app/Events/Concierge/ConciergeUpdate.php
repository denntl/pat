<?php

namespace App\Events\Concierge;

use App\Concierge;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ConciergeUpdate.
 * @package App\Events\Concierge
 */
class ConciergeUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var number
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var number
     */
    public $role;

    /**
     * @var string
     */
    public $sender;

    /**
     * Create a new event instance.
     *
     * @param int $id
     * @param string $name
     * @param string $email
     * @param int $role
     * @param string $sender
     */
    public function __construct(
        int $id = null,
        string $name = null,
        string $email = null,
        int $role = null,
        string $sender = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->role = $role;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['concierges', 'system'];
    }
}
