<?php

namespace App\Events\Concierge;

use App\Concierge;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ConciergeSwitchDistribution.
 * @package App\Events\Concierge
 */
class ConciergeSwitchDistribution implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var number
     */
    public $id;
    /**
     * @var number
     */
    public $isPaused;

    /**
     * @var string
     */
    public $sender;

    /**
     * Create a new event instance.
     *
     * @param int $id
     * @param boolean $isPaused
     * @param string $sender
     */
    public function __construct(
        int $id = null,
        bool $isPaused = false,
        string $sender = null
    ) {
        $this->id = $id;
        $this->isPaused = $isPaused;
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['concierge-' . $this->id, 'system'];
    }
}
