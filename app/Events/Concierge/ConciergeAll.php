<?php

namespace App\Events\Concierge;

use App\Concierge;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ConciergeAll.
 * @package App\Events\Concierge
 */
class ConciergeAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Collection
     */
    public $concierges;

    /**
     * Create a new event instance.
     *
     * @param string $sender
     */
    public function __construct(string $sender = null)
    {
        $concierges = Concierge::all();
        $this->concierges = $concierges->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system'];
    }
}
