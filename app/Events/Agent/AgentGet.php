<?php

namespace App\Events\Agent;

use App\Agent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AgentGet.
 * @package App\Events\Agent
 */
class AgentGet implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Agent
     */
    public $agent;

    /**
     * Create a new event instance.
     *
     * @inheritDoc
     *
     * @param string $sender
     * @param int $agentId
     */
    public function __construct(int $agentId = null, string $sender = null)
    {
        $this->id = $agentId;
        $this->sender = $sender;
        $this->agent = Agent::find($agentId)->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id];
    }
}
