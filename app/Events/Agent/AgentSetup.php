<?php

namespace App\Events\Agent;

use App\Agent;
use App\AgentOnBoarding;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * TODO: Vitaly please move data-changing code from here
 * Class AgentSetup.
 * @package App\Events\Agent
 */
class AgentSetup implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Agent
     */
    public $data;

    /**
     * @inheritDoc
     *
     * @param string $sender
     * @param int $agentId
     */
    public function __construct(int $agentId = null, string $sender = null)
    {
        $this->id = $agentId;
        $this->sender = $sender;

        /** @var $agent Agent */
        /** @var $agentOnBoarding AgentOnBoarding */
        $agent = Agent::find($agentId);

        /** @var AgentOnBoarding $agentOnBoarding */
        $agentOnBoarding = AgentOnBoarding::where('agentId', $agentId)->first();

        $step = 1;
        $tokenProvided = false;

        if ($agentOnBoarding) {
            $step = $agentOnBoarding->step;
            $tokenProvided = $agentOnBoarding->tokenReceived;
        }

        $data = [
            'step' => $step,
            'tokenEmailScopeProvided' => $tokenProvided,
            'agentData' => [
                'phoneNumber' => $agent->twilioNumber ?? '',
                'areaCode' => $agent->areaCode ?? '',
                'conciergeName' => $agent->personaName ?? '',
                'conciergeTitle' => $agent->personaTitle ?? '',
                'teamName' => $agent->personaTeamName ?? '',
                'emailForwarding' => $agent->emailForwarding ?? ''
            ]
        ];

        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id];
    }
}
