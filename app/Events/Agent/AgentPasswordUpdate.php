<?php

namespace App\Events\Agent;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AgentPasswordUpdate.
 * @package App\Events\Agent
 */
class AgentPasswordUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * Status can be success, invalid or error
     * @var string
     */
    public $status;

    /**
     * Create a new event instance.
     *
     * @param integer $id
     * @param string $status
     */
    public function __construct(int $id = null, string $status = null)
    {
        $this->id = $id;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id];
    }
}
