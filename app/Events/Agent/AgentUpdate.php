<?php

namespace App\Events\Agent;

use App\Agent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AgentUpdate.
 * @package App\Events\Agent
 */
class AgentUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Agent
     */
    public $agent;

    /**
     * Create a new event instance.
     *
     * @param string $sender
     * @param Agent $agent
     */
    public function __construct(string $sender = null, Agent $agent = null)
    {
        $this->id = $agent->id;
        $this->sender = $sender;
        $this->agent = $agent->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id, 'system'];
    }
}
