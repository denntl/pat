<?php

namespace App\Events\Agent;

use App\Agent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AgentUpdateFailed.
 * @package App\Events\Agent
 */
class AgentUpdateFailed implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Agent
     */
    public $failedFields;

    /**
     * Create a new event instance.
     *
     * @param string $sender
     * @param integer $agentId
     * @param string[] $failedFields
     */
    public function __construct(string $sender = null, int $agentId = null, $failedFields = [])
    {
        $this->id = $agentId;
        $this->sender = $sender;
        $this->failedFields = $failedFields;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id];
    }
}
