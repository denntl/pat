<?php

namespace App\Events\Agent;

use App\Agent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class AgentAll.
 * @package App\Events\Agent
 */
class AgentAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var array
     */
    public $agents;

    /**
     * Create a new event instance.
     */
    public function __construct()
    {
        $agents = Agent::with('agentOnBoarding')->orderBy('updatedAt', 'asc')->get();
        $this->agents = $agents->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['system'];
    }
}
