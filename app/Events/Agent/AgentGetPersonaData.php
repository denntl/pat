<?php

namespace App\Events\Agent;

use App\Agent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AgentSettings.
 * @package App\Events\Agent
 */
class AgentGetPersonaData implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Agent
     */
    public $agentPersonaData;


    /**
     * Create a new event instance.
     *
     * @param string $sender
     * @param int $agentId
     */
    public function __construct(int $agentId = null, string $sender = null)
    {
        $this->id = $agentId;
        $this->sender = $sender;

        $agent = Agent::find($agentId);

        $agentPersonaData = [
            'personaName' => '',
            'personaTeamName' => '',
            'personaTitle' => ''
        ];

        if ($agent) {
            $agentPersonaData = [
                'personaName' => $agent->personaName,
                'personaTeamName' => $agent->personaTeamName,
                'personaTitle' => $agent->personaTitle
            ];
        }

        $this->agentPersonaData = $agentPersonaData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id];
    }
}
