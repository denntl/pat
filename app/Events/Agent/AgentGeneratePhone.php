<?php

namespace App\Events\Agent;

use App\Agent;
use App\Facades\Twilio;
use App\Services\TwilioService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

/**
 * Class AgentGeneratePhone.
 * @package App\Events\Agent
 */
class AgentGeneratePhone implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Agent
     */
    public $phone;

    /**
     * Create a new event instance.
     *
     * @inheritDoc
     *
     * @param string $sender
     * @param int $agentId
     */
    public function __construct(string $areaCode, int $agentId = null, string $sender = null)
    {
        $this->id = $agentId;
        $this->sender = $sender;

        $prodEnv = App::environment('production');
        $demoEnv = App::environment('demo');

        $this->phone = ($prodEnv || $demoEnv)
            ? Twilio::generateNumberByArea($areaCode)
            : TwilioService::TEST_FROM_PHONE;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return $this->id ? ['agent-' . $this->id] : [];
    }
}
