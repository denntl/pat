<?php

namespace App\Events\Agent;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class AgentUpdateOnBoarding.
 * @package App\Events\Agent
 */
class AgentUpdateOnBoarding implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var int
     */
    public $step;

    /**
     * Create a new event instance.
     *
     * @param string $sender
     * @param int $agentId
     * @param int $step
     */
    public function __construct(string $sender = null, int $agentId = null, int $step = 1)
    {
        $this->id = $agentId;
        $this->sender = $sender;
        $this->step = $step;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id];
    }
}
