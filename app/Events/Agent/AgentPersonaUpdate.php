<?php

namespace App\Events\Agent;

use App\Agent;
use App\Facades\Intercom;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\App;
use GuzzleHttp\Client;

/**
 * TODO: Maxim please move all data-changing logic from here to commands or other place
 * Class AgentSettings.
 * @package App\Events\Agent
 */
class AgentPersonaUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    const REGISTER_SEND_GRID_WEB_HOOK_URL = 'https://api.sendgrid.com/v3/user/webhooks/parse/settings';
    const CHECK_SEND_GRID_WEB_HOOK_URL = 'https://api.sendgrid.com/v3/user/webhooks/parse/settings/';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var Agent
     */
    public $agent;

    /**
     * @var integer
     */
    public $conciergeId;

    /**
     * Create a new event instance.
     *
     * @param int $agentId
     * @param array $data
     * @param string $sender
     */
    public function __construct(int $agentId = null, array $data = [], string $sender = null)
    {
        $agent = Agent::find($agentId);
        $sendGridEmailGenerated = false;

        $agent->personaTeamName = $data['personaTeamName'];
        $agent->personaName = $data['personaName'];
        $agent->personaTitle = $data['personaTitle'];
        $agent->extraNoteForRep = $data['extraNoteForRep'];

        if (!$agent->sendGridEmail) {
            $index = 0;
            do {
                $sendGridEmail = Agent::generateEmailForAgent($data['personaName'], $data['personaTeamName'], $index);
                $index++;
            } while (Agent::where('sendGridEmail', $sendGridEmail)->first());
            $agent->sendGridEmail = $sendGridEmail;
            $sendGridEmailGenerated = true;
        }

        $testingEnv = App::environment('testing');
        if ($agent->saveOrFail() && !$testingEnv) {
            if ($sendGridEmailGenerated) {
                $this->registerAgentSendGridWebHook($data['personaTeamName']);
            };

            if ($agent->salesForceId) {
                Intercom::createOrUpdateUser($agent);
            }
        }

        $this->id = $agentId;
        $this->sender = $sender;
        $this->agent = $agent;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id, 'system'];
    }

    /**
     * Register send_grid inbound host.
     *
     * @param string $teamName
     * @return array|null
     */
    private function registerAgentSendGridWebHook($teamName)
    {
        $client = new Client();
        $requestBody = [];
        $hostname = camel_case($teamName) . '.' . config('services.sendgrid.email_domain');

        if (!$this->checkIfInboundHostRegistered($hostname)) {
            $requestBody['hostname'] = $hostname;
            $url = config('services.sendgrid.webhook_inbound_url') . config('services.sendgrid.webhook_secret');
            $requestBody['url'] = $url;
            $requestBody['spam_check'] = false;
            $requestBody['send_raw'] = false;

            try {
                $res = $client->request('POST', self::REGISTER_SEND_GRID_WEB_HOOK_URL, [
                    'headers' => [
                        'authorization' => 'Bearer ' . config('services.sendgrid.key'),
                    ],
                    'json' => $requestBody
                ]);
                if ($res->getStatusCode() == 200) {
                    return \GuzzleHttp\json_decode($res->getBody());
                }
            } catch (ClientException $exception) {
                return null;
            }
        }

        return null;
    }

    /**
     * Check if inbound host registered in send grid
     *
     * @param string $hostname
     * @return boolean
     */
    private function checkIfInboundHostRegistered($hostname)
    {
        try {
            $client = new Client();

            $res = $client->request(
                'GET',
                self::CHECK_SEND_GRID_WEB_HOOK_URL . $hostname,
                [
                    'headers' => [
                        'authorization' => 'Bearer ' . config('services.sendgrid.key'),
                    ]
                ]
            );
            if ($res->getStatusCode() == 200) {
                return true;
            }

            return false;
        } catch (ClientException $exception) {
            return false;
        }
    }
}
