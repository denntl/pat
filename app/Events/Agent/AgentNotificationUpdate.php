<?php

namespace App\Events\Agent;

use App\Agent;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class AgentNotificationUpdate.
 * @package App\Events\Agent
 */
class AgentNotificationUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var array
     */
    public $agentData;

    /**
     * Create a new event instance.
     *
     * @param int $agentId
     * @param array $data
     * @param string $sender
     */
    public function __construct(int $agentId = null, array $data = [], string $sender = null)
    {
        $this->id = $agentId;
        $this->sender = $sender;
        $this->agentData = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id, 'system'];
    }
}
