<?php

namespace App\Events\Email;

use App\Email;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class EmailCreate.
 * @package App\Events\Email
 */
class EmailCreate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var int
     */
    public $email;

    /**
     * @var string
     */
    public $tag;

    /**
     * @var string
     */
    public $icon;

    /**
     * Create a new event instance.
     *
     * @param Email $email
     * @param string $sender
     */
    public function __construct(Email $email = null, string $sender = null)
    {
        $this->sender = $sender;

        if ($email) {
            $this->id = $email->id;
            $this->leadId = $email->leadId;
            $this->email = $email;
            $this->tag = $email->lead->tag;
            $this->icon = $email->lead->icon;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if (!$this->leadId) {
            return [];
        }

        return ['system', 'lead-' . $this->leadId, 'leads'];
    }
}
