<?php

namespace App\Events\Email;

use App\Email;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Collection;

/**
 * Class EmailAll.
 * @package App\Events\Email
 */
class EmailAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var integer
     */
    public $leadId;

    /**
     * @var Collection<Emails>
     */
    public $emails;

    /**
     * @var boolean
     */
    private $broadcastToLeads;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param string $sender
     * @param boolean $broadcastToLeads
     */
    public function __construct(string $leadId = null, string $sender = null, $broadcastToLeads = false)
    {
        $this->sender = $sender;
        $this->leadId = $leadId;
        $this->emails = Email::where('leadId', $leadId)->get();
        $this->broadcastToLeads = $broadcastToLeads;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $ret = ['lead-' . $this->leadId];
        if ($this->broadcastToLeads) {
            $ret[] = 'leads';
        }

        return $ret;
    }
}
