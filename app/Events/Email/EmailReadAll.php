<?php

namespace App\Events\Email;

use App\Email;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class EmailReadAll.
 *
 * @example php artisan email:read:all
 * @example php artisan email:read:all -S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsImxpZCI6bnVsbH0=
 *
 * @package App\Events\Email
 */
class EmailReadAll implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var array
     */
    public $readEmailsIds;

    /**
     * Create a new event instance.
     *
     * @param string $leadId
     * @param array $readEmailsIds
     * @param string $sender
     */
    public function __construct(string $leadId = null, array $readEmailsIds = [], string $sender = null)
    {
        $this->sender = $sender;
        $this->leadId = $leadId;
        $this->readEmailsIds = $readEmailsIds;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if (!$this->leadId) {
            return [];
        }

        return ['lead-' . $this->leadId];
    }
}
