<?php

namespace App\Events\Email;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class EmailUpdateStatus.
 * @package App\Events\Emails
 */
class EmailUpdateStatus implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $emailId;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var string
     */
    public $status;

    /**
     * Create a new event instance.
     *
     * @param string $emailId
     * @param string $leadId
     * @param string $status - Message status
     */
    public function __construct(
        string $emailId = null,
        string $leadId = null,
        string $status = null
    ) {
        $this->emailId = $emailId;
        $this->leadId = $leadId;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        if (!$this->leadId) {
            return [];
        }

        return ['lead-' . $this->leadId];
    }
}
