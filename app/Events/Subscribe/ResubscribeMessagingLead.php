<?php

namespace App\Events\Subscribe;

use App\Email;
use App\Lead;
use App\LeadCall;
use App\Message;
use App\Note;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ResubscribeMessagingLead.
 * @package App\Events\Subscribe
 */
class ResubscribeMessagingLead implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $leadId;

    /**
     * @var integer
     */
    public $repId;

    /**
     * @var integer
     */
    public $oldRepId;

    /**
     * Create a new event instance.
     *
     * @param string|null $sender
     * @param string|null $leadId
     * @param integer|null $repId
     * @param integer|null $oldRepId
     */
    public function __construct(
        string $sender = null,
        string $leadId = null,
        int $repId = null,
        int $oldRepId = null
    ) {
        $this->sender = $sender;
        $this->leadId = $leadId;
        $this->repId = $repId;
        $this->oldRepId = $oldRepId;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['subscribe'];
    }
}
