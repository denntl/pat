<?php

namespace App\Events\Subscribe;

use App\Email;
use App\Lead;
use App\LeadCall;
use App\Attachment;
use App\Message;
use App\Note;
use App\Concierge;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SubscribeMessaging.
 * @package App\Events\Subscribe
 */
class SubscribeMessaging implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var array
     */
    public $leads;

    /**
     * @var array
     */
    public $messages;

    /**
     * @var array
     */
    public $emails;

    /**
     * @var array
     */
    public $attachments;

    /**
     * @var array
     */
    public $leadCalls;

    /**
     * @var array
     */
    public $notes;

    /**
     * Create a new event instance.
     *
     * @param string|null $sender
     * @param integer|null $conciergeId
     * @param string|null $leadId
     */
    public function __construct(
        string $sender = null,
        int $conciergeId = null,
        string $leadId = null
    ) {
        if ($conciergeId === null) {
            $this->sender = $sender;
            $this->leads = [];
            $this->messages = [];
            $this->emails = [];
            $this->attachments = [];
            $this->leadCalls = [];
            $this->notes = [];

            return;
        }

        $query = Lead::with('source', 'agent');

        // For admin we should use leadId otherwise we should use conciergeId
        $isAdmin = Concierge::checkAdminRole($conciergeId);
        if ((!$isAdmin && $leadId) || !$leadId) {
            $query->where('conciergeId', $conciergeId);
        }

        if ($leadId) {
            $query->orWhere('id', $leadId);
        }

        /** @var Lead[] $leads */
        $leads = $query->orderBy('updatedAt', 'asc')->get();
        $ids = $leads->pluck('id');

        $this->leads = $leads->toArray();
        $this->messages = Message::whereIn('leadId', $ids)->get();
        $this->emails = Email::whereIn('leadId', $ids)->get();
        // TODO: Make request to handle situation when to many attachments
        $this->attachments = Attachment::whereIn('leadId', $ids)->get();
        $this->leadCalls = LeadCall::whereIn('leadId', $ids)->where('isForwarded', 0)->get();
        $this->notes = Note::whereIn('leadId', $ids)->get();
        $this->sender = $sender;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['subscribe'];
    }
}
