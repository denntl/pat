<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class Logout.
 * @package App\Events
 */
class Logout implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var null|number
     */
    public $id;

    /**
     * @var null|string
     */
    public $sender;

    /**
     * @var string
     */
    public $text;

    /**
     * Create a new event instance.
     *
     * @param int|null $id
     * @param string|null $sender
     * @param string $text
     */
    public function __construct(int $id = null, string $sender = null, string $text = null)
    {
        $this->id = $id;
        $this->sender = $sender;
        $this->text = $text;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['concierge-' . $this->id];
    }
}
