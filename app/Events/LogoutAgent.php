<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class LogoutAgent.
 * @package App\Events
 */
class LogoutAgent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var null|number
     */
    public $id;

    /**
     * @var null|string
     */
    public $sender;

    /**
     * @var string
     */
    public $senderSessionId;

    /**
     * @var string
     */
    public $text;


    /**
     * Create a new event instance.
     *
     * @param int|null $id
     * @param string|null $sender
     * @param string $senderSessionId
     * @param string $text
     */
    public function __construct(
        int $id = null,
        string $sender = null,
        string $senderSessionId = null,
        string $text = null
    ) {
        $this->id = $id;
        $this->sender = $sender;
        $this->senderSessionId = $senderSessionId;
        $this->text = $text;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['agent-' . $this->id];
    }
}
