<?php

namespace App\Exceptions;

use App\Agent;
use App\Concierge;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\User;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class Handler.
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \League\OAuth2\Server\Exception\OAuthServerException::class,
        \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class,
        \libphonenumber\NumberParseException::class,
        \Predis\Connection\ConnectionException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldntReport($exception)) {
            return;
        }

        /** @var Agent|Concierge $user */
        $user = $this->getUser();
        // If user exists pass it to the rollbar person
        if ($user) {
            Log::error($exception, [
                'person' => $this->getUserData($user)
            ]);
            // Else just enable rollbar
        } else {
            Log::error($exception);
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $errorMessage = 'Unauthenticated.';

        if ($request->is('api/*')) {
            $authorizationHeader = $request->header('Authorization');
            if (!$authorizationHeader) {
                $errorMessage = 'No Authorization Header.';
            } elseif (!starts_with($authorizationHeader, 'Bearer')) {
                $errorMessage = 'No Bearer In Authorization Header.';
            }

            return response()->json([
                'errors' => [
                    ['message' => $errorMessage]
                ]
            ], 401);
        }

        return redirect('/');
    }

    /**
     * Method to get user from guard or from
     * command line argument user id and token
     * @return Authenticatable|null
     */
    private function getUser()
    {
        // Init user
        $user = null;

        // If Auth has user return user
        if ($user = Auth::user()) {
            return $user;
        }

        // If Auth web guard has user return user
        if ($user = Auth::guard('web')->user()) {
            return $user;
        }

        // If Auth portal guard has user return user
        if ($user = Auth::guard('portal')->user()) {
            return $user;
        }

        // Get request
        $request = Auth::getRequest();
        // Get server from request
        $server = $request->server();

        // If exists argv[2] and argv[3](userId and token) from command line
        if (isset($server['argv']) && isset($server['argv'][2]) && isset($server['argv'][3])) {
            // Get user id
            $userId = $server['argv'][2];
            // Get token
            $token = $server['argv'][3];

            // Check if user is Agent. If true return Agent user
            if (Agent::checkStaticToken($userId, $token)) {
                return Agent::find($userId);
                // Else Check if user is Concierge. If true return Concierge user
            } elseif (Concierge::checkStaticToken($userId, $token)) {
                return Concierge::find($userId);
            }
        }

        return $user;
    }

    /**
     * Get user data for persona array for rollbar
     * @param Agent|Concierge|User $user
     * @return array
     */
    private function getUserData($user)
    {
        return [
            'id' => $user->id,
            'username' => $user->firstName . ' ' . $user->lastName,
            'email' => $user->email
        ];
    }
}
