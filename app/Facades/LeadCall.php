<?php

namespace App\Facades;

use App\LeadCall as LeadCallModel;
use App\Services\LeadCallService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Message
 *
 * @method static LeadCallModel create(...$args)
 *
 * @see LeadCallService
 * @package App\Facades
 */
class LeadCall extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return LeadCallService::class;
    }
}
