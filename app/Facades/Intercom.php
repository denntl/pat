<?php

namespace App\Facades;

use App\Services\IntercomService;
use Illuminate\Support\Facades\Facade;
use App\Agent;

/**
 * Class Intercom.
 *
 * @method static void registerUser(Agent $agent)
 * @method static void createOrUpdateUser(Agent $agent)
 * @method static void updateLastSeen(string $agentEmail)
 * @method static boolean updateLeadsMetrics(integer $agentId)
 * @method static boolean updateCallsMetrics(integer $agentId)
 *
 * @see IntercomService
 * @package App\Support\Facades
 */
class Intercom extends Facade
{
    protected static function getFacadeAccessor()
    {
        return IntercomService::class;
    }
}
