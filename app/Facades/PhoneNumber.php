<?php

namespace App\Facades;

use App\Services\PhoneNumberService;
use Illuminate\Support\Facades\Facade;

/**
 * Class PhoneNumber
 *
 * @method static string|null getTwilioPhone(string $phone = null)
 * @method static string|null getNationalPhone(string $phone = null)
 * @method static bool checkIfPhoneValid(string $phone)
 * @method static array mappingPhoneToTimezone(string $phone)
 *
 * @see PhoneNumberService
 * @package App\Facades
 */
class PhoneNumber extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return PhoneNumberService::class;
    }
}
