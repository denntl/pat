<?php

namespace App\Facades;

use App\Services\VikingLeadHelperService;
use Illuminate\Support\Facades\Facade;

/**
 * Class VikingLeadHelper
 *
 * @method static boolean createAttachmentForLead(array $attachment, string $leadId)
 * @method static array getVikingLeadValidationRules()
 * @method static array handleInvalidVikingData(array $data, array $invalidFields)
 * @method static boolean ifLeadCanBeStored(array $invalidFields)
 * @method static void synchronizeLead(array $data, array $logDataForWorkers)
 *
 * @see VikingLeadHelperService
 * @package App\Facades
 */
class VikingLeadHelper extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return VikingLeadHelperService::class;
    }
}
