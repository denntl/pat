<?php

namespace App\Facades;

use App\Message as MessageModel;
use App\Services\MessageService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Message
 *
 * @method static bool updateTwilioStatus(string $id = null, string $status = null)
 * @method static bool updateTwilioStatusByMessageId(string $messageId = null, string $status = null)
 * @method static MessageModel createMessage($data, $automated = false)
 * @method static void processOutboundMessage(MessageModel $message)
 *
 * @see MessageService
 * @package App\Facades
 */
class Message extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return MessageService::class;
    }
}
