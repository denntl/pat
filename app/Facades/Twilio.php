<?php

namespace App\Facades;

use App\Services\TwilioService;
use Illuminate\Support\Facades\Facade;
use Twilio\Rest\Api\V2010\Account\MessageInstance;

/**
 * Class Twilio.
 *
 * @method static bool hasSid(string $given)
 * @method static string createNumber()
 * @method static string generateToken()
 * @method static string getPhoneType(string $phone)
 * @method static string generateNumberByArea(string $areaCode)
 * @method static MessageInstance|bool sendSms(string $to, string $from, string $body, bool $withCallback = true)
 * @method static bool checkMessageStatus(string $status = null)
 * @method static bool validatePhoneNumber(string $phone = null)
 * @method static getPhoneCarrierType(string $phone)
 *
 * @see TwilioService
 * @package App\Support\Facades
 */
class Twilio extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return TwilioService::class;
    }
}
