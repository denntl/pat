<?php

namespace App\Facades;

use App\Agent;
use App\Email;
use App\Lead as LeadModel;
use App\Note;
use App\Services\SendGridService;
use Illuminate\Support\Facades\Facade;

/**
 * Class SendGrid.
 *
 * @method static void sendResetPasswordLink(string $email, string $firstName, string $token)
 * @method static void sendQualifiedEmail(LeadModel $lead, string $note)
 * @method static void sendNoteForAgent(string $leadId, Note $note)
 * @method static void|bool sendLeadEmail(Email $email)
 * @method static void sendLeadToEmails(string $leadId, int $agentId, string $emails)
 * @method static void sendFirstLeadSource(LeadModel $lead)
 * @method static void sendLeadOverageEmail(Agent $agent)
 * @method static bool emailCanBeSend(LeadModel $lead)
 *
 * @see SendGridService
 * @package App\Facades
 */
class SendGrid extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return SendGridService::class;
    }
}
