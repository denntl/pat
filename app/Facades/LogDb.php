<?php

namespace App\Facades;

use App\Services\LogDbService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Facade;

/**
 * Class LogDb.
 *
 * @method static void setParams(array $params)
 * @method static void addParam(string $key, string $param)
 * @method static array getParams()
 * @method static string getParam(string $key)
 * @method static $this setInstance(string $instance)
 * @method static string getInstance()
 * @method static bool save()
 * @method static void logApi(Request $request, string $action, string $responseStatus, array $responseData)
 *
 * @see LogDbService
 * @package App\Facades
 */
class LogDb extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return LogDbService::class;
    }
}
