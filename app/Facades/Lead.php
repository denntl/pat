<?php

namespace App\Facades;

use App\Lead as LeadModel;
use App\Services\LeadService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Lead
 *
 * @method static LeadService processNewLead(LeadModel $lead)
 * @method static string removeAssign(LeadModel $lead)
 * @method static integer|bool assignToRep(LeadModel $lead)
 * @method static boolean isAnotherActiveTierIIRepOnline(int $repId)
 * @method static LeadModel updateRep(string $id = null, int $repId = null)
 * @method static LeadModel updateStatus(string $id = null, string $status = null)
 * @method static LeadModel updateTag(string $id = null, string $tag = null)
 * @method static bool checkStatus(string $status = null)
 * @method static bool checkAction(string $action = null)
 * @method static bool addSource(LeadModel $lead)
 * @method static bool attachAgentViaPatId(LeadModel $lead)
 * @method static array formatVikingLeadData(array $data)
 * @method static int assignActiveLeadsToRep(int $conciergeId)
 * @method static bool leadHasAnsweredLetters(LeadModel $lead)
 * @method static bool leadHasAnsweredMessages(LeadModel $lead)
 * @method static bool leadHasIncomingActivities(LeadModel $lead)
 * @method static createLeadNote(array $data, string $sender = null)
 * @method static bool processAction($lead, $action, $causedByAutomationFlow = false, $notesText = '', $sender = null)
 * @method static bool checkLeadAllowedFlowStatuses(LeadModel $lead)
 *
 * @see LeadService
 * @package App\Facades
 */
class Lead extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return LeadService::class;
    }
}
