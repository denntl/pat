<?php

namespace App\Facades;

use App\Note as NoteModel;
use App\Services\NoteService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Note.
 *
 * @method static NoteModel create(string $type, string $note, string $leadId, int $conciergeId)
 *
 * @package App\Facades
 */
class Note extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return NoteService::class;
    }
}
