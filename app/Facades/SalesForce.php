<?php

namespace App\Facades;

use App\Services\SalesForceService;
use Illuminate\Support\Facades\Facade;
use App\Agent as AgentModel;
use Psr\Http\Message\ResponseInterface;

/**
 * Class SalesForce
 *
 * @method static ResponseInterface|null postAgent(AgentModel $agent)
 *
 * @see SalesForceService
 * @package App\Facades
 */
class SalesForce extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return SalesForceService::class;
    }
}
