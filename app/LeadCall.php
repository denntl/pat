<?php

namespace App;

use App\Facades\PhoneNumber;
use App\Models\AbstractModel as Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class LeadCall
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $twilioCallSid
 * @property string $phone
 * @property integer $agentId
 * @property integer $conciergeId
 * @property integer $isForwarded
 * @property Carbon $createdAt
 * @property Carbon $finishedAt
 * @property Lead $lead
 *
 * @package App
 */
class LeadCall extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leadCalls';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'leadId',
        'twilioCallSid',
        'phone',
        'agentId',
        'conciergeId',
        'isForwarded',
        'createdAt',
        'finishedAt'
    ];

    /**
     * Get the lead from the call
     *
     * @return BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead', 'leadId');
    }

    /**
     * Get the notes for the lead.
     *
     * @return HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Note', 'callId');
    }

    /**
     * Set the call's phone to formatted form.
     *
     * @param string $value
     *
     * @return void
     */
    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = PhoneNumber::getTwilioPhone($value);
    }
}
