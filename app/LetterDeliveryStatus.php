<?php

namespace App;

use App\Models\AbstractModel as Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class LetterDeliveryStatus
 *
 * @property int $id
 * @property string $name
 *
 * @property Collection $leads
 *
 * @package App
 */
class LetterDeliveryStatus extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @inherit bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'letterDeliveryStatuses';

    /**
     * Get lead letter delivery status.
     *
     * @return BelongsToMany
     */
    public function leads()
    {
        return $this->belongsToMany(
            'App\Lead',
            'leadLetterDeliveryStatus',
            'letterDeliveryStatusId',
            'leadId'
        );
    }
}
