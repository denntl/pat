<?php

namespace App;

use App\Models\AbstractModel as Model;

/**
 * Class LogsApi
 *
 * @property integer $id
 * @property string $class
 * @property string $status
 * @property string $data
 *
 * @package App
 */
class LogsJobs extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logsJobs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'class',
        'status',
        'data',
    ];
}
