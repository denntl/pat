<?php

namespace App;

use App\Facades\SendGrid;
use App\Models\AbstractAuthenticatable as Authenticatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * Class Agent.
 *
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $calendly
 * @property string $city
 * @property string $state
 * @property string $company
 * @property string $password
 * @property string $provider
 * @property string $providerId
 * @property string $twilioNumber
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 * @property string $sendGridEmail
 * @property string $personaTeamName
 * @property string $personaName
 * @property string $personaTitle
 * @property boolean $gmailConnected
 * @property string $googleEmailScopeToken
 * @property string $agentologyUUID
 * @property integer $areaCode
 * @property string $notificationPhone
 * @property string $notificationEmail
 * @property string $callForwardingNumber
 * @property string $emailForwarding
 * @property string $extraNoteForRep
 * @property string $vikingPatId
 * @property string $salesForceId
 * @property boolean $vikingGmailParse
 * @property boolean $createdAtViking
 *
 * @property Collection $leads - Lead array collection
 *
 * @package App
 */
class Agent extends Authenticatable
{
    use SoftDeletes;

    public static $snakeAttributes = false;

    const HASH = 'M5OU3i7pll';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETED_AT = 'deletedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agents';

    /**
     * The column name of the "remember me" token.
     *
     * @var string
     */
    protected $rememberTokenName = 'rememberToken';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'password',
        'phone',
        'email',
        'website',
        'calendly',
        'city',
        'state',
        'company',
        'provider',
        'providerId',
        'sendGridEmail',
        'twilioNumber',
        'personaTeamName',
        'personaName',
        'personaTitle',
        'gmailConnected',
        'agentologyUUID',
        'areaCode',
        'notificationPhone',
        'notificationEmail',
        'callForwardingNumber',
        'tokenEmailScopeProvided',
        'emailForwarding',
        'extraNoteForRep',
        'vikingPatId',
        'salesForceId',
        'agentOnBoarding',
        'vikingGmailParse',
        'createdAtViking'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'rememberToken', 'googleEmailScopeToken'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deletedAt'];

    /**
     * Get the leads for the agent.
     *
     * @return HasMany
     */
    public function leads()
    {
        return $this->hasMany('App\Lead', 'agentId');
    }

    /**
     * Get the AgentOnBoarding for the agent.
     *
     * @return HasOne
     */
    public function agentOnBoarding()
    {
        return $this->hasOne('App\AgentOnBoarding', 'agentId');
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return md5($this->id.self::HASH);
    }

    /**
     * Check token.
     *
     * @param string $token
     * @return bool
     */
    public function checkToken($token)
    {
        return $this->getToken() == $token;
    }

    /**
     * Check static token.
     *
     * @param mixed $id
     * @param string $token
     *
     * @return bool
     */
    public static function checkStaticToken($id, $token)
    {
        return md5($id.self::HASH) == $token;
    }

    /**
     * @inheritDoc
     */
    public function sendPasswordResetNotification($token)
    {
        SendGrid::sendResetPasswordLink($this->email, $this->firstName, $token);
    }

    /**
     * TODO check on duplicate after generating this email
     * TODO agent has "sendGridEmail" field, so write this email there
     * Method to generate agent unique email
     *
     * @param string $personaName
     * @param string $teamName
     * @param integer $index
     * @return string
     */
    public static function generateEmailForAgent($personaName, $teamName, $index = 0)
    {
        $name = camel_case($personaName);
        if ($index != 0) {
            $name .= '-' . $index;
        }
        $team = camel_case($teamName);

        $alias = strtolower($name . '@' . $team);

        return $alias . '.' . config('services.sendgrid.email_domain');
    }

    public function getFullName()
    {

        return trim($this->firstName . ' ' . $this->lastName);
    }
}
