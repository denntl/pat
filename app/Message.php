<?php

namespace App;

use App\Models\AbstractModel as Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Message
 *
 * @property integer $id
 * @property string $leadId
 * @property integer $conciergeId
 * @property boolean $incoming
 * @property boolean $isRead
 * @property integer $isAutomatic
 * @property string $from
 * @property string $to
 * @property string $text
 * @property string $twilioMessageSid
 * @property string $twilioMessageStatus
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 *
 * @property Lead $lead
 *
 * @package App
 */
class Message extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'leadId',
        'incoming',
        'isRead',
        'conciergeId',
        'isAutomatic',
        'twilioMessageSid',
        'twilioMessageStatus',
        'from',
        'to',
        'text'
    ];

    /**
     * Get the lead that owns the note.
     *
     * @return BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead', 'leadId');
    }
    /**
     * Scope a query to only outgoing messages.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOutgoing($query)
    {
        return $query->where('incoming', 0);
    }
}
