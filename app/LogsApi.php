<?php

namespace App;

use App\Models\AbstractModel as Model;
use Carbon\Carbon;

/**
 * Class LogsApi
 *
 * @property integer $id
 * @property string $requestUrl
 * @property string $action
 * @property string $requestData
 * @property string $clientIp
 * @property string $responseText
 * @property integer $responseStatus
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 *
 * @package App
 */
class LogsApi extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logsApi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requestUrl',
        'clientIp',
        'action',
        'requestData',
        'responseText',
        'responseStatus'
    ];
}
