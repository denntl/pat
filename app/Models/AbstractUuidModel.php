<?php

namespace App\Models;

use App\Traits\ModelAnnotations;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AbstractUuidModel
 *
 * @package App\Models
 */
abstract class AbstractUuidModel extends Model
{
    use Uuid;
    use ModelAnnotations;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
}
