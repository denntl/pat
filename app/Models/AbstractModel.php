<?php

namespace App\Models;

use App\Traits\ModelAnnotations;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AbstractModel
 *
 * @package App\Models
 */
abstract class AbstractModel extends Model
{
    use ModelAnnotations;
}
