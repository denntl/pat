<?php

namespace App\Models;

use App\Traits\ModelAnnotations;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;

/**
 * Class AbstractAuthenticatable
 *
 * @package App\Models
 */
abstract class AbstractAuthenticatable extends User
{
    use Notifiable;
    use ModelAnnotations;
}
