<?php

namespace App;

use App\Models\AbstractModel as Model;
use App\Scopes\CreatedAtDescScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Note
 *
 * @property string $id
 * @property integer $conciergeId
 * @property integer $callId
 * @property string $leadId
 * @property string $type
 * @property string $note
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 *
 * @package App
 */
class Note extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'note',
        'conciergeId',
        'leadId',
        'callId'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CreatedAtDescScope);
    }

    /**
     * Get the concierge that owns the note.
     *
     * @return BelongsTo
     */
    public function concierge()
    {
        return $this->belongsTo('App\Concierge', 'conciergeId');
    }

    /**
     * Get the lead that owns the note.
     *
     * @return BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead', 'leadId');
    }

    /**
     * Get the call for this note.
     *
     * @return BelongsTo
     */
    public function call()
    {
        return $this->belongsTo('App\LeadCall', 'callId');
    }
}
