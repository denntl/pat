<?php

namespace App\Services\Time\Facades;

use App\Lead;
use App\Services\Time\TimeService;
use Illuminate\Support\Facades\Facade;
use Carbon\Carbon;

/**
 * Class Geo
 *
 * @method static Carbon returnAllowedDelay(Carbon $date)
 * @method static Carbon|null createDelay(int $hours, int $minutes, string $timezone)
 *
 * @see GeoService
 * @package Services\Geo\Facades
 */
class TimeHelper extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return TimeService::class;
    }
}
