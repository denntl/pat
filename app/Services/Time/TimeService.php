<?php

namespace App\Services\Time;

use App\Services\Geo\Facades\GeoHelper;
use Carbon\Carbon;

/**
 * Class TimeService
 * @package App\Services\Time
 */
class TimeService
{
    const EARLIEST_WORKING_HOUR = 8;
    const LATEST_WORKING_HOUR = 21;

    /**
     * @param Carbon $date
     *
     * @return Carbon
     */
    public function returnAllowedDelay(Carbon $date): Carbon
    {
        $hour = $date->hour;

        if ($hour < self::EARLIEST_WORKING_HOUR) {
            $date->hour = self::EARLIEST_WORKING_HOUR;
            $date->minute = 0;
        } elseif ($hour >= self::LATEST_WORKING_HOUR) {
            $date->hour = self::EARLIEST_WORKING_HOUR;
            $date->minute = 0;
            $date->addDay();
        }

        return $date;
    }

    /**
     * Create delay fro selected timezone in hours
     *
     * @param int $hours
     * @param int $minutes
     * @param string|null $timezone
     *
     * @return Carbon|null
     */
    public function createDelay(int $hours, int $minutes = 0, string $timezone = null)
    {
        if (!GeoHelper::isValidAmericanTimezone($timezone)) {
            $timezone = GeoHelper::getDefaultTimezone();
        }
        $delay = Carbon::now()->addHours($hours)->addMinutes($minutes)->timezone($timezone);
        $delay = $this->returnAllowedDelay($delay);


        return $delay;
    }
}
