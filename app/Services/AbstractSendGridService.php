<?php

namespace App\Services;

use App\Agent;
use App\Email;
use App\Lead;
use App\Note;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\UuidFactoryInterface;
use SendGrid;

/**
 * Class AbstractSendGridService
 * @package App\Services
 */
abstract class AbstractSendGridService
{
    /**
     * Api key.
     *
     * @var string
     */
    private $sendGridApiKey;

    /**
     * From email value.
     *
     * @var string
     */
    private $fromEmail;

    /**
     * Templates Ids with names as keys.
     *
     * @var array
     */
    private $templates = [
        'New Note' => 'c48b2e62-4cc0-4710-b79b-4ac5509f5702',
        'Qualified Lead' => '577094af-ecbc-4222-8859-7cf36e39ee01',
        'Forgot Password' => '20d089b3-1dcb-4fc9-aaab-5c520bea8287',
        'New Shared Lead' => 'ed9b163c-12e2-4dac-be8f-cd9f8a41603e',
        'First Source Lead' => 'e55847a7-dbcb-459c-a023-19ce1754ea35',
        'Note Helper' => 'f75b9be2-894f-4c05-8aa8-5c90cccda474',
        'Lead Overage' => '8dacb39b-9753-440e-b04e-3a9f9870a603'
    ];

    /**
     * SendGridService constructor.
     *
     * @param string $sendGridApiKey
     * @param string $mailFrom
     */
    public function __construct(string $sendGridApiKey, string $mailFrom)
    {
        $this->sendGridApiKey = $sendGridApiKey;
        $this->fromEmail = $mailFrom;
    }

    /**
     * Method to send reset password via SendGrid.
     *
     * @param string $email
     * @param string $firstName
     * @param string $token
     */
    public function sendResetPasswordLink(string $email, string $firstName, string $token)
    {
        $subject = "Forgot Your Password?";

        $emailData = $this->prepareEmail($email, $subject);

        $mail = $emailData['mail'];
        $mail->setTemplateId($this->templates['Forgot Password']);

        $to = [];
        $to[] = $emailData['to'];

        $defaultSubstitutions = $this->getDefaultSubstitutions();
        $substitutions = [
            ':agentFirstName' => $firstName,
            ':resetLink' => route('password.reset', $token)
        ] + $defaultSubstitutions;

        $mail->addPersonalization([
            'to' => $to,
            'substitutions' => $substitutions
        ]);

        $this->sendPostEmail($mail);
    }

    /**
     * Send email on change lead status to qualified.
     *
     * @param Lead $lead
     * @param string $note
     */
    public function sendQualifiedEmail(Lead $lead, string $note)
    {
        /** @var $agent Agent */
        $agent = $lead->agent;
        if (!$agent) {
            return;
        }
        $noteText = 'There no notes';

        if (trim($note) != '') {
            $noteText = $note;
        }

        $currentDate = new \DateTime();
        $subject = "New Qualified Lead!";
        $emailData = $this->prepareEmail($agent->email, $subject);
        // TODO add emails from notifications here when notifications
        // TODO save functionality tab in portal will be done
        $to = [];
        $to[] = $emailData['to'];

        $mail = $emailData['mail'];
        $mail->setTemplateId($this->templates['Qualified Lead']);

        $defaultSubstitutions = $this->getDefaultSubstitutions();
        $substitutions = [
            ':agentFirstName' => $agent->firstName,
            ':leadFullName' => $lead->firstName . ' ' . $lead->lastName,
            ':leadType' => $lead->leadType,
            ':leadEmail' => $lead->email,
            ':leadPhone' => $lead->phone,
            ':leadAddress' => $lead->street ?? '-',
            ':leadSource' => '-',
            ':leadCreatedDate' => $lead->createdAt->format('m/d/Y'),
            ':leadLink' => route('portal_lead', ['id' => $lead->id]),
            ':agentNote' => $noteText,
            ':noteCreatedAt' => $currentDate->format('m/d/Y')
        ] + $defaultSubstitutions;

        if ($lead->source && $lead->source->name) {
            $substitutions[':leadSource'] = $lead->source->name;
        }
        $mail->addPersonalization([
            'to' => $to,
            'substitutions' => $substitutions
        ]);

        if (!App::environment('testing')) {
            $this->sendPostEmail($mail);
        }
    }

    /**
     * Send email when concierge in console send note to agent.
     *
     * @param string $leadId
     * @param Note $note
     *
     * @return void
     */
    public function sendNoteForAgent(string $leadId, Note $note)
    {
        $lead = Lead::find($leadId);

        $agent = $lead->agent;
        if (!$agent) {
            return;
        }

        $subject = "You Have a New Note!";
        $emailData = $this->prepareEmail($agent->email, $subject);

        $mail = $emailData['mail'];
        $mail->setTemplateId($this->templates['New Note']);

        $defaultSubstitutions = $this->getDefaultSubstitutions();
        $substitutions = [
            ':leadName' => $lead->firstName . ' ' . $lead->lastName,
            ':leadType' => $lead->leadType,
            ':leadEmail' => $lead->email,
            ':leadPhone' => $lead->phone,
            ':leadAddress' => $lead->street ?? '-',
            ':leadSource' => '-',
            ':leadStatus' => $lead->status,
            ':leadCreatedAt' => $lead->createdAt->format('m/d/Y'),
            ':leadLink' => route('portal_lead', ['id' => $lead->id]),
            ':agentFirstName' => $agent->firstName,
            ':leadNote' => $note->note,
            ':noteCreatedAt' => $note->createdAt->format('m/d/Y'),
        ] + $defaultSubstitutions;

        if ($lead->source && $lead->source->name) {
            $substitutions[':leadSource'] = $lead->source->name;
        }

        // TODO add emails from notifications here when notifications
        // TODO save functionality tab in portal will be done
        $to = [];
        $to[] = $emailData['to'];

        $mail->addPersonalization([
            'to' => $to,
            'substitutions' => $substitutions
        ]);

        $this->sendPostEmail($mail);
    }

    /**
     * Send email on change lead status to qualified.
     *
     * @param Email $email
     *
     * @return void
     */
    public function sendLeadEmail(Email $email)
    {
        $from = new SendGrid\Email($email->conciergeName ?? null, $email->from);
        $subject = $email->subject;
        $to = new SendGrid\Email(null, $email->to);
        $content = new SendGrid\Content('text/html', '<pre>' . $email->text . '</pre>');

        $mail = new SendGrid\Mail($from, $subject, $to, $content);

        $mail->addCustomArg('UUID', $email->UUID);

        $this->sendPostEmail($mail);
    }

    /**
     * Send lead data to emails that agent enter in field send to on portal.
     *
     * @param string $leadId - lead id
     * @param int $agentId - agent id
     * @param string $emails - string emails separated by coma e.g. "test@example.com,test1@example.com"
     *
     * @return void
     */
    public function sendLeadToEmails(string $leadId, int $agentId, string $emails)
    {
        $emailArray = explode(',', $emails);
        $subject = "New shared lead!";

        $emailData = $this->prepareEmail($emailArray[0], $subject);

        $mail = $emailData['mail'];
        $mail->setTemplateId($this->templates['New Shared Lead']);

        $agent = Agent::find($agentId);
        $lead = Lead::find($leadId);
        $htmlNotes = $this->createSendToHtml($lead);
        $defaultSubstitutions = $this->getDefaultSubstitutions();
        $substitutions = $this->createSendToSubs($agent, $lead, $htmlNotes) + $defaultSubstitutions;

        if ($lead->source && $lead->source->name) {
            $substitutions[':leadSource'] = $lead->source->name;
        }

        $to = [];

        foreach ($emailArray as $email) {
            $to[] = new SendGrid\Email(null, $email);
        }

        $mail->addPersonalization([
            'to' => $to,
            'substitutions' => $substitutions
        ]);

        $this->sendPostEmail($mail);
    }

    /**
     * Method to send email to agent on first lead from source.
     *
     * @param Lead $lead
     * @return void
     */
    public function sendFirstLeadSource(Lead $lead)
    {
        $sourceName = $lead->source ? $lead->source->name : '';

        $agent = $lead->agent;
        $subject = 'Your first ' . $sourceName . ' Lead!';

        $emailData = $this->prepareEmail($agent->email, $subject);

        $mail = $emailData['mail'];

        $mail->setTemplateId($this->templates['First Source Lead']);

        // TODO add emails from notifications here when notifications
        // TODO save functionality tab in portal will be done
        $to = [];
        $to[] = $emailData['to'];

        $defaultSubstitutions = $this->getDefaultSubstitutions();
        $substitutions = [
            ':sourceName' => $sourceName,
            ':agentFirstName' => $agent->firstName,
            ':leadLink' => route('portal_lead', ['id' => $lead->id]),
        ] + $defaultSubstitutions;

        $mail->addPersonalization([
            'to' => $to,
            'substitutions' => $substitutions
        ]);

        $this->sendPostEmail($mail);
    }

    /**
     * TODO: @vitaliy Add description
     *
     * @param Agent $agent
     *
     * @return void
     */
    public function sendLeadOverageEmail(Agent $agent)
    {
        $subject = 'So Many Leads!';

        $emailData = $this->prepareEmail($agent->email, $subject);

        /** @var SendGrid\Mail $mail */
        $mail = $emailData['mail'];

        $mail->setTemplateId($this->templates['Lead Overage']);

        // TODO add emails from notifications here when notifications
        // TODO save functionality tab in portal will be done
        $to = [];
        $to[] = $emailData['to'];

        $defaultSubstitutions = $this->getDefaultSubstitutions();
        $substitutions = [
            ':agentFirstName' => $agent->firstName,
            ':upgradeLink' => route('billing_portal'),
            ':sourcePageLink' => route('lead_source_portal'),
        ] + $defaultSubstitutions;

        $mail->addPersonalization([
            'to' => $to,
            'substitutions' => $substitutions
        ]);

        $this->sendPostEmail($mail);
    }

    /**
     * Method to set default substitutions that exists in all templates.
     *
     * @return array
     */
    private function getDefaultSubstitutions(): array
    {
        $uuid = Uuid::uuid1()->toString();

        return [
            ':notificationSettingsLink' => route('settings_portal', [
                'tab' => 'notifications'
            ])
            ,
            ':instagramLink' => 'https://www.instagram.com/agentologyisa/',
            ':facebookLink' => 'https://www.facebook.com/agentologyISA/',
            ':youtubeLink' => 'https://www.youtube.com/channel/UCbmdicfBJrpz_7SlCYjYprg',
            ':linkedinLink' => 'https://www.linkedin.com/company-beta/6615264/',
            ':viewLink' => config('app.url') . '/emails/' . $uuid . '/view'
        ];
    }

    /**
     * Method to create substitutions for sendToEmail.
     *
     * @param Agent $agent
     * @param Lead $lead
     * @param string $htmlNotes
     *
     * @return array
     */
    private function createSendToSubs(Agent $agent, Lead $lead, string $htmlNotes): array
    {
        return [
            ':agentFirstName' => $agent->firstName,
            ':leadFullName' => $lead->firstName . ' ' . $lead->lastName,
            ':leadType' => $lead->leadType,
            ':leadEmail' => $lead->email,
            ':leadPhone' => $lead->phone,
            ':leadAddress' => $lead->street ?? '-',
            ':leadSource' => '-',
            ':leadCreatedDate' => $lead->createdAt->format('m/d/Y'),
            ':leadUpdatedAt' => $lead->updatedAt->format('m/d/Y'),
            ':leadTargetPrice' => $lead->priceRange,
            ':leadTimeFrame' => $lead->timeFrame,
            ':leadBeds' => $lead->bedrooms,
            ':leadBaths' => $lead->bathrooms,
            ':leadMortgageStatus' => $lead->mortgage,
            ':htmlNotes' => $htmlNotes,
            ':notificationSettingsLink' => route('index_portal')
        ];
    }

    /**
     * Create html notes for sendTo email.
     *
     * @param Lead $lead
     *
     * @return string
     */
    private function createSendToHtml(Lead $lead): string
    {
        $notes = $lead->notes->whereNotIn('type', ['Admin Note']);
        $htmlNotes = 'There no notes for this lead yet.';

        if ($notes) {
            $htmlNotes = '';

            foreach ($notes as $note) {
                $htmlNotes .= $this->createNoteForEmail($note);
            }
        }

        return $htmlNotes;
    }

    /**
     * Get additional template to get html code for notes for send to functionality.
     *
     * @param Note $note
     *
     * @return string
     */
    private function createNoteForEmail(Note $note): string
    {
        $sendGrid = new SendGrid($this->sendGridApiKey);

        /** @var SendGrid\Response $sendGridResp */
        $sendGridResp = $sendGrid->client->templates()->_($this->templates['Note Helper'])->get();

        $body = json_decode($sendGridResp->body());

        $firstVersion = $body->versions[0];

        $htmlContent = $firstVersion->html_content;
        $substitutions = [
            ':noteText' => $note->note,
            ':noteType' => $note->type,
            ':noteDate' => $note->createdAt->format('m/d/Y')
        ];

        return $this->injectSubstitutions($htmlContent, $substitutions);
    }

    /**
     * Method to create default mail SendGrid\Mail.
     *
     * @param string $email
     * @param string $subject
     * @param SendGrid\Email|null $from
     * @param string $contentType
     * @param string $content
     *
     * @return array
     */
    private function prepareEmail(
        string $email,
        string $subject,
        SendGrid\Email $from = null,
        string $contentType = 'text/html',
        string $content = 'text'
    ): array {
        if (!$from) {
            $from = new SendGrid\Email(null, $this->fromEmail);
        }

        $mail = new SendGrid\Mail();

        $mail->setFrom($from);
        $mail->setSubject($subject);

        $content = new SendGrid\Content($contentType, $content);
        $mail->addContent($content);

        $to = new SendGrid\Email(null, $email);

        return [
            'mail' => $mail,
            'to' => $to
        ];
    }

    /**
     * Send post email via SendGrid.
     *
     * @param SendGrid\Mail $mail
     *
     * @return void
     */
    private function sendPostEmail(SendGrid\Mail $mail)
    {
        $sendGrid = new SendGrid($this->sendGridApiKey);
        $sendGrid->client->mail()->send()->post($mail);

        $this->putTemplate($mail);
    }

    /**
     * Puts sent email template (with content) to the local storage.
     *
     * @param SendGrid\Mail $mail
     *
     * @return void
     */
    private function putTemplate(SendGrid\Mail $mail)
    {
        // Get template id from email the was sent
        $sendGrid = new SendGrid($this->sendGridApiKey);
        $templateId = $mail->getTemplateId();
        if (!$templateId) {
            return;
        }

        /** @var SendGrid\Response $resp */
        // Get template from SendGrid service by it's id and fetch an html content
        $resp = $sendGrid->client->templates()->_($templateId)->get();
        $body = json_decode($resp->body(), true);
        $version = $body['versions'][0];
        $htmlContent = $version['html_content'];
        $substitutions = $mail->getPersonalizations()[0]['substitutions'];

        $content = $this->injectSubstitutions($htmlContent, $substitutions);

        $viewLink = $substitutions[':viewLink'];
        preg_match('/emails\/(.*)\/view/', $viewLink, $match);

        // Put template with content to storage dir: storage/app/emails
        // TODO: @anybody store templates into: storage/app/emails/{leadId}/{uuid-of-email}.html
        Storage::disk('emails')->put($match[1] . '.html', $content);
    }

    /**
     * Inject substitutions to the html template content.
     *
     * @param string $content
     * @param array $substitutions
     *
     * @return string
     */
    private function injectSubstitutions(string $content, array $substitutions)
    {
        foreach ($substitutions as $tag => $value) {
            $content = preg_replace('/' . $tag . '/', $value, $content);
        }

        return $content;
    }

    /**
     * Check if email can be send
     *
     * @var Lead $lead
     *
     * @return bool
     */
    public function emailCanBeSend(Lead $lead): bool
    {
        $result = false;
        $agent = null;
        $hasLead = false;
        $hasAgent = false;
        $hasEmailFrom = false;
        $hasEmailTo = false;

        if ($lead) {
            $agent = $lead->agent()->first();
            $hasLead = true;
            $hasAgent = $agent != null;
        }

        if ($hasLead && $hasAgent) {
            $hasEmailFrom = $lead->email ?? null;
            $hasEmailTo = $agent->sendGridEmail ?? null;
        }

        $result = $hasLead && $hasAgent && $hasEmailFrom && $hasEmailTo;

        return $result;
    }
}
