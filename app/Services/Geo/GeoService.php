<?php

namespace App\Services\Geo;

use App\Facades\PhoneNumber;
use App\Lead;
use DateTImeZone;

/**
 * Class AbstractSendGridService
 * @package App\Services
 */
class GeoService
{
    /**
     *
     */
    const DEFAULT_TIMEZONE = 'America/Los_Angeles';

    /**
     * Get timezone using lead's data
     *
     * @param Lead $lead
     *
     * @return string
     */
    public function getLeadTimezone(Lead $lead): string
    {
        $timezone = self::DEFAULT_TIMEZONE;

        if ($lead->timezone) {
            $timezone = $lead->timezone;
        } elseif ($lead->city) {
            $address = $lead->city;
            $address .= 'USA';
            $timezone = $this->getTimezoneByAddress($address);
        } elseif ($lead->phone) {
            $timezone = $this->getTimezoneByPhone($lead->phone);
        } elseif ($lead->postalcode) {
            $postalCode = $lead->postalcode . ', USA';
            $timezone = $this->getTimezoneByPostalCode($postalCode);
        }

        if ($this->isValidAmericanTimezone($timezone)) {
            return $timezone;
        }

        return self::DEFAULT_TIMEZONE;
    }

    /**
     * Get timezone by phone number
     *
     * @param string $phone
     *
     * @return string
     */
    public function getTimezoneByPhone(string $phone): string
    {
        $timezone = '';

        $timezones = PhoneNumber::mappingPhoneToTimezone($phone);

        foreach ($timezones as $item) {
            if ($this->isValidAmericanTimezone($item)) {
                $timezone = $item;
                continue;
            }
        }

        return $timezone;
    }

    /**
     * Get timezone by address
     *
     * @param string $address
     *
     *
     * @return string
     */
    public function getTimezoneByAddress(string $address): string
    {
        $timezone = '';

        $coordinates = $this->getCoordinates($address);
        if ($coordinates) {
            $timezone = $this->getTimezoneByCoordinates($coordinates->lat, $coordinates->lng);
        }

        return $timezone;
    }

    /**
     * Get timezone by postal code
     *
     * @param string $postalCode
     *
     * @return string
     */
    public function getTimezoneByPostalCode(string $postalCode): string
    {
        $timezone = '';

        $coordinates = $this->getCoordinates($postalCode);

        if ($coordinates) {
            $timezone = $this->getTimezoneByCoordinates($coordinates->lat, $coordinates->lng);
        }

        return $timezone;
    }

    /**
     * Get coordinates from address
     *
     * @param string $address
     *
     * @return null
     */
    public function getCoordinates(string $address)
    {
        $coordinates = null;

        $response = \GoogleMaps::load('geocoding')
            ->setParam(['address' => $address])
            ->get();
        $responseDecoded = json_decode($response);

        if (!empty($responseDecoded->results[0])) {
            $coordinates = $responseDecoded->results[0]->geometry->location;
        }

        return $coordinates;
    }

    /**
     * Get timezone by coordinates
     *
     * @param string $latitude
     * @param string $longitude
     *
     * @return string
     */
    public function getTimezoneByCoordinates(string $latitude, string $longitude): string
    {
        $result = '';
        $response = \GoogleMaps::load('timezone')->setParam(['location' => $latitude . ',' . $longitude])->get();

        $decode = json_decode($response);

        if (isset($decode->timeZoneId)) {
            $result = $decode->timeZoneId;
        }

        return $result;
    }

    /**
     * Check if timezone is american timezone
     *
     * @param string $timezone
     *
     * @return bool
     */
    public function isValidAmericanTimezone(string $timezone = null): bool
    {
        if ($timezone) {
            return false;
        }

        $allAmericanTimezones = DateTimeZone::listidentifiers(DateTimeZone::PER_COUNTRY, 'US');

        return in_array($timezone, $allAmericanTimezones);
    }

    /**
     * @return string
     */
    public function getDefaultTimezone()
    {
        return self::DEFAULT_TIMEZONE;
    }
}
