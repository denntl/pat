<?php

namespace App\Services\Geo\Facades;

use App\Lead;
use App\Services\Geo\GeoService;
use Illuminate\Support\Facades\Facade;

/**
 * Class Geo
 *
 * @method static string getLeadTimezone(Lead $lead)
 * @method static string isValidAmericanTimezone(string $timezone = null)
 * @method static string getDefaultTimezone()
 *
 * @see GeoService
 * @package Services\Geo\Facades
 */
class GeoHelper extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return GeoService::class;
    }
}
