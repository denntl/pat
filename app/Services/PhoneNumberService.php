<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberToTimeZonesMapper;
use libphonenumber\PhoneNumberUtil;

/**
 * Class PhoneNumberService
 *
 * @package App\Services
 */
class PhoneNumberService
{
    /**
     * @var PhoneNumberUtil
     */
    private $util;

    /**
     * PhoneNumberService constructor.
     */
    public function __construct()
    {
        $this->util = PhoneNumberUtil::getInstance();
    }

    /**
     * Get phone number in E164 format.
     *
     * @example +13334444333
     *
     * @param string|null $phone
     *
     * @return string|null
     */
    public function getTwilioPhone(string $phone = null)
    {
        if ($phone === null) {
            return $phone;
        }

        $phoneNumber = $this->util->parse($phone, 'US');
        $formatted = $this->util->format($phoneNumber, PhoneNumberFormat::E164);

        return $formatted;
    }

    /**
     * Get phone number in national format.
     *
     * @example (333) 333-4444
     *
     * @param string|null $phone
     *
     * @return string|null
     */
    public function getNationalPhone(string $phone = null)
    {
        if (!$phone) {
            return null;
        }

        try {
            $phoneNumber = $this->util->parse($phone, 'US');
            if (!$this->util->isValidNumber($phoneNumber)) {
                return null;
            }

            $formatted = $this->util->format($phoneNumber, PhoneNumberFormat::NATIONAL);
        } catch (NumberParseException $exception) {
            // Info because in case of error we will have a spam on rollbar.
            // Because when concierge edit phone we have live save.
            Log::info($exception);
            return null;
        }

        return $formatted;
    }

    /**
     * Check phone validation
     *
     * @param string $phone
     *
     * @return bool
     */
    public function checkIfPhoneValid(string $phone): bool
    {
        $phoneNumber = $this->util->parse($phone, 'US');

        return $this->util->isValidNumber($phoneNumber);
    }

    /**
     * Get timezone by phone
     *
     * @param string $phone
     *
     * @return array
     */
    public function mappingPhoneToTimezone(string $phone)
    {
        $timezones = [];

        if ($phone) {
            $phoneNumber = $this->util->parse($phone, 'US');
            $timezoneMapper = PhoneNumberToTimeZonesMapper::getInstance();
            $timezones = $timezoneMapper->getTimeZonesForNumber($phoneNumber);
        }

        return $timezones;
    }
}
