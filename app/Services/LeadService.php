<?php

namespace App\Services;

use App\Agent;
use App\Concierge;
use App\Events\Lead\LeadUnassignRep;
use App\Events\Lead\LeadUpdateStatus;
use App\Events\Note\NoteCreate;
use App\Facades\Intercom;
use App\Facades\LogDb;
use App\Facades\SendGrid;
use App\Facades\Twilio;
use App\Jobs\Lead\GoodBadEmailFlow\HandleFlowStep as GoodEmailFlow;
use App\Jobs\Lead\LandlineFlow\HandleFlowStep as LandlineFlow;
use App\Jobs\Lead\SmsFlow\HandleFlowStep as SmsFlow;
use App\Lead;
use App\LeadSource;
use App\Events\Lead\LeadUpdateRep;
use App\LogsJobs;
use App\Note;
use Illuminate\Support\Facades\Redis;

/**
 * Class LeadService.
 * @package App\Services
 */
class LeadService
{
    const REMIND_TO_COMMUNICATE_WITH_LEAD = 12;

    /**
     * Array of fields from wiring that should be transformed.
     * @var array
     */
    private $vikingToPatLeadFields = [
        'firstname' => 'firstName',
        'lastname' => 'lastName',
        'leadtype' => 'leadType',
        'timeframe' => 'timeFrame',
        'gmail_id' => 'gmailId',
        'gmail_thread' => 'gmailThread',
        'user_id' => 'vikingUserId',
        'pat_id' => 'vikingPatId',
        'duplicate_lead_id' => 'duplicateLeadId',
        'phone_carrier_type' => 'phoneCarrierType',
        'price' => 'priceRange'
    ];

    /**
     * Lead model.
     * @var Lead|null
     */
    private $lead;

    /**
     * Lead role.
     * @var string|null
     */
    private $role;

    /**
     * Array of Tier I + Tier II Reps ids that online and active
     * @var array
     */
    private $activeConciergesIds = [];

    /**
     * Array of Tier I Reps ids that online and active
     * @var array
     */
    private $tierIRepsOnlineIds = [];

    /**
     * Array of Tier II Reps ids that online and active
     * @var array
     */
    private $tierIIRepsOnlineIds = [];

    /**
     * Check on lead statuses if automation flow can move on.
     *
     * @param Lead $lead - Lead object
     *
     * @return bool - can continue flow or not
     */
    public function checkLeadAllowedFlowStatuses(Lead $lead): bool
    {
        if (!$lead ||
            $lead->status == Lead::STATUS_UNQUALIFIED ||
            $lead->status == Lead::STATUS_QUALIFIED ||
            $lead->status == Lead::STATUS_NOT_LEAD ||
            $lead->lastAction == Lead::ACTION_CALL_REQUESTED
        ) {
            return false;
        }

        return true;
    }

    /**
     * Process New Lead.
     *
     * @param Lead $lead
     */
    public function processNewLead(Lead $lead)
    {
        $this->lead = $lead;
        $this->setIcon();
        $this->assignToRep($this->lead);
    }

    /**
     * Set appropriate icon.
     */
    private function setIcon()
    {
        $isRealPhoneNumber = Twilio::validatePhoneNumber($this->lead->phone);

        if ($this->lead->phone && $isRealPhoneNumber) {
            $this->lead->icon = ($this->lead->phoneCarrierType == 'landline') ? Lead::ICON_CALL : Lead::ICON_SMS;
        } else {
            $this->lead->icon = $this->lead->email ? Lead::ICON_EMAIL : Lead::ICON_GENERAL;
        }

        $this->lead->save();
    }

    /**
     * Assign Lead to Rep (Concierge).
     *
     * This method runs algorithm which makes auto-assign lead to concierge.
     * It takes an active concierge (by role) with the least number of related leads and assign
     * to him one lead per time.
     *
     * @param Lead $lead - Lead entity
     *
     * @return integer|null
     */
    public function assignToRep(Lead $lead)
    {
        $this->initializeLog();
        LogDb::addParam('data', json_encode($lead->toArray()));
        $this->clearOnlineRepsData();

        $this->lead = $lead;

        $isActiveRepOnline = $this->parseOnlineReps($this->lead->conciergeId);
        if (!$isActiveRepOnline) {
            event(new LeadUnassignRep($lead->id));

            info('Lead ' . $lead->id . ' unassigned from concierge');
            LogDb::addParam('status', 'Lead ' . $lead->id . ' unassigned from concierge');
            LogDb::save();

            return null;
        }

        if (!Lead::isActiveStatus($this->lead->status) && $lead->lastAction != Lead::ACTION_CALL_REQUESTED) {
            LogDb::addParam('status', 'Lead ' . $lead->id . ' is not active');
            LogDb::save();

            return null;
        }

        $activeRepId = $this->getActiveRep();

        $this->clearOnlineRepsData();

        if (!$activeRepId) {
            LogDb::addParam('status', 'There no active reps');
            LogDb::save();

            event(new LeadUnassignRep($lead->id));

            return null;
        }

        LogDb::addParam('status', 'Lead ' . $lead->id . 'assigned to Concierge ' . $activeRepId);
        LogDb::save();

        event(new LeadUpdateRep($lead->id, $activeRepId));

        return $activeRepId;
    }

    /**
     * Initiate log db service.
     *
     * @return void
     */
    private function initializeLog()
    {

        $logInstance = LogDb::getInstance() != ''
            ? LogDb::getInstance()
            : LogsJobs::class;
        $logParams = LogDb::getParams();

        LogDb::setInstance($logInstance);
        LogDb::setParams($logParams);

        $logParams['class'] = $logParams['class'] ?? LogDb::addParam('class', __CLASS__);
    }

    /**
     * Parse online Reps.
     *
     * @param integer|null $repId - Rep (Concierge) id
     *
     * @return boolean - True if another Reps online except $repId
     */
    private function parseOnlineReps($repId = null): bool
    {
        $redisData = json_decode(Redis::get('concierges:online'), true);
        if (!$redisData) {
            info('Any concierge online');

            return false;
        }

        $activeConciergesIds = array_keys($redisData, true);

        $key = array_search($repId, $activeConciergesIds);
        if ($repId && $key !== false) {
            unset($activeConciergesIds[$key]);
        }

        if (!$activeConciergesIds) {
            return false;
        }

        $this->activeConciergesIds = $activeConciergesIds;

        $concierges = Concierge::where('isPaused', false)->get();
        foreach ($concierges as $concierge) {
            if ($concierge->role == Concierge::ROLE_TIER_I) {
                $this->tierIRepsOnlineIds[] = $concierge->id;
            } elseif ($concierge->role == Concierge::ROLE_TIER_II) {
                $this->tierIIRepsOnlineIds[] = $concierge->id;
            }
        }

        return true;
    }

    /**
     * Get active online Rep(Concierge) id.
     *
     * @return integer|null
     */
    private function getActiveRep()
    {
        if (!$this->tierIRepsOnlineIds && !$this->tierIIRepsOnlineIds) {
            return null;
        }

        if ($this->lead->lastAction == Lead::ACTION_CALL_REQUESTED) {
            $this->role = Concierge::ROLE_TIER_II;
            $repsId = $this->tierIIRepsOnlineIds;
        } else {
            $repsId = $this->activeConciergesIds;
        }

        if (count($repsId) == 1) {
            return array_values($repsId)[0];
        }

        /** @var Concierge $activeRep */
        $activeRep = Concierge::activeForAssign($this->role)->whereIn('concierges.id', $repsId)->get()->first();

        return (!($activeRep instanceof Concierge) || $activeRep == null) ? null : $activeRep->id;
    }

    /**
     * Check another active online Tier II Rep(Concierge) except $repId.
     *
     * @param integer $repId - Rep (Concierge) id
     *
     * @return boolean
     */
    public function isAnotherActiveTierIIRepOnline(int $repId): bool
    {
        $isActiveRepOnline = $this->parseOnlineReps($repId);
        if (!$isActiveRepOnline) {
            return false;
        }

        return (bool)$this->tierIIRepsOnlineIds;
    }

    /**
     * Clear online Reps data
     *
     * @return void
     */
    private function clearOnlineRepsData()
    {
        $this->tierIRepsOnlineIds = [];
        $this->tierIIRepsOnlineIds = [];
    }

    /**
     * Remove assigned concierge.
     *
     * @param Lead $lead - Lead entity
     *
     * @return boolean
     */
    public function removeAssign(Lead $lead): bool
    {
        $lead->conciergeId = null;

        return $lead->save();
    }

    /**
     * Update lead status.
     *
     * @param string|null $id
     * @param string|null $status
     *
     * @return Lead
     */
    public function updateStatus(string $id = null, string $status = null): Lead
    {
        if (!$this->checkStatus($status)) {
            throw new \InvalidArgumentException('Incorrect lead status');
        }

        $lead = Lead::findOrFail($id);
        $lead->status = $status;
        $lead->save();

        return $lead;
    }

    /**
     * Update lead rep.
     *
     * @param string|null $leadId
     * @param string|null $repId
     *
     * @return Lead
     */
    public function updateRep(string $leadId = null, string $repId = null): Lead
    {
        $lead = Lead::with('source', 'agent')->find($leadId);

        $lead->conciergeId = $repId;
        $lead->save();

        return $lead;
    }

    /**
     * Check if lead status is a valid status.
     * @param string|null $status
     * @return bool
     */
    public function checkStatus(string $status = null): bool
    {
        switch ($status) {
            case Lead::STATUS_NEW:
            case Lead::STATUS_NOT_LEAD:
            case Lead::STATUS_QUALIFIED:
            case Lead::STATUS_REFERRED:
            case Lead::STATUS_UNQUALIFIED:
                return true;
            default:
                return false;
        }
    }

    /**
     * Check if lead action is a valid action.
     * @param string|null $action
     * @return bool
     */
    public function checkAction(string $action = null): bool
    {
        info($action == Lead::ACTION_INVALID_PHONE_VALID_EMAIL ? 'ok' : 'not_ok');
        switch ($action) {
            case Lead::ACTION_QUALIFY:
            case Lead::ACTION_UNQUALIFY:
            case Lead::ACTION_CALL_REQUESTED:
            case Lead::ACTION_NOT_LEAD:
            case Lead::ACTION_NO_RESPONSE_NEEDED:
            case Lead::ACTION_INVALID_PHONE_VALID_EMAIL:
                return true;
            default:
                return false;
        }
    }

    /**
     * Check if lead tag is a valid status.
     *
     * @param string|null $tag
     *
     * @return bool
     */
    public function checkLeadTag(string $tag = null): bool
    {
        switch ($tag) {
            case Lead::TAG_URGENT:
            case Lead::TAG_AWAITING_RESPONSE:
            case Lead::TAG_ACTION_REQUIRED:
            case Lead::TAG_ONGOING_CHAT:
            case Lead::TAG_ARCHIVE:
            case Lead::TAG_AUTOMATION:
                return true;
            default:
                return false;
        }
    }

    /**
     * Add lead source to lead.
     *
     * @param Lead $lead
     *
     * @return bool
     */
    public function addSource(Lead &$lead): bool
    {
        if (empty($lead->channelwebsite)) {
            return false;
        }

        /** @var LeadSource $leadSource */
        $leadSource = LeadSource::where('name', $lead->channelwebsite)->firstOrCreate([
            'name' => $lead->channelwebsite
        ]);

        $lead->sourceId = $leadSource->id;

        return $lead->saveOrFail();
    }

    /**
     * Attach agent to lead by pat_id.
     *
     * @param Lead $lead
     *
     * @return bool
     */
    public function attachAgentViaPatId($lead)
    {
        $agent = Agent::where('vikingPatId', $lead->vikingPatId)->first();

        if ($agent) {
            if ($agent->leads()->save($lead)) {
                return true;
            }
        }

        return false;
    }

    /**
     *Formatting Viking lead data for PAT lead instance.
     *
     * @param array $data
     *
     * @return array
     */
    public function formatVikingLeadData(array $data): array
    {
        foreach ($this->vikingToPatLeadFields as $oldKey => $newKey) {
            if (isset($data[$oldKey])) {
                $data[$newKey] = $data[$oldKey];
                unset($data[$oldKey]);
            }
        }

        $data['fromViking'] = true;

        return $data;
    }

    /**
     * Assign all active leads to provided concierge
     *
     * @example App\Lead::assignActiveLeadsToRep(2);
     *
     * @param integer $conciergeId
     *
     * @return void
     */
    public static function assignActiveLeadsToRep(int $conciergeId)
    {
        $concierge = Concierge::find($conciergeId);
        $query = Lead::where('status', Lead::STATUS_NEW)->whereNull('conciergeId');

        switch ($concierge->role) {
            case Concierge::ROLE_TIER_I:
                $query->where(function ($query) use ($conciergeId) {
                    $query->where('lastAction', '!=', Lead::ACTION_CALL_REQUESTED)
                        ->orWhereNull('lastAction');
                });
                break;
            case Concierge::ROLE_TIER_II:
                break;
            default:
                return;
        }

        $leads = $query->get();

        if (count($leads) == 0) {
            return;
        }

        foreach ($leads as $lead) {
            event(new LeadUpdateRep($lead->id, $conciergeId));
        }
    }

    /**
     * Check if lead has answered any letter
     *
     * @param Lead $lead
     *
     * @return bool
     */
    public function leadHasAnsweredLetters(Lead $lead): bool
    {
        return $lead->emails()->where('incoming', 1)->count() > 0;
    }

    /**
     * Check if lead has answered any messages
     *
     * @param Lead $lead
     *
     * @return bool
     */
    public function leadHasAnsweredMessages(Lead $lead): bool
    {
        return $lead->messages->where('incoming', true)->count() > 0;
    }

    /**
     * Check if lead has incoming emails or messages
     *
     * @param Lead $lead
     *
     * @return bool - if lead has incoming messages or emails
     */
    public function leadHasIncomingActivities($lead)
    {
        return $this->leadHasAnsweredLetters($lead) || $this->leadHasAnsweredMessages($lead);
    }

    /**
     * Change lead status to unqualified
     *
     * @param array $data
     * @param string $sender
     *
     * @return Note|null
     */
    public function createLeadNote(array $data, string $sender = null)
    {
        $note = null;
        $hasLeadId = $data['leadId'] ?? null;
        $notEmptyText = isset($data['note']) && trim($data['note']);

        if ($hasLeadId && $notEmptyText) {
            $note = Note::create($data);

            if ($note) {
                event(new NoteCreate($note, $sender));
            }
        }

        return $note;
    }

    /**
     * Process action on lead
     *
     * @param Lead $lead
     * @param string $action
     * @param boolean $causedByAutomationFlow
     * @param string $notesText
     * @param string $sender
     *
     * @return bool
     */
    public function processAction(
        Lead $lead,
        string $action,
        bool $causedByAutomationFlow = false,
        string $notesText = '',
        string $sender = null
    ): bool {

        $result = false;
        $lead->lastAction = $action;

        switch ($action) {
            case Lead::ACTION_QUALIFY:
                $lead->tag = Lead::TAG_ARCHIVE;
                $lead->icon = Lead::ICON_ARCHIVE;
                $lead->status = Lead::STATUS_QUALIFIED;

                $result = $lead->save();

                if ($result) {
                    SendGrid::sendQualifiedEmail($lead, $notesText);
                }

                break;
            case Lead::ACTION_UNQUALIFY:
                $lead->tag = Lead::TAG_ARCHIVE;
                $lead->icon = Lead::ICON_ARCHIVE;
                $lead->status = Lead::STATUS_UNQUALIFIED;

                $result = $lead->save();

                if ($result && $causedByAutomationFlow) {
                    event(new LeadUnassignRep($lead->id, $sender));
                }
                break;
            case Lead::ACTION_CALL_REQUESTED:
                $lead->tag = $causedByAutomationFlow ? Lead::TAG_ACTION_REQUIRED : Lead::TAG_AWAITING_RESPONSE;
                $lead->icon = Lead::ICON_CALL;

                $result = $lead->save();

                if ($result && !Concierge::checkTierIIRole($lead->conciergeId)) {
                    $this->assignToRep($lead);
                }
                break;
            case Lead::ACTION_NOT_LEAD:
                $lead->tag = Lead::TAG_ARCHIVE;
                $lead->icon = Lead::ICON_ARCHIVE;
                $lead->status = LEAD::STATUS_NOT_LEAD;

                $result = $lead->save();

                if ($result) {
                    event(new LeadUnassignRep($lead->id, $sender));
                }
                break;
            case Lead::ACTION_INVALID_PHONE_VALID_EMAIL:
                $lead->tag = Lead::TAG_ONGOING_CHAT;
                $lead->icon = Lead::ICON_EMAIL;

                $result = $lead->save();

                if ($result) {
                    dispatch(new GoodEmailFlow($lead));
                }
                break;
            case Lead::ACTION_CONTINUE_LANDLINE_AUTOMATION:
                $lead->tag = Lead::TAG_ONGOING_CHAT;
                $lead->icon = Lead::ICON_EMAIL;

                $result = $lead->save();

                if ($result) {
                    dispatch(new LandlineFlow($lead));
                }
                break;
            case Lead::ACTION_NO_RESPONSE_NEEDED:
                $lead->tag = Lead::TAG_ARCHIVE;
                $lead->icon = Lead::ICON_ARCHIVE;

                $result = $lead->save();

                if ($result) {
                    event(new LeadUnassignRep($lead->id, $sender));
                }
                break;
            case Lead::ACTION_CONTINUE_SMS_AUTOMATION:
                // Unpause sms flow
                $lead->isPaused = false;
                $result = $lead->save();

                if ($result) {
                    $job = (new SmsFlow($lead->id));
                    dispatch($job);
                }
                break;
            default:
                break;
        }

        if ($result) {
            event(new LeadUpdateStatus($lead, $sender));

            if ($lead->agentId) {
                Intercom::updateLeadsMetrics($lead->agentId);
            }
        }

        return $result;
    }
}
