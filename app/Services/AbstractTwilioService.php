<?php

namespace App\Services;

use App\Facades\PhoneNumber;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Exceptions\RestException;
use Twilio\Jwt\ClientToken;
use Twilio\Rest\Api\V2010\Account\MessageInstance;
use Twilio\Rest\Client;

/**
 * Class AbstractTwilioService.
 *
 * @package App\Services
 */
abstract class AbstractTwilioService
{
    const INFO_TYPE_CARRIER = 'carrier';
    const INFO_TYPE_CALLER = 'caller-name';

    /**
     * Twilio SDK client.
     *
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    private $sid;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $appSid;

    /**
     * Twilio constructor.
     *
     * @param string $sid
     * @param string $token
     * @param string $appSid
     *
     * @throws TwilioException
     */
    public function __construct(string $sid = null, string $token = null, string $appSid = null)
    {
        if (!$sid || !$token || !$appSid) {
            throw new TwilioException('Could not initialize Twilio service');
        }

        $this->sid = $sid;
        $this->token = $token;
        $this->appSid = $appSid;

        try {
            $this->client = new Client($this->sid, $this->token);
        } catch (ConfigurationException $exception) {
            throw new TwilioException('Could not initialize Twilio client');
        }
    }

    /**
     * Send SMS.
     *
     * @param string $to
     * @param string $from
     * @param string $body
     * @param bool $withCallback
     *
     * @return MessageInstance|false
     */
    public function sendSms(string $to, string $from, string $body, bool $withCallback = true)
    {
        $parameters = [
            'from' => $from,
            'body' => $body
        ];

        if ($withCallback) {
            $callback = route('twilio_message_status_updated_hook');
            // Use ngrok for local tests:
            // $callback = 'http://b97931f6.ngrok.io/api/v1/twilio/messages/status-updated';

            $parameters['statusCallback'] = $callback;
        }

        $resp = $this->client->messages->create(PhoneNumber::getTwilioPhone($to), $parameters);

        return $resp;
    }

    /**
     * Generate token for outgoing calls.
     *
     * @link https://www.twilio.com/docs/api/client/capability-tokens
     *
     * @return string
     */
    public function generateToken(): string
    {
        $capability = new ClientToken($this->sid, $this->token);
        $capability->allowClientOutgoing($this->appSid);

        return $capability->generateToken();
    }

    /**
     * Checks if the given argument is Twilio account sid.
     *
     * @param string $given
     *
     * @return bool
     */
    public function hasSid(string $given): bool
    {
        return $given === $this->sid;
    }

    /**
     * Method to generate random phone number by areaCode.
     *
     * @param string $areaCode
     *
     * @return string|bool
     */
    public function generateNumberByArea(string $areaCode)
    {
        $numbers = $this->client->availablePhoneNumbers('US')->local->read([
            'areaCode' => $areaCode,
        ]);

        shuffle($numbers);

        if (!$numbers) {
            return false;
        }

        return $this->getNumber($numbers);
    }

    /**
     * Create twilio phone number.
     *
     * @link https://www.twilio.com/docs/api/rest/available-phone-numbers
     *
     * @return string
     */
    public function createNumber(): string
    {
        $numbers = $this->client->availablePhoneNumbers('US')->local->read();

        return $this->getNumber($numbers);
    }

    /**
     * Update the number as a IncomingPhoneNumberInstance.
     *
     * @param array $numbers
     *
     * @return string
     */
    private function getNumber(array $numbers): string
    {
        $number = $this->client->incomingPhoneNumbers->create([
            'phoneNumber' => $numbers[0]->phoneNumber
        ]);

        $number->update([
            'voiceApplicationSid' => $this->appSid,
            'smsApplicationSid' => $this->appSid,
        ]);

        return $number->phoneNumber;
    }
}
