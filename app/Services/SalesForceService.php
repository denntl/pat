<?php
// LEAVE THIS CODE TILL PAT-189 is done
namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Agent;
use Illuminate\Support\Facades\Cache;
use Psr\Http\Message\ResponseInterface;

/**
 * Class SalesForceService.
 * @package App\Services
 *
 * @deprecated
 */
class SalesForceService
{

    const ROOT_OBJECTS_URL = '/services/data/v20.0/sobjects';
    const CONTACT_OBJECTS_URL = self::ROOT_OBJECTS_URL . '/Contact';
    /**
     * @var string
     */
    private $client;

    private $url;

    private $token;


    /**
     * SalesForceService constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
        $this->client = new Client();

        $this->setTokenForRestAPI();
    }

    /**
     * Get token for SalesForce REST API.
     */
    private function getToken()
    {
        // TODO: CHANGE FOR REAL DYNAMIC TOKEN; ALSO CHANGE DEPENDING ON PROVIDED CRED
        $token = '';

        return $token;
    }

    private function setTokenForRestAPI()
    {
        $token = Cache::get('salesForce');

        if (!$token) {
            $token = $this->getToken();
            $this->saveTokenInCache($token);
        }

        $this->token = $token;
    }

    private function saveTokenInCache($token)
    {
        Cache::forever('salesForce', $token);
    }

    /**
     * Post agent to SalesForce.
     * @param Agent $agent
     *
     * @return ResponseInterface
     */
    public function postAgent(Agent $agent)
    {
        $requestBody = [
            'firstName' => $agent->firstName,
            'lastName' => $agent->lastName,
            'email' => $agent->email,
            'phone' => $agent->phone,
        ];

        try {
            $res = $this->client->request('POST', $this->url . self::CONTACT_OBJECTS_URL, [
                'headers' => [
                    'authorization' => 'Bearer ' . $this->token,
                ],
                'json' => $requestBody
            ]);
        } catch (ClientException $exception) {
            $res = $exception->getResponse();
        }

        return $res;
    }
}
