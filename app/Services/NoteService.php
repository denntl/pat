<?php

namespace App\Services;

use App\Note;

/**
 * Class NoteService.
 * @package App\Services
 */
class NoteService
{
    /**
     * Create new note.
     *
     * @param string|null $type
     * @param string|null $note
     * @param string|null $leadId
     * @param int|null $conciergeId
     *
     * @return Note
     */
    public function create(
        string $type = null,
        string $note = null,
        string $leadId = null,
        int $conciergeId = null
    ) : Note {
        $note = new Note(compact('type', 'note', 'leadId', 'conciergeId'));
        $note->saveOrFail();

        return $note;
    }
}
