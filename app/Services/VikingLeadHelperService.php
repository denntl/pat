<?php

namespace App\Services;

use App\Agent;
use App\Events\Lead\LeadCreate;
use App\Facades\LogDb;
use App\Jobs\CreateAttachmentForLead;
use App\Lead;
use App\Attachment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use TonchikTm\PdfToHtml\Pdf;
use App\Facades\Lead as LeadFacade;

/**
 * Class LeadService.
 * @package App\Services
 */
class VikingLeadHelperService
{
    /**
     * @var array
     */
    private $requiredLeadFields = [
        'id',
        'pat_id'
    ];

    /**
     * Method to create record in database with original email from viking.
     *
     * @param array $attachment
     * @param string $leadId
     *
     * @return boolean
     */
    public function createAttachmentForLead(array $attachment, string $leadId): bool
    {
        if (empty($attachment['location'])) {
            return false;
        }

        /** @var Attachment $leadVikingEmail */
        $leadVikingEmail = Attachment::find($attachment['id']);
        if (!$leadVikingEmail) {
            $leadVikingEmail = new Attachment();

            $leadVikingEmail->id = $attachment['id'];
            $leadVikingEmail->createdAt = $attachment['created_at']['date'];

            $content = $this->getEmailContent($attachment['location'], $attachment['id']);

            $leadVikingEmail->content = $content;

            if ($content === null) {
                return false;
            }
        }

        $leadVikingEmail->leadId = $leadId;
        $leadVikingEmail->location = $attachment['location'];
        $leadVikingEmail->updatedAt = $attachment['updated_at']['date'];

        $leadVikingEmail->save();

        // TODO: add event 'attachement create/update' here

        return true;
    }

    /**
     * Returns validation rules for Lead from Viking.
     *
     * @return array - validation rules
     */
    public function getVikingLeadValidationRules(): array
    {
        $validationRules = [
            'id' => 'bail|uuid|required',
            'pat_id' => 'bail|uuid|required',
            'user_id' => 'uuid|nullable',
            'duplicate_lead_id' => 'uuid|nullable',
            'email' => 'email|nullable',
            'leadtype' => ['nullable',
                Rule::in(['seller', 'buyer', 'Seller', 'Buyer']),
            ],
            'phone_carrier_type' => ['nullable',
                Rule::in([
                    'voip',
                    'mobile',
                    'landline'
                ]),
            ],
            'timeframe' => ['nullable',
                Rule::in([
                    'Asap',
                    'Within 3 months',
                    'I\'m not sure',
                    '0-1 months',
                    '3-6 months',
                    '6 months or longer',
                    '6-12 months',
                    '12+ months'
                ])
            ],
            'mortgage' => ['nullable',
                Rule::in([
                    'Pre-Qualified',
                    'Pre-Approved',
                    'Haven`t Applied Yet',
                    'Cash Buyer',
                    'I don`t know',
                    'Not Applicable'
                ]),
            ],
            'price' => 'price_range|nullable',
            'bedrooms' => 'numeric|nullable',
            'bathrooms' => 'numeric|nullable'
        ];

        return $validationRules;
    }

    /**
     * // TODO: Split this method to several simple methods
     *
     * Method to get Email content from pdf.
     *
     * @param string $location
     * @param string $attachmentId
     *
     * @return string|null
     */
    private function getEmailContent(string $location, string $attachmentId)
    {
        $fileName = 'temp-pdf-' . time() . '.pdf';
        $tempFile = sys_get_temp_dir() . '/' . $fileName;

        copy($location, $tempFile);

        $pdf = $this->newPdf($tempFile);

        try {
            $pdf->getHtml();
        } catch (\Exception $e) {
            // This code mean that all the time we wait for error here.
        } finally {
            $dest = storage_path() . '/app/public/attachment-' . $attachmentId;
            if (!file_exists($dest)) {
                mkdir($dest, 0755, true);
            }

            $source = $pdf->getOutputDir();
            $skipIterators = new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS);
            $SELF_FIRST = \RecursiveIteratorIterator::SELF_FIRST;
            $willOverride = [];
            $existsHtml = false;

            foreach ($iterator = new \RecursiveIteratorIterator($skipIterators, $SELF_FIRST) as $item) {
                if ($item->isDir()) {
                    mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
                } else {
                    copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());

                    $itemFilename = $item->getFileName();
                    if (substr($itemFilename, -4) == '.png') {
                        $willOverride[] = $itemFilename;
                    }

                    if (substr($itemFilename, -5) == '.html') {
                        $existsHtml = true;
                    }
                }
            }

            if (!$existsHtml) {
                return null;
            }

            $htmlFilename = $pdf->getOutputDir() . '/' . preg_replace('/\.pdf$/i', '', $fileName);
            $content = file_get_contents($htmlFilename . '-html.html');

            foreach ($willOverride as $itemFilename) {
                $replace = '/storage/attachment-' . $attachmentId . '/' . $itemFilename;
                $content = str_replace($itemFilename, $replace, $content);
            }

            $content = str_replace('font-family:Times;', '', $content);
            // TODO: remove other exists files at dist dir
        }

        return $content;
    }

    /**
     * Create new Pdf instance.
     *
     * @param string $temp
     *
     * @return Pdf
     */
    private function newPdf(string $temp): Pdf
    {
        $pdf = new Pdf($temp, [
            'pdftohtml_path' => config('services.pdf.html_path'),
            'pdfinfo_path' => config('services.pdf.info_path'),
            // Settings for generating HTML
            'generate' => [
                // We do not want separate pages
                'singlePage' => true,
                // Only .PNG images
                'imageJpeg' => false,
                // We need images
                'ignoreImages' => false,
                // We do not want separate pages
                'noFrames' => true,
            ],
            'clearAfter' => true,
            'removeOutputDir' => false,
            'outputDir' => '/tmp/' . uniqid(),
            // Settings for processing HTML
            'html' => [
                // Use inline CSS rules instead of CSS classes
                'inlineCss' => true,
                // Search for images in HTML and replace 'src' attribute to base64 hash
                'inlineImages' => true,
                // Take only <body> content from HTML
                'onlyContent' => true,
            ]
        ]);

        return $pdf;
    }


    /**
     * Set invalid viking fields to null.
     *
     * @param array $data - Viking lead data
     * @param array $invalidFields - invalid Viking data keys
     *
     * @return array - transformed Viking lead data
     */
    public function handleInvalidVikingData(array $data, array $invalidFields): array
    {
        $result = [];
        if ($this->ifLeadCanBeStored($invalidFields)) {
            foreach ($invalidFields as $item) {
                $data[$item] = null;
            }

            $result = $data;
        }

        return $result;
    }

    /**
     * Check if viking lead can be stored.
     *
     * @param array $invalidFields - invalid Viking lead data keys
     *
     * @return bool - if lead can be stored to PAT
     */
    public function ifLeadCanBeStored(array $invalidFields): bool
    {
        foreach ($this->requiredLeadFields as $field) {
            if (in_array($field, $invalidFields)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Synchronize lead between Viking and Pat.
     *
     * @param array $data
     * @param array $logDataForWorkers
     *
     * @return void
     */
    public function synchronizeLead(array $data, array $logDataForWorkers)
    {
        $logDataForWorkers['params']['responseData'] = json_encode($data);
        $validateVikingLead = $this->validateVikingLead($data);

        if (!$validateVikingLead['success']) {
            // Returns empty array if lead can't be stored
            $data = $this->handleInvalidVikingData($data, $validateVikingLead['data']);
        }

        if (!$data) {
            // Here we have a spam of log. We do not need this
            return;
        }

        $formattedLeadData = LeadFacade::formatVikingLeadData($data);

        $noAgents = 0;
        $agents = Agent::where('vikingPatId', $formattedLeadData['vikingPatId']);
        if ($agents->count() == $noAgents) {
            // Here we also should not log error.
            return;
        }

        $this->saveLogToDB($logDataForWorkers);

        $this->processLead($formattedLeadData);
    }

    /**
     * Save logs to db.
     *
     * @param $logData
     *
     * @return void
     */
    private function saveLogToDB(array $logData)
    {
        LogDb::setInstance($logData['instance']);
        LogDb::setParams($logData['params']);
        LogDb::save();
    }

    /**
     * Validate Viking lead data.
     *
     * @param array $data - Viking lead data
     *
     * @return array
     */
    private function validateVikingLead(array $data): array
    {
        $result = [];
        $result['success'] = true;
        $result['data'] = [];

        $validator = Validator::make($data, $this->getVikingLeadValidationRules());

        if ($validator->fails()) {
            $errors = $validator->errors()->messages();

            $result['success'] = false;
            $result['data'] = array_keys($errors);
        }

        return $result;
    }

    /**
     * Process lead data to update or insert lead from Viking.
     *
     * @param array $data
     */
    private function processLead(array $data)
    {
        $leadId = $data['id'];
        $lead = Lead::find($leadId);
        $attachments = $data['attachments'];

        if (!$lead) {
            $lead = new Lead();

            $lead->id = $leadId;
            $lead->status = Lead::STATUS_NEW;
            $lead->tag = Lead::TAG_URGENT;

            $lead->fill($data);
            $lead->save();

            LeadFacade::attachAgentViaPatId($lead);
            LeadFacade::addSource($lead);

            LeadFacade::processNewLead($lead);
            event(new LeadCreate($lead));
        }

        if ($attachments) {
            $jobAttachment = (new CreateAttachmentForLead($attachments, $leadId));
            dispatch($jobAttachment->onQueue('sync-attachments'));
        }
    }
}
