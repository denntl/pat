<?php

namespace App\Services\Api\FakeRequest;

use App\Email;
use App\Services\Api\BaseApiService;
use Illuminate\Http\Request;
use Psr\Http\Message\ResponseInterface;

/**
 * Class FakeRequestService.
 * @package App\Services\Api\FakeRequest
 */
class FakeRequestService extends BaseApiService
{
    /**
     * FakeRequestService constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->url = url('/');
    }


    /**
     * Makes fake request to API to update email status
     *
     * @param Email $email
     *
     * @return null|ResponseInterface
     */
    public function postEmailStatuses(Email $email)
    {
        $url = $this->url . '/api/v1/sendGrid/letterStatus?key=' . config('services.sendgrid.webhook_secret');

        $requestBody = [
            [
                "sg_message_id" => "sendgrid_internal_message_id",
                "email" => $email->from,
                "timestamp" => "1337966815",
                "newuser" => "1337966815",
                "smtp-id" => "<20120525181309.C1A9B40405B3@Example-Mac.local>",
                "event" => "delivered",
                "url" => "https://sendgrid.com",
                "UUID" => $email->UUID
            ]
        ];

        $request = $this->makeRequest(Request::METHOD_POST, $url, $requestBody);

        return $request;
    }
}
