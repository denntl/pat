<?php

namespace App\Services\Api\FakeRequest\Facades;

use App\Services\Api\FakeRequest\FakeRequestService;
use Illuminate\Support\Facades\Facade;
use App\Email;
use Psr\Http\Message\ResponseInterface;

/**
 * Class FakeRequest
 *
 * @method static ResponseInterface|null postEmailStatuses(Email $email)
 *
 * @see FakeRequestService
 * @package Services\Api\FakeRequest\Facades
 */
class FakeRequest extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return FakeRequestService::class;
    }
}
