<?php

namespace App\Services\Api\FakeRequest\Providers;

use App\Services\Api\FakeRequest\FakeRequestService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FakeRequestServiceProvider.
 * @package App\Services\Api\FakeRequest\Providers
 */
class FakeRequestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FakeRequestService::class, function () {
            return new FakeRequestService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [FakeRequestService::class];
    }
}
