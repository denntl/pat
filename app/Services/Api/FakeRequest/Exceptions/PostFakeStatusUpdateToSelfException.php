<?php

namespace App\Services\Api\Viking\Exceptions;

use App\Services\Api\BaseApiServiceResponseFailedException;
use stdClass;

/**
 * Class PostAgentToVikingException
 *
 * @package App\Services\Api\Viking\Exceptions\Viking
 */
class PostFakeStatusUpdateToSelfException extends BaseApiServiceResponseFailedException
{
    public function __construct(stdClass $responseObj = null)
    {
        $message = 'Failed to make fake request';
        parent::__construct($responseObj, $message);
    }
}
