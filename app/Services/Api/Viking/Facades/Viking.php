<?php

namespace App\Services\Api\Viking\Facades;

use App\Services\Api\Viking\VikingService;
use Illuminate\Support\Facades\Facade;
use App\Agent as AgentModel;
use stdClass;

/**
 * Class Viking
 *
 * @method static stdClass|null getSingleAgentData(string $vikingPatId)
 * @method static stdClass|null getMultipleAgentsData(integer $page)
 * @method static stdClass|null sendNewAgent(AgentModel $agent)
 * @method static stdClass|null updateExistingAgent(AgentModel $agent)
 * @method static bool removeAgent(string $patId)
 * @method static bool disconnectGmailFromAgent(AgentModel $agent)
 *
 * @see VikingService
 * @package Services\Api\Viking\Facades
 */
class Viking extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return VikingService::class;
    }
}
