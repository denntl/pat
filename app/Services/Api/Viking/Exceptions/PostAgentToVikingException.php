<?php
namespace App\Services\Api\Viking\Exceptions;

use App\Services\Api\BaseApiServiceResponseFailedException;
use stdClass;

/**
 * Class PostAgentToVikingException
 *
 * @package App\Services\Api\Viking\Exceptions\Viking
 */
class PostAgentToVikingException extends BaseApiServiceResponseFailedException
{
    public function __construct(stdClass $responseObj = null)
    {
        $message = 'Failed to post new agent to Viking';
        parent::__construct($responseObj, $message);
    }
}
