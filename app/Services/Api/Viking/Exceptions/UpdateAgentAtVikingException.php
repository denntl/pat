<?php
namespace App\Services\Api\Viking\Exceptions;

use App\Services\Api\BaseApiServiceResponseFailedException;
use stdClass;

/**
 * Class PostAgentToVikingException
 *
 * @package App\Services\Api\Viking\Exceptions
 */
class UpdateAgentAtVikingException extends BaseApiServiceResponseFailedException
{
    public function __construct(stdClass $responseObj = null)
    {
        $message = 'Failed to update agent at Viking';
        parent::__construct($responseObj, $message);
    }
}
