<?php

namespace App\Services\Api\Viking\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Api\Viking\VikingService;

/**
 * Class VikingServiceProvider.
 * @package App\Services\Api\Viking\Providers
 */
class VikingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(VikingService::class, function () {
            $url = 'https://' . config('services.viking.domain') . '/api';

            return new VikingService(
                config('services.viking.auth_bearer'),
                $url
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [VikingService::class];
    }
}
