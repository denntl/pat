<?php

namespace App\Services\Api\Viking;

use App\Agent;
use App\Services\Api\BaseApiService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;
use stdClass;

/**
 * Class VikingService.
 * @package App\Services\Api\Viking
 */
class VikingService extends BaseApiService
{
    /**
     * @var string
     */
    private $token;

    /**
     * VikingService constructor.
     *
     * @param  string $token
     * @param  string $url
     */
    public function __construct(string $token, string $url)
    {
        parent::__construct();
        $this->token = $token;
        $this->url = $url;
    }

    /**
     * Send post request for Viking to create agent.
     *
     * @param array $requestBody
     *
     * @return ResponseInterface|null
     */
    protected function postAgent($requestBody)
    {
        $res = null;

        if ($requestBody) {
            $method = Request::METHOD_POST;
            $url = $this->url . '/users';
            $res = $this->makeRequest($method, $url, $requestBody, $this->token);
        }

        return $res;
    }

    /**
     * Send put request for Viking to update.
     *
     * @param string $vikingPatId
     * @param array $requestBody
     *
     * @return ResponseInterface|null
     */
    protected function putAgent(string $vikingPatId, array $requestBody)
    {
        $method = Request::METHOD_PUT;
        $url = $this->url . '/users/' . $vikingPatId;

        $res = $this->makeRequest($method, $url, $requestBody, $this->token);

        return $res;
    }

    /**
     * Send get request for Viking to get single agent.
     *
     * @param string $vikingPatId
     *
     * @return ResponseInterface|null
     */
    protected function getAgent(string $vikingPatId)
    {
        $method = Request::METHOD_GET;
        $url = $this->url . '/users/' . $vikingPatId;
        $res = $this->makeRequest($method, $url, [], $this->token);

        return $res;
    }


    /**
     * Send get request for Viking to get agents.
     *
     * @param int $page
     *
     * @return ResponseInterface|null
     */
    protected function getAgentsList(int $page = 1)
    {
        $method = Request::METHOD_GET;
        $url = $this->url . '/users?page=' . $page;
        $res = $this->makeRequest($method, $url, [], $this->token);

        return $res;
    }

    /**
     * Send delete request for Viking to delete agent.
     *
     * @param string $vikingPatId
     *
     * @return ResponseInterface|null
     */
    protected function deleteAgent(string $vikingPatId)
    {
        $method = Request::METHOD_DELETE;
        $url = $this->url . '/users/' . $vikingPatId;
        $res = $this->makeRequest($method, $url, [], $this->token);

        return $res;
    }

    /**
     * Send agent data to Viking API.
     *
     * @param Agent $agent
     *
     * @return stdClass|null
     */
    public function sendNewAgent(Agent $agent)
    {
        $requestBody = [
            'email' => $agent->email,
            'pat_email_alias' => $agent->emailForwarding,
            'active' => $agent->vikingGmailParse,
            'pat_id' => $agent->vikingPatId
        ];

        $res = $this->postAgent($requestBody);

        $responseObject = $this->getFormattedResponseBody($res);

        if ($responseObject) {
            $responseObject->statusCode = $res->getStatusCode();
        }

        return $responseObject;
    }

    /**
     * Update agent data at Viking API.
     *
     * @param Agent $agent
     *
     * @return stdClass|null
     */
    public function updateExistingAgent(Agent $agent)
    {
        $requestBody = [
            'sfid' => $agent->salesForceId,
            'email' => $agent->email,
            'pat_email_alias' => $agent->emailForwarding,
            'active' => true,
            'pat_id' => $agent->vikingPatId
        ];

        $res = $this->putAgent($agent->vikingPatId, $requestBody);
        $responseObject = $this->getFormattedResponseBody($res);

        if ($responseObject) {
            $responseObject->statusCode = $res->getStatusCode();
        }

        return $responseObject;
    }

    /**
     * Get agent data from Viking API by pat id.
     *
     * @param string $vikingPatId
     *
     * @return stdClass|null
     */
    public function getSingleAgentData(string $vikingPatId)
    {
        $res = $this->getAgent($vikingPatId);

        return $this->getFormattedResponseBody($res);
    }

    /**
     * Get agents list with pagination from Viking.
     *
     * @param int $page
     *
     * @return stdClass|null
     */
    public function getMultipleAgentsData(int $page = 1)
    {
        $data = null;

        $res = $this->getAgentsList($page);


        return $this->getFormattedResponseBody($res);
    }

    public function removeAgent($vikingPatId)
    {
        $res = $this->deleteAgent($vikingPatId);

        return $res->getStatusCode() == Response::HTTP_NO_CONTENT;
    }

    /**
     * Disconnect gmail account from viking
     *
     * @param Agent $agent - Agent model instance
     *
     * @return bool
     */
    public function disconnectGmailFromAgent(Agent $agent): bool
    {
        $method = Request::METHOD_POST;
        $url = $this->url . '/callback/google/disconnect';
        $body = [
            'pat_id' => $agent->vikingPatId,
        ];

        $res = $this->makeRequest($method, $url, $body);

        $responseBody = $this->getFormattedResponseBody($res);

        if ($responseBody->response->success) {
            return true;
        }

        return false;
    }
}
