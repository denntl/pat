<?php

namespace App\Services\Api\Agentology\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Api\Agentology\AgentologyService;

/**
 * Class AgentologyServiceProvider.
 * @package App\Providers
 */
class AgentologyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AgentologyService::class, function () {
            return new AgentologyService(
                config('services.agentology.url'),
                config('services.agentology.key'),
                config('services.agentology.secret')
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [AgentologyService::class];
    }
}
