<?php

namespace App\Services\Api\Agentology;

use App\Agent;
use App\Lead;
use App\Services\Api\BaseApiService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;
use stdClass;

/**
 * Class AgentologyService.
 * @package App\Services\Api\Agentology
 */
class AgentologyService extends BaseApiService
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $secret;

    /**
     * AgentologyService constructor.
     *
     * @param  string $url
     * @param  string $key
     * @param  string $secret
     */
    public function __construct(string $url, string $key, string $secret)
    {
        parent::__construct();
        $this->url = $url;
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * Get agentology auth data.
     *
     * @return array
     */
    private function getAuthData(): array
    {
        $data = [
            'api_key' => $this->key,
            'secret_key' => $this->secret,
        ];

        return $data;
    }

    /**
     * Send post request for agentology to create agent.
     *
     * @param array $requestBody
     *
     * @return ResponseInterface|null
     */
    public function postAgent(array $requestBody)
    {
        $res = null;
        $method = Request::METHOD_POST;
        $url = $this->url . '/user/' . $requestBody['agentologyUUID'] ?? '';
        $res = $this->makeRequest($method, $url, $requestBody);

        return $res;
    }

    /**
     * Send post request for agentology to create opportunity (refer lead).
     *
     * @param array $requestBody
     *
     * @return ResponseInterface|null
     */
    public function postOpportunity(array $requestBody)
    {
        $res = null;
        $method = Request::METHOD_POST;
        $url = $this->url . '/user/opp';
        $res = $this->makeRequest($method, $url, $requestBody);

        return $res;
    }

    /**
     * Send agent data to Agentology API.
     *
     * @param Agent $agent
     *
     * @return stdClass|null
     */
    public function upsertAgent(Agent $agent)
    {
        $requestBody = $this->getAuthData();
        $requestBody['agentologyUUID'] = $agent->agentologyUUID ?? '';
        $requestBody['firstname'] = $agent->firstName;
        $requestBody['lastname'] = $agent->lastName;
        $requestBody['email'] = $agent->email;
        $requestBody['password'] = $agent->password;
        $requestBody['phone'] = $agent->phone;
        $requestBody['vendor_type'] = "Agent";
        $requestBody['state'] = $agent->state;
        $requestBody['city'] = $agent->city;
        $requestBody['zip'] = $agent->areaCode;

        $res = $this->postAgent($requestBody);

        $responseObject = $this->getFormattedResponseBody($res);

        if ($responseObject) {
            $responseObject->statusCode = $res->getStatusCode();
        }

        return $responseObject;
    }

    /**
     * Send agent data to Agentology API.
     *
     * @param string $email
     *
     * @return stdClass|null
     */
    public function checkIfAgentExists(string $email)
    {
        $res = null;
        $requestBody = $this->getAuthData();
        $requestBody['email'] = $email;
        $method = Request::METHOD_POST;
        $url = $this->url . '/user/exist';

        $res = $this->makeRequest($method, $url, $requestBody);

        return $this->getFormattedResponseBody($res);
    }

    /**
     * Send referred lead data to Agentology API.
     *
     * @param Lead $lead
     *
     * @return stdClass|null
     */
    public function referLead(Lead $lead)
    {
        $responseObject = null;
        $requestBody = $this->getAuthData();

        if ($this->opportunityCanBeSend($lead)) {
            $requestBody['affiliateAgent'] = $lead->agent->agentologyUUID;
            $requestBody['patId'] = $lead->id;
            $requestBody['firstName'] = $lead->firstName;
            $requestBody['lastName'] = $lead->lastName;
            $requestBody['phone'] = $lead->phone;
            $requestBody['email'] = $lead->email;
            $requestBody['buyerSeller'] = $lead->leadType;
            $requestBody['bedrooms'] = $lead->bedrooms;
            $requestBody['bathrooms'] = $lead->bathrooms;
            $requestBody['timeframe'] = $lead->timeFrame;
            $requestBody['propertyStreet'] = $lead->street;
            $requestBody['propertyCity'] = $lead->city;
            $requestBody['propertyState'] = $lead->state;
            $requestBody['propertyZip'] = $lead->postalcode;
            $requestBody['priceRange'] = $lead->priceRange;
            $requestBody['channelwebsite'] = $lead->channelwebsite;

            $res = $this->postOpportunity($requestBody);

            $responseObject = $this->getFormattedResponseBody($res);
        }

        return $responseObject;
    }

    /**
     * Check agent agentologyUUID
     *
     * @param Agent $agent
     *
     * @return bool
     */
    public function checkAgentologyUUIDForAgent(Agent $agent): bool
    {
        if ($agent->agentologyUUID) {
            return true;
        }

        if ($this->restoreAgentologyUUIDForExistingAgent($agent)) {
            return true;
        }

        return $this->restoreAgentologyUUIDForNewAgent($agent);
    }

    /**
     * Restore agent agentologyUUID for exiting agent
     *
     * @param Agent $agent
     *
     * @return bool
     */
    private function restoreAgentologyUUIDForExistingAgent(Agent $agent): bool
    {
        $existsResponse = $this->checkIfAgentExists($agent->email);

        if (isset($existsResponse->response->success) && $existsResponse->response->success) {
            $agentologyUUID = $existsResponse->response->user->uuid ?? null;

            if ($agentologyUUID) {
                $agent->agentologyUUID = $existsResponse->response->user->uuid;
                return $agent->save();
            }
        }

        return false;
    }

    /**
     * Restore agent agentologyUUID for new agent
     *
     * @param Agent $agent
     *
     * @return bool
     */
    private function restoreAgentologyUUIDForNewAgent(Agent $agent): bool
    {
        $upsertResponse = $this->upsertAgent($agent);

        if (isset($upsertResponse->response->success) && $upsertResponse->response->success) {
            $agentologyUUID = $upsertResponse->response->user->uuid ?? null;

            if ($agentologyUUID) {
                $agent->agentologyUUID = $upsertResponse->response->user->uuid;
                return $agent->save();
            }
        }

        return false;
    }

    /**
     * Checks if opportunity can be send
     *
     * @param Lead $lead
     *
     * @return bool
     */
    private function opportunityCanBeSend(Lead $lead): bool
    {
        if ($lead) {
            $agent = $lead->agent;

            if ($agent) {
                return $lead->firstName && $lead->lastName && $this->checkAgentologyUUIDForAgent($agent);
            }
        }

        return false;
    }
}
