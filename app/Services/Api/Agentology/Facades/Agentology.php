<?php

namespace App\Services\Api\Agentology\Facades;

use App\Services\Api\Agentology\AgentologyService;
use Illuminate\Support\Facades\Facade;
use App\Agent as AgentModel;
use App\Lead as LeadModel;
use stdClass;

/**
 * Class Agentology
 *
 * @method static stdClass|null upsertAgent(AgentModel $agent)
 * @method static stdClass|null referLead(LeadModel $lead)
 * @method static stdClass|null checkIfAgentExists(string $email)
 *
 * @see AgentologyService
 * @package App\Services\Api\Agentology\Facades
 */
class Agentology extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return AgentologyService::class;
    }
}
