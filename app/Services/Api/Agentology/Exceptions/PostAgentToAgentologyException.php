<?php
namespace App\Services\Agentology\Exceptions;

use App\Services\Api\BaseApiServiceResponseFailedException;
use stdClass;

/**
 * Class PostAgentToAgentException
 *
 * @package App\Services\Agentology\Exceptions
 */
class PostAgentToAgentologyException extends BaseApiServiceResponseFailedException
{
    public function __construct(stdClass $responseObj = null)
    {
        $message = 'Failed to post new agent to Agentology';
        parent::__construct($responseObj, $message);
    }
}
