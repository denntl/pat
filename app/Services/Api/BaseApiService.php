<?php

namespace App\Services\Api;

use App\Traits\MakeHttpRequests;

/**
 * Class AgentologyService.
 * @package App\Services\Api
 */
class BaseApiService
{
    use MakeHttpRequests;

    /**
     * @var string
     */
    protected $url;

    /**
     * ApiService constructor.
     */
    public function __construct()
    {
        $this->initHttpClient();
    }
}
