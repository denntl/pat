<?php
namespace App\Services\Api;

use App\Exceptions\BaseCustomException;
use stdClass;

/**
 * Class BaseApiServiceResponseFailedException
 * @package App\Exceptions
 */
class BaseApiServiceResponseFailedException extends BaseCustomException
{
    /**
     * @var stdClass
     */
    private $responseObj;

    /**
     * BaseApiServiceResponseFailedException constructor.
     *
     * @param stdClass $responseObj response object from Api services
     * @param string $message
     */
    public function __construct(stdClass $responseObj, $message)
    {
        $this->responseObj = $responseObj;
        $message = '';
        $code = $responseObj->stausCode ?? 0;
        $previous = null;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return stdClass
     */
    public function getResponseObject()
    {
        return $this->responseObj;
    }
}
