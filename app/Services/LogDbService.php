<?php

namespace App\Services;

use App\LogsApi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class LogDbService.
 *
 * @package App\Services
 */
class LogDbService
{
    /**
     * @var array
     */
    protected $logData = [];

    /**
     * Set params array, that will be filled in model.
     *
     * @param array $params
     *
     * @return void
     */
    public function setParams(array $params)
    {
        $lastIndex = count($this->logData) - 1;
        $this->logData[$lastIndex]['params'] = $params;
    }

    /**
     * Add param to params array.
     *
     * @param string $key
     * @param string $param
     *
     * @return void
     */
    public function addParam(string $key, string $param)
    {
        $lastIndex = count($this->logData) - 1;
        $this->logData[$lastIndex]['params'][$key] = $param;
    }

    /**
     * Get all params.
     *
     * @return array
     */
    public function getParams(): array
    {
        $lastIndex = count($this->logData) - 1;

        if (!isset($this->logData[$lastIndex])) {
            return [];
        }

        return $this->logData[$lastIndex]['params'] ?? [];
    }

    /**
     * Get param by key.
     *
     * @param $key
     *
     * @return string
     */
    public function getParam(string $key): string
    {
        $lastIndex = count($this->logData) - 1;

        if (!isset($this->logData[$lastIndex])) {
            return '';
        }

        return $this->logData[$lastIndex]['params'][$key];
    }

    /**
     * Set model instance(class name), that will be used to save logs to.
     *
     * @param string $instance
     *
     * @return $this
     */
    public function setInstance(string $instance)
    {
        $this->logData[]['instance'] = $instance;

        return $this;
    }

    /**
     * Get current model instance.
     *
     * @return string
     */
    public function getInstance(): string
    {
        $lastIndex = count($this->logData) - 1;

        if (!isset($this->logData[$lastIndex])) {
            return '';
        }

        return $this->logData[$lastIndex]['instance'] ?? '';
    }

    /**
     * Method to create object from instance and save data to database.
     * Clear params array.
     *
     * @return bool
     */
    public function save(): bool
    {
        $logModelAndParams = array_pop($this->logData);

        if (config('services.dblogs.save')) {
            if (!isset($logModelAndParams['instance'])) {
                return false;
            }
            if (!class_exists($logModelAndParams['instance'])) {
                return false;
            }
            /** @var Model $model */
            $model = new $logModelAndParams['instance']();
            $model->fill($logModelAndParams['params']);
    
            try {
                $isSave = $model->saveOrFail();
            } catch (\Exception $e) {
                Log::error($e);
    
                return false;
            }
    
            if ($isSave) {
                return true;
            }
        }

        return false;
    }

    /**
     * Logs api request
     *
     * @param Request $request
     * @param string $action description text
     * @param string $responseStatus status code for response
     * @param array $responseData response data array
     *
     * @return void
     */
    public function logApi(Request $request, string $action, string $responseStatus, array $responseData)
    {
        try {
            $requestData = json_encode($request->all());
            $responseData = json_encode($responseData);
            $loggerData = [
                'requestUrl' => $request->fullUrl(),
                'clientIp' => $request->getClientIp(),
                'action' => $action,
                'requestData' => $requestData,
                'responseText' => $responseData,
                'responseStatus' => $responseStatus
            ];

            LogsApi::create($loggerData);
        } catch (\Exception $e) {
            Log::error('api logger failed', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }
    }
}
