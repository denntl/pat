<?php

namespace App\Services;

use App\LeadCall;

/**
 * Class LeadCallService.
 *
 * @package App\Services
 */
class LeadCallService
{
    /**
     * Create New LeadCall.
     *
     * @param string $leadId
     * @param string $twilioCallSid
     * @param string $phone
     * @param boolean $isForwarded
     * @param string $createdAt
     * @param integer $agentId
     *
     * @return LeadCall
     */
    public function create(
        string $leadId,
        string $twilioCallSid,
        string $phone,
        bool $isForwarded,
        string $createdAt,
        int $agentId = null,
        int $conciergeId = null
    ): LeadCall {
        $call = new LeadCall();

        $call->leadId = $leadId;
        $call->twilioCallSid = $twilioCallSid;
        $call->phone = $phone;
        $call->isForwarded = $isForwarded;
        $call->createdAt = $createdAt;
        $call->agentId = $agentId;
        $call->conciergeId = $conciergeId;

        $call->save(); // TODO: make event about it

        return $call;
    }
}
