<?php

namespace App\Services;

use App\Lead;
use App\Message;
use App\Services\Time\Facades\TimeHelper;
use App\Traits\CheckAllowedTimeFrame;
use Carbon\Carbon;
use App\Facades\Twilio;
use App\Jobs\SetFailedMessageStatus;
use App\Events\Message\MessageUpdateStatus;
use Twilio\Exceptions\TwilioException;
use App\Jobs\Lead\SmsFlow\HandleFlowStep as SmsFlow;

/**
 * Class MessageService.
 * @package App\Services
 */
class MessageService
{
    use CheckAllowedTimeFrame;

    /** Update Twilio status
     *
     * @param string|null $id - Twilio message sid
     * @param string|null $status - Twilio message status
     *
     * @return bool
     */
    public function updateTwilioStatus(string $id = null, string $status = null): bool
    {
        if ($id === null) {
            info('Failed to update message status: message id is null');
            return false;
        }

        $statusValid = Twilio::checkMessageStatus($status);
        if (!$statusValid) {
            info('Failed to update message status: invalid status');
            return false;
        }

        /** @var Message $message */
        $message = Message::where('twilioMessageSid', $id)->firstOrFail();
        $message->twilioMessageStatus = $status;

        return $message->saveOrFail();
    }

    /** Update Twilio status by messageId
     *
     * @param string|null $messageId - Message id
     * @param string|null $status - Twilio message status
     *
     * @return bool
     */
    public function updateTwilioStatusByMessageId(string $messageId = null, string $status = null): bool
    {
        if ($messageId === null) {
            info('Failed to update message status by message id: Message id is null');
            return false;
        }

        $statusValid = Twilio::checkMessageStatus($status);
        if (!$statusValid) {
            info('Failed to update message status by message id: invalid status');
            return false;
        }

        /** @var Message $message */
        $message = Message::find($messageId);
        $message->twilioMessageStatus = $status;

        return $message->saveOrFail();
    }

    /**
     * Create message.
     *
     * @param array $data - data for new message
     * @param boolean $automated - Automated message or not
     *
     * @return Message
     */
    public function createMessage($data, $automated = false)
    {
        /** @var Message $message */
        $message = Message::create($data);
        if (!$message->lead) {
            return $message;
        }

        if ($message->incoming) {
            $message->lead->tag = Lead::TAG_AWAITING_RESPONSE;
            $message->lead->icon = Lead::ICON_SMS;
            $message->lead->isPaused = true;
            $message->lead->save();

            return $message;
        }

        if ($this->isInitialMessage($message)) {
            $this->startSmsAutomationFlow($message->lead->id, $message->lead->timezone);
            $automated = true;
        }

        $hasInboundMessages = $message->lead->messages->where('incoming', true)->count() > 0;

        $message->lead->tag = ($automated || !$hasInboundMessages) ? Lead::TAG_AUTOMATION : Lead::TAG_ONGOING_CHAT;
        $message->lead->icon = ($automated || !$hasInboundMessages) ? Lead::ICON_AUTO : Lead::ICON_SMS;

        $this->processOutboundMessage($message);

        $message->lead->save();

        return $message;
    }

    /**
     * Processing outbound message.
     *
     * @param Message $message
     *
     * @return void
     */
    public function processOutboundMessage(Message $message)
    {
        try {
            $resp = Twilio::sendSms($message->to, $message->from, $message->text);

            $message->twilioMessageSid = $resp ? $resp->sid : '';
            $message->twilioMessageStatus = TwilioService::MESSAGE_SENDING_STATUS;
            $message->save();

            $delay = Carbon::now()->addMinutes(1);
            $job = (new SetFailedMessageStatus($message))->delay($delay);
            dispatch($job);
        } catch (TwilioException $e) {
            info($e->getMessage());
            $this->updateTwilioStatusByMessageId($message->id, 'failed');

            event(new MessageUpdateStatus(
                $message->leadId,
                $message->twilioMessageSid,
                TwilioService::MESSAGE_FAILED_STATUS,
                $message->id
            ));
        }
    }

    /**
     * Start automation flow for Lead.
     *
     * @param string $leadId
     * @param string/null $timezone
     *
     * @return void
     */
    private function startSmsAutomationFlow(string $leadId, $timezone)
    {
        $delay = TimeHelper::createDelay(0, 15, $timezone);
        $job = (new SmsFlow($leadId))->delay($delay);
        dispatch($job);
    }

    /**
     * Check if message initial.
     *
     * @param Message $message
     *
     * @return boolean - True if message is initial
     */
    private function isInitialMessage(Message $message)
    {
        $hasEmails = $message->lead->emails->where('incoming', true)->count() == 0;
        $isFirstMessage = $message->lead->messages->count() == 1;

        return $hasEmails && $isFirstMessage;
    }
}
