<?php

namespace App\Services;

use App\Agent;
use App\LeadCall;
use Illuminate\Support\Facades\DB;
use App\Lead;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Intercom\IntercomClient;
use GuzzleHttp\Exception\ClientException;

/**
 * Class IntercomService.
 * @package App\Services
 */
class IntercomService
{
    /**
     * Intercom SDK client.
     * @var IntercomClient
     */
    private $client;

    /**
     * Aggregation functions for metrics calculation.
     * @var array
     */
    private $leadsMetricsCalculations = [
        'total_leads' => 'COUNT(*)',
        'total_active' => "SUM(CASE WHEN (status = ?) THEN 1 ELSE 0 END)",
        'total_leads_7' => 'SUM(CASE WHEN ("agentAssignedAt" > ?) THEN 1 ELSE 0 END)',
        'total_leads_14' => 'SUM(CASE WHEN ("agentAssignedAt" > ?) THEN 1 ELSE 0 END)',
        'total_leads_30' => 'SUM(CASE WHEN ("agentAssignedAt" > ?) THEN 1 ELSE 0 END)',
        'total_qualified' => "SUM(CASE WHEN (status = ?) THEN 1 ELSE 0 END)",
        'total_unqualified' => "SUM(CASE WHEN (status = ?) THEN 1 ELSE 0 END)",
        'total_referred' => "SUM(CASE WHEN (status = ?) THEN 1 ELSE 0 END)",
    ];

    /**
     * Aggregation functions for calls metrics calculation.
     * @var array
     */
    private $callsMetricsCalculations = [
        'total_calls_forwarded' => 'COUNT(*)',
        'total_calls_forwarded_7' => 'SUM(CASE WHEN ("createdAt" > ?) THEN 1 ELSE 0 END)',
        'total_calls_forwarded_14' => 'SUM(CASE WHEN ("createdAt" > ?) THEN 1 ELSE 0 END)',
        'total_calls_forwarded_30' => 'SUM(CASE WHEN ("createdAt" > ?) THEN 1 ELSE 0 END)',
    ];

    /**
     * Intercom constructor.
     *
     * @param string $token
     *
     * @throws ClientException
     */
    public function __construct($token)
    {
        try {
            $this->client = new IntercomClient($token, null);
        } catch (ClientException $e) {
            Log::error($e->getMessage(), [
                'environment' => App::environment(),
                'env_access_token' => config('services.intercom.access_token'),
                'env_access_token_test' => config('services.intercom.test_access_token'),
                'token_used_for_intercom' => $token
            ]);
            throw new $e;
        }
    }

    /**
     * Register new user.
     *
     * @param Agent $agent
     */
    public function registerUser(Agent $agent)
    {
        $this->createOrUpdateUser($agent);
        $this->setSignedUpDate($agent);
    }

    /**
     * Create or update user in Intercom.
     *
     * @param Agent $agent
     */
    public function createOrUpdateUser(Agent $agent)
    {
        $data = [
            "name" => $agent->firstName . ' ' . $agent->lastName,
            "email" => $agent->email,
            "user_id" => $agent->agentologyUUID,
            "phone" => $agent->phone,
            "last_request_at" => time(),
            // TODO: add "Lead Parser Forwarding Alias" after PAT-167
            "custom_attributes" => [
                "Company" => $agent->company,
                "Website" => $agent->website,
                "Mailing Address" => $agent->city . ' ' . $agent->state,
                "Concierge Email Alias" => $agent->sendGridEmail,
                "Concierge Name" => $agent->personaName,
                "Concierge Phone" => $agent->twilioNumber,
                "Concierge Role" => $agent->personaTitle,
                "Team Name" => $agent->personaTeamName,
            ],
        ];

        $this->client->users->create($data);
    }

    /**
     * Set signed up date for new user.
     *
     * @param Agent $agent
     */
    private function setSignedUpDate(Agent $agent)
    {
        $data = [
            "user_id" => $agent->salesForceId,
            "signed_up_at" => time(),
        ];

        $this->client->users->update($data);
    }

    /**
     * Update last seen date custom field.
     *
     * @param string $agentEmail
     */
    public function updateLastSeen(string $agentEmail)
    {
        $data = [
            "email" => $agentEmail,
            "last_request_at" => time(),
        ];

        $this->client->users->update($data);
    }

    /**
     * Update gmail connection data.
     *
     * @param Agent $agent
     *
     * @return void
     */
    public function updateGmailConnectionData(Agent $agent)
    {
        if (!$agent->salesForceId) {
            return;
        }

        $data = [
            "user_id" => $agent->salesForceId,
            "email" => $agent->email,
            // TODO:  add "Viking Authorized Email" after PAT-126
            "custom_attributes" => [
                "Gmail Connected?" => $agent->gmailConnected
            ]
        ];

        $this->client->users->update($data);
    }

    /**
     * Update metrics.
     *
     * @param integer $agentId
     *
     * @return boolean
     */
    public function updateLeadsMetrics(int $agentId): bool
    {
        $salesForceId = Agent::where('id', $agentId)->first()->salesForceId;

        if (!$salesForceId) {
            return false;
        }

        $queryMetrics = static::getQueryMetrics($this->leadsMetricsCalculations);
        $timestamps = static::getTimestamps();

        $metrics = Lead::select($queryMetrics)
            ->where("agentId", "?")
            ->setBindings([
                Lead::STATUS_NEW,
                $timestamps["days_7"],
                $timestamps["days_14"],
                $timestamps["days_30"],
                Lead::STATUS_QUALIFIED,
                Lead::STATUS_UNQUALIFIED,
                Lead::STATUS_REFERRED,
                $agentId
            ])
            ->first();

        $data = [
            "user_id" => $salesForceId,
            "custom_attributes" => [
                "Total Leads" => $metrics->total_leads,
                "Total Leads (Last 7 Days)" => $metrics->total_leads_7,
                "Total Leads (Last 14 Days)" => $metrics->total_leads_14,
                "Total Leads (Last 30 Days)" => $metrics->total_leads_30,
                "Total Active" => $metrics->total_active,
                "Total Qualified" => $metrics->total_qualified,
                "Total Unqualified" => $metrics->total_unqualified,
                "Total Referred" => $metrics->total_referred,
            ],
        ];

        try {
            $this->client->users->update($data);
        } catch (ClientException $e) {
            Log::error($e->getMessage());

            return false;
        }

        return true;
    }


    /**
     * Update calls metrics.
     *
     * @param integer $agentId
     *
     * @return boolean
     */
    public function updateCallsMetrics(int $agentId): bool
    {
        $salesForceId = Agent::where('id', $agentId)->first()->salesForceId;

        if (!$salesForceId) {
            return false;
        }

        $queryMetrics = static::getQueryMetrics($this->callsMetricsCalculations);
        $timestamps = static::getTimestamps();
        $isForwarded = 1;

        $metrics = LeadCall::select($queryMetrics)
            ->where("agentId", "?")
            ->where("isForwarded", "?")
            ->setBindings([
                $timestamps["days_7"],
                $timestamps["days_14"],
                $timestamps["days_30"],
                $agentId,
                $isForwarded
            ])
            ->first();

        $data = [
            "user_id" => $salesForceId,
            "custom_attributes" => [
                "Number of Calls Forwarded" => $metrics->total_calls_forwarded,
                "Calls Forwarded (last 7 days)" => $metrics->total_calls_forwarded_7,
                "Calls Forwarded (Last 14 days)" => $metrics->total_calls_forwarded_14,
                "Calls Forwarded (Last 30 days)" => $metrics->total_calls_forwarded_30,
            ],
        ];

        try {
            $this->client->users->update($data);
        } catch (ClientException $e) {
            Log::error($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * Prepares the query for metrics.
     *
     * @param array $metricsCalculations
     *
     * @return array
     */
    private static function getQueryMetrics(array $metricsCalculations): array
    {
        $metrics = [];

        foreach ($metricsCalculations as $metricName => $metricFunction) {
            $metrics[] = DB::raw($metricFunction . " AS " . $metricName);
        }

        return $metrics;
    }

    /**
     * Prepares timestamps for the query.
     *
     * @return array
     */
    public static function getTimestamps(): array
    {
        return [
            "days_7" => Carbon::now()->subDays(7),
            "days_14" => Carbon::now()->subDays(14),
            "days_30" => Carbon::now()->subDays(30)
        ];
    }
}
