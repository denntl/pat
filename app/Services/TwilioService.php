<?php

namespace App\Services;

use App\Traits\SendSmsStopple;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\RestException;

/**
 * Class TwilioService.
 * @package App\Services
 */
class TwilioService extends AbstractTwilioService
{
    use SendSmsStopple;

    const MESSAGE_SENT_STATUS = 'sent';
    const MESSAGE_SENDING_STATUS = 'sending';
    const MESSAGE_FAILED_STATUS = 'failed';
    const MESSAGE_QUEUED_STATUS = 'queued';
    const MESSAGE_RECEIVED_STATUS = 'received';
    const MESSAGE_DELIVERED_STATUS = 'delivered';
    const MESSAGE_UNDELIVERED_STATUS = 'undelivered';

    const CALL_INITIATED_STATUS = 'initiated';
    const CALL_RINGING_STATUS = 'ringing';
    const CALL_IN_PROGRESS_STATUS = 'in-progress';
    const CALL_COMPLETED_STATUS = 'completed';

    const TEST_TO_PHONE = '+15005550010';
    const TEST_FROM_PHONE = '+17817804681';

    /**
     * Check if status is valid.
     *
     * @param string|null $status
     * @return bool
     */
    public function checkMessageStatus(string $status = null): bool
    {
        if ($status === null) {
            return false;
        }

        switch ($status) {
            case self::MESSAGE_SENT_STATUS:
            case self::MESSAGE_SENDING_STATUS:
            case self::MESSAGE_FAILED_STATUS:
            case self::MESSAGE_QUEUED_STATUS:
            case self::MESSAGE_DELIVERED_STATUS:
            case self::MESSAGE_UNDELIVERED_STATUS:
            case self::MESSAGE_RECEIVED_STATUS:
                return true;
            default:
                Log::warning(sprintf('Detected undefined Twilio message status: %s', $status));
                return false;
        }
    }

    /**
     * Checks if the number is real
     *
     * @param string|null $phone
     *
     * @return bool
     */
    public function validatePhoneNumber(string $phone = null): bool
    {
        if ($phone === null) {
            return false;
        }

        $response = $this->postPhoneForValidation($phone);

        if ($response) {
            return true;
        }

        return false;
    }

    /**
     * Get data from Twilio by phone number
     *
     * @param string $phone
     *
     * @return null|\Twilio\Rest\Lookups\V1\PhoneNumberInstance
     */
    private function postPhoneForValidation(string $phone)
    {
        $response = null;

        try {
            $response = $this->client->lookups
                ->phoneNumbers($phone)->fetch(["type" => "carrier"]);
        } catch (RestException $e) {
            $response = null;
        }

        return $response;
    }

    /**
     * Get phone carrier type
     *
     * @param string $phone
     *
     * @return null|string
     */
    public function getPhoneCarrierType(string $phone)
    {
        $response = $this->postPhoneForValidation($phone);

        if ($response) {
            return $response->carrier['type'];
        }

        return null;
    }
}
