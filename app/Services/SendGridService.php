<?php

namespace App\Services;

use App\Traits\SendEmailStopple;

/**
 * Class SendGridService.
 * @package App\Services
 */
class SendGridService extends AbstractSendGridService
{
    use SendEmailStopple;

    const PENDING_EMAIL_STATUS_ID = 1;
    const INVALID_EMAIL_STATUS_ID = 2;
    const VALID_EMAIL_STATUS_ID = 3;
    const ANSWERED_EMAIL_STATUS_ID = 4;
}
