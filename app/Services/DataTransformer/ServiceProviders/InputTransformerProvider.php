<?php

namespace App\Services\DataTransformer\ServiceProviders;

use App\Services\DataTransformer\DataTransformerService;
use Illuminate\Support\ServiceProvider;

/**
 * Class InputTransformerProvider.
 * @package App\Providers
 */
class InputTransformerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DataTransformerService::class, function () {
            return new DataTransformerService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [DataTransformerService::class];
    }
}
