<?php

namespace App\Services\DataTransformer;

use App\Services\DataTransformer\Interfaces\ReturnTransformationRulesInterface;

/**
 * Class DataTransformerService.
 */
class DataTransformerService
{
    /**
     * Transforms data from Api response
     *
     * @param array $data
     * @param ReturnTransformationRulesInterface $transformer
     *
     * @return array
     */
    public function transformInputData(array $data, ReturnTransformationRulesInterface $transformer): array
    {
        foreach ($transformer->getTransformationRules() as $oldKey => $newKey) {
            if (isset($data[$oldKey])) {
                $data[$newKey] = $data[$oldKey];
                unset($data[$oldKey]);
            }
        }

        return $data;
    }

    /**
     * Transforms data for Api request
     *
     * @param array $data
     * @param ReturnTransformationRulesInterface $transformer
     *
     * @return array
     */
    public function transformOutputData(array $data, ReturnTransformationRulesInterface $transformer): array
    {
        foreach ($transformer->getTransformationRules() as $newKey => $oldKey) {
            if (isset($data[$oldKey])) {
                $data[$newKey] = $data[$oldKey];
                unset($data[$oldKey]);
            }
        }

        return $data;
    }
}
