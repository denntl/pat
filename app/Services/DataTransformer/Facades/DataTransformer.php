<?php

namespace App\Services\DataTransformer\Facades;

use App\Services\DataTransformer\DataTransformerService;
use App\Services\DataTransformer\Interfaces\ReturnTransformationRulesInterface;
use Illuminate\Support\Facades\Facade;

/**
 * Class DataTransformer
 *
 * @method static array transformInputData(array $data,ReturnTransformationRulesInterface $transformer)
 *
 * @see DataTransformerService
 * @package App\Facades
 */
class DataTransformer extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return DataTransformerService::class;
    }
}
