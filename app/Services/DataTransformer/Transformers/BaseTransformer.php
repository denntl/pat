<?php

namespace App\Services\DataTransformer\Transformers;

use App\Services\DataTransformer\Interfaces\ReturnTransformationRulesInterface;

/**
 * Class BaseTransformer
 * @package App\Services\DataTransformer\Transformers
 */
class BaseTransformer implements ReturnTransformationRulesInterface
{
    /**
     * @var
     */
    protected $transformationRules;

    /**
     * @return array
     */
    public function getTransformationRules(): array
    {
        return $this->transformationRules;
    }
}
