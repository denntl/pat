<?php

namespace App\Services\DataTransformer\Transformers\Viking;

use App\Services\DataTransformer\Transformers\BaseTransformer;

/**
 * Class AgentTransformer
 * @package App\Services\DataTransformer\Transformers\Viking
 */
class AgentTransformer extends BaseTransformer
{
    /**
     * @var array
     */
    protected $transformationRules = [
        'id' => 'vikingId',
        'pat_email_alias' => 'emailForwarding',
        'pat_id' => 'vikingPatId',
        'sfid' => 'salesForceId'
    ];
}
