<?php

namespace App\Services\DataTransformer\Interfaces;

/**
 * Interface ReturnTransformationRulesInterface
 * @package App\Services\DataTransformer\Interfaces
 */
interface ReturnTransformationRulesInterface
{
    /**
     * @return array
     */
    public function getTransformationRules(): array ;
}
