<?php

namespace App;

use App\Models\AbstractModel as Model;
use Carbon\Carbon;

/**
 * Class LogsWebsocket
 *
 * @property integer $id
 * @property string $command
 * @property string $data
 * @property string $socketData
 * @property string $class
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 *
 * @package App
 */
class LogsWebsocket extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logsWebsocket';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'command',
        'data',
        'socketData',
        'class'
    ];
}
