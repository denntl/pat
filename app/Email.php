<?php

namespace App;

use App\Facades\SendGrid;
use App\Models\AbstractModel as Model;
use App\Events\Email\EmailCreate as CreateEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Jobs\SetFailedEmailStatus;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

/**
 * Class Email
 *
 * @property string $id // TODO: remove it as integer and use UUID as primary key and rename it to `id`
 * @property string $from
 * @property string $to
 * @property string $type
 * @property string $subject
 * @property integer $statusId
 * @property string $leadId
 * @property integer $incoming
 * @property integer $conciergeId
 * @property integer $conciergeName
 * @property boolean $isRead
 * @property string $UUID
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 *
 * @property Lead $lead
 *
 * @package App
 */
class Email extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    const NEW_EMAIL_STATUS = 1;
    const INVALID_EMAIL_STATUS_ID = 2;
    const VALID_EMAIL_STATUS_ID = 3;
    const ANSWERED_EMAIL_STATUS_ID = 4;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'emails';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from',
        'to',
        'type',
        'subject',
        'statusId',
        'agentId',
        'leadId',
        'senderFullName',
        'incoming',
        'conciergeId',
        'text',
        'isRead',
        'UUID'
    ];

    /**
     * Get the concierge that currently chatting with lead.
     *
     * @return BelongsTo
     */
    public function concierge()
    {
        return $this->belongsTo('App\Concierge', 'conciergeId');
    }

    /**
     * Get the lead from message.
     *
     * @return BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead', 'leadId');
    }

    /**
     * Get email delivery status.
     *
     * @return HasOne
     */
    public function status()
    {
        return $this->hasOne('App\LetterDeliveryStatus', 'statusId');
    }

    /**
     * Get the agent that owns the email from letter.
     *
     * @return BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agentId');
    }

    /**
     * Create new email
     *
     * @example App\Email::createEmail($data);
     *
     * @param array $data
     * @param string $sender
     *
     * @return int
     */
    public static function createEmail(array $data = [], string $sender = null)
    {
        $validationRules = [
            'from' => 'required|email',
            'to' => 'required|email',
            'leadId' => 'required',
        ];

        $validator = Validator::make($data, $validationRules);
        $errors = $validator->errors()->all();
        if (count($errors)) {
            Log::info('createEmail validation fails', $errors);

            return;
        }

        $data['UUID'] = Uuid::uuid1()->toString();
        /** @var Email|null $email */
        $email = Email::create($data);
        if ($email) {
            if ($email->incoming) {
                $email->lead->tag = Lead::TAG_AWAITING_RESPONSE;
                $email->lead->isPaused = true; // Pause sms flow
            } else {
                $email->subject = $email->subject . ' ' . '[CustomerID: '. $email->leadId . ']';
                $email->save();

                $email->conciergeName = $email->lead->agent->personaName;

                $email->lead->tag = Lead::TAG_ONGOING_CHAT;
                SendGrid::sendLeadEmail($email);

                $delay = Carbon::now()->addMinutes(1);
                $job = (new SetFailedEmailStatus($email))->delay($delay);
                dispatch($job);
            }
            $email->lead->icon = Lead::ICON_EMAIL;
            $email->lead->save();

            return event(new CreateEmail($email, $sender));
        }
    }
}
