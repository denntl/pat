<?php

namespace App;

use App\Facades\PhoneNumber;
use App\Models\AbstractUuidModel;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Lead
 *
 * @property string $id
 * @property string $firstName
 * @property string $lastName
 * @property string $phone
 * @property string $phoneSource
 * @property string $email
 * @property string $emailSource
 * @property string $agentId
 * @property string $conciergeId
 * @property string $leadType
 * @property string $priceRange
 * @property string $mortgage
 * @property string $bedrooms
 * @property string $bathrooms
 * @property string $timeFrame
 * @property string $status
 * @property string $originalEmail
 * @property Carbon $updatedAt
 * @property Carbon $createdAt
 * @property string $city
 * @property string $state
 * @property string $sourceId
 * @property string $channelwebsite
 * @property string $leadcomment
 * @property string $postalcode
 * @property string $street
 * @property string $gmailId
 * @property string $gmailThread
 * @property bool $fromViking
 * @property string $vikingUserId
 * @property string $vikingPatId
 * @property string $duplicateLeadId
 * @property string $duplicate
 * @property string $phoneCarrierType
 * @property Carbon $agentAssignedAt
 * @property string|null $tag
 * @property bool $isPaused
 * @property string|null $smsFlowStep
 * @property string $icon
 * @property string $lastAction
 * @property string $timezone
 * @property string $opportunityId
 *
 * @property Agent $agent
 * @property ArrayCollection $notes - Note array collection
 * @property LeadSource $source
 * @property ArrayCollection $messages - array collection of Message objects
 * @property ArrayCollection $emails - array collection of Email objects
 * @property ArrayCollection $calls - array collection of LeadCall objects
 *
 * @property array lastActivity
 *
 * @method static Builder byConcierge(integer $conciergeId)
 * @method static Builder byAgentId(integer $agentId)
 *
 * @package App
 */
class Lead extends AbstractUuidModel
{
    const STATUS_NEW = 'New lead';
    const STATUS_QUALIFIED = 'Qualified';
    const STATUS_UNQUALIFIED = 'Unqualified';
    const STATUS_NOT_LEAD = 'Not a lead';
    const STATUS_REFERRED = 'Referred';

    const TAG_URGENT = 'Urgent';
    const TAG_AWAITING_RESPONSE = 'Awaiting Response';
    const TAG_ACTION_REQUIRED = 'Action Required';
    const TAG_ONGOING_CHAT = 'Ongoing Chat';
    const TAG_ARCHIVE = 'Archive';
    const TAG_AUTOMATION = 'Automation';

    const ICON_SMS = 'sms';
    const ICON_EMAIL = 'email';
    const ICON_CALL = 'call';
    const ICON_GENERAL = 'general';
    const ICON_ARCHIVE = 'archive';
    const ICON_AUTO = 'auto';

    const ACTION_QUALIFY = 'Qualify';
    const ACTION_UNQUALIFY = 'Unqualify';
    const ACTION_CALL_REQUESTED = 'Call Requested';
    const ACTION_NO_RESPONSE_NEEDED = 'No Response Needed';
    const ACTION_NOT_LEAD = 'Not a lead';
    const ACTION_INVALID_PHONE_VALID_EMAIL = 'Good email / bad phone';
    const ACTION_CONTINUE_LANDLINE_AUTOMATION = 'Continue Landline Automation';
    const ACTION_CONTINUE_SMS_AUTOMATION = 'Continue SMS Automation';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * @var array
     */
    public static $activeStatuses = [
        self::STATUS_NEW,
    ];

    /**
     * @var array
     */
    public static $archivedStatuses = [
        self::STATUS_QUALIFIED,
        self::STATUS_UNQUALIFIED,
        self::STATUS_NOT_LEAD,
        self::STATUS_REFERRED,
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leads';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'firstName',
        'lastName',
        'phone',
        'phoneSource',
        'email',
        'emailSource',
        'agentId',
        'conciergeId',
        'leadType',
        'priceRange',
        'mortgage',
        'bedrooms',
        'bathrooms',
        'timeFrame',
        'status',
        'originalEmail',
        'updatedAt',
        'createdAt',
        'city',
        'state',
        'sourceId',
        'channelwebsite',
        'leadcomment',
        'postalcode',
        'street',
        'gmailId',
        'gmailThread',
        'fromViking',
        'vikingUserId',
        'vikingPatId',
        'duplicateLeadId',
        'duplicate',
        'phoneCarrierType',
        'agentAssignedAt',
        'tag',
        'isPaused',
        'smsFlowStep',
        'icon',
        'lastAction',
        'timezone',
        'opportunityId'
    ];

    /**
     * The attributes that are virtual.
     *
     * @var array
     */
    protected $appends = [
        'lastMessage'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'emails',
        'messages',
        'calls'
    ];

    /**
     * Get the source that owns the lead.
     *
     * @return BelongsTo
     */
    public function source()
    {
        return $this->belongsTo('App\LeadSource', 'sourceId');
    }

    /**
     * Get the agent that owns the lead.
     *
     * @return BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agentId');
    }

    /**
     * Get the concierge that owns the concierge.
     *
     * @return BelongsTo
     */
    public function concierge()
    {
        return $this->belongsTo('App\Concierge', 'conciergeId');
    }

    /**
     * Get the notes for the lead.
     *
     * @return HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Note', 'leadId');
    }

    /**
     * Get the messages for the lead.
     *
     * @return HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message', 'leadId');
    }

    /**
     * Get the emails of the lead.
     *
     * @return HasMany
     */
    public function emails()
    {
        return $this->hasMany('App\Email', 'leadId');
    }

    /**
     * Get the leadCalls for the lead.
     *
     * @return HasMany
     */
    public function calls()
    {
        return $this->hasMany('App\LeadCall', 'leadId');
    }

    /**
     * Get active leads by concierge id.
     *
     * @example App\Lead::byConcierge(2)->get();
     *
     * @param Builder $query
     * @param integer $conciergeId
     *
     * @return Builder
     */
    public function scopeByConcierge($query, int $conciergeId)
    {
        return $query->where('conciergeId', $conciergeId);
    }

    /**
     * Get leads by agent id.
     *
     * @param Builder $query
     * @param integer $agentId
     *
     * @return Builder
     */
    public function scopeByAgentId($query, int $agentId)
    {
        return $query->where('agentId', $agentId);
    }

    /**
     * Last message.
     *
     * @return string
     */
    public function getLastMessageAttribute()
    {
        $lastMessage = $this->messages->last();
        $lastEmail = $this->emails->last();

        if ($lastMessage && $lastEmail) {
            return $lastMessage->createdAt > $lastEmail->createdAt ? $lastMessage->text : $lastEmail->text;
        } elseif ($lastMessage && !$lastEmail) {
            return $lastMessage->text;
        } elseif (!$lastMessage && $lastEmail) {
            return $lastEmail->text;
        }

        return '';
    }

    /**
     * Check if status is Archived.
     *
     * @param string $status
     *
     * @return boolean
     */
    public static function isArchivedStatus(string $status) : bool
    {
        return in_array($status, self::$archivedStatuses);
    }

    /**
     * Check if status is Active.
     *
     * @param string $status
     *
     * @return boolean
     */
    public static function isActiveStatus(string $status) : bool
    {
        return in_array($status, self::$activeStatuses);
    }

    /**
     * Set the leads's timeFrame to null if empty.
     *
     * @param  string  $value
     *
     * @return void
     */
    public function setTimeFrameAttribute($value)
    {
        $this->attributes['timeFrame'] = $value ? $value : null;
    }

    /**
     * Set the leads's mortgage to null if empty.
     *
     * @param  string  $value
     *
     * @return void
     */
    public function setMortgageAttribute($value)
    {
        $this->attributes['mortgage'] = $value ? $value : null;
    }

    /**
     * Set the leads's price range to null if empty.
     *
     * @param  string  $value
     *
     * @return void
     */
    public function setPriceRangeAttribute($value)
    {
        $this->attributes['priceRange'] = $value ? $value : null;
    }

    /**
     * Set the leads's phone to formatted form.
     *
     * @param string $value
     *
     * @return void
     */
    public function setPhoneAttribute($value)
    {
        $phone = PhoneNumber::getNationalPhone($value);

        $this->attributes['phone'] = $phone ?? $value;

        $emptyPhone = empty($this->attributes['phone']) || !preg_match('/\d/', $this->attributes['phone']);

        if ($emptyPhone) {
            $this->attributes['phone'] = null;
        }
    }

    /**
     * Set the leads's type to lower case or null.
     *
     * @param string $value
     *
     * @return void
     */
    public function setLeadTypeAttribute($value)
    {
        if (($value !== null) && (in_array(strtolower($value), ['seller', 'buyer']))) {
            $this->attributes['leadType'] = strtolower($value);
            return;
        }
        $this->attributes['leadType'] = null;
    }
}
