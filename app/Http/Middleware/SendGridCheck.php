<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class SendGridCheck
 * @package App\Http\Middleware
 */
class SendGridCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->input('key');

        $secret = config('services.sendgrid.webhook_secret');
        if ($key !== $secret) {
            return response()->json([
                'errors' => [
                    ['message' => 'no valid key']
                ]
            ], 401);
        }

        return $next($request);
    }
}
