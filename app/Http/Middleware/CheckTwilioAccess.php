<?php

namespace App\Http\Middleware;

use App\Facades\Twilio;
use Closure;

/**
 * Class CheckTwilioAccess.
 * @package App\Http\Middleware
 */
class CheckTwilioAccess
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hasAccountSid = Twilio::hasSid($request->AccountSid);
        $isTwilioAgent = $request->header('User-Agent') === 'TwilioProxy/1.1';

        if (!$hasAccountSid && !$isTwilioAgent) {
            return response()->json([
                'errors' => [
                    ['message' => 'Not Twilio request.']
                ]
            ], 400);
        }

        return $next($request);
    }
}
