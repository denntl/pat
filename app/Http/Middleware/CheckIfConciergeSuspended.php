<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfConciergeSuspended
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->checkSuspendedPermission()) {
            Auth::logout();
            return redirect()->route('index');
        }

        return $next($request);
    }

    /**
     * Check concierge role for suspended permission
     *
     * @return Boolean
     */
    private function checkSuspendedPermission()
    {
        return Auth::user()->role === 0;
    }
}
