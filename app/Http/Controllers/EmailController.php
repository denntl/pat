<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;

class EmailController extends BaseController
{
    /**
     * View in browser an email template.
     *
     * @param string $uuid
     *
     * @return Response
     */
    public function viewInBrowser(string $uuid)
    {
        $disk = Storage::disk('emails');

        $file = $uuid . '.html';
        if (!$disk->exists($file)) {
            logger()->warning('Try to get non-existent email template');
            abort(404);
        }

        $content = $disk->get($file);

        return response($content, Response::HTTP_OK, [
            'Content-Type' => 'text/html'
        ]);
    }
}
