<?php

namespace App\Http\Controllers;

use App\Events\Agent\AgentUpdate as AgentUpdateEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

/**
 * Class GoogleController
 *
 * @package App\Http\Controllers
 */
class GoogleController extends Controller
{
    /**
     * Redirect agent to get google auth token for Viking
     *
     * @return RedirectResponse
     */
    public function vikingGoogleAuth()
    {
        $authenticatedAgent = Auth::guard('portal')->user();
        $redirectUrl = 'https://' . config('services.viking.domain') . '/api/login/google';
        $redirectParams = [
            'pat_id' => $authenticatedAgent->vikingPatId,
            'redirect_url' => route('viking_google_response')
        ];

        $urlWithParams = $redirectUrl .'?'. http_build_query($redirectParams);

        return redirect($urlWithParams);
    }

    /**
     * Process Viking response in getting google auth token
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function vikingGoogleAuthProcessing(Request $request)
    {
        if ($request->input('result') == 'success') {
            $authenticatedAgent = Auth::guard('portal')->user();
            $agentOnBoarding = $authenticatedAgent->agentOnBoarding;
            $agentOnBoarding->tokenReceived = true;
            $agentOnBoarding->save();
            if ($agentOnBoarding->step > 3) {
                event(new AgentUpdateEvent(null, $authenticatedAgent));
                return redirect(route('lead_source_portal'));
            }
        }

        return redirect(route('index_portal'));
    }
}
