<?php

namespace App\Http\Controllers;

use App\Concierge;
use App\Facades\Twilio;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

/**
 * Class Controller.
 * @package App\Http\Controllers
 */
class ConsoleController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * ConsoleController constructor.
     */
    public function __construct()
    {
        View::share('googleStaticMapsKey', config('services.google.maps_key'));
        View::share('rollBarAccessToken', config('services.rollbar.front_token'));
        View::share('googleStylesData', Controller::$GOOGLE_MAPS_STYLES);
    }

    /**
     * Get index view.
     *
     * @param Request $request
     * @return View
     */
    public function getIndex(Request $request)
    {
        // TODO: check security of this code. Seems like agent can access to console at few cases
        /** @var Concierge $concierge */
        $concierge = Auth::guard('web')->user();
        $role = 0;
        $id = 0;
        $token = null;
        $twilioToken = '';

        if ($concierge) {
            $role = $concierge->role;
            $id = $concierge->id;
            $token = $concierge->getToken();

            $twilioToken = Twilio::generateToken();
        }

        $googleData = $request->session()->get('googleData', array());
        $title = 'Console :: Pat';

        return view('index', [
            'title' => $title,
            'role' => $role,
            'id' => $id,
            'token' => $token,
            'googleData' => $googleData,
            'twilioToken' => $twilioToken,
            'isConcierge' => true
        ]);
    }
}
