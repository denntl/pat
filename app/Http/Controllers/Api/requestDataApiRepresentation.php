<?php
/**
 * @SWG\Definition(
 *          definition="LeadCreate",
 *          required={"id", "pat_id"},
 *              @SWG\Property(property="id", type="string", example="1401e01b-aa4d-4f4a-882b-dc11ee0209f0"),
 *              @SWG\Property(property="firstname", type="string", example="John"),
 *              @SWG\Property(property="lastname", type="string", example="Snow"),
 *              @SWG\Property(property="email", type="string", example="some@test.com"),
 *              @SWG\Property(property="phone", type="string", example="000-000-0000"),
 *              @SWG\Property(property="city", type="string", example="TestCity"),
 *              @SWG\Property(property="state", type="string", example="TestState"),
 *              @SWG\Property(property="street", type="string", example="TestStreet"),
 *              @SWG\Property(property="postalcode", type="string", example="6100"),
 *              @SWG\Property(property="leadcomment", type="string", example="leadcomment"),
 *              @SWG\Property(property="leadtype", type="string", example="seller",
 *                  enum={"seller", "buyer"}
 *              ),
 *              @SWG\Property(property="duplicate", type="boolean", example="true"),
 *              @SWG\Property(property="duplicate_lead_id", type="string",
 *                  example="1401e01b-aa4d-4f4a-882b-dc11ee0209f0"
 *              ),
 *              @SWG\Property(property="phone_carrier_type", type="string", example="mobile",
 *                  enum={
 *                      "voip",
 *                      "mobile",
 *                      "landline"
 *                  }
 *              ),
 *              @SWG\Property(property="channelwebsite", type="string", example="http://test"),
 *              @SWG\Property(property="gmail_id", type="string", example="test"),
 *              @SWG\Property(property="gmail_thread", type="string", example="test"),
 *              @SWG\Property(property="user_id", type="string", example="1401e01b-aa4d-4f4a-882b-dc11ee0209f0"),
 *              @SWG\Property(property="pat_id",  type="string", example="1401e01b-aa4d-4f4a-882b-dc11ee0209f0"),
 *              @SWG\Property(property="mortgage", type="string", example="Cash Buyer",
 *                  enum={
 *                      "Pre-Qualified",
 *                      "Pre-Approved",
 *                      "Haven`t Applied Yet",
 *                      "Cash Buyer",
 *                      "I don`t know",
 *                      "Not Applicable"
 *                  }
 *               ),
 *               @SWG\Property(property="bedrooms", type="integer", example="1"),
 *               @SWG\Property(property="bathrooms", type="integer", example="1"),
 *               @SWG\Property(property="timeframe", type="string", example="Asap",
 *                   enum={
 *                       "Asap",
 *                       "Within 3 months",
 *                       "I'm not sure",
 *                       "0-1 months",
 *                       "3-6 months",
 *                       "6 months or longer"
 *                       }
 *               ),
 *               @SWG\Property(property="price", type="string", example="$800,000 - $1,000,000",
 *                   enum={
 *                        "Under $200,000",
 *                        "$200,000 - $400,000",
 *                        "$400,000 - $600,000",
 *                        "$600,000 - $800,000",
 *                        "$800,000 - $1,000,000",
 *                        "Over $1,000,000"
 *                       }
 *               ),
 *               @SWG\Property(property="attachments", type= "array",
 *                  @SWG\Items(
 *                      type="object",
 *                      ref="#/definitions/LeadAttachments",
 *                  ),
 *               )
 * ),
 *
 * @SWG\Definition(
 *          definition="LeadAttachments",
 *              @SWG\Property(property="id", type="string", example="1401e01b-aa4d-4f4a-882b-dc11ee0209f0"),
 *              @SWG\Property(property="lead_id", type="string", example="1401e01b-aa4d-4f4a-882b-dc11ee0209f0"),
 *              @SWG\Property(property="location", type="string",
 *                  example="http://cdn.agentology.com/15d09b3e7f231949_15d09b3e7f231949_b5ae0f4c-481c-4ba9-855f-92eed91bde8d.pdf",
 *              ),
 *              @SWG\Property(property="created_at", type= "object", ref="#/definitions/LeadAttachmentsCreatedAt"),
 *              @SWG\Property(property="updated_at", type= "object", ref="#/definitions/LeadAttachmentsUpdatedAt")
 *
 * ),
 *
 *
 * @SWG\Definition(
 *          definition="LeadAttachmentsCreatedAt",
 *              @SWG\Property(property="date", type="string", example="2017-07-03 23:36:10"),
 *              @SWG\Property(property="timezone_type", type="integer", example="3"),
 *              @SWG\Property(property="timezone", type="string", example="UTC"),
 *  ),
 *
 * @SWG\Definition(
 *          definition="LeadAttachmentsUpdatedAt",
 *              @SWG\Property(property="date", type="string", example="2017-07-03 23:36:10"),
 *              @SWG\Property(property="timezone_type", type="integer", example="3"),
 *              @SWG\Property(property="timezone", type="string", example="UTC"),
 *  ),
 *
 */
