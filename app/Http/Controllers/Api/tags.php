<?php
/**
 * @SWG\Tag(
 *     name="Leads",
 *     description="Access to endpoints for leads"
 * ),
 * @SWG\Tag(
 *     name="Passport",
 *     description="Access to Laravel Passport endpoints"
 * ),
 * @SWG\Tag(
 *     name="Twilio",
 *     description="Access to Twilio web-hooks"
 * ),
 * @SWG\Tag(
 *     name="SendGrid",
 *     description="SendGrid web-hooks"
 * ),
 */
