<?php
/**
 * @SWG\Definition(
 *          definition="SendGridMessage",
 *              @SWG\Property(property="sg_message_id", type="string", example="sendgrid_internal_message_id"),
 *              @SWG\Property(property="email", type="string", example="john.doe@sendgrid.com"),
 *              @SWG\Property(property="timestamp", type="string", example="1337966815"),
 *              @SWG\Property(property="newuser", type= "string", example="1337966815"),
 *              @SWG\Property(property="smtp-id", type= "string",
 *                           example="<20120525181309.C1A9B40405B3@Example-Mac.local>"),
 *              @SWG\Property(property="event", type= "string", example="click"),
 *              @SWG\Property(property="url", type= "object", example="https://sendgrid.com"),
 *              @SWG\Property(property="UUID", type= "object", example="1401e01b-aa4d-4f4a-882b-dc11ee0209f0")
 * ),
 *
 */
