<?php
/**
 * @SWG\Post(
 *     path="/oauth/token/",
 *     tags={"Passport"},
 *     summary="Get authorization header.",
 *     description="Getting authorization token from passport",
 *     produces={"application/json"},
 *
 *     @SWG\Parameter(
 *         name="grant_type",
 *         in="formData",
 *         description="Passport grant type",
 *         required=true,
 *         type="string",
 *     ),
 *     @SWG\Parameter(
 *         name="client_id",
 *         in="formData",
 *         description="Passport client id",
 *         required=true,
 *         type="integer",
 *     ),
 *     @SWG\Parameter(
 *         name="client_secret",
 *         in="formData",
 *         description="Passport client secret",
 *         required=true,
 *         type="integer",
 *     ),
 *     @SWG\Parameter(
 *         name="scope",
 *         in="formData",
 *         description="Passport grant scope",
 *         required=false,
 *         type="string",
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="Token provided",
 *     ),
 *      @SWG\Response(
 *         response=400,
 *         description="Bad request",
 *     ),
 * )
 */
