<?php

namespace App\Http\Controllers\Api\V1\SendGrid;

use App\Agent;
use App\Events\Email\EmailUpdateStatus;
use App\Facades\LogDb;
use App\Lead;
use App\Email;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use App\Services\SendGridService;

/**
 * Class WebHookController
 * @package App\Http\Controllers\Api\V1\SendGrid
 */
class WebHookController extends Controller
{
    /**
     * The request object.
     *
     * @var Request
     */
    private $request;

    /**
     * Controller constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
    }

    /**
     * @SWG\Post(path="/api/v1/sendGrid/letterStatus",
     *     tags={"SendGrid"},
     *     summary="Change letters statuses",
     *     description="Web hook for changing multiple emails statuses.",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Lead that needs to be added.",
     *         required=true,
     *         type="array",
     *         @SWG\Schema(ref="#/definitions/SendGridMessage"),
     *     ),
     *     @SWG\Parameter(
     *            name="key",
     *            in="query",
     *            required=true,
     *            type="string",
     *            description="Authorization key",
     *        ),
     *     @SWG\Response(
     *         response=200,
     *         description="Letters array processed",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad request",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthenticated",
     *     ),
     * )
     *
     * @return JsonResponse
     */
    public function sendGridEmailEvent()
    {
        $data = $this->request->except('key');
        $errors = [];
        $responseStatus = Response::HTTP_OK;

        $validationRules = [
            'UUID' => 'uuid|required|exists:emails'
        ];

        foreach ($data as $item) {
            $lead = null;
            $email = null;

            $validator = Validator::make($item, $validationRules);

            if ($validator->fails()) {
                foreach ($validator->errors()->messages() as $message) {
                    $errors[] = ['message' => $message[0]];
                }

                continue;
            }

            $email = Email::where('UUID', '=', $item['UUID'])->first();

            $lead = Lead::where('id', $email->leadId)->first();
            $sendGridEvent = $item['event'];


            if ($lead) {
                switch ($sendGridEvent) {
                    case 'dropped':
                    case 'deferred':
                    case 'bounce':
                        $email->statusId = SendGridService::INVALID_EMAIL_STATUS_ID;
                        break;
                    case 'delivered':
                        $email->statusId = SendGridService::VALID_EMAIL_STATUS_ID;
                        break;
                }

                if ($email->save()) {
                    event(new EmailUpdateStatus($email->id, $lead->id, $email->statusId));
                } else {
                    logger()->error('Failed to update email status', $item);
                }
            } else {
                $errors[] = [
                    'email' => $item['email'],
                    'message' => 'No leads associated to current email.'
                ];
            }
        }

        $responseData['data'] = ['message' => 'Processed.'];
        $responseData['errors'] = $errors;

        LogDb::logApi($this->request, 'failed sendGridParseInbound', $responseStatus, $responseData);

        return response()->json($responseData, $responseStatus);
    }

    /**
     * @SWG\Post(path="/api/v1/sendGrid/inboundParse",
     *     tags={"SendGrid"},
     *     summary="Parse inbound SendGrid emails",
     *     description="Web-hook for Send Grid inbound emails.",
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *          name="dkim",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          description="dkim",
     *      ),
     *      @SWG\Parameter(
     *          name="to",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="to email",
     *      ),
     *      @SWG\Parameter(
     *          name="from",
     *          in="formData",
     *          required=true,
     *          type="string",
     *          description="from email",
     *      ),
     *      @SWG\Parameter(
     *          name="html",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          description="html letter",
     *      ),
     *     @SWG\Parameter(
     *            name="text",
     *            in="formData",
     *            required=true,
     *            type="string",
     *            description="Letter text",
     *        ),
     *     @SWG\Parameter(
     *            name="sender_ip",
     *            in="formData",
     *            required=false,
     *            type="string",
     *            description="Sender IP",
     *        ),
     *     @SWG\Parameter(
     *            name="envelope",
     *            in="formData",
     *            required=false,
     *            type="string",
     *            description="to and from emails arrays in json format",
     *        ),
     *     @SWG\Parameter(
     *            name="attachments",
     *            in="formData",
     *            required=false,
     *            type="file",
     *            description="Attachments",
     *        ),
     *     @SWG\Parameter(
     *            name="subject",
     *            in="formData",
     *            required=true,
     *            type="string",
     *            description="Letter subject",
     *        ),
     *     @SWG\Parameter(
     *            name="charsets",
     *            in="formData",
     *            required=false,
     *            type="string",
     *            description="Charsets",
     *        ),
     *     @SWG\Parameter(
     *            name="key",
     *            in="query",
     *            required=true,
     *            type="string",
     *            description="Ath key",
     *        ),
     *     @SWG\Response(
     *         response=200,
     *         description="Inbound letter processed",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad request",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthenticated",
     *     ),
     * )
     *
     * @return JsonResponse
     */
    public function sendGridParseInbound()
    {
        $data = $this->request->all();

        $envelope = json_decode($data['envelope']);
        $from = $envelope->from;
        $to = $envelope->to[0];
        $text = $data['text'];
        $subject = $data['subject'];
        $responseStatus = Response::HTTP_OK;
        $responseData = [];

        /** @var Agent $agent */
        $agent = Agent::where('sendGridEmail', 'ILIKE', $to)->first();
        // Parse lead id from subject
        $leads = $this->getLeadsBySubject($subject);
        // If there is no leads with such id or no id in subject try to search by email
        if (!$leads) {
            $leads = $agent->leads()->where('email', 'ILIKE', $from)->get();
        }

        if ($leads && $agent) {
            foreach ($leads as $lead) {
                $data = [
                    'conciergeId' => $lead->conciergeId ?? null,
                    'leadId' => $lead->id ?? null,
                    'from' => $from,
                    'to' => $to,
                    'agentId' => $agent->id,
                    'statusId' => SendGridService::VALID_EMAIL_STATUS_ID,
                    'senderFullName' => $lead->firstName . " " . $lead->lastName,
                    'incoming' => true,
                    'subject' => $subject,
                    'text' => $text
                ];
                Email::createEmail($data);
            }

            $responseData['data'] = ['message' => 'Processed.'];

            LogDb::logApi($this->request, 'success sendGridParseInbound', $responseStatus, $responseData);

            return response()->json($responseData, $responseStatus);
        }

        $responseData['errors'] = ['message' => 'No leads associated to current email.'];
        $responseStatus = Response::HTTP_BAD_REQUEST;

        LogDb::logApi($this->request, 'failed sendGridParseInbound', $responseStatus, $responseData);

        return response()->json($responseData, $responseStatus);
    }

    /**
     * Getting lead by parsing subject.
     *
     * @param string $subject
     *
     * @return array - array with one Lead object
     */
    private function getLeadsBySubject(string $subject)
    {
        $leads = [];
        $leadId = $this->parseInboundEmailSubjectForLeadId($subject);

        if ($leadId && Uuid::isValid($leadId)) {
            $leads[] = Lead::find($leadId);
        }

        return $leads;
    }

    /**
     * Parsing leadId from subject
     *
     * @param string $subject
     *
     * @return string
     */
    private function parseInboundEmailSubjectForLeadId(string $subject): string
    {
        $pattern = '/\[CustomerID:\s([^&]*)\]/';
        $leadId = '';

        if (preg_match($pattern, $subject, $result)) {
            $leadId = rtrim($result[1]);
        }

        return $leadId;
    }
}
