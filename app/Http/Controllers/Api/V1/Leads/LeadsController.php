<?php

namespace App\Http\Controllers\Api\V1\Leads;

use App\Agent;
use App\Events\Lead\LeadCreate;
use App\Events\Lead\LeadUpdate;
use App\Facades\Lead as LeadFacade;
use App\Facades\LogDb;
use App\Facades\VikingLeadHelper;
use App\Http\Controllers\Controller;
use App\Lead;
use App\LogsApi;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

/**
 * Class LeadsController
 * @package App\Http\Controllers\Api\V1\Leads
 */
class LeadsController extends Controller
{
    /**
     * @SWG\Post(path="/api/v1/leads",
     *     tags={"Leads"},
     *     summary="Save lead",
     *     description="Saves lead from viking.",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Lead that needs to be added.",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/LeadCreate"),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Lead saved",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad request",
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthenticated",
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createLead(Request $request)
    {
        $fullUrl = $request->getHttpHost() . ':'
            . $request->getPort()
            . $request->getPathInfo()
            . ($request->getQueryString() ? '?' . $request->getQueryString() : '');

        $logService = LogDb::setInstance(LogsApi::class);
        $logService->addParam('requestUrl', $fullUrl);
        $logService->addParam('clientIp', $request->getClientIp());
        $logService->addParam('action', 'Leads api');
        $logService->addParam('requestData', json_encode($request->request->all()));

        $data = [];

        $leadActionPerformed = 'Updated';
        $needsToEventCreate = false;

        $acceptableFields = config('api-settings.leads.fields');
        $requestData = $request->only($acceptableFields);

        $validate = $this->validateLeadData($requestData);

        if (!$validate['success']) {
            $logService->addParam('responseText', json_encode($validate['data']));
            $logService->addParam('responseStatus', 400);
            $logService->save();

            return response()->json([
                'errors' => $validate['data']
            ], 400);
        } else {
            if (isset($requestData['attachments'])) {
                foreach ($requestData['attachments'] as $item) {
                    VikingLeadHelper::createAttachmentForLead($item, $requestData['id']);
                }
            }
        }

        try {
            $lead = Lead::find($requestData['id']);

            if (!$lead) {
                $lead = new Lead();
                $leadActionPerformed = 'Saved';
                $lead->id = $requestData['id'];
                $lead->status = Lead::STATUS_NEW;
                $lead->tag = Lead::TAG_URGENT;

                $needsToEventCreate = true;
            }

            $formattedLeadData = LeadFacade::formatVikingLeadData($requestData);
            $lead->fill($formattedLeadData);
            $lead->save();

            $agent = Agent::where('vikingPatId', $lead->vikingPatId)->first();

            if ($agent) {
                $agent->leads()->save($lead);
            }

            LeadFacade::addSource($lead);
            LeadFacade::processNewLead($lead);

            if ($needsToEventCreate) {
                event(new LeadCreate($lead));
            } else {
                event(new LeadUpdate($lead));
            }
        } catch (Exception $exception) {
            $errorMessage = 'Failed to process new lead: ' . $exception->getMessage();
            logger()->error($errorMessage);

            $logService->addParam('responseText', $errorMessage);
            $logService->addParam('responseStatus', 500);
            $logService->save();

            return response()->json([
                'errors' => ['message' => 'Server exception.'],
            ], 500);
        }

        $responseText = 'Lead ' . $leadActionPerformed . '.';
        $data['message'] = $responseText;
        $logService->addParam('responseText', $responseText);
        $logService->save();

        return response()->json([
            'data' => $data,
        ], 200);
    }


    /**
     * Check lead data fields.
     * @param array $data - data from request
     *
     * @return array
     */
    private function validateLeadData(array $data)
    {
        $result = [];
        $result['success'] = true;
        $result['data'] = [];

        $validator = Validator::make($data, VikingLeadHelper::getVikingLeadValidationRules());

        if ($validator->fails()) {
            $errors = [];

            foreach ($validator->errors()->messages() as $message) {
                $errors[] = ['message' => $message[0]];
            }

            $result['success'] = false;
            $result['data'] = $errors;
        }

        return $result;
    }
}
