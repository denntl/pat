<?php

namespace App\Http\Controllers\Api\V1\Twilio;

use App\Agent;
use App\Events\Lead\LeadCallStatusUpdate;
use App\Events\Message\MessageCreate;
use App\Facades\Intercom;
use App\Facades\LogDb;
use App\Facades\PhoneNumber;
use App\Http\Controllers\Controller;
use App\Lead;
use App\Facades\LeadCall;
use App\LogsApi;
use App\Message;
use App\Facades\Message as MessageService;
use App\Services\TwilioService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Twilio\Twiml;
use App\Events\Message\MessageUpdateStatus;

/**
 * Class WebhookController
 * @package App\Http\Controllers\Api\V1\Twilio
 */
class WebhookController extends Controller
{
    /**
     * @var LogDb
     */
    private $logService;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->logService = LogDb::setInstance(LogsApi::class);
    }

    /**
     * @SWG\Post(path="/api/v1/twilio/messages/receive",
     *     tags={"Twilio"},
     *     summary="Twilio webhook",
     *     description="Twilio webhook for received messages",
     *     produces={"text/xml"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Twilio received message request",
     *         required=true,
     *         @SWG\Schema(
     *             type="object",
     *             required={
     *                 "AccountSid",
     *                 "ApiVersion",
     *                 "Body",
     *                 "From",
     *                 "FromCity",
     *                 "FromCountry",
     *                 "FromState",
     *                 "FromZip",
     *                 "MessageSid",
     *                 "NumMedia",
     *                 "NumSegments",
     *                 "SmsMessageSid",
     *                 "SmsSid",
     *                 "SmsStatus",
     *                 "To",
     *                 "ToCity",
     *                 "ToCountry",
     *                 "ToState",
     *                 "ToZip"
     *              },
     *              @SWG\Property(
     *                  property="AccountSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ApiVersion",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Body",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="From",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="FromCity",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="FromCountry",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="FromState",
     *                  type="string"
     *               ),
     *               @SWG\Property(
     *                   property="FromZip",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="MessageSid",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="NumMedia",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="NumSegments",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="SmsMessageSid",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="SmsSid",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="SmsStatus",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="To",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="ToCity",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="ToCountry",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="ToState",
     *                   type="string"
     *               ),
     *               @SWG\Property(
     *                   property="ToZip",
     *                   type="string"
     *               )
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Accepted Twilio received message request",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Not Twilio request",
     *     )
     * )
     *
     * @param Request $request
     * @return Twiml
     */
    public function receive(Request $request)
    {
        $this->createBaseLogData($request, 'message receive');
        $response = new Twiml();
        $messageText = $request->Body;

        /** @var Agent $agent */
        $agent = Agent::where('twilioNumber', $request->To)->first();
        if (!$agent) {
            return $response;
        }

        $phone = PhoneNumber::getNationalPhone($request->From);
        $leads = $agent->leads()->where('phone', $phone)->get();
        if (!$leads) {
            $logMessage = 'Leads with phone number: ' . $phone . ' not found.';

            $this->logService->addParam('responseText', 'Leads with phone number: ' . $phone . ' not found.');
            $this->logService->addParam('responseStatus', 404);
            $this->logService->save();

            info($logMessage);

            return $response;
        }

        foreach ($leads as $lead) {
            $incoming = 1;
            $isRead = 0;
            $isAutomatic = 0;
            $to = null;
            $from = null;
            $sender = null;
            $twilioMessageSid = $request->SmsSid;
            $twilioMessageStatus = $request->SmsStatus;

            $message = MessageService::createMessage([
                'conciergeId' => $lead->conciergeId,
                'leadId' => $lead->id,
                'incoming' => $incoming,
                'isRead' => $isRead,
                'isAutomatic' => $isAutomatic,
                'to' => $to,
                'from' => $from,
                'text' => $messageText,
                'twilioMessageSid' => $twilioMessageSid,
                'twilioMessageStatus' => $twilioMessageStatus
            ]);

            event(new MessageCreate($message, $sender));
        }

        $this->logService->addParam('responseText', 'Ok');
        $this->logService->save();

        return $response;
    }

    /**
     * @SWG\Post(path="/api/v1/twilio/messages/status-updated",
     *     tags={"Twilio"},
     *     summary="Twilio webhook",
     *     description="Twilio webhook for message status updates",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Twilio message status updated request",
     *         required=true,
     *         @SWG\Schema(
     *             type="object",
     *             required={
     *                 "AccountSid",
     *                 "ApiVersion",
     *                 "From",
     *                 "MessageSid",
     *                 "MessageStatus",
     *                 "SmsSid",
     *                 "SmsStatus",
     *                 "To"
     *              },
     *              @SWG\Property(
     *                  property="AccountSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ApiVersion",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="From",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="MessageSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="MessageStatus",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="SmsSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="SmsStatus",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="To",
     *                  type="string"
     *              )
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Accepted Twilio message status updated request",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Not Twilio request",
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function statusUpdated(Request $request)
    {
        $this->createBaseLogData($request, 'status updated');
        $twilioMessageSid = $request->MessageSid;
        $messageStatus = $request->MessageStatus;

        switch ($messageStatus) {
            case TwilioService::MESSAGE_DELIVERED_STATUS:
                $messages = Message::where('twilioMessageSid', $twilioMessageSid)->get();
                if (!$messages) {
                    $errorMessage = 'Messages with sid: ' . $twilioMessageSid . ' not found.';

                    $this->logService->addParam('responseStatus', 500);
                    $this->logService->addParam('responseText', $errorMessage);
                    $this->logService->save();

                    return response()->json([
                        'data' => [],
                        'error' => 'Message with sid: ' . $twilioMessageSid . ' not found.'
                    ], 500); // TODO: check allow to do that or not
                }

                foreach ($messages as $message) {
                    $message->twilioMessageStatus = $messageStatus;

                    if ($message->save()) {
                        event(
                            new MessageUpdateStatus($message->leadId, $twilioMessageSid, $messageStatus, $message->id)
                        );
                    }
                }
                break;
            case TwilioService::MESSAGE_SENT_STATUS:
                // TODO: Add sent status handler here
                break;
            default:
                // TODO: Add incorrect status handler here
                break;
        }

        $this->logService->addParam('responseText', 'Ok');
        $this->logService->save();

        return response()->json([
            'data' => []
        ], 200);
    }

    /**
     * @SWG\Post(path="/api/v1/twilio/call",
     *     tags={"Twilio"},
     *     summary="Twilio webhook",
     *     description="Twilio webhook for proccessing calls",
     *     produces={"text/xml"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Twilio call proccessing request",
     *         required=true,
     *         @SWG\Schema(
     *             type="object",
     *             required={
     *                 "AccountSid",
     *                 "ApiVersion",
     *                 "ApplicationSid",
     *                 "Called",
     *                 "Caller",
     *                 "CallStatus",
     *                 "CallSid",
     *                 "Direction",
     *                 "From",
     *                 "phoneNumber",
     *                 "To",
     *                 "twilioNumber",
     *              },
     *              @SWG\Property(
     *                  property="AccountSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ApiVersion",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ApplicationSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Called",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Caller",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="CallStatus",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="CallSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Direction",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="From",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="phoneNumber",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="To",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="twilioNumber",
     *                  type="string"
     *              )
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Accepted Twilio call proccessing request",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Not Twilio request",
     *     )
     * )
     *
     * @param Request $request
     * @return Twiml
     */
    public function call(Request $request)
    {
        $this->createBaseLogData($request, 'call');
        $response = new Twiml();
        if (!$request->twilioNumber) {
            if ($request->To) {
                /** @var Agent $agent */
                $agent = Agent::where('twilioNumber', $request->To)->first();
                if ($agent) {
                    $leadId = null;
                    $leadPhone = null;
                    if ($request->From) {
                        $phone = PhoneNumber::getNationalPhone($request->From);
                        /** @var Lead $lead */
                        $lead = Lead::where('phone', $phone)->first();
                        if ($lead) {
                            $leadId = $lead->id;
                            $leadPhone = $lead->phone;
                        }
                    }
                    $isForwarded = true;
                    $leadCall = LeadCall::create(
                        $leadId,
                        $request->CallSid,
                        $leadPhone,
                        $isForwarded,
                        Carbon::now()->toDateTimeString(),
                        $agent->id
                    ); // TODO: send some event about it

                    if ($leadCall->agentId) {
                        Intercom::updateCallsMetrics($leadCall->agentId);
                    }

                    $phone = $agent->callForwardingNumber ? $agent->callForwardingNumber : $agent->phone;
                    $phone = PhoneNumber::getTwilioPhone($phone);
                    $response->dial($phone);

                    $this->logService->addParam('responseText', json_encode([
                        'leadId' => $leadId,
                        'callSid' => $request->CallSid,
                        'leadPhone' => $leadPhone,
                        'agentId' => $agent->id,
                        'isForwarded' => $isForwarded,
                        'agentCallForwardingPhone' => $agent->callForwardingNumber,
                        'agentPhone' => $agent->phone
                    ]));
                    $this->logService->save();
                } else {
                    $this->logService->addParam(
                        'responseText',
                        'Agent with twilioNumber ' . $request->To . ' not found'
                    );
                    $this->logService->addParam('responseStatus', 404);
                    $this->logService->save();
                }

                return $response;
            }

            $this->logService->addParam(
                'responseText',
                'Request does not contain parameter "To"'
            );
            $this->logService->addParam('responseStatus', 400);
            $this->logService->save();

            return $response;
        }

        $this->logService->addParam(
            'responseText',
            'Request does not contain twilio number'
        );
        $this->logService->addParam('responseStatus', 400);
        $this->logService->save();

        $dial = $response->dial([
            'callerId' => $request->twilioNumber
        ]);
        $dial->number($request->phoneNumber, [
            'statusCallbackEvent' => 'initiated ringing answered completed',
            'statusCallback' => route('twilio_call_status_updated_hook'),
            'statusCallbackMethod' => 'POST'
        ]);

        return $response;
    }

    /**
     * @SWG\Post(path="/api/v1/twilio/call/status-updated",
     *     tags={"Twilio"},
     *     summary="Twilio webhook",
     *     description="Twilio webhook for call status updates",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Twilio message status updated request",
     *         required=true,
     *         @SWG\Schema(
     *             type="object",
     *             required={
     *                 "AccountSid",
     *                 "ApiVersion",
     *                 "CallbackSource",
     *                 "Called",
     *                 "Caller",
     *                 "CallSid",
     *                 "CallStatus",
     *                 "Direction",
     *                 "ParentCallSid",
     *                 "From",
     *                 "SequenceNumber",
     *                 "Timestamp",
     *                 "To"
     *              },
     *              @SWG\Property(
     *                  property="AccountSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ApiVersion",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="CallbackSource",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Called",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Caller",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="CallSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="CallStatus",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Direction",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ParentCallSid",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="From",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="SequenceNumber",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="Timestamp",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="To",
     *                  type="string"
     *              )
     *         ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Accepted Twilio call status updated request",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Not Twilio request",
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function callStatusUpdated(Request $request)
    {
        $this->createBaseLogData($request, 'call status updated');
        if ($request->Direction == 'inbound') {
            $this->logService->addParam('responseText', 'Inbound. Ok');
            $this->logService->save();
            return response()->json([
                'data' => []
            ], 200);
        }

        $leadPhone = PhoneNumber::getNationalPhone($request->Called);
        $agentPhone = $request->Caller;
        $lead = null;
        $agent = null;
        /** @var Agent $agent */
        $agent = Agent::where('twilioNumber', $agentPhone)->first();

        if ($agent) {
            $lead = $agent->leads()->where('phone', $leadPhone)->first();
        }

        if (!$lead) {
            $logMessage = 'Lead with phone number: ' . $leadPhone . ' not found.';

            $this->logService->addParam('responseText', $logMessage);
            $this->logService->addParam('responseStatus', 404);
            $this->logService->save();

            return response()->json([
                'data' => []
            ], 200);
        }

        $callStatus = $request->CallStatus;

        switch ($callStatus) {
            case TwilioService::CALL_INITIATED_STATUS:
                // TODO: Add initiated status handler here
                break;
            case TwilioService::CALL_RINGING_STATUS:
                event(new LeadCallStatusUpdate(
                    $lead->id,
                    $request->ParentCallSid,
                    TwilioService::CALL_RINGING_STATUS,
                    null
                ));
                break;
            case TwilioService::CALL_IN_PROGRESS_STATUS:
                break;
            case TwilioService::CALL_COMPLETED_STATUS:
                break;
            default:
                // TODO: Add incorrect status handler here
                break;
        }

        $this->logService->addParam('responseText', 'Ok.');
        $this->logService->save();

        return response()->json([
            'data' => []
        ], 200);
    }

    /**
     * Create record in database for api log.
     *
     * @param Request $request
     * @param string $action
     *
     * @return void
     */
    protected function createBaseLogData(Request $request, string $action)
    {
        $fullUrl = $request->getHttpHost() . ':'
            . $request->getPort()
            . $request->getPathInfo()
            . ($request->getQueryString() ? '?' . $request->getQueryString() : '');

        $this->logService->addParam('requestUrl', $fullUrl);
        $this->logService->addParam('clientIp', $request->getClientIp());
        $this->logService->addParam('action', 'Twilio api(' . $action . ')');

        $data = $request->request->all();
        $this->logService->addParam('requestData', json_encode($data));
    }
}
