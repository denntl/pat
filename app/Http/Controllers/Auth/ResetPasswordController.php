<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\PortalPasswordResets;

/**
 * Class ResetPasswordController.
 * @package App\Http\Controllers\Auth
 */
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * @inheritDoc
     */
    public function reset(Request $request, $broker = 'null')
    {
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker($broker)->reset(
            $this->credentials($request),
            function ($user, $password) use ($broker) {
                $this->resetPassword($user, $password, $broker);
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET;
    }

    /**
     * Reset password for portal.
     *
     * @param Request $request
     * @param null $token
     *
     * @return View
     */
    public function portalPasswordReset(Request $request, $token = null)
    {
        $users = PortalPasswordResets::get();

        foreach ($users as $user) {
            $checked = Hash::check($token, $user->token);
            if ($checked) {
                return view('index', array(
                    'title' => 'Portal :: Reset password',
                    'resetToken' => $token,
                    'email' => $user->email,
                    'role' => null,
                    'id' => null,
                    'token' => null,
                    'googleData' => null,
                ));
            }
        }

        return redirect()->route('index_portal');
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules($email): array
    {
        return [
            'email' => 'required|email',
            'token' => 'required|token_validation:'.$email,
            'password' => 'required|confirmed|min:6|same_as_old:'.$email
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages(): array
    {
        return [
            'email.required' => 'Internal error, please try to generate a new link with help of forget password form',
            'email.email' => 'Incorrect email, please try to generate a new link with help of forget password form',
            'token.required' => 'You are not authorize to make this request',
            'token.token_validation' => 'Invalid token',
            'password.required' => 'Please fill passwords fields',
            'password.confirmed' => 'Passwords don\'t match',
            'password.min' => 'New password must be at least 6 characters long',
            'password.same_as_old' => 'New password can\'t be the same as the old one'
        ];
    }
    /**
     * Validate request and set new password for agent.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function portalSetNewPassword(Request $request)
    {
        $email = $request->request->get('email', []);

        $this->validate($request, $this->rules($email), $this->validationErrorMessages());

        $result = $this->reset($request, 'portal');

        return new JsonResponse([
            'success' => $result
        ]);
    }

    /**
     * @inheritDoc
     */
    public function broker($broker = null)
    {
        return Password::broker($broker);
    }

    /**
     * @inheritDoc
     */
    protected function resetPassword($user, $password, $guard = null)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'rememberToken' => Str::random(60)
        ])->save();

        $this->guard($guard)->login($user);
    }
}
