<?php

namespace App\Http\Controllers\Auth;

use App\Facades\Intercom;
use App\Http\Controllers\Controller;
use App\Agent;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Two\User as SocialiteUser;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class LoginController
 *
 * @package App\Http\Controllers\Auth
 */
class LoginPortalController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Logout user from console.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function logout(Request $request)
    {
        /** @var $agent Agent */
        $agent = Auth::guard('portal')->user();

        Auth::guard('portal')->logout();

        $request->session()->flush();
        $request->session()->regenerate();

        $rememberTokenName = $agent->getRememberTokenName();
        $agent->{$rememberTokenName} = null;
        $agent->save();

        return redirect()->route('index_portal');
    }

    /**
     * For auth users -> redirect to main auth page
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function redirectMain(Request $request)
    {
        return redirect()->route('portal_user');
    }

    /**
     * Login portal user.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function loginPortalUser(Request $request)
    {
        $loginData = $request->request->get('loginForm', false);
        if ($loginData) {
            $agent = Agent::whereRaw('LOWER(email) = ?', strtolower($loginData['email']))->first();

            $attemptData = [
                'email' => $loginData['email'],
                'password' => $loginData['password']
            ];

            if (!$agent) {
                return new JsonResponse([
                    'success' => false,
                    'reason' => 'email'
                ]);
            }
            if ($agent->provider == 'google') {
                return new JsonResponse([
                    'success' => false,
                    'reason' => 'provider'
                ]);
            }
            if (Auth::guard('portal')->attempt($attemptData, true)) {
                if ($agent->salesForceId) {
                    Intercom::updateLastSeen($loginData['email']);
                }

                return new JsonResponse([
                    'success' => true
                ]);
            }
        }
    }

    /**
     * Redirect the user to portal login page.
     *
     * @return RedirectResponse
     */
    public function redirectToLoginPortalProvider()
    {
        $route = route('portal_google_auth_login_callback');
        $scope = ['profile'];

        return Socialite::driver('google')->redirectUrl($route)->scopes($scope)->redirect();
    }

    /**
     * Redirect the user to the Google authentication page (for portal).
     *
     * @return RedirectResponse
     */
    public function redirectToRegisterPortalProvider()
    {
        $route = route('portal_google_auth_register_callback');
        $scope = ['profile'];

        return Socialite::driver('google')->redirectUrl($route)->scopes($scope)->redirect();
    }

    /**
     * Handle portal login provider callback.
     *
     * @return RedirectResponse
     */
    public function handlePortalLoginProviderCallback()
    {
        try {
            /** @var SocialiteUser $user */
            $route = route('portal_google_auth_login_callback');
            $user = Socialite::driver('google')->redirectUrl($route)->user();
        } catch (\Exception $e) {
            return redirect()
                ->route('index_portal')
                ->withErrors(
                    'Oops! It looks like you declined Google’s authorization. '.
                    'Please accept the terms or sign in below.',
                    'googleError'
                );
        }

        $authUser = Agent::whereRaw('LOWER(email) = ?', strtolower($user->email))->first();

        if ($authUser) {
            Auth::guard('portal')->login($authUser, true);

            return redirect()->route('portal_user');
        }

        return redirect()
            ->route('index_portal')
            ->withErrors(
                'Oops! It looks like we don’t have a record of that Google account on file.' .
                'Please try a different Google account or sign in below.',
                'googleError'
            );
    }

    /**
     * Handle portal provider callback.
     *
     * @return RedirectResponse
     */
    public function handleRegisterPortalProviderCallback()
    {
        /** @var SocialiteUser $user */
        $user = Socialite::driver('google')
            ->redirectUrl(route('portal_google_auth_register_callback'))
            ->user();

        $googleUserData = $this->getGoogleUserData($user, 'google');

        $agent = Agent::whereRaw('LOWER(email) = ?', strtolower($googleUserData['email']))->first();

        if ($agent) {
            return redirect()
                ->route('index_portal')
                ->withErrors(
                    'Oops! It looks like agent with such email is already registered!',
                    'googleError'
                );
        }

        return redirect()
            ->route('create_account_portal')
            ->with('googleData', $googleUserData);
    }

    /**
     * Portal login callback
     *
     * @return RedirectResponse|\Illuminate\View\View
     */
    public function portalUser()
    {
        $user = Auth::guard('portal')->user();
        $role = 0;
        $id = 0;
        $token = null;
        if ($user) {
            $role = $user->role;
            $id = $user->id;
            $token = $user->token;
        }

        return view('index', [
            'title' => 'Portal :: User',
            'role' => $role,
            'id' => $id,
            'token' => $token,
            'googleData' => [],
            'portalPage' => true,
            'isConcierge' => false,
        ]);
    }

    /**
     * Get Google user data.
     *
     * @param SocialiteUser $user
     * @param string $provider
     *
     * @return array
     */
    private function getGoogleUserData($user, $provider)
    {
        return [
            'firstName' => $user->user['name']['givenName'],
            'lastName' => $user->user['name']['familyName'],
            'email' => $user->email,
            'provider' => $provider,
            'providerId' => $user->id,
        ];
    }

    /**
     * Make request to google to get token for email parsing.
     *
     * @deprecated
     *
     * @return RedirectResponse
     */
    public function getTokenForUserEmails()
    {
        $route = route('process_google_email_auth_token');
        $scope = ['profile', 'https://www.googleapis.com/auth/gmail.readonly'];

        return Socialite::driver('google')->redirectUrl($route)->scopes($scope)->redirect();
    }

    /**
     * Handle receiving token from google for email parsing.
     *
     * @deprecated
     *
     * @return RedirectResponse
     */
    public function handleTokenForEmailCallback()
    {
        $googleUser = Socialite::driver('google')
            ->redirectUrl(route('process_google_email_auth_token'))
            ->user();

        $authenticatedAgent = Auth::guard('portal')->user();

        $authenticatedAgent->forceFill([
            'googleEmailScopeToken' => $googleUser->token
        ])->save();

        return redirect()->route('index_portal');
    }
}
