<?php

namespace App\Http\Controllers\Auth;

use App\Agent;
use App\Concierge;
use App\Events\Agent\AgentCreate;
use App\Facades\Agentology;
use App\Facades\Viking;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Facades\Intercom;

/**
 * Class RegisterController.
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Register portal user.
     * @return JsonResponse
     */
    public function registerPortalUser()
    {
        $validatorArray = [
            'email' => 'required|email|max:255|unique:agents',
            'password' => 'required|min:6',
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required|min:12',
        ];

        $userData = Input::all();

        if ($userData['providerId']) {
            unset($validatorArray['password']);
            $userData['password'] = null;
        }

        $validator = Validator::make($userData, $validatorArray, [
            'firstName.required' => 'We need your first name',
            'lastName.required' => 'We need your last name',
            'phone.regex' => 'Numbers only',
            'phone.min' => 'Phone number should contain 10 digits'
        ]);

        if ($validator->fails()) {
            // Handles soft deleted agent
            $trashedAgent = Agent::onlyTrashed()
                    ->where('email', 'ILIKE', $userData['email'])->first();

            if ($trashedAgent) {
                return new JsonResponse([
                    'errors' => [
                        'email' => [
                            'You can restore account by using "restore password."'
                        ]
                    ]
                ]);
            }

            return new JsonResponse([
                'errors' => $validator->errors()
            ]);
        }

        $agent = Agent::whereRaw('LOWER(email) = ?', strtolower($userData['email']))->first();
        if ($agent) {
            return new JsonResponse([
                'errors' => [
                    'email' => ['The email has already been taken.']
                ]
            ]);
        }

        $emailForwarding = $this->generateForwardingEmail($userData);

        $agent = Agent::create([
            'email' => $userData['email'],
            'firstName' => $userData['firstName'],
            'lastName' => $userData['lastName'],
            'phone' => $userData['phone'],
            'password' => bcrypt($userData['password']),
            'provider' => $userData['provider'],
            'providerId' => $userData['providerId'],
            'notificationPhone' => $userData['phone'],
            'notificationEmail' => $userData['email'],
            'emailForwarding' => $emailForwarding
        ]);

        if (!$agent) {
            return new JsonResponse([
                'errors' => [
                    'email' => ['The email has already been taken.']
                ]
            ]);
        }

        event(new AgentCreate($agent));

        Auth::guard('portal')->login($agent, true);

        return new JsonResponse([
            'success' => true
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new concierge instance after a valid registration.
     *
     * @param  array $data
     * @return Concierge
     */
    protected function create(array $data)
    {
        return Concierge::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Method to generate unique email for agent to send to viking.
     *
     * @param array $userData
     * @return string
     */
    private function generateForwardingEmail(array $userData): string
    {
        $i = 0;

        do {
            $agentFullName = $userData['firstName'] . $userData['lastName'];
            if ($i != 0) {
                $agentFullName .= $i;
            }
            $email = $agentFullName . '@agentology.me';
            $email = strtolower($email);
            $i++;
        } while (Agent::where('emailForwarding', $email)->first());

        return $email;
    }
}
