<?php

namespace App\Http\Controllers\Auth;

use App\Agent;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

/**
 * Class ForgotPasswordController.
 * @package App\Http\Controllers\Auth
 */
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * @inheritDoc
     */
    public function broker($broker = null)
    {
        return Password::broker($broker);
    }

    /**
     * Send new password.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sendNewPassword(Request $request)
    {
        return $this->sendAppResetLinkEmail($request, 'portal');
    }

    /**
     * Send app reset link via email.
     *
     * @param Request $request
     * @param null $broker
     *
     * @return JsonResponse
     */
    public function sendAppResetLinkEmail(Request $request, $broker = null)
    {
        $data = $request->only('email');
        $data['email'] = strtolower($data['email']);
        $validatorRules = ['email' => 'required|email|exists:agents'];
        $validator = Validator::make($data, $validatorRules, [
            'exists' => 'Cound not find email in our system'
        ]);
        /*@var Agent*/
        $trashedAgent = Agent::onlyTrashed()
            ->whereRaw('LOWER(email) = ?', strtolower($data['email']))->first();

        if ($trashedAgent) {
            $this->restoreTrashedAgent($trashedAgent);
        }

        if ($validator->fails()) {
            $errors = [];

            foreach ($validator->errors()->messages() as $error => $value) {
                $errors[$error] = $value[0];
            }

            $result['success'] = false;
            $result['errors'] = $errors;

            return response()->json($result);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker($broker)->sendResetLink($data);

        if (!$response == Password::RESET_LINK_SENT) {
            $result['errors'] = ['reset_link' => 'Reset link not send'];

            return response()->json($result);
        }

        return response()->json(['success' => true]);
    }

    /**
     * Restore trashed Agent.
     *
     * @param Agent $trashedAgent
     */
    private function restoreTrashedAgent(Agent $trashedAgent)
    {
        if ($this->checkIfAgentCanBeRestored($trashedAgent)) {
            $trashedAgent->restore();
        }
    }

    /**
     * Checks if agent can be restored.
     *
     * @param Agent $trashedAgent
     *
     * @return bool
     */
    private function checkIfAgentCanBeRestored(Agent $trashedAgent): bool
    {
        if ($trashedAgent && $trashedAgent->trashed()) {
            return true;
        }
        // TODO: add validation when addition info will be provided
        return false;
    }
}
