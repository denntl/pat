<?php

namespace App\Http\Controllers\Auth;

use App\Concierge;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\User;

/**
 * Class LoginController.
 * @package App\Http\Controllers\Auth
 */
class LoginConsoleController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Logout concierge from console.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request)
    {
        /** @var $concierge Concierge */
        $concierge = Auth::guard('web')->user();

        Auth::guard('web')->logout();

        $request->session()->flush();
        $request->session()->regenerate();

        $rememberTokenName = $concierge->getRememberTokenName();
        $concierge->{$rememberTokenName} = null;
        $concierge->save();

        return redirect()->route('index');
    }

    /**
     * Redirect the concierge to the Google authentication page (for console).
     *
     * @return RedirectResponse
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->scopes(['profile'])->redirect();
    }

    /**
     * Obtain the concierge information from Google.
     *
     * @return RedirectResponse
     */
    public function handleProviderCallback()
    {
        try {
            /** @var User $user */
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect()->route('index')->withErrors('User declined Google authorization', 'authError');
        }

        /** @var Concierge $authUser */
        $authUser = $this->findUser($user, 'google');
        if (!$authUser) {
            return redirect()->route('index')->withErrors('Account does not exist', 'authError');
        }

        if ($authUser->role === 0) {
            return redirect()->route('index')->withErrors('Account is suspended', 'authError');
        }

        Auth::login($authUser, true);

        if ($authUser->role === 3) {
            return redirect()->route('permission');
        }

        return redirect()->route('messaging');
    }

    /**
     * If a concierge has registered before using social auth, return the concierge.
     *
     * @param User $user
     * @param string $provider
     *
     * @return Concierge|false
     */
    private function findUser($user, $provider)
    {
        // search for concierge with email in db if no concierge redirect to login else search by provider_id
        $existentUser = Concierge::whereRaw('LOWER(email) = ?', strtolower($user->email))->first();
        if (!$existentUser) {
            return false;
        }

        $authUser = Concierge::where('providerId', $user->id)->first();

        // if user with provider_id exists return concierge else set to Concierge provider and provider_id
        // and return this concierge
        if ($authUser) {
            return $authUser;
        }

        $existentUser->provider = $provider;
        $existentUser->providerId = $user->id;
        $existentUser->name = $user->name;

        return $existentUser;
    }
}
