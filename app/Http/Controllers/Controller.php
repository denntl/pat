<?php

namespace App\Http\Controllers;

use App\Concierge;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

/**
 * Class Controller.
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var string
     */
    public static $GOOGLE_MAPS_STYLES = 'style=feature%3Alandscape.man_made%7Celement%3Ageometry%7Ccolor%3A0xf7f1df%7C&'
            .'style=feature%3Alandscape.natural%7Celement%3Ageometry%7Ccolor%3A0xd0e3b4%7C&'
            .'style=feature%3Alandscape.natural.terrain%7Celement%3Ageometry%7Cvisibility%3Aoff%7C&'
            .'style=feature%3Apoi%7Celement%3Alabels%7Cvisibility%3Aoff%7C&'
            .'style=feature%3Apoi.business%7Celement%3Aall%7Cvisibility%3Aoff%7C&'
            .'style=feature%3Apoi.medical%7Celement%3Ageometry%7Ccolor%3A0xfbd3da%7C&'
            .'style=feature%3Apoi.park%7Celement%3Ageometry%7Ccolor%3A0xbde6ab%7C&'
            .'style=feature%3Aroad%7Celement%3Ageometry.stroke%7Cvisibility%3Aoff%7C&'
            .'style=feature%3Aroad%7Celement%3Alabels%7Cvisibility%3Aoff%7C&'
            .'style=feature%3Aroad.highway%7Celement%3Ageometry.fill%7Ccolor%3A0xffe15f%7C&'
            .'style=feature%3Aroad.highway%7Celement%3Ageometry.stroke%7Ccolor%3A0xefd151%7C&'
            .'style=feature%3Aroad.arterial%7Celement%3Ageometry.fill%7Ccolor%3A0xffffff%7C&'
            .'style=feature%3Aroad.local%7Celement%3Ageometry.fill%7Ccolor%3Ablack%7C&'
            .'style=feature%3Atransit.station.airport%7Celement%3Ageometry.fill%7Ccolor%3A0xcfb2db%7C&'
            .'style=feature%3Awater%7Celement%3Ageometry%7Ccolor%3A0xa2daf2%7C';

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        View::share('googleStaticMapsKey', config('services.google.maps_key'));
        View::share('rollBarAccessToken', config('services.rollbar.front_token'));
        View::share('googleStylesData', static::$GOOGLE_MAPS_STYLES);
    }

    /**
     * Get index view for console.
     *
     * @param Request $request
     * @return View
     */
    public function getIndexView(Request $request)
    {
        return $this->getMainView($request, 'Console :: Pat');
    }

    /**
     * Get index view for portal.
     *
     * @param Request $request
     * @return View
     */
    public function getPortalView(Request $request)
    {
        return $this->getMainView($request, 'Portal :: Pat');
    }

    /**
     * Get main view.
     *
     * @param Request $request
     * @param string $title
     * @return View
     */
    private function getMainView(Request $request, $title)
    {
        /** @var Concierge $concierge */
        $concierge = Auth::user();
        $role = 0;
        $id = 0;
        $token = null;
        if ($concierge) {
            $role = $concierge->role;
            $id = $concierge->id;
            $token = $concierge->getToken();
        }

        $googleData = $request->session()->get('googleData', array());

        return view('index', [
            'title' => $title,
            'role' => $role,
            'id' => $id,
            'token' => $token,
            'googleData' => $googleData,
            'isConcierge' => true,
        ]);
    }
}
