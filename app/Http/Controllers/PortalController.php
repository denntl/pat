<?php

namespace App\Http\Controllers;

use App\Agent;
use App\AgentOnBoarding;
use App\Facades\Intercom;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use \Request as StaticRequest;

/**
 * Class Controller.
 * @package App\Http\Controllers
 */
class PortalController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * PortalController constructor.
     */
    public function __construct()
    {
        View::share('googleStaticMapsKey', config('services.google.maps_key'));
        View::share('rollBarAccessToken', config('services.rollbar.front_token'));
        View::share('googleStylesData', Controller::$GOOGLE_MAPS_STYLES);
    }

    /**
     * Get index view.
     *
     * @param Request $request
     * @return RedirectResponse|View
     */
    public function getIndex(Request $request)
    {
        /** @var Agent $agent */
        $agent = Auth::guard('portal')->user();
        if ($agent) {
            if ($agent->salesForceId) {
                Intercom::updateLastSeen($agent->email);
            }

            $checkSetup  = $this->checkSetup($agent);

            if ($checkSetup) {
                return redirect()->route('portal_setup');
            }

            return redirect()->route('portal_user');
        }

        $googleData = $request->session()->get('googleData', []);
        $role = 0;
        $id = 0;
        $token = null;
        if ($agent) {
            $role = $agent->role;
            $id = $agent->id;
            $token = $agent->getToken();
        }

        return view('index', [
            'title' => 'Portal :: Pat',
            'role' => $role,
            'id' => $id,
            'token' => $token,
            'googleData' => $googleData,
            'isConcierge' => false,
        ]);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function resetPortalPasswordView(Request $request)
    {
        $googleData = $request->session()->get('googleData', []);

        return view('index', [
            'title' => 'Portal :: Reset password',
            'role' => 0,
            'id' => 0,
            'token' => null,
            'googleData' => $googleData,
            'isConcierge' => false,
        ]);
    }

    /**
     * Get auth view.
     *
     * @param Request $request
     * @return RedirectResponse|View
     */
    public function getContent(Request $request)
    {
        /** @var Agent $agent */
        $agent = Auth::guard('portal')->user();
        if (!$agent) {
            return redirect()->route('index_portal');
        }

        $checkSetup = $this->checkSetup($agent);

        if ($checkSetup) {
            return redirect()->route('portal_setup');
        }

        $googleData = $request->session()->get('googleData', []);
        $role = 0;
        $id = 0;
        $token = null;
        $sessionId = '';
        if ($agent) {
            $role = $agent->role;
            $id = $agent->id;
            $token = $agent->getToken();
            $sessionId = $request->session()->getId();
        }

        return view('index', [
            'title' => 'Portal :: Content',
            'role' => $role,
            'id' => $id,
            'token' => $token,
            'googleData' => $googleData,
            'portalPage' => true,
            'sessionId' => $sessionId,
            'isConcierge' => false,
            'referralNetworkUrl' => env('REFERRAL_NETWORK_URL')
        ]);
    }

    /**
     * Method to check onboarding tutorial.
     *
     * @param Agent $agent
     * @return boolean
     */
    private function checkSetup($agent)
    {
        $agentOnBoarding = AgentOnBoarding::where('agentId', $agent->id)->first();
        $setupRoute = 'portal_setup';
        $currentRouteName = StaticRequest::route()->getName();
        $agentOnBoardingEnded = $agentOnBoarding && $agentOnBoarding->step <= 3;
        if ((!$agentOnBoarding || $agentOnBoardingEnded) && $currentRouteName != $setupRoute) {
            return true;
        }

        return false;
    }
}
