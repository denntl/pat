<?php

namespace App\Console;

use App\Console\Commands\Message\FailedOutgoingSMSTimeout;
use App\Console\Commands\Viking\VikingLeadSync;
use App\Console\Commands\Intercom\UpdateMetrics;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Agent\AgentAll::class,
        Commands\Agent\AgentGet::class,
        Commands\Agent\AgentGetPersona::class,
        Commands\Agent\AgentPersonaUpdate::class,
        Commands\Agent\AgentNotificationUpdate::class,
        Commands\Agent\AgentUpdate::class,
        Commands\Agent\AgentPasswordUpdate::class,
        Commands\Agent\AgentSetup::class,
        Commands\Agent\AgentSendLeadTo::class,
        Commands\Agent\AgentUpdateOnBoarding::class,
        Commands\Agentology\RegenerateAgentologyUUID::class,
        Commands\Agentology\Lead\RegenerateOpportunity::class,
        Commands\Concierge\ConciergeCreate::class,
        Commands\Concierge\ConciergeDelete::class,
        Commands\Concierge\ConciergeUpdate::class,
        Commands\Concierge\ConciergeSwitchDistribution::class,
        Commands\Concierge\ConciergeAll::class,
        Commands\Lead\LeadGetPortal::class,
        Commands\Lead\LeadGet::class,
        Commands\Lead\LeadUpdate::class,
        Commands\Lead\LeadUpdateByAgent::class,
        Commands\Lead\LeadSendEmails::class,
        Commands\Lead\LeadAll::class,
        Commands\Lead\LeadCall::class,
        Commands\Lead\LeadCreate::class,
        Commands\Lead\LeadCallEnd::class,
        Commands\Lead\LeadCallNotes::class,
        Commands\Lead\LeadCreate::class,
        Commands\Lead\LeadRefer::class,
        Commands\Lead\LeadUpdateRep::class,
        Commands\Lead\LeadUpdateStatus::class,
        Commands\Lead\LeadUnassignRep::class,
        Commands\Lead\LeadReassign::class,
        Commands\Lead\LeadReassignSingle::class,
        Commands\Viking\VikingLeadSync::class,
        Commands\Viking\AgentSync::class,
        Commands\Viking\AttachExistingAgents::class,
        Commands\Viking\RegenerateAgentId::class,
        Commands\Viking\DisconnectAgentGoogle::class,
        Commands\Note\NoteCreate::class,
        Commands\Message\MessageAll::class,
        Commands\Message\MessageCreate::class,
        Commands\Message\MessageUpdateStatus::class,
        Commands\Message\MessageReadAll::class,
        Commands\Message\FailedOutgoingSMSTimeout::class,
        Commands\Email\EmailAll::class,
        Commands\Email\EmailCreate::class,
        Commands\Email\EmailReadAll::class,
        Commands\Email\UpdateEmailsStatusesForDemoAndDev::class,
        Commands\Email\SetDeliveredStatusForInboundLetters::class,
        Commands\Twilio\Sms::class,
        Commands\Twilio\GeneratePhoneByArea::class,
        Commands\Twilio\ValidatePhoneNumber::class,
        Commands\Intercom\UpdateMetrics::class,
        Commands\Subscribe\SubscribeMessaging::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(VikingLeadSync::class)->cron('7 */1 * * *');
        $schedule->command(UpdateMetrics::class)->dailyAt('02:12');
        $schedule->command(FailedOutgoingSMSTimeout::class)->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
