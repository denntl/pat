<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\AgentOnBoarding;
use App\Facades\Intercom;
use App\Events\LogoutAgent as Logout;
use App\Events\Agent\AgentUpdateOnBoarding as AgentUpdateOnBoardingEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use App\Events\Agent\AgentPersonaUpdate as AgentPersonaUpdateEvent;

/**
 * Class AgentUpdateOnBoarding.
 *
 * @example php artisan agent:update:tutorial 1 2
 * @example php artisan agent:update:tutorial -S=eyJhaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGx9
 *
 * @package App\Console\Commands\Agent
 */
class AgentUpdateOnBoarding extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:update:tutorial {id?} {step?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update agent tutorial';

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handleCommand()
    {
        /** @var int $id - Agent id */
        $id = $this->argument('id');
        $step = $this->argument('step');
        if (!$id) {
            $this->error('Please set up agent id');
            return;
        }

        /** @var $agentOnBoarding AgentOnBoarding */
        $agentOnBoarding = AgentOnBoarding::where('id', $id);
        $agentOnBoarding->step = $step;
        $agentOnBoarding->save();
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'data' => array - concierge identity data
     *     'step' => int - current step of tutorial
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');
        $data = $this->socket->get('data');
        $step = $this->socket->get('step');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        $agent = Agent::find($agentId);

        /** @var $agentOnBoarding AgentOnBoarding */
        $agentOnBoarding = AgentOnBoarding::where('agentId', $agentId)->first();
        if (!$agentOnBoarding) {
            $agentOnBoarding = new AgentOnBoarding();
            $agentOnBoarding->agentId = $agentId;
        }
        if ($agentOnBoarding->step < $step) {
            $agentOnBoarding->step = $step;
        }

        $agentOnBoarding->save();

        if ($agentOnBoarding->step == 2) {
            $agent->fill([
                'twilioNumber' => $data['phoneNumber'],
                'areaCode' => $data['areaCode'],
            ]);
            $agent->saveOrFail();
        }

        if ($agentOnBoarding->step == 3) {
            $agentPersonaData = [
                'personaTeamName' => $data['teamName'],
                'personaName' => $data['conciergeName'],
                'personaTitle' => $data['conciergeTitle'],
                'extraNoteForRep' => null,
            ];

            event(new AgentPersonaUpdateEvent($agentId, $agentPersonaData, $socketId));
        }

        if ($agentOnBoarding->step > 3) {
            if ($agent->agentologyUUID) {
                Intercom::registerUser($agent);
            }
        }

        return event(new AgentUpdateOnBoardingEvent($socketId, $agentId, $step));
    }
}
