<?php

namespace App\Console\Commands\Agent;

use App\Concierge;
use App\Events\LogoutAgent as Logout;
use App\Events\Agent\AgentAll as AgentAllEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class AgentAll.
 *
 * @example php artisan agent:all 0 10 -Cid -Cemail
 * @example php artisan agent:all -S=eyJyaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGx9
 *
 * @package App\Console\Commands\Agent
 */
class AgentAll extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:all {offset?} {limit?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get all agents';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $offset = $this->argument('offset');
        $limit = $this->argument('limit');
        if ($offset === null || !$limit) {
            $this->error('Please set up an offset & limit parameters to fetch agents');
            return;
        }

        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'firstName',
                'lastName',
                'email',
                'phone',
                'createdAt',
                'updatedAt'
            ];
        }

        $agents = Agent::all($columns)->toBase()->slice($offset, $limit);

        $this->table($columns, $agents->toArray());
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        return event(new AgentAllEvent());
    }
}
