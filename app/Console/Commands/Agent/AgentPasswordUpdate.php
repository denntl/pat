<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Events\Agent\AgentPasswordUpdate as AgentPasswordUpdateEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

/**
 * Class AgentPasswordUpdate.
 *
 * @example php artisan agent:password:update 1 'newPassword'
 * @example php artisan agent:password:update -S=eyJyaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGx9
 *
 * @package App\Console\Commands\Agent
 */
class AgentPasswordUpdate extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:password:update {id?} {password?} {newPassword?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update agent password';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        $password = $this->argument('password');
        $newPassword = $this->argument('newPassword');

        if (!$id || !$password || !$newPassword) {
            $this->error('Some argument is missed. Please check command signature');
            return;
        }

        $agent = Agent::findOrFail($id);
        if ($agent->provider == 'google') {
            $this->info('Can\'t update password for current agent');
            return;
        }

        $validPassword = Hash::check($password, $agent->password);
        if (!$validPassword) {
            $this->error('Incorrect current password');
            return;
        }

        $agent->password = Hash::make($newPassword);
        $agent->saveOrFail();

        $this->info('Password for agent ' . $id . ' was successful updated');
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'session' => string - Laravel session id
     *     'password' => string - Current agent password
     *     'newPassword' => string - New agent password
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $token = $this->socket->get('token');
        $socketId = $this->socket->get('sid');
        $sessionId = $this->socket->get('session');

        // Prevent from CSRF attacks
        $tokenValid = Agent::checkStaticToken($agentId, $token);
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, $sessionId, $msg));
        }

        $agent = Agent::find($agentId);

        // Prevent password updating for agents who used Google Auth to sign up
        if ($agent->provider == 'google') {
            $msg = 'googleAuth';
            $this->info($msg);
            return event(new AgentPasswordUpdateEvent($agentId, $msg));
        }

        // Avoids incorrect current password
        $passwordValid = Hash::check($this->socket->get('password'), $agent->password);
        if (!$passwordValid) {
            $msg = 'invalid';
            $this->info($msg);
            return event(new AgentPasswordUpdateEvent($agentId, $msg));
        }

        $agent->password = Hash::make($this->socket->get('newPassword'));

        $saved = $agent->save();
        if (!$saved) {
            $msg = 'error';
            $this->info($msg);
            return event(new AgentPasswordUpdateEvent($agentId, $msg));
        }

        event(new AgentPasswordUpdateEvent($agentId, 'success'));

        return event(new Logout($agentId, $socketId, $sessionId, 'Your password is updated'));
    }
}
