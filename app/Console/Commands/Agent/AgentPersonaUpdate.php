<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Events\Agent\AgentPersonaUpdate as AgentPersonaUpdateEvent;
use App\Events\LogoutAgent as Logout;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class AgentSettings.
 *
 * @example php artisan agent:persona:update
 * @example php artisan agent:persona:update -S=eyJzaWQiOm51bGwsInRva2VuIjpudWxsLCJkYXRhIjpbXX0=
 *
 * @package App\Console\Commands\Agent
 */
class AgentPersonaUpdate extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:persona:update {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update agent persona settings';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token,
     *     'data' => array - Settings data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        return event(new AgentPersonaUpdateEvent($agentId, $this->socket->get('data'), $socketId));
    }
}
