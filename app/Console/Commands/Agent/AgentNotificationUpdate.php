<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Events\Agent\AgentNotificationUpdate as AgentNotificationUpdateEvent;
use App\Events\LogoutAgent as Logout;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class AgentNotificationUpdate.
 *
 * @example php artisan agent:notification:update
 * @example php artisan agent:notification:update -S=eyJzaWQiOm51bGwsInRva2VuIjpudWxsLCJkYXRhIjpbXX0=
 *
 * @package App\Console\Commands\Agent
 */
class AgentNotificationUpdate extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:notification:update {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update agent notification settings';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token,
     *     'data' => array - Settings data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');
        $data = $this->socket->get('data');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        $agent = Agent::find($agentId);
        $agent->notificationPhone = $data['notificationPhone'];
        $agent->notificationEmail = $data['notificationEmail']; // TODO: here should be few email usage
        $agent->callForwardingNumber = $data['callForwardingNumber'];
        $agent->saveOrFail();

        return event(new AgentNotificationUpdateEvent($agentId, $data, $socketId));
    }
}
