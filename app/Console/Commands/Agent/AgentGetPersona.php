<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Concierge;
use App\Events\LogoutAgent as Logout;
use App\Events\Agent\AgentGetPersonaData as AgentGetPersonaDataEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class AgentGetPersona.
 *
 * @example php artisan agent:persona:get 1
 * @example php artisan agent:persona:get -S=eyJyaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGx9
 *
 * @package App\Console\Commands\Agent
 */
class AgentGetPersona extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:persona:get {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get persona data for one agent';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Please set up agent id');
            return;
        }

        $columns = [
            'personaName',
            'personaTeamName',
            'personaTitle'
        ];

        $agent = Agent::all($columns)->where('id', $id)->toArray();

        $this->table($columns, $agent);
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, null, $msg));
        }

        return event(new AgentGetPersonaDataEvent($this->socket->get('aid'), $socketId));
    }
}
