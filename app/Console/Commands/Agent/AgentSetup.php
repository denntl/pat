<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use App\Events\Agent\AgentSetup as AgentSetupEvent;

/**
 * Class AgentSetup.
 *
 * @example php artisan agent:get:setup 1
 * @example php artisan agent:get:setup -S=eyJhaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGx9
 *
 * @package App\Console\Commands\Agent
 */
class AgentSetup extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:get:setup {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get agent setup';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        /** @var int $id - Agent id */
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Please set up agent id');
            return;
        }

        return;
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        return event(new AgentSetupEvent($agentId, $socketId));
    }
}
