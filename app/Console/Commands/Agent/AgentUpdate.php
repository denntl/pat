<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Events\Agent\AgentUpdate as AgentUpdateEvent;
use App\Events\Agent\AgentUpdateFailed as AgentUpdateFailedEvent;
use App\Traits\WebSocket;
use App\Facades\Intercom;
use Illuminate\Console\Command;

/**
 * Class AgentUpdate.
 *
 * @example php artisan agent:update
 * @example php artisan agent:update -S=eyJzaWQiOm51bGwsInRva2VuIjpudWxsLCJkYXRhIjpbXX0=
 *
 * @package App\Console\Commands\Agent
 */
class AgentUpdate extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update agent';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token,
     *     'data' => array - Settings data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');
        $data = $this->socket->get('data');

        unset($data['agentOnBoarding']);

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            event(new Logout($agentId, $socketId, null, $msg));
            return;
        }

        $agent = Agent::with('agentOnBoarding')->find($agentId);

        if ($agent->email != $data['email']) {
            $otherAgent = Agent::whereRaw('LOWER(email) = ?', strtolower($data['email']))->first();
            if ($otherAgent) {
                event(new AgentUpdateFailedEvent($socketId, $agentId, ['email' => 'Agent with this email exists']));
                return;
            }
        }

        $agent->fill($data);
        $saved = $agent->saveOrFail();

        if ($saved) {
            if ($agent->salesForceId) {
                Intercom::createOrUpdateUser($agent);
            }
        }

        event(new AgentUpdateEvent($socketId, $agent));
    }
}
