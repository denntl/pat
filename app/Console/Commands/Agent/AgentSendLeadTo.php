<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Facades\SendGrid;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class AgentSendLeadTo.
 *
 * @example php artisan agent:send:lead a4bc19c0-477d-11e7-a364-91b5d89fc617 'mail@to.com, mail1@to.com'
 * @example php artisan agent:send:lead -S=eyJzaWQiOm51bGwsInRva2VuIjpudWxsLCJkYXRhIjpbXX0=
 *
 * @package App\Console\Commands\Agent
 */
class AgentSendLeadTo extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:send:lead {leadId?} {emails?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Send lead to email';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token,
     *     'leadId' => string - Lead uuid,
     *     'emails' => string - emails separated by coma
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');
        $leadId = $this->socket->get('leadId');
        $emails = $this->socket->get('emails');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        SendGrid::sendLeadToEmails($leadId, $agentId, $emails);

        return true;
    }
}
