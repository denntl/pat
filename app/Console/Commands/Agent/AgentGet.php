<?php

namespace App\Console\Commands\Agent;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Events\Agent\AgentGet as AgentGetEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class AgentGet.
 *
 * @example php artisan agent:get 1 -Cid -Cemail
 * @example php artisan agent:get -S=eyJhaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGx9
 *
 * @package App\Console\Commands\Agent
 */
class AgentGet extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:get {id?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get one agent';

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handleCommand()
    {
        /** @var int $id - Agent id */
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Please set up agent id');
            return;
        }

        /** @var array $columns - Agents table columns names */
        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'phone',
                'email',
                'twilioNumber',
                'sendGridEmail',
                'firstName',
                'lastName',
                'createdAt',
                'updatedAt'
            ];
        }

        $agent = Agent::all($columns)->where('id', $id)->toArray();

        $this->table($columns, $agent);
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        return event(new AgentGetEvent($agentId, $socketId));
    }
}
