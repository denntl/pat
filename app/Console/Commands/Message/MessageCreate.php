<?php

namespace App\Console\Commands\Message;

use App\Concierge;
use App\Events\Logout;
use App\Events\Message\MessageCreate as MessageCreateEvent;
use App\Facades\Message as MessageService;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class MessageCreate.
 *
 * @example php artisan message:create "bc7a9c26-9c62-11e7-8104-0800274e00a9" 1 0 +12055091929 +12053866015 "text" 2253
 * @example php artisan message:create -S=eyJzaWQiOm51bGwsInJpZCI6bnVsbCwidG9rZW4iOm51bGwsImRhdGEiOltdfQ==
 *
 * @package App\Console\Commands\Message
 */
class MessageCreate extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Message was created successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:create {leadId?} {incoming?} {isRead?} {to?} {from?} {text?} {conciergeId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Create message';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $conciergeId = $this->argument('conciergeId') ?? null;
        $leadId = $this->argument('leadId');
        $incoming = $this->argument('incoming');
        $to = $this->argument('to');
        $from = $this->argument('from');
        $text = $this->argument('text');

        if ($this->invalidArguments()) {
            $this->error('Missed one of arguments. Please check command signature');
            return;
        }

        $message = MessageService::createMessage([
            'conciergeId' => $conciergeId,
            'leadId' => $leadId,
            'incoming' => $incoming,
            'isRead' => false,
            'isAutomatic' => false,
            'to' => $to,
            'from' => $from,
            'text' => $text,
            'twilioMessageSid' => null,
            'twilioMessageStatus' => null
        ]);

        event(new MessageCreateEvent($message));

        $this->info('Created message with id: ' . $message->id);
    }

    private function invalidArguments(): bool
    {
        $leadId = $this->argument('leadId');
        $incoming = $this->argument('incoming');
        $to = $this->argument('to');
        $from = $this->argument('from');
        $text = $this->argument('text');

        return (!$leadId || !$incoming || !$to || !$from || !$text);
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'data' => array - Message data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, self::WRONG_TOKEN_MESSAGE));
        }

        $data = $this->socket->get('data');

        $message = MessageService::createMessage($data);
        if ($message) {
            $this->info(self::SUCCESS_MESSAGE);

            return event(new MessageCreateEvent($message, $socketId));
        }
    }
}
