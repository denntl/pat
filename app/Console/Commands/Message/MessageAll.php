<?php

namespace App\Console\Commands\Message;

use App\Concierge;
use App\Events\Logout;
use App\Events\Message\MessageAll as MessageAllEvent;
use App\Message;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class MessageAll.
 *
 * @example php artisan message:all 7f61c220-4f70-11e7-bba3-0db29d2c2728 0 10 -Ctext
 * @example php artisan message:all -S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsImxpZCI6bnVsbH0=
 *
 * @package App\Console\Commands\Message
 */
class MessageAll extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Messages were retrived successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:all {leadId?} {offset?} {limit?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get all messages';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $leadId = $this->argument('leadId');
        if (!$leadId) {
            $this->error('Missed lead id argument');
            return;
        }

        $offset = $this->argument('offset');
        $limit = $this->argument('limit');
        if ($offset === null || !$limit) {
            $this->error('Please set up an offset & limit parameters to fetch messages');
            return;
        }

        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'text',
                'conciergeId',
                'leadId',
                'incoming',
                'isRead',
                'createdAt',
                'updatedAt'
            ];
        }

        $leads = Message::all($columns)->where('leadId', $leadId)->toBase()->slice($offset, $limit);

        $this->table($columns, $leads->toArray());
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, self::WRONG_TOKEN_MESSAGE));
        }
        $this->info(self::SUCCESS_MESSAGE);

        return event(new MessageAllEvent($this->socket->get('lid'), $socketId));
    }
}
