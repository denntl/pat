<?php

namespace App\Console\Commands\Message;

use App\Concierge;
use App\Events\Logout;
use App\Events\Message\MessageReadAll as MessageReadAllEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class MessageReadAll.
 *
 * @example php artisan message:read:all
 * @example php artisan message:read:all -S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsImxpZCI6bnVsbH0=
 *
 * @package App\Console\Commands\Message
 */
class MessageReadAll extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Messages were retrived successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:read:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Read all messages from lead';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        // TODO: Feature requested
        $this->info('Not implemented');
    }

    /**
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, self::WRONG_TOKEN_MESSAGE));
        }
        $this->info(self::SUCCESS_MESSAGE);

        return event(new MessageReadAllEvent($this->socket->get('lid'), $socketId));
    }
}
