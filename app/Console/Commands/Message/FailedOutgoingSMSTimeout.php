<?php

namespace App\Console\Commands\Message;

use App\Message;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Events\Message\MessageUpdateStatus;
use Illuminate\Database\Eloquent\Collection;

class FailedOutgoingSMSTimeout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:timeout:failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change outgoing sms status after some time to failed.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $minutesBeforeTimeout = 2;
        $messages = Message::where('incoming', 0)
            ->whereNull('twilioMessageStatus')
            ->where('createdAt', '<', Carbon::now()->subMinutes($minutesBeforeTimeout))->get();

        $count = $messages->count();
        if ($messages->count()) {
            $this->info('Total messages to update :'.$count);
            $this->changeMessageStatusToFailed($messages);
            $this->info('Finished');
        }
    }


    /**
     * Change message status
     *
     * @param Collection $messages
     */
    private function changeMessageStatusToFailed(Collection $messages)
    {
        foreach ($messages as $message) {
            $message->twilioMessageStatus = 'failed';
            if ($message->save()) {
                $status = $message->twilioMessageStatus;
                $messageId = $message->id;
                $leadId = $message->leadId;
                $twilioMessageSid = $message->twilioMessageSid;
                event(new MessageUpdateStatus($leadId, $twilioMessageSid, $status, $messageId));
            }
        }
    }
}
