<?php

namespace App\Console\Commands\Message;

use App\Concierge;
use App\Events\Logout;
use App\Events\Message\MessageUpdateStatus as MessageUpdateStatusEvent;
use App\Facades\Message;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class MessageUpdateStatus.
 *
 * @example php artisan message:status:update 7f61c220-4f70-11e7-bba3-0db29d2c2728 received
 * @example php artisan message:status:update -S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsImxpZCI6bnVsbH0=
 *
 * @package App\Console\Commands\Message
 */
class MessageUpdateStatus extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Message status was updated successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:status:update {twilioSid?} {status?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update message status';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $twilioSid = $this->argument('twilioSid');
        $status = $this->argument('status');
        if (!$twilioSid || !$status) {
            $this->error('Missed arguments. Please check command signature.');
            return;
        }

        $updated = Message::updateTwilioStatus($twilioSid, $status);
        if (!$updated) {
            $this->error('Failed to update message status');
            return;
        }

        $this->info(sprintf('Message with Twilio id: %s was changed to %s', $twilioSid, $status));
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'twilioMessageSid' => UUID - Message id
     *     'status' => string - Status
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, self::WRONG_TOKEN_MESSAGE));
        }
        $this->info(self::SUCCESS_MESSAGE);

        return event(new MessageUpdateStatusEvent(
            $this->socket->get('lid'),
            $this->socket->get('twilioMessageSid'),
            $this->socket->get('status')
        ));
    }
}
