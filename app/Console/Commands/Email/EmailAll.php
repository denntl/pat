<?php

namespace App\Console\Commands\Email;

use App\Concierge;
use App\Email;
use App\Events\Logout;
use App\Events\Email\EmailAll as EmailAllEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

/**
 * Class EmailAll.
 *
 * @example php artisan email:all 7ed5f740-4f70-11e7-9049-7904a864a737 0 10
 * @example php artisan email:all -S=eyJzaWQiOm51bGwsInJpZCI6bnVsbCwibGlkIjpudWxsLCJ0b2tlbiI6bnVsbCwiZGF0YSI6W119
 *
 * @package App\Console\Commands\Email
 */
class EmailAll extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:all {leadId?} {offset?} {limit?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get all lead emails';

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Emails were retrived successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $leadId = $this->argument('leadId');
        if (!$leadId) {
            $this->error('Missed lead id argument');
            return;
        }

        $offset = $this->argument('offset');
        $limit = $this->argument('limit');
        if ($offset === null || !$limit) {
            $this->error('Please set up an offset & limit parameters to fetch emails');
            return;
        }

        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'to',
                'from',
                'subject',
                'text',
                'createdAt',
                'updatedAt'
            ];
        }

        /** @var Collection $emails */
        $emails = Email::all($columns)->where('leadId', $leadId)->toBase();

        $this->table($columns, $emails->slice($offset, $limit));
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, 'Wrong token'));
        }
        $this->info(self::SUCCESS_MESSAGE);

        return event(new EmailAllEvent($this->socket->get('lid'), $socketId));
    }
}
