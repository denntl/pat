<?php

namespace App\Console\Commands\Email;

use App\Email;
use Illuminate\Console\Command;

class UpdateEmailsStatusesForDemoAndDev extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:update:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update statuses for demo and dev emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (app()->environment(['dev', 'demo'])) {
            $emails = Email::where('incoming', 0)
                ->where('statusId', 1)->update(['statusId' => 3]);
            $this->info('Inbound emails updated: ' . $emails);
        } else {
            $this->info('This command only for dev and demo');
        }
    }
}
