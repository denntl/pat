<?php

namespace App\Console\Commands\Email;

use App\Concierge;
use App\Email;
use App\Events\Logout;
use App\Events\Email\EmailReadAll as EmailReadAllEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class EmailReadAll.
 * @package App\Console\Commands\Email
 */
class EmailReadAll extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:read:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Read all email from lead';

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Emails were read successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token,
     *     'data' => array - Data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $leadId = $this->socket->get('lid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, 'Wrong token'));
        }

        $readEmailsIds = [];

        $emails = Email::where('leadId', $leadId)->where('isRead', false)->get();

        foreach ($emails as $email) {
            $email->isRead = true;
            $email->saveOrFail();
            $readEmailsIds[] = $email->id;
        }
        $this->info(self::SUCCESS_MESSAGE);

        return event(new EmailReadAllEvent($leadId, $readEmailsIds, $socketId));
    }
}
