<?php

namespace App\Console\Commands\Email;

use App\Concierge;
use App\Email;
use App\Events\Logout;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class EmailCreate.
 *
 * @example php artisan email:create
 * @example php artisan email:create -S=eyJzaWQiOm51bGwsInJpZCI6bnVsbCwidG9rZW4iOm51bGwsImRhdGEiOltdfQ==
 *
 * @package App\Console\Commands\Email
 */
class EmailCreate extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Create an email for lead';

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Message was created successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token,
     *     'data' => array - Email data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, 'Wrong token'));
        }

        $this->info(self::SUCCESS_MESSAGE);

        return Email::createEmail($this->socket->get('data'), $socketId);
    }
}
