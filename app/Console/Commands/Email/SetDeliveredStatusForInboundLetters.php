<?php

namespace App\Console\Commands\Email;

use App\Email;
use Illuminate\Console\Command;

class SetDeliveredStatusForInboundLetters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:update:inbound';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all inbound letters statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = Email::where('incoming', 1)
            ->where('statusId', 1)->update(['statusId' => 3]);
        $this->info('Inbound emails updated: ' . $emails);
    }
}
