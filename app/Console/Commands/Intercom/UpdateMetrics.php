<?php

namespace App\Console\Commands\Intercom;

use App\Agent;
use App\Facades\Intercom;
use Illuminate\Console\Command;

/**
 * Class UpdateMetrics.
 * @package App\Console\Commands\Intercom
 */
class UpdateMetrics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'intercom:update:metrics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Intercom :: Update metrics';

    /**
     * Create a new command instance.
     *
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handle()
    {
        $agents = Agent::all();

        foreach ($agents as $agent) {
            if (Intercom::updateLeadsMetrics($agent->id)) {
                $this->info('Leads metrics for agentId = ' . $agent->id . ' successfully updated');
            } else {
                $this->error('Leads metrics for agentId = ' . $agent->id . ' not updated');
            }

            if (Intercom::updateCallsMetrics($agent->id)) {
                $this->info('Calls metrics for agentId = ' . $agent->id . ' successfully updated');
            } else {
                $this->error('Calls metrics for agentId = ' . $agent->id . ' not updated');
            }
        }
    }
}
