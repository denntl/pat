<?php

namespace App\Console\Commands\Note;

use App\Concierge;
use App\Events\Logout;
use App\Facades\Lead as LeadService;
use App\Facades\SendGrid;
use App\Note;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class NoteCreate.
 *
 * @example php artisan note:create
 * @example php artisan note:create -S=eyJyaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbH0=
 *
 * @package App\Console\Commands\Note
 */
class NoteCreate extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'note:create {conciergeId?} {leadId?} {type?} {text?} {sendToAgent?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Create note';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $conciergeId = $this->argument('conciergeId');
        $leadId = $this->argument('leadId');
        $type = $this->argument('type');
        $note = $this->argument('text');
        if (!$conciergeId || !$leadId || !$type || !$note) {
            $this->error('Missed argument. Please check command signature');
            return;
        }

        $note = Note::create(compact('conciergeId', 'leadId', 'type', 'note'));

        $sendToAgent = (bool)$this->argument('sendToAgent');
        if ($sendToAgent) {
            SendGrid::sendNoteForAgent($leadId, $note);
        }

        $this->info('Note created successful');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $sendToAgent = $this->socket->get('sendToAgent');
        $data = $this->socket->get('data');
        $token = $this->socket->get('token');
        $leadId = $data['leadId'] ?? null;

        $tokenValid = Concierge::checkStaticToken($conciergeId, $token);

        if (!$tokenValid) {
            return event(new Logout($conciergeId, $socketId, 'Wrong token'));
        }

        $note = LeadService::createLeadNote($data, $socketId);

        if ($sendToAgent && $note) {
            SendGrid::sendNoteForAgent($leadId, $note);
        }
    }
}
