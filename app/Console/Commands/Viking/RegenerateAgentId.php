<?php

namespace App\Console\Commands\Viking;

use App\Agent;
use App\Services\Api\Viking\Facades\Viking;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RegenerateAgentId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viking:regenerate:agent_id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate pat_id for agent from Viking';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $allLeadsPatIds = DB::table('leads')
            ->select('vikingPatId')
            ->whereNotNull('vikingPatId')
            ->groupBy('vikingPatId')
            ->get();

        foreach ($allLeadsPatIds as $item) {
            $vikingPatId = $item->vikingPatId;
            $agentData = Viking::singleAgentData($vikingPatId);
            $agentEmail = $agentData->data->email ?? null;

            $agent = Agent::where('email', "ILIKE", $agentEmail)
                ->whereNull('vikingPatId')->first();

            if ($agent) {
                $agent->vikingPatId = $vikingPatId;

                if ($agent->save()) {
                    $this->info('Agent with email '. $agentEmail .' updated.');
                    continue;
                }
            }

            $this->info('Fail to update agent with email '. $agentEmail .'.');
        }

        $this->info('Agent sync finished');
    }
}
