<?php

namespace App\Console\Commands\Viking;

use App\Agent;
use App\AgentOnBoarding;
use App\Events\Agent\AgentUpdate as AgentUpdateEvent;
use App\Events\LogoutAgent as Logout;
use App\Services\Api\Viking\Facades\Viking;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

class DisconnectAgentGoogle extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viking:disconnect:google {agentId?} {sendEvent?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Disconnect google from viking';

    /**
     * @inheritdoc
     */
    public function handleCommand()
    {
        $agentId = $this->argument('agentId');
        $sendEvent = $this->argument('sendEvent', false);
        $agent = Agent::find($agentId);

        if ($agent) {
            $isGoogleDisconnected = Viking::disconnectGmailFromAgent($agent);
            if (!$isGoogleDisconnected) {
                $this->info('Viking response not success');
            }
            /** @var $agentOnBoarding AgentOnBoarding */
            $agentOnBoarding = AgentOnBoarding::where('agentId', $agentId)->first();
            $agentOnBoarding->tokenReceived = false;
            $agentOnBoarding->save();
            $this->info('Agent on boarding updated');

            if ($sendEvent) {
                $agent = Agent::with('agentOnBoarding')->find($agentId);
                return event(new AgentUpdateEvent(null, $agent));
            }
        } else {
            $this->info('Agent not found');
        }
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token,
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $socketId = $this->socket->get('sid');
        $agentId = $this->socket->get('aid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            return event(new Logout($agentId, $socketId, null, 'Wrong token'));
        }

        $agent = Agent::find($agentId);

        if ($agent && Viking::disconnectGmailFromAgent($agent)) {
            /** @var $agentOnBoarding AgentOnBoarding */
            $agentOnBoarding = AgentOnBoarding::where('agentId', $agentId)->first();
            $agentOnBoarding->tokenReceived = false;
            $agentOnBoarding->save();
        }

        $agent = Agent::with('agentOnBoarding')->find($agentId);

        return event(new AgentUpdateEvent($socketId, $agent));
    }
}
