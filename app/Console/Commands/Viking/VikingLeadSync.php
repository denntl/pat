<?php

namespace App\Console\Commands\Viking;

use App\Facades\LogDb;
use App\Jobs\SyncLeads;
use App\LogsLeadsVikingSync;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

/**
 * Class VikingLeadSync.
 * @package App\Console\Commands\Viking
 */
class VikingLeadSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viking:lead_sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Sync leads from Viking API';

    private $logDataForWorkers = [];

    /**
     * Create a new command instance.
     *
     * @inheritDoc
     */
    public function __construct()
    {
        $this->logDataForWorkers['instance'] = LogsLeadsVikingSync::class;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handle()
    {
        // Check if there something in sync queues
        $redis = Redis::keys('queues:sync*');
        if (empty($redis)) {
            $page = 1;
            // Value = 2 for prevent canceling task if we fail at first request.
            $itemsTotal = 2;
            $itemsCurrent = 0;

            do {
                $body = $this->loadData($page);

                if ($body === null) {
                    $this->info('Sleep 5 seconds');

                    // TODO: Not best solution maybe need to be updated.
                    sleep(5);
                } else {
                    if ($page == 1) {
                        $this->output->progressStart($body['total']);
                    }
                    $itemsCurrent = $body['per_page'] * $body['current_page'];
                    $this->output->progressAdvance($body['per_page']);
                    $itemsTotal = $body['total'];

                    $this->syncData($body['data']);

                    $page++;
                }
            } while ($itemsTotal > $itemsCurrent);

            $this->output->progressFinish();

            Log::info('Sync finished');

            $this->info('Sync finished');
        } else {
            $this->info('Previous command\'s jobs are still running');
        }
    }

    /**
     * Load data.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return null|array
     */
    private function loadData(int $id = 1)
    {
        $client = new Client();

        try {
            $url = 'https://' . config('services.viking.domain') . '/api/leads?page=' . $id;
            $this->logDataForWorkers['params']['requestUrl'] = $url;
            $res = $client->request('GET', $url, [
                'headers' => [
                    'authorization' => 'Bearer ' . config('services.viking.auth_bearer'),
                    'postman-token' => config('services.viking.token')
                ],
                [
                    'connect_timeout' => 5
                ]
            ]);
            $statusCode = $res->getStatusCode();
            if ($statusCode !== 200) {
                $this->logDataForWorkers['params']['responseStatus'] = $statusCode;
                Log::info('Invalid response code');

                $this->info('Invalid response code');

                return null;
            }

            return \GuzzleHttp\json_decode($res->getBody(), true);
        } catch (ClientException $exception) {
            if (isset($statusCode)) {
                $this->logDataForWorkers['params']['commandStatus'] = $statusCode;
            }
            LogDb::setInstance($this->logDataForWorkers['instance']);
            LogDb::setParams($this->logDataForWorkers['params']);
            LogDb::save();

            return null;
        }
    }

    /**
     * Sync data.
     *
     * @param array $data
     */
    private function syncData(array $data)
    {
        dispatch((new SyncLeads($data, $this->logDataForWorkers))->onQueue('sync-leads'));
    }
}
