<?php

namespace App\Console\Commands\Viking;

use App\Agent;

use App\Services\Api\Viking\Facades\Viking;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AttachExistingAgents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viking:attach_existing_agents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attaching existing agents from pat to lead from viking';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command
     */
    public function handle()
    {
        $allLeadsPatIds = DB::table('leads')
            ->select('vikingPatId')
            ->whereNotNull('vikingPatId')
            ->whereNull('agentId')
            ->groupBy('vikingPatId')
            ->get();

        foreach ($allLeadsPatIds as $item) {
            $vikingPatId = $item->vikingPatId;

            $agent = $this->getVikingAgent($vikingPatId);

            if ($agent) {
                DB::table('leads')->where('vikingPatId', $vikingPatId)->update(['agentId' => $agent->id]);
            } else {
                $this->info('Lead with pat_id not exists : '. $vikingPatId);
            }
        }

        $this->info('Lead sync finished');
    }

    /**
     * Get agent for attaching to lead.
     *
     * @param string $vikingPatId
     *
     * @return Agent|null
     */
    private function getVikingAgent(string $vikingPatId)
    {
        /** @var Agent $agent */
        $agent = Agent::where('vikingPatId', $vikingPatId)->first();

        if (!$agent) {
            $agentData = Viking::getSingleAgentData($vikingPatId);
            $agentEmail = $agentData->data->email ?? null;
            $agent = Agent::where('email', "ILIKE", $agentEmail)->first();
            if ($agent) {
                $agent->vikingPatId = $vikingPatId;
                $agent->save();
            }
        }

        return $agent;
    }
}
