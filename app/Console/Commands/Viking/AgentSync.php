<?php

namespace App\Console\Commands\Viking;

use App\Agent;
use App\Services\Api\Viking\Facades\Viking;
use Illuminate\Console\Command;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use stdClass;

/**
 * Class AgentSync
 * @package App\Console\Commands\Viking
 */
class AgentSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viking:agent:sync {--local}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronise Pat\'s agents with Viking\'s agents';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $environment = App::environment();

        if (starts_with($environment, 'local')) {
            $this->info('Please don\'t run this locally');
            return;
        }

        $local = $this->option('local');

        if ($local) {
            $this->sendUnusedLocalAgents();
        } else {
            $this->syncRemoteAgents();
        }
    }

    /**
     * Getting data from Viking to check which agent not exists at Pat
     */
    public function syncRemoteAgents()
    {
        $page = 1;

        $result = [
            "updated" => 0,
            "removed" => 0,
            "errors" => 0
        ];

        $this->info('Started');

        do {
            $data = Viking::getMultipleAgentsData($page);

            if (!$data) {
                return;
            }

            $counter = $this->updateRemoteAgents($data);
            $this->info('Updated Page ' . $page);

            if ($counter['removed'] == 0) {
                $page++;
            }

            $this->info('Next page ' . $page);

            $result['removed'] += $counter['removed'];
            $result['updated'] += $counter['updated'];
            $result['errors'] += $counter['errors'];
        } while (isset($data->last_page) && $page <= $data->last_page);

        $this->info('Ended');
        $this->info('Total updated: ' . $result['updated'] . '/'
            . 'Total removed: ' . $result['removed'] . '/'
            . 'Total errors: ' . $result['errors']);
    }

    /**
     * Remove agents from pat if they not exists locally
     *
     * @param stdClass $data
     *
     * @return  array
     */
    private function updateRemoteAgents($data)
    {
        $removed = 0;
        $updated = 0;
        $errors = 0;
        $result = [];

        foreach ($data->data as $remoteAgent) {
            /** @var Agent $agent */
            $agent = Agent::where('vikingPatId', '=', $remoteAgent->pat_id)->first();

            if (!$agent) {
                // Remove if there is no such agent at PAT
                if (Viking::removeAgent($remoteAgent->pat_id)) {
                    $removed++;
                } else {
                    $errors++;
                }
            } else {
                // Update if such agent exists
                $res = Viking::updateExistingAgent($agent);

                if (isset($res->statusCode) && $res->statusCode == Response::HTTP_OK) {
                    $updated++;
                } else {
                    $errors++;
                }
            }
        }

        $result['updated'] = $updated;
        $result['removed'] = $removed;
        $result['errors'] = $errors;

        return $result;
    }

    /**
     * Get all agents and try to send it to viking (will fire error if agent exists)
     */
    private function sendUnusedLocalAgents()
    {
        $localAgents = Agent::all();

        foreach ($localAgents as $localAgent) {
            // If user already exists we will get 400 status from Viking (we have now method to check if user exists)
            Viking::sendNewAgent($localAgent);
        }
    }
}
