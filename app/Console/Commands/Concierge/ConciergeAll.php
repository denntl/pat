<?php

namespace App\Console\Commands\Concierge;

use App\Concierge;
use App\Events\Concierge\ConciergeAll as ConciergeAllEvent;
use App\Events\Logout;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class ConciergeAll.
 *
 * @example php artisan concierge:all
 * @example php artisan concierge:all -S=
 *
 * @package App\Console\Commands\Concierge
 */
class ConciergeAll extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Concierges were retrived successfully';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * Invalid cid error message
     *
     * @var string
     */
    const INVALID_CID_MESSAGE = 'Invalid cid';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'concierge:all {offset?} {limit?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get all concierges';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $offset = $this->argument('offset');
        $limit = $this->argument('limit');
        if ($offset === null || !$limit) {
            $this->error('Please set up an offset & limit parameters to fetch concierges');
            return;
        }

        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'name',
                'email',
                'role',
                'createdAt',
                'updatedAt'
            ];
        }

        $concierges = Concierge::all($columns)->toBase()->slice($offset, $limit);

        $this->table($columns, $concierges->toArray());
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        if ($conciergeId) {
            $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
            if (!$tokenValid) {
                $this->info(self::WRONG_TOKEN_MESSAGE);

                return event(new Logout($conciergeId, $socketId, self::WRONG_TOKEN_MESSAGE));
            }
        }

        $this->info(self::SUCCESS_MESSAGE);

        return event(new ConciergeAllEvent($socketId));
    }
}
