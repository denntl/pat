<?php

namespace App\Console\Commands\Concierge;

use App\Concierge;
use App\Events\Concierge\ConciergeDelete as ConciergeDeleteEvent;
use App\Events\Logout;
use App\Jobs\Lead\AutoReassign;
use App\Lead;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class ConciergeDelete.
 *
 * @example php artisan concierge:delete 1
 * @example php artisan concierge:delete --S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsImlkIjpudWxsfQ==
 *
 * @package App\Console\Commands\Concierge
 */
class ConciergeDelete extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Concierge was successfully deleted';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * Wrong role error message
     *
     * @var string
     */
    const WRONG_ROLE_MESSAGE = 'Wrong role';

    /**
     * Invalid id error message
     *
     * @var string
     */
    const INVALID_ID_ERROR_MESSAGE = 'Invalid id';

    /**
     * Concierge was removed error message
     *
     * @var string
     */
    const CONCIERGE_REMOVED_MESSAGE = 'Concierge was removed';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'concierge:delete {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Delete concierge';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Missed concierge id argument');
            return;
        }

        Concierge::destroy($id);

        $this->info('Concierge with id: ' . $id . ' was deleted');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'id' => int - Id of concierge that need to be deleted
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $id = (int)$this->socket->get('id');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        $adminRole = $tokenValid ? Concierge::checkAdminRole($conciergeId) : $tokenValid;
        if (!$tokenValid || !$adminRole) {
            $errorMessage = !$tokenValid ? self::WRONG_TOKEN_MESSAGE : self::WRONG_ROLE_MESSAGE;
            $this->info($errorMessage);

            return event(new Logout($conciergeId, $socketId, $errorMessage));
        }

        $removeStatus = Concierge::where('id', $id)
            ->update([
                'isDeleted'=> true,
                'role' => 0
            ]);
        event(new Logout($id, null, self::CONCIERGE_REMOVED_MESSAGE));

        /** @var array $concierge */
        $leads = Lead::byConcierge($id)->get();

        /** @var Lead $lead */
        foreach ($leads as $lead) {
            $job = (new AutoReassign($lead));
            dispatch($job);
        }

        if ($removeStatus) {
            $this->info(self::SUCCESS_MESSAGE);

            return event(new ConciergeDeleteEvent($id, $socketId));
        }
        $this->info(self::INVALID_ID_ERROR_MESSAGE);

        return null;
    }
}
