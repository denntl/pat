<?php

namespace App\Console\Commands\Concierge;

use App\Concierge;
use App\LogsConciergesPauseChanges;
use App\Events\Logout;
use App\Events\Concierge\ConciergeSwitchDistribution as ConciergeSwitchDistributionEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\Log;

/**
 * Class ConciergeSwitchDistribution.
 *
 * @example php artisan concierge:switchDistribution 1 true
 * @example php artisan concierge:switchDistribution --S=
 *
 * @package App\Console\Commands\Concierge
 */
class ConciergeSwitchDistribution extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'concierge:switchDistribution {id?} {isPaused?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Concierge switch distribution';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Missed concierge id argument');
            return;
        }

        $concierge = Concierge::find($id);
        if ($concierge) {
            $isPaused = $this->argument('isPaused');
            $concierge->isPaused = $isPaused;
            $concierge->saveOrFail();

            LogsConciergesPauseChanges::create([
                'conciergeId' => $id,
                'isPaused' => $isPaused
            ]);

            $action = $isPaused ? ' was paused' : ' was activated';

            $this->info('Concierge with id: ' . $id . $action);
        }
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'id' => string - New concierge name
     *     'isPaused' => boolean - Is paused lead distribution
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);

            return event(new Logout($conciergeId, $socketId, $msg));
        }

        $id = (int)$this->socket->get('id');
        $isPaused = $this->socket->get('isPaused');

        /** @var Concierge $concierge */
        $concierge = Concierge::find($id);
        if ($concierge) {
            $concierge->isPaused = $isPaused;
            $concierge->saveOrFail();

            LogsConciergesPauseChanges::create([
                'conciergeId' => $id,
                'isPaused' => $isPaused
            ]);

            return event(new ConciergeSwitchDistributionEvent(
                $id,
                $isPaused,
                $socketId
            ));
        }

        $this->line('Wrong concierge id');
    }
}
