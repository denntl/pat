<?php

namespace App\Console\Commands\Concierge;

use App\Concierge;
use App\Events\Concierge\ConciergeCreate as ConciergeCreateEvent;
use App\Events\Logout;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class ConciergeCreate.
 *
 * @example php artisan concierge:create "John Doe" "john.doe@email.com" 0
 * @example php artisan concierge:create --S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsIm5hbWUi
 *                                           OiJKb2huIERvZSIsImVtYWlsIjoiam9obi5kb2VAZW1haWwuY29tIiwicm9sZSI6MH0=
 *
 * @package App\Console\Commands\Concierge
 */
class ConciergeCreate extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Concierge was successfully created';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_PARAMS_MESSAGE = 'Please check socket params';

    /**
     * Wrong role error message
     *
     * @var string
     */
    const WRONG_ROLE_MESSAGE = 'Wrong role';

    /**
     * Saving error message
     *
     * @var string
     */
    const SAVING_ERROR_MESSAGE = 'Saving error';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'concierge:create {name?} {email?} {role?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Create concierge';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $name = $this->argument('name');
        $email = $this->argument('email');
        $role = $this->argument('role');

        if (!$name || !$email || !$role) {
            $this->error('Missed one of arguments. Please check command signature');
            return;
        }

        $concierge = new Concierge(compact('name', 'email', 'role'));
        $concierge->saveOrFail();

        $this->line('Created concierge with id: ' . $concierge->id);
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'name' => string - New concierge name
     *     'email' => string - New concierge email
     *     'role' => int - New concierge role (0-3)
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $name = $this->socket->get('name');
        $email = $this->socket->get('email');
        $permission = (int)$this->socket->get('role');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        $adminRole = $tokenValid ? Concierge::checkAdminRole($conciergeId) : $tokenValid;
        if (!$tokenValid || !$adminRole) {
            $errorMessage = !$tokenValid ? self::WRONG_TOKEN_MESSAGE : self::WRONG_ROLE_MESSAGE;
            $this->line($errorMessage);

            return event(new Logout($conciergeId, $socketId, $errorMessage));
        }
        if (!$email || !$name) {
            $this->line(self::WRONG_PARAMS_MESSAGE);
            return;
        }

        $concierge = Concierge::where('email', $email)->first();

        if (! $concierge) {
            $concierge = new Concierge();
            $concierge->name = $name;
            $concierge->email = $email;
            $concierge->role = $permission;

            if ($concierge->saveOrFail()) {
                $this->line(self::SUCCESS_MESSAGE);

                return event(new ConciergeCreateEvent(
                    $name,
                    $email,
                    $permission,
                    $concierge->id,
                    $socketId
                ));
            }
        }

        $this->line(self::SAVING_ERROR_MESSAGE);

        return event(new ConciergeCreateEvent(
            $name,
            $email,
            $permission,
            -1, // Email already used
            $socketId
        ));
    }
}
