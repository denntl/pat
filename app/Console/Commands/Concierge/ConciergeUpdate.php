<?php

namespace App\Console\Commands\Concierge;

use App\Concierge;
use App\Events\Logout;
use App\Events\Concierge\ConciergeUpdate as ConciergeUpdateEvent;
use App\Jobs\Lead\AutoReassign;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class ConciergeUpdate.
 *
 * @example php artisan concierge:update 1 "John Doe" "john.doe@email.com" 0
 * @example php artisan concierge:update --S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsIm5hbWUiOiJKb2huIERvZSIsIm
 *                                           VtYWlsIjoiam9obi5kb2VAZW1haWwuY29tIiwicm9sZSI6MH0=
 *
 * @package App\Console\Commands\Concierge
 */
class ConciergeUpdate extends Command
{
    use WebSocket;

    /**
     * Success message
     *
     * @var string
     */
    const SUCCESS_MESSAGE = 'Concierge was successfully updated';

    /**
     * Wrong token error message
     *
     * @var string
     */
    const WRONG_TOKEN_MESSAGE = 'Bad token';

    /**
     * Wrong role error message
     *
     * @var string
     */
    const WRONG_ROLE_MESSAGE = 'Wrong role';

    /**
     * Invalid id error message
     *
     * @var string
     */
    const INVALID_ID_ERROR_MESSAGE = 'Invalid id';

    /**
     * Your account is suspended
     *
     * @var string
     */
    const SUSPENDED_ACCOUNT = 'Your account is suspended';

    /**
     * Your permissions have been changed
     *
     * @var string
     */
    const PERMISSIONS_CHANGED = 'Your permissions have been changed';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'concierge:update {id?} {name?} {email?} {role?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update concierge';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Missed concierge id argument');
            return;
        }

        $concierge = Concierge::find($id);

        $name = $this->argument('name');
        $email = $this->argument('email');
        $role = $this->argument('role');

        $concierge->fill(compact(
            $name === null ? '' : 'name',
            $email === null ? '' : 'email',
            $role === null ? '' : 'role'
        ));

        $concierge->saveOrFail();

        $this->info('Concierge with id: ' . $id . ' was updated');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'name' => string - New concierge name
     *     'email' => string - New concierge email
     *     'role' => int - New concierge role (0-3)
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $this->info(self::WRONG_TOKEN_MESSAGE);

            return event(new Logout($conciergeId, $socketId, self::WRONG_TOKEN_MESSAGE));
        }

        $roleAdmin = Concierge::checkAdminRole($conciergeId);
        if (!$roleAdmin) {
            return event(new Logout($conciergeId, $socketId, self::WRONG_ROLE_MESSAGE));
        }

        $id = (int)$this->socket->get('id');
        $newRole = (int)$this->socket->get('role');
        $name = $this->socket->get('name');
        $email = $this->socket->get('email');

        /** @var Concierge $concierge */
        $concierge = Concierge::with('leads')->find($id);
        if ($concierge) {
            if ($concierge->role != $newRole) {
                switch ($newRole) {
                    case Concierge::ROLE_ADMIN:
                    case Concierge::ROLE_TIER_I:
                    case Concierge::ROLE_TIER_II:
                        event(new Logout($concierge->id, $socketId, self::PERMISSIONS_CHANGED));
                        break;
                    case Concierge::ROLE_SUSPENDED:
                        event(new Logout($concierge->id, $socketId, self::SUSPENDED_ACCOUNT));
                        break;
                    default:
                        break;
                }

                $this->reassignLeads($concierge);
            }

            $concierge->name = $name;
            $concierge->email = $email;
            $concierge->role = $newRole;
            $concierge->save();

            $this->info(self::SUCCESS_MESSAGE);

            return event(new ConciergeUpdateEvent(
                $id,
                $name,
                $email,
                $newRole,
                $socketId
            ));
        }

        $this->info(self::INVALID_ID_ERROR_MESSAGE);

        return null;
    }

    /**
     * Reassing concierge`s leads.
     *
     * @param Concierge $concierge
     *
     * @return void
     */
    private function reassignLeads(Concierge $concierge)
    {
        foreach ($concierge->leads as $lead) {
            dispatch(new AutoReassign($lead));
        }
    }
}
