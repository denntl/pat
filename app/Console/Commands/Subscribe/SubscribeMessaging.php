<?php

namespace App\Console\Commands\Subscribe;

use App\Concierge;
use App\Events\Logout;
use App\Events\Subscribe\SubscribeMessaging as SubscribeMessagingEvent;
use App\Facades\Lead;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadGetConsole.
 *
 * @example php artisan lead:get:console -S=eyJyaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbH0=
 *
 * @package App\Console\Commands\Subscribe
 */
class SubscribeMessaging extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscribe:messaging';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Subscribe request processing for console messaging page';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('You should not run this command via command line');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $leadId = $this->socket->get('lid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            return event(new Logout($conciergeId, $socketId, 'Wrong token'));
        }

        Lead::assignActiveLeadsToRep($conciergeId);

        return event(new SubscribeMessagingEvent($socketId, $conciergeId, $leadId));
    }
}
