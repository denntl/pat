<?php

namespace App\Console\Commands\Twilio;

use App\Facades\Twilio;
use App\Services\TwilioService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Twilio\Rest\Api\V2010\Account\MessageInstance;

/**
 * Class Sms.
 * @package App\Console\Commands\Twilio
 */
class Sms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twilio:sms ' .
        '{to : Number in format +XXXXXXXXXXX} ' .
        '{from : Number in format +XXXXXXXXXX} ' .
        '{body : Text which you want to send via SMS}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Twilio :: Send sms';

    /**
     * Create a new command instance.
     *
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handle()
    {
        $to = $this->argument('to');
        $from = $this->argument('from');

        if (!App::environment('production') || !App::environment('demo')) {
            $to = TwilioService::TEST_TO_PHONE;
            $from = TwilioService::TEST_FROM_PHONE;
        }

        $resp = Twilio::sendSms($to, $from, $this->argument('body'));

        $this->handleResponse($resp);
    }

    /**
     * Handle Twilio response.
     *
     * @param MessageInstance $resp
     * @return void
     */
    private function handleResponse(MessageInstance $resp)
    {
        if ($resp->errorCode !== null) {
            $this->error($resp->errorMessage);
            return;
        }

        $this->info("Message added to Twilio queue!");
        return;
    }
}
