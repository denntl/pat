<?php

namespace App\Console\Commands\Twilio;

use App\Agent;
use App\Events\Agent\AgentGeneratePhone as AgentGeneratePhoneEvent;
use App\Events\LogoutAgent as Logout;
use App\Facades\Twilio;
use App\Services\TwilioService;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

/**
 * Class GeneratePhoneByArea.
 * @package App\Console\Commands\Twilio
 */
class GeneratePhoneByArea extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twilio:generate:phone {areaCode?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'TWILIO :: Generate phone number by area';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $areaCode = $this->argument('areaCode');

        if (App::environment('production') || App::environment('demo')) {
            $this->info(Twilio::generateNumberByArea($areaCode));
        } else {
            $this->info(TwilioService::TEST_FROM_PHONE);
        }
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'areaCode' => string - area code to generate phone number
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $areaCode = $this->socket->get('areaCode');

        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));

        if (!$tokenValid) {
            return event(new Logout($agentId, $socketId, null, 'Wrong token'));
        }

        return event(new AgentGeneratePhoneEvent($areaCode, $this->socket->get('aid'), $socketId));
    }
}
