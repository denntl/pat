<?php

namespace App\Console\Commands\Twilio;

use App\Facades\Twilio;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use App\Concierge;
use App\Events\Lead\LeadPhoneIsValid;
use App\Events\Lead\LeadPhoneIsInvalid;

/**
 * Class ValidatePhoneNumber.
 * @package App\Console\Commands\Twilio
 */
class ValidatePhoneNumber extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twilio:validate:phone {phone?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'TWILIO :: check if phone number exists';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $phone = $this->argument('phone');
        $isValid = Twilio::validatePhoneNumber($phone);

        if ($isValid) {
            $this->info('number is valid');
        } else {
            $this->info('number is not valid');
        }
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'lid' => string - Lead id
     *     'token' => string - Auth token
     *     'phone' => string - phone number for checking
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId= $this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $leadId = $this->socket->get('lid');
        $phone = $this->socket->get('phone');

        if (!$conciergeId) {
            return;
        }

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            return event(new Logout($conciergeId, $socketId, 'Wrong token'));
        }
        $isValid = Twilio::validatePhoneNumber($phone);

        if ($isValid) {
            return event(new LeadPhoneIsValid($leadId, $socketId));
        } else {
            return event(new LeadPhoneIsInvalid($leadId, $socketId));
        }
    }
}
