<?php

namespace App\Console\Commands\Agentology\Lead;

use App\Jobs\Lead\Agentology\ReferLead;
use App\Lead;
use Illuminate\Console\Command;

class RegenerateOpportunity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agentology:opportunity:regenerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get opportunity id for referred lead';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $leads = Lead::where('status', Lead::STATUS_REFERRED)
            ->whereNotNull('agentId')
            ->whereNull('opportunityId')->get();
        $this->info('started');
        $this->info('Total: ' . $leads->count());

        foreach ($leads as $lead) {
            $job = new ReferLead($lead);
            dispatch($job);
        }

        $this->info('done');
    }
}
