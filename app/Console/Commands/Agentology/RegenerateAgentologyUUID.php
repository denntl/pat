<?php

namespace App\Console\Commands\Agentology;

use App\Agent;
use App\Services\Api\Agentology\Facades\Agentology;
use Illuminate\Console\Command;

class RegenerateAgentologyUUID extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agentology:agent:regenerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerates uuid of agent at Agentology if possible';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $agents = Agent::whereNull('agentologyUUID')->get();

        $this->info('Total agents with empty agentologyUUID: ' . $agents->count());

        foreach ($agents as $agent) {
            $existsResponse = Agentology::checkIfAgentExists($agent->email);

            if ($existsResponse->response->success) {
                $agent->agentologyUUID = $existsResponse->response->user->uuid;
                $agent->save();
            }

            $upsertResponse = Agentology::upsertAgent($agent);

            $agent->agentologyUUID = $upsertResponse->response->user->uuid;
            $agent->save();
        }

        $this->info('done');
    }
}
