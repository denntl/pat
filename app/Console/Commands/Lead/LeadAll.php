<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Lead;
use App\Events\Logout;
use App\Events\Lead\LeadAll as LeadAllEvent;
use App\Events\Lead\LeadPart as LeadPartEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

/**
 * Class LeadAll.
 *
 * @example php artisan lead:all 0 10 -Cid -Cemail
 * @example php artisan lead:all -S=eyJyaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbCwiYWRtaW4iOnRydWV9
 *
 * @package App\Console\Commands\Lead
 */
class LeadAll extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:all {offset?} {limit?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get all leads';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $offset = $this->argument('offset');
        $limit = $this->argument('limit');
        if ($offset === null || !$limit) {
            $this->error('Please set up an offset & limit parameters to fetch leads');
            return;
        }

        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'firstName',
                'lastName',
                'email',
                'phone',
                'createdAt',
                'updatedAt'
            ];
        }

        $leads = Lead::all($columns)->toBase()->slice($offset, $limit);

        $this->table($columns, $leads->toArray());
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'admin' => bool - Grant access for admin only
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $isAdmin = $this->socket->get('admin');

        if ($conciergeId) {
            $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
            if (!$tokenValid) {
                $msg = 'Wrong token';
                $this->line($msg);
                return event(new Logout($conciergeId, $socketId, $msg));
            }

            $roleValid = Concierge::checkAdminRole($conciergeId);
            if ($isAdmin && !$roleValid) {
                $msg = 'Wrong role';
                $this->line($msg);
                return event(new Logout($conciergeId, $socketId, $msg));
            }
        }

        if ($socketId) {
            /** @var int|null $conciergeId - Concierge id */
            $conciergeId = $isAdmin ? null : $conciergeId;

            return event(new LeadAllEvent($this->socket->get('lid'), $conciergeId, null, $socketId));
        }

        $id = null;
        $index = 0;

        do {
            /** @var Builder $query */
            if ($id) {
                $query = Lead::where('id', '>', $id);
            } else {
                $query = Lead::where('id', '!=', null);
            }

            /** @var Collection $leads */
            $leads = $query->orderBy('id', 'asc')->limit(1000)->get();
            if ($leads->count() === 0) {
                break;
            }

            $id = $leads->last()->id;
            event(new LeadPartEvent($leads));

            $index++;
        } while ($index < 1000000);

        return true;
    }
}
