<?php

namespace App\Console\Commands\Lead;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Jobs\Lead\Agentology\ReferLead;
use App\Lead;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadRefer.
 *
 * @example php artisan lead:refer a4bc19c0-477d-11e7-a364-91b5d89fc617
 * @example php artisan lead:refer -S=eyJhaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbH0=
 *
 * @package App\Console\Commands\Lead
 */
class LeadRefer extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:refer {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Send lead to refer table';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        /** @var string $id - Lead id */
        $id = $this->argument('id');

        // TODO: Move this code to service and make call via facade.
        /** @var Lead $lead */
        $lead = Lead::find($id);
        if ($lead) {
            $lead->status = Lead::STATUS_REFERRED;
            $lead->save();
        }

        $this->info(sprintf('Status "%s" was set up for lead with id: "%s"', Lead::STATUS_REFERRED, $id));
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');
        $leadId = $this->socket->get('lid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        $lead = Lead::find($leadId);

        if ($lead) {
            $job = new ReferLead($lead);
            dispatch($job);
        }
    }
}
