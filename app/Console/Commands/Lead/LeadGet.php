<?php

namespace App\Console\Commands\Lead;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Events\Lead\LeadGet as LeadGetEvent;
use App\Lead;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadGet.
 *
 * @example php artisan lead:get a4bc19c0-477d-11e7-a364-91b5d89fc617 -Cid -Cemail
 * @example php artisan lead:get -S=eyJhaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbH0=
 *
 * @package App\Console\Commands\Lead
 */
class LeadGet extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:get {id?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get lead';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        /** @var string $id */
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Please set up lead id');
            return;
        }

        /** @var array $headers */
        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'firstName',
                'lastName',
                'email',
                'phone',
                'createdAt',
                'updatedAt'
            ];
        }

        $lead = Lead::all($columns)->where('id', $id)->toArray();

        $this->table($columns, $lead);
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        return event(new LeadGetEvent($this->socket->get('lid'), true, $socketId));
    }
}
