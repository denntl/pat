<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Lead;
use App\Events\Logout;
use App\Events\Lead\LeadUpdate as LeadUpdateEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadUpdate.
 *
 * @example php artisan lead:update
 * @example php artisan lead:update -S=eyJyaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGwsImRhdGEiOiIifQ==
 *
 * @package App\Console\Commands\Lead
 */
class LeadUpdate extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update lead';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'data' => string - Base64 encoded json string which stores lead data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, null, $msg));
        }

        $data = $this->socket->get('data');

        unset(
            $data['lastActivity'],
            $data['countUnreadActivity'],
            $data['agent'],
            $data['source'],
            $data['messages'],
            $data['emails'],
            $data['createdAt'],
            $data['updatedAt']
        );

        $lead = Lead::find($data['id']);
        if (!$lead) {
            $this->info('Lead not found');

            return null;
        }
        $lead->update($data);

        return event(new LeadUpdateEvent($lead, $socketId));
    }
}
