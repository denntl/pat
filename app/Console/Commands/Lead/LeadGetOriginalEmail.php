<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Events\Logout;
use App\Events\Lead\LeadGetOriginalEmail as LeadGetOriginalEmailEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadGet.
 * TODO: this command should be refactored
 *
 * @example php artisan lead:get:original-email
 * @example php artisan lead:get:original-email -S=eyJzaWQiOm51bGwsImNpZCI6bnVsbCwidG9rZW4iOm51bGwsImxpZCI6bnVsbH0=
 *
 * @package App\Console\Commands\Lead
 */
class LeadGetOriginalEmail extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:get:original-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get lead original email';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        // TODO: Feature requested
        $this->info('Not implemented');
    }

    /**
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            return event(new Logout($conciergeId, $socketId, null, 'Wrong token'));
        }

        return event(new LeadGetOriginalEmailEvent($socketId, $this->socket->get('lid')));
    }
}
