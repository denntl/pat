<?php

namespace App\Console\Commands\Lead;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Events\Lead\LeadSendEmails as LeadSendEmailsEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadSendEmails.
 *
 * @example php artisan lead:send-to
 * @example php artisan lead:send-to -S=eyJhaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbCwiZW1haWxzIjoiIn0=
 *
 * @package App\Console\Commands\Lead
 */
class LeadSendEmails extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:send:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Send lead emails';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'emails' => string - Email addresses (comma separated)
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            event(new Logout($agentId, $socketId, null, $msg));
        }

        event(new LeadSendEmailsEvent($this->socket->get('lid'), $this->socket->get('emails'), $socketId));
    }
}
