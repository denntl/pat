<?php

namespace App\Console\Commands\Lead;

use App\Facades\LeadCall as LeadCallFacade;
use App\Lead;
use Carbon\Carbon;
use App\Concierge;
use App\Events\Logout;
use App\Events\Lead\LeadCall as LeadCallEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadCall.
 *
 * @example php artisan lead:call
 * @example php artisan lead:call -S=eyJyaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbH0=
 *
 * @package App\Console\Commands\Lead
 */
class LeadCall extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:call';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Call to lead start notification';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'csid' => string - Call Sid
     *     'phone' => string - Phone number
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }

        $leadId = $this->socket->get('lid');
        $twilioCallSid = $this->socket->get('csid');
        $phone = $this->socket->get('phone');

        $lead = Lead::find($leadId);
        $agentId = $lead ? $lead->agentId : null;
        $conciergeId = $lead ? $lead->conciergeId : null;

        $leadCall = LeadCallFacade::create(
            $leadId,
            $twilioCallSid,
            $phone,
            false,
            Carbon::now()->toDateTimeString(),
            $agentId,
            $conciergeId
        );

        return event(new LeadCallEvent(
            $leadCall,
            $socketId
        ));
    }
}
