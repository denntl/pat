<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Events\Logout;
use App\Events\Lead\LeadUpdateRep as LeadUpdateRepEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadUpdateRep.
 *
 * @example php artisan lead:update:rep a4bc19c0-477d-11e7-a364-91b5d89fc617 1
 * @example php artisan lead:update:rep
 *     -S=eyJyaWQiOm51bGwsInNpZCI6bnVsbCwiY2lkIjpudWxsLCJsaWQiOm51bGwsInRva2VuIjpudWxsfQ==
 *
 * @package App\Console\Commands\Lead
 */
class LeadUpdateRep extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:update:rep {id?} {repId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update concierge relation for lead';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        $repId = $this->argument('repId');
        if (!$id || !$repId) {
            $this->error('Missed command argument. Please check the signature.');
            return;
        }

        event(new LeadUpdateRepEvent($id, (int)$repId));

        $this->info('Updated concierge id ' . $repId . ' for lead ' . $id);
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'rid' => int - Rep id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }
        if (!Concierge::find($conciergeId)) {
            $msg = 'Concierge doesn`t exist';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }

        $roleAdmin = Concierge::checkAdminRole($conciergeId);
        if (!$roleAdmin) {
            $msg = 'Wrong role';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }

        return event(new LeadUpdateRepEvent(
            $this->socket->get('lid'),
            (int)$this->socket->get('rid'),
            $conciergeId,
            $socketId
        ));
    }
}
