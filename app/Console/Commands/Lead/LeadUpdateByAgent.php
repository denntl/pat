<?php

namespace App\Console\Commands\Lead;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Events\Lead\LeadUpdate as LeadUpdateEvent;
use App\Lead;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadUpdateByAgent.
 *
 * @example php artisan lead:update:by-agent
 * @example php artisan lead:update:by-agent -S=eyJyaWQiOm51bGwsInNpZCI6bnVsbCwidG9rZW4iOm51bGwsImRhdGEiOiIifQ==
 *
 * @package App\Console\Commands\Lead
 */
class LeadUpdateByAgent extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:update:by-agent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update lead by agent';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     *     'data' => string - Base64 encoded json string which stores lead data
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        $data = $this->socket->get('data');

        unset(
            $data['lastActivity'],
            $data['countUnreadActivity'],
            $data['agent'],
            $data['source'],
            $data['messages'],
            $data['emails'],
            $data['createdAt'],
            $data['updatedAt']
        );

        $leadId = $data['id'];
        $lead = Lead::find($leadId);
        if ($lead) {
            $lead->update($data);

            return event(new LeadUpdateEvent($lead, $socketId));
        }

        $this->line('Invalid leadId');
        return null;
    }
}
