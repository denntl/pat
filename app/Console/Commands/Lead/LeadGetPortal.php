<?php

namespace App\Console\Commands\Lead;

use App\Agent;
use App\Events\LogoutAgent as Logout;
use App\Events\Lead\LeadAll as LeadAllEvent;
use App\Lead;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadGetPortal.
 *
 * @example php artisan lead:get-portal 1 -Cid -Cemail
 * @example php artisan lead:get-portal -S=eyJyaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbH0=
 *
 * @package App\Console\Commands\Lead
 */
class LeadGetPortal extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:get-portal {aid?} {--C|column=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Get all leads for agent';

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handleCommand()
    {
        /** @var int $aid - Agent id */
        $aid = $this->argument('aid');
        if (!$aid) {
            $this->error('Please set up agent id');
            return;
        }

        $columns = $this->option('column');
        if (empty($columns)) {
            $columns = [
                'id',
                'firstName',
                'lastName',
                'email',
                'phone',
                'createdAt',
                'updatedAt'
            ];
        }

        $leads = Lead::all($columns)->where('agentId', $aid)->toArray();

        $this->table($columns, $leads);
    }

    /**
     * Socket params: [
     *     'aid' => int - Agent id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $agentId = (int)$this->socket->get('aid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Agent::checkStaticToken($agentId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($agentId, $socketId, null, $msg));
        }

        return event(new LeadAllEvent(null, null, $agentId, $socketId));
    }
}
