<?php

namespace App\Console\Commands\Lead;

use App\Lead;
use App\LeadCall as LeadCallModel;
use Carbon\Carbon;
use App\Concierge;
use App\Events\Logout;
use App\Events\Lead\LeadCallEnd as LeadCallEndEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadCallEnd.
 *
 * @example php artisan lead:call:end
 * @example php artisan lead:call:end -S=eyJyaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbH0=
 *
 * @package App\Console\Commands\Lead
 */
class LeadCallEnd extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:call:end';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Call to lead end notification';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'csid' => string - Call Sid
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }

        $leadId = $this->socket->get('lid');
        $twilioCallSid = $this->socket->get('csid');
        $phone = null;
        $leadCallId = null;
        $finishedAt = null;

        if ($twilioCallSid) {
            /** @var LeadCallModel $leadCall */
            $leadCall = LeadCallModel::where('twilioCallSid', $twilioCallSid)->first();
            if ($leadCall) {
                $leadCall->finishedAt = Carbon::now()->toDateTimeString();
                $leadCall->save();

                $phone = $leadCall->phone;
                $leadCallId = $leadCall->id;
                $finishedAt = $leadCall->finishedAt;
            }
        }

        return event(new LeadCallEndEvent($leadId, $phone, $leadCallId, $socketId, $finishedAt));
    }
}
