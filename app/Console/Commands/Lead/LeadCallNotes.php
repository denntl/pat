<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Events\Logout;
use App\Events\Lead\LeadCallNotes as LeadCallNotesEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadCallNotes.
 *
 * @example php artisan lead:call:notes
 * @example php artisan lead:call:notes
 *     -S=eyJyaWQiOm51bGwsImxpZCI6bnVsbCwic2lkIjpudWxsLCJ0b2tlbiI6bnVsbCwibm90ZXMiOiIifQ==
 *
 * @package App\Console\Commands\Lead
 */
class LeadCallNotes extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:call:notes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Call to lead finish send notes notification';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $this->info('Not implemented');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id,
     *     'callId' => int - LeadCall id
     *     'token' => string - Auth token
     *     'notes' => string - Notes text
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }

        return event(new LeadCallNotesEvent(
            $this->socket->get('lid'),
            $this->socket->get('notes'),
            $this->socket->get('callId'),
            $conciergeId,
            $socketId
        ));
    }
}
