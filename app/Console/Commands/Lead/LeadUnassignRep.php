<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Events\Logout;
use App\Events\Lead\LeadUnassignRep as LeadUnassignRepEvent;
use App\Traits\WebSocket;
use Illuminate\Console\Command;

/**
 * Class LeadUnassignRep.
 *
 * @example php artisan lead:unassign:rep a4bc19c0-477d-11e7-a364-91b5d89fc617
 * @example php artisan lead:unassign:rep
 *     -S=eyJyaWQiOm51bGwsInNpZCI6bnVsbCwiY2lkIjpudWxsLCJsaWQiOm51bGwsInRva2VuIjpudWxsfQ==
 *
 * @package App\Console\Commands\Lead
 */
class LeadUnassignRep extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:unassign:rep {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update concierge relation for lead';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        if (!$id) {
            $this->error('Missed command argument. Please check the signature.');
            return;
        }

        event(new LeadUnassignRepEvent($id));

        $this->info('Lead' . $id . ' unassigned from concierge');
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }
        if (!Concierge::find($conciergeId)) {
            $msg = 'Concierge doesn`t exist';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }

        return event(new LeadUnassignRepEvent($this->socket->get('lid'), $socketId));
    }
}
