<?php

namespace App\Console\Commands\Lead;

use App\Jobs\Lead\AutoReassign;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use App\Lead as LeadModel;

class LeadReassignSingle extends Command
{
    use WebSocket;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:reassignSingle {lid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Reassign lead to another concierge';

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handleCommand()
    {
        /** @var string $leadId */
        $leadId = $this->argument('lid');
        if (!$leadId) {
            $this->error('Please set up lead id');
            return;
        }

        $lead = LeadModel::find($leadId);

        dispatch(new AutoReassign($lead));
    }

    /**
     * Socket params: [
     *     'lid' => string - Lead id
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $leadId = $this->socket->get('lid');
        $lead = LeadModel::find($leadId);

        dispatch(new AutoReassign($lead));
    }
}
