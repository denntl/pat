<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Events\Logout;
use App\Facades\Lead;
use App\Jobs\Lead\AutoReassign;
use App\Lead as LeadModel;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

/**
 * Class LeadReassign.
 *
 * @example php artisan lead:reassign 1
 * @example php artisan lead:reassign -S=eyJyaWQiOm51bGwsInRva2VuIjpudWxsfQ==
 *
 * @package App\Console\Commands\Lead
 */
class LeadReassign extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:reassign {cid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Makes leads reassign from specific concierge';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $repId = (int)$this->argument('cid');

        /** @var Collection $leads */
        $leads = LeadModel::byConcierge($repId)->get();

        /** @var LeadModel $lead */
        foreach ($leads as $lead) {
            $repOutId = Lead::assignToRep($lead);

            $this->info('Reassign lead id-' . $lead->id . ' to concierge id-' . $repOutId);
        }
    }

    /**
     * Socket params: [
     *     'cid' => int - Rep (Concierge) id
     *     'sid' => string - Publisher socket id
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $repId = (int)$this->socket->get('cid');

        if ($this->socket->get('system')) {
            Log::info('Reassign event emitted by system');
            $this->reassign($repId);

            return true;
        }
        $tokenValid = Concierge::checkStaticToken($repId, $this->socket->get('token'));
        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($repId, $this->socket->get('sid'), $msg));
        }

        $this->reassign($repId);

        return true;
    }

    /**
     * Reassing leads.
     *
     * @param integer $repId
     *
     * @return void
     */
    private function reassign(int $repId)
    {
        /** @var array $concierge */
        $leads = LeadModel::byConcierge($repId)->get();

        /** @var LeadModel $lead */
        foreach ($leads as $lead) {
            dispatch(new AutoReassign($lead));
        }
    }
}
