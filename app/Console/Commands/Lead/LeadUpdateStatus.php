<?php

namespace App\Console\Commands\Lead;

use App\Concierge;
use App\Jobs\Lead\ChangeStatusByAction;
use App\Lead;
use App\Events\Logout;
use App\Facades\Lead as LeadService;
use App\Traits\WebSocket;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Events\Lead\LeadUpdateStatus as LeadUpdateStatusEvent;

/**
 * Class LeadUpdateStatus.
 *
 * @example php artisan lead:update:status a4bc19c0-477d-11e7-a364-91b5d89fc617 'Escalate'
 * @example php artisan lead:update:status
 *      -S=eyJsaWQiOm51bGwsInNpZCI6bnVsbCwiY2lkIjpudWxsLCJzdGF0dXMiOiJOZXcgbGVhZCIsIm5vdGUiOiIifQ==
 *
 * @package App\Console\Commands\Lead
 */
class LeadUpdateStatus extends Command
{
    use WebSocket;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:update:status {id?} {action?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Update lead status with some notes';

    /**
     * @inheritDoc
     */
    public function handleCommand()
    {
        $id = $this->argument('id');
        $action = $this->argument('action');
        if (!$id || !$action) {
            $this->line('Missed command argument. Please check the signature.');
            return;
        }

        $lead = Lead::find($id);

        if ($lead) {
            LeadService::processAction($lead, $action);
        }
    }

    /**
     * Socket params: [
     *     'cid' => int - Concierge id
     *     'lid' => string - Lead id
     *     'sid' => string - Publisher socket id
     *     'note' => string - Lead note describes reason for 'Update status'
     *     'action' => string - Action type
     *     'token' => string - Auth token
     * ]
     *
     * @inheritDoc
     */
    public function handleSocketCommand()
    {
        $conciergeId = (int)$this->socket->get('cid');
        $socketId = $this->socket->get('sid');
        $leadId = $this->socket->get('lid');
        $action = $this->socket->get('action');
        $noteText = $this->socket->get('note');
        $date = $this->socket->get('date') ?? '';
        $timezone = $this->socket->get('timezone') ?? '';

        $noteData = [
            'leadId' => $leadId,
            'conciergeId' =>  $conciergeId,
            'note' => $noteText,
            'type' => $action,
        ];

        $tokenValid = Concierge::checkStaticToken($conciergeId, $this->socket->get('token'));

        if (!$tokenValid) {
            $msg = 'Wrong token';
            $this->line($msg);
            return event(new Logout($conciergeId, $socketId, $msg));
        }

        $lead = Lead::find($leadId);

        if ($lead) {
            $delay = $this->prepareDateObject($date, $timezone);

            // Update tag and icon for scheduled call
            if ($action == Lead::ACTION_CALL_REQUESTED && $delay) {
                $lead->tag = Lead::TAG_ONGOING_CHAT;
                $lead->icon = Lead::ICON_CALL;
                $lead->save();

                event(new LeadUpdateStatusEvent($lead, $socketId));
            }

            $job = new ChangeStatusByAction($lead, $action, false, $noteText, $socketId);
            dispatch($job->delay($delay));

            if ($noteText && trim($noteText)) {
                $noteData = [
                    'leadId' => $leadId,
                    'conciergeId' =>  $conciergeId,
                    'note' => $noteText,
                    'type' => $action,
                ];

                LeadService::createLeadNote($noteData, $socketId);
            }
        }
    }

    /**
     * Prepare object for jobs delay
     *
     * @param string $date
     * @param string $timezone
     *
     * @return null|Carbon
     */
    private function prepareDateObject(string $date, string $timezone = '')
    {
        $carbon = null;
        $format = 'm/d/Y H:i A';
        $formattedTimezone = null;
        $defaultTimezone = 'America/Los_Angeles';

        $subtractMinutes = 5;

        if ($date) {
            switch ($timezone) {
                case '':
                    $formattedTimezone = $defaultTimezone;
                    break;
                case 'Pacific':
                    $formattedTimezone = 'America/Los_Angeles';
                    break;
                case 'Mountain':
                    $formattedTimezone = 'America/Denver';
                    break;
                case 'Central':
                    $formattedTimezone = 'America/Chicago';
                    break;
                case 'Eastern':
                    $formattedTimezone = 'America/New_York';
                    break;
                default:
                    $formattedTimezone = $defaultTimezone;
                    break;
            }

            $carbon = Carbon::createFromFormat($format, $date, $formattedTimezone)->subMinutes($subtractMinutes);
        }

        return $carbon;
    }
}
