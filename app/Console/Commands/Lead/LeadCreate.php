<?php

namespace App\Console\Commands\Lead;

use App\Lead;
use App\Events\Lead\LeadCreate as CreateLead;
use Illuminate\Console\Command;

/**
 * Class LeadCreate.
 *
 * @example php artisan lead:create James Bond 007@gmail.com 5 42
 *
 * @package App\Console\Commands\Lead
 */
class LeadCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:create {firstName} {lastName} {email} {agentId} {sourceId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PAT :: Command for creating new lead. Created for using(testing functionality) locally.';

    /**
     * Execute the console command.
     *
     * @inheritDoc
     */
    public function handle()
    {
        $lead = new Lead();

        $lead->firstName = $this->argument('firstName');
        $lead->lastName = $this->argument('lastName');
        $lead->email = $this->argument('email');
        $lead->agentId = $this->argument('agentId');
        $lead->sourceId = $this->argument('sourceId');
        $lead->status = 'New lead';

        $lead->save();
        event(new CreateLead($lead));
    }
}
