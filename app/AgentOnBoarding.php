<?php

namespace App;

use App\Models\AbstractModel as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class AgentOnBoarding.
 *
 * @property integer $id
 * @property boolean $skip
 * @property integer $step
 * @property integer $agentId
 * @property integer $tokenReceived
 *
 * @property Agent agent
 *
 * @package App
 */
class AgentOnBoarding extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agentOnBoarding';

    /**
     * Indicates if the model should be timestamped.
     *
     * @inherit bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step',
        'skip',
        'agentId',
        'tokenReceived'
    ];

    /**
     * Get the leads for the agent.
     *
     * @return BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent', 'agentId');
    }
}
