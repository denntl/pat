<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SalesForceService;

/**
 * Class SalesForceServiceProvider.
 * @package App\Providers
 * @deprecated
 */
class SalesForceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SalesForceService::class, function () {
            return new SalesForceService(env("SALES_FORCE_API_URL"));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [SalesForceService::class];
    }
}
