<?php

namespace App\Providers;

use App\Services\VikingLeadHelperService;
use Illuminate\Support\ServiceProvider;

/**
 * Class VikingLeadHelperServiceProvider.
 * @package App\Providers
 */
class VikingLeadHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(VikingLeadHelperService::class, function () {
            return new VikingLeadHelperService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [VikingLeadHelperService::class];
    }
}
