<?php

namespace App\Providers;

use App\Services\IntercomService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

/**
 * Class IntercomServiceProvider.
 * @package App\Providers
 */
class IntercomServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IntercomService::class, function () {
            $isProduction = App::environment('production');

            $token = $isProduction
                ? config('services.intercom.access_token')
                : config('services.intercom.test_access_token');

            return new IntercomService($token);
        });
    }
}
