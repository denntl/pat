<?php

namespace App\Providers;

use App\Services\LeadService;
use Illuminate\Support\ServiceProvider;

/**
 * Class LeadServiceProvider.
 * @package App\Providers
 */
class LeadServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LeadService::class, function () {
            return new LeadService();
        });
    }
}
