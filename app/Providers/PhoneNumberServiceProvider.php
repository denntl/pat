<?php

namespace App\Providers;

use App\Services\PhoneNumberService;
use Illuminate\Support\ServiceProvider;

/**
 * Class PhoneNumberServiceProvider
 * @package App\Providers
 */
class PhoneNumberServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PhoneNumberService::class, function () {
            return new PhoneNumberService();
        });
    }
}
