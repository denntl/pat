<?php

namespace App\Providers;

use App\Services\NoteService;
use Illuminate\Support\ServiceProvider;

/**
 * Class NoteServiceProvider.
 * @package App\Providers
 */
class NoteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(NoteService::class, function () {
            return new NoteService();
        });
    }
}
