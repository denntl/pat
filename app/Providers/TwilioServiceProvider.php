<?php

namespace App\Providers;

use App\Services\TwilioService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Twilio\Exceptions\TwilioException;
use Illuminate\Support\Facades\Log;

/**
 * Class TwilioServiceProvider.
 * @package App\Providers
 */
class TwilioServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TwilioService::class, function () {
            $isProduction = App::environment('production');
            $isDemo = App::environment('demo');

            $sid = ($isProduction || $isDemo)
                ? config('services.twilio.account_sid')
                : config('services.twilio.test_account_sid');
            $token = ($isProduction || $isDemo)
                ? config('services.twilio.auth_token')
                : config('services.twilio.test_auth_token');
            $appSid = config('services.twilio.app_sid');

            try {
                $twilioService = new TwilioService($sid, $token, $appSid);
            } catch (TwilioException $e) {
                Log::error($e->getMessage(), [
                    'environment' => App::environment(),
                    'appSid' => $appSid,
                    'sid' => $sid,
                    'token' => $token
                ]);
                
                throw $e;
            }

            return $twilioService;
        });
    }
}
