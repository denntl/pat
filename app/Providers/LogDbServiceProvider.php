<?php

namespace App\Providers;

use App\Services\LogDbService;
use Illuminate\Support\ServiceProvider;

/**
 * Class LogDbServiceProvider.
 *
 * @package App\Providers
 */
class LogDbServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LogDbService::class, function () {
            return new LogDbService();
        });
    }
}
