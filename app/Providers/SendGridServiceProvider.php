<?php

namespace App\Providers;

use App\Services\SendGridService;
use Illuminate\Support\ServiceProvider;

/**
 * Class SendGridServiceProvider.
 * @package App\Providers
 */
class SendGridServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SendGridService::class, function () {
            return new SendGridService(
                config('services.sendgrid.key'),
                config('mail.from.address')
            );
        });
    }
}
