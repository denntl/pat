<?php

namespace App\Providers;

use App\Agent;
use App\Concierge;
use App\Email;
use App\Lead;
use App\Observers\AgentObserver;
use App\Observers\ConciergeObserver;
use App\Observers\EmailObserver;
use App\Observers\LeadObserver;
use Illuminate\Support\ServiceProvider;
use \L5Swagger\L5SwaggerServiceProvider;
use Jenssegers\Rollbar\RollbarServiceProvider;
use App\Traits\Validators;

class AppServiceProvider extends ServiceProvider
{
    use Validators;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Agent::observe(AgentObserver::class);
        Lead::observe(LeadObserver::class);
        Concierge::observe(ConciergeObserver::class);
        Email::observe(EmailObserver::class);

        $this->initializeValidators();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(L5SwaggerServiceProvider::class);

        if (($this->app->environment() == 'production') ||
            ($this->app->environment() == 'demo') ||
            ($this->app->environment() == 'dev')
        ) {
            $this->app->register(RollbarServiceProvider::class);
        }
    }
}
