<?php

namespace App\Jobs;

use App\Facades\LogDb;
use App\Facades\VikingLeadHelper;
use App\LogsJobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SyncLeads implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $logDataForWorkers;

    /**
     * Create a new job instance.
     *
     * @param array $data
     * @param array $logDataForWorkers
     */
    public function __construct(array $data, array $logDataForWorkers)
    {
        $this->data = $data;
        $this->logDataForWorkers = $logDataForWorkers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $item) {
            LogDb::setInstance(LogsJobs::class);
            LogDb::addParam('class', __CLASS__);
            VikingLeadHelper::synchronizeLead($item, $this->logDataForWorkers);
        }
    }

    /**
     * Fail job handler.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $message = 'lead sync failed: ' . $exception->getMessage();
        LogDb::addParam('status', $message);
        LogDb::save();
        Log::error($message);
    }
}
