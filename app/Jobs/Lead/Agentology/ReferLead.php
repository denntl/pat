<?php

namespace App\Jobs\Lead\Agentology;

use App\Events\Lead\LeadRefer;
use App\Events\Lead\LeadReferFailed;
use App\Events\Lead\LeadUpdate;
use App\Facades\Intercom;
use App\Lead;
use App\Services\Api\Agentology\Facades\Agentology;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReferLead implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $lead;
    private $socketId;

    /**
     * Create a new job instance.
     *
     * @param  Lead $lead
     * @param  string $socketId
     */
    public function __construct(Lead $lead, string $socketId = null)
    {
        $this->lead = $lead;
        $this->socketId = $socketId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $lead = $this->lead;
        $response = Agentology::referLead($lead);

        if ($response && $response->response->success) {
            $opportunityId = $response->response->opportunity->id;
            $this->referLead($opportunityId);
        } else {
            event(new LeadReferFailed($lead->id, $this->socketId));
        }
    }

    /**
     * Fail job handler.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $message = 'lead refer failed: ' . $exception->getMessage();
        logger()->error($message);
        event(new LeadReferFailed($this->lead->id, $this->socketId));
    }

    /**
     * Process lead refer.
     *
     * @param string $opportunityId
     *
     * @return void
     */
    private function referLead(string $opportunityId)
    {
        $lead = $this->lead;
        $lead->status = Lead::STATUS_REFERRED;
        $lead->opportunityId = $opportunityId;

        if ($lead->opportunityId && $lead->save()) {
            event(new LeadUpdate($lead));
            event(new LeadRefer($lead->id, $this->socketId));
            try {
                Intercom::updateLeadsMetrics($lead->agentId);
            } catch (\Exception $exception) {
                $message = 'Intercom update metrics failed: ' . $exception->getMessage();
                logger()->error($message);
                event(new LeadReferFailed($lead->id, $this->socketId));
            }
        } else {
            event(new LeadReferFailed($lead->id, $this->socketId));
        }
    }
}
