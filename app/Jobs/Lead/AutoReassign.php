<?php

namespace App\Jobs\Lead;

use App\Facades\LogDb;
use App\Lead as LeadModel;
use App\Facades\Lead;
use App\LogsJobs;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

/**
 * Class AutoReassign.
 * @package App\Jobs\Lead
 */
class AutoReassign implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var LeadModel
     */
    protected $lead;

    /**
     * Create a new job instance.
     *
     * @param LeadModel $lead
     */
    public function __construct(LeadModel $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        LogDb::setInstance(LogsJobs::class);
        LogDb::addParam('class', __CLASS__);

        Lead::assignToRep($this->lead);
        LogDb::save();
    }

    /**
     * The job failed to process.
     *
     * @param \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        LogDb::setInstance(LogsJobs::class);
        LogDb::addParam('status', 'FAILED REASSIGNED JOB: ' . $exception->getMessage());
        LogDb::save();
        Log::error('FAILED REASSIGNED JOB: ' . $exception->getMessage());
    }
}
