<?php

namespace App\Jobs\Lead\GoodBadEmailFlow;

use App\Facades\Lead as LeadService;
use App\Facades\SendGrid;
use App\Jobs\Email\SendAutomationEmail;
use App\Lead;
use App\Services\Time\Facades\TimeHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class HandleFlowStep
 * @package App\Jobs\Lead\GoodBadEmailFlow
 */
class HandleFlowStep implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Lead $lead
     */
    public $lead;

    /**
     * @var int $step
     */
    public $step;


    /**
     * HandleFlowStep constructor.
     * @param $lead
     * @param int $step
     */
    public function __construct($lead, $step = 1)
    {
        $this->lead = $lead;
        $this->step = $step;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (LeadService::leadHasIncomingActivities($this->lead) ||
            !LeadService::checkLeadAllowedFlowStatuses($this->lead)
        ) {
            return;
        }

        switch ($this->step) {
            case 1:
                $nextStep = 2;
                $delay = 40;
                $this->processStep($nextStep, $delay);
                break;
            case 2:
                $nextStep = 3;
                $delay = 50;
                $this->processStep($nextStep, $delay);
                break;
            case 3:
                $nextStep = 4;
                $delay = 72;
                $this->processStep($nextStep, $delay);
                break;
            case 4:
                $nextStep = 5;
                $delay = 24;
                $this->processStep($nextStep, $delay);
                break;
            case 5:
                $this->makeLeadUnqualified();
                break;
            default:
                break;
        }
    }

    /**
     *
     */
    private function sendEmail()
    {
        $job = new SendAutomationEmail($this->lead, $this->step);
        $delay = null;

        dispatch($job->delay($delay));
    }

    /**
     * @param int $nextStep
     * @param int $delay
     */
    private function processStep(int $nextStep, int $delay = 0)
    {
        if (SendGrid::emailCanBeSend($this->lead)) {
            $this->sendEmail();

            $delay = TimeHelper::createDelay($delay, 0, $this->lead->timezone);
            $job = new HandleFlowStep($this->lead, $nextStep);

            dispatch($job->delay($delay));
        }
    }

    /**
     * Mark lead as "Unqualified"
     */
    private function makeLeadUnqualified()
    {
        $lead = $this->lead;
        $note = 'Went through all attempts. Lead was unresponsive';
        $action = Lead::ACTION_UNQUALIFY;

        $result = LeadService::processAction($lead, $action, true);

        if ($result) {
            $noteData = [
                'leadId' => $this->lead->id,
                'note' => $note,
                'type' => $action,
            ];
            LeadService::createLeadNote($noteData, $action);
        }
    }

    /**
     * Fail job handler.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $message = 'Good email flow failed: ' . $exception->getMessage();
        info($message);
    }
}
