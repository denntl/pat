<?php

namespace App\Jobs\Lead;

use App\Facades\Lead as LeadService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ChangeStatusByAction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $lead;

    private $action;

    private $casedByAutomationFlow;

    private $noteText;

    private $socketId;

    public function __construct($lead, $action, $casedByAutomationFlow = false, $noteText = '', $socketId = null)
    {
        $this->lead = $lead;
        $this->action = $action;
        $this->noteText = $noteText;
        $this->socketId = $socketId;
        $this->casedByAutomationFlow = $casedByAutomationFlow;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->processAction();
    }


    private function processAction()
    {
        $lead = $this->lead;
        $action = $this->action;
        $noteText = $this->noteText;
        $socketId = $this->socketId;
        $casedByAutomationFlow = $this->casedByAutomationFlow;

        LeadService::processAction($lead, $action, $casedByAutomationFlow, $noteText, $socketId);
    }

    /**
     * Fail job handler.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $message = 'lead status change failed: ' . $exception->getMessage();
        info($message);
    }
}
