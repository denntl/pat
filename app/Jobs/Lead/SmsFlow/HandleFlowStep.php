<?php

namespace App\Jobs\Lead\SmsFlow;

use App\Events\Lead\LeadPauseSmsFlow;
use App\Facades\Lead as LeadFacade;
use App\Events\Message\MessageCreate;
use App\Facades\Lead as LeadService;
use App\Facades\LogDb;
use App\Facades\SendGrid;
use App\Jobs\Email\SendAutomationEmail;
use App\Lead;
use App\Facades\Message as MessageService;
use App\LogsJobs;
use App\Services\Time\Facades\TimeHelper;
use App\Traits\CheckAllowedTimeFrame;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class HandleFlowStep implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, CheckAllowedTimeFrame;
    /**
     * @var string $leadId
     */
    public $leadId;

    /**
     * @var Lead $lead
     */
    public $lead;

    /**
     * HandleFlowStep constructor.
     * @param $leadId
     */
    public function __construct($leadId)
    {
        $this->leadId = $leadId;
        $this->lead = Lead::find($this->leadId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->checkLeadOnConditions($this->leadId, $this->lead)) {
            return;
        }

        if (!LogDb::getInstance() || LogDb::getInstance() != LogsJobs::class) {
            LogDb::setInstance(LogsJobs::class);
        }

        if (!$this->lead->smsFlowStep) {
            $this->lead->smsFlowStep = 1;
        }

        switch ($this->lead->smsFlowStep) {
            case 1:
                $messageText = 'Let me know :)';

                if ($this->sendMessage($this->lead, $messageText)) {
                    $this->runNextStep('now +40min');
                }

                break;
            case 2:
                $messageText = 'Is there a more convenient time to talk?';

                if ($this->sendMessage($this->lead, $messageText)) {
                    $this->runNextStep('now +1hour');
                }

                break;
            case 3:
                LeadFacade::processAction($this->lead, Lead::ACTION_CALL_REQUESTED, true);

                // Pause sms flow
                $this->lead->isPaused = true;
                $this->lead->smsFlowStep++;
                $this->lead->save();

                event(new LeadPauseSmsFlow($this->lead->id, $this->lead->isPaused, $this->lead->smsFlowStep));

                break;
            case 4:
                if (SendGrid::emailCanBeSend($this->lead)) {
                    $this->sendEmail(1);
                    $this->runNextStep('tomorrow +9hour +24min');

                    event(new LeadPauseSmsFlow($this->lead->id, $this->lead->isPaused, $this->lead->smsFlowStep));
                }

                break;
            case 5:
                $textBuyer =
                    'Morning! Not sure if I’ve mentioned this before. ' .
                    'Our team has access to properties that haven’t hit the market yet and will be coming soon. ' .
                    'Are you interested?';
                $textSeller =
                    'Morning! I’ve started to research similar properties. ' .
                    'Curious, when was the last time you looked into the value of your home?';
                $messageText = ($this->lead->leadType == 'buyer') ? $textBuyer : $textSeller;

                if ($this->sendMessage($this->lead, $messageText)) {
                    $this->runNextStep('tomorrow +17hour +30min');
                }

                break;
            case 6:
                $city = $this->lead->city != '' ? $this->lead->city : 'your city';

                $textBuyer =
                    'I’m doing a quick survey on how many texts it takes to get a response :) ' .
                    'Are you still interested in connecting with a top agent in ' . $city . '?' ;
                $textSeller =
                    'Hey ' . $this->lead->firstName . ', I`m doing a quick survey on how many texts it takes' .
                    ' to get a response :) Are you still interested in getting a personalized home valuation?';
                $messageText = ($this->lead->leadType == 'buyer') ? $textBuyer : $textSeller;

                if ($this->sendMessage($this->lead, $messageText)) {
                    $this->runNextStep('tomorrow +15hour +30min');
                }

                break;
            case 7:
                if (SendGrid::emailCanBeSend($this->lead)) {
                    $this->sendEmail(2);
                    $this->runNextStep('tomorrow +16hour');
                }

                break;
            case 8:
                $messageText = $this->lead->firstName . '? Don’t worry, It’s not you, it’s me. I can sometimes '.
                    'come on too strong - Real Estate is just so exciting! Let me know if you need any questions ' .
                    'answered. I am always here to help! :)';

                if ($this->sendMessage($this->lead, $messageText)) {
                    $this->runNextStep('now +24hour');
                }

                break;
            case 9:
                $this->makeLeadUnqualified();

                break;
            default:
                break;
        }
    }

    /**
     * The job failed to process.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(\Exception $exception)
    {
        if (!LogDb::getInstance() || LogDb::getInstance() != LogsJobs::class) {
            LogDb::setInstance(LogsJobs::class);
        }

        LogDb::addParam('status', 'FAILED SMS FLOW JOB: ' . $exception->getMessage());
        LogDb::save();

        Log::error('FAILED SMS FLOW JOB: ' . $exception->getMessage());
    }

    /**
     * @param string $time
     */
    private function runNextStep($time)
    {
        $delay = Carbon::parse($time, $this->lead->timezone);

        $delay = TimeHelper::returnAllowedDelay($delay);

        $this->lead->smsFlowStep++;
        $this->lead->save();

        $job = new HandleFlowStep($this->lead->id);
        dispatch($job->delay($delay));
    }

    /**
     * Send email to lead.
     *
     * @param int $template
     *
     */
    private function sendEmail($template)
    {
        $job = new SendAutomationEmail($this->lead, $template);
        dispatch($job);
    }

    /**
     * Send SMS message to lead.
     *
     * @param Lead $lead
     * @param string $messageText
     *
     * @return boolean
     */
    private function sendMessage(Lead $lead, $messageText)
    {
        $data = [
            'conciergeId' => $lead->conciergeId,
            'leadId' => $this->leadId,
            'incoming' => '0',
            'isRead' => false,
            'isAutomatic' => true,
            'to' => $lead->phone,
            'from' => $lead->agent ? $lead->agent->twilioNumber : '',
            'text' => $messageText,
            'twilioMessageSid' => null,
            'twilioMessageStatus' => null,
        ];

        LogDb::addParam('data', 'Create message data: ' . json_encode($data));

        $message = MessageService::createMessage($data, true);

        if (!$message) {
            LogDb::addParam('status', 'Message not created');
            LogDb::save();

            return false;
        }

        LogDb::addParam('status', 'Message ' . $message->id . ' for lead ' . $this->leadId . ' created');
        LogDb::save();

        event(new MessageCreate($message));

        return true;
    }

    /**
     * Mark lead as "Unqualified"
     */
    private function makeLeadUnqualified()
    {
        $lead = $this->lead;
        $note = 'Went through all attempts. Lead was unresponsive';
        $action = Lead::ACTION_UNQUALIFY;

        $result = LeadService::processAction($lead, $action);

        if ($result) {
            $noteData = [
                'leadId' => $this->lead->id,
                'note' => $note,
                'type' => $action,
            ];

            $note = LeadFacade::createLeadNote($noteData, null);
        }
    }

    /**
     * Check lead on forwarded calls, messages and emails and if lead exists.
     *
     * @param string $leadId
     * @param Lead $lead
     *
     * @return bool
     */
    private function checkLeadOnConditions(string $leadId, Lead $lead = null): bool
    {
        $hasIncomingActivities = LeadService::leadHasIncomingActivities($lead);
        $statusMessageText = 'Stop automation flow for lead ' . $leadId . ', because of status ' . $lead->status;

        switch (true) {
            case !LeadService::checkLeadAllowedFlowStatuses($lead):
                LogDb::addParam('status', $statusMessageText);
                break;
            case !$lead:
                LogDb::addParam('status', 'Lead with id ' . $leadId . ' not found');
                break;
            case $lead->isPaused && $hasIncomingActivities:
                LogDb::addParam('status', 'Return null. Incoming emails or messages found for lead ' . $leadId);
                break;
            case $lead->isPaused:
                LogDb::addParam('status', 'Work flow is paused for lead ' . $leadId);
                break;
            default:
                // We should get here to `Continue SMS Automation`
                return true;
        }
        LogDb::save();

        return false;
    }
}
