<?php

namespace App\Jobs;

use App\Facades\VikingLeadHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class CreateAttachmentForLead implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $attachments;

    private $leadId;

    /**
     * Create a new job instance.
     *
     * @param $attachments
     * @param $leadId
     *
     * @return void
     */
    public function __construct($attachments, $leadId)
    {
        $this->attachments = $attachments;
        $this->leadId = $leadId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->attachments as $attachment) {
            VikingLeadHelper::createAttachmentForLead($attachment, $this->leadId);
        }
    }

    public function failed(\Exception $exception)
    {
        Log::error('attachment failed');
    }
}
