<?php

namespace App\Jobs\Email;

use App\Email;
use App\Events\Email\EmailCreate;
use App\Facades\SendGrid;
use App\Lead;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\View;
use Ramsey\Uuid\Uuid;

/**
 * Class SendAutomationEmail
 * @package App\Jobs\Email
 */
class SendAutomationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Lead
     */
    public $lead;

    /**
     * @var int
     */
    public $step;

    /**
     * SendAutomationEmail constructor.
     * @param Lead $lead
     * @param int $step
     */
    public function __construct(Lead $lead, int $step = 1)
    {
        $this->lead = $lead;
        $this->step = $step;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sendEmail();
    }

    /**
     * Sends lead email via sendgrid
     */
    private function sendEmail()
    {
        $result = null;
        $email = $this->prepareEmailObject();

        if ($email) {
            $email->lead->tag = Lead::TAG_AUTOMATION;
            $email->lead->icon = Lead::ICON_AUTO;
            $email->save();

            SendGrid::sendLeadEmail($email);
            event(new EmailCreate($email));
        }
    }

    /**
     * @return Email|null
     */
    private function prepareEmailObject()
    {
        $email = null;

        $agent = $this->lead->agent()->first();

        if (SendGrid::emailCanBeSend($this->lead)) {
            $from = $agent->sendGridEmail;
            $to = $this->lead->email;
            $subject = $this->prepareSubject();
            $text = $this->prepareTemplate();

            $email = Email::create([
                'from' => $from,
                'to' => $to,
                'subject' => $subject,
                'statusId' => Email::NEW_EMAIL_STATUS,
                'agentId' => $agent->id,
                'leadId' => $this->lead->id,
                'senderFullName' => $agent->getFullName(),
                'conciergeId' => $this->lead->consiergeId,
                'text' => $text,
                'UUID' => Uuid::uuid1()->toString()
            ]);
        }

        return $email;
    }

    /**
     * @return string
     */
    private function prepareSubject(): string
    {
        $customerData  = '[CustomerID: ' . $this->lead->id . ']';
        $subjectText = 'I\'m here if you need me...';

        if ($this->step == 4) {
            $subjectText = 'quick question';
        }

        $subject = $subjectText . ' ' . $customerData;

        return $subject;
    }

    /**
     * @return string
     */
    private function prepareTemplate(): string
    {
        $leadType = $this->lead->leadType ?? 'seller';
        $template = 'email.templates.' . strtolower($leadType) . '.email-' . $this->step;
        $emailText = View::make($template, [
            'lead' => $this->lead,
            'agent' => $this->lead->agent()->first()
        ])->render();

        return $emailText;
    }
}
