<?php

namespace App\Jobs\Email;

use App\Email;
use App\Services\Api\FakeRequest\Facades\FakeRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class MakeFakeUpdateStatusRequest
 * @package App\Jobs\Email
 */
class MakeFakeUpdateStatusRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Email
     */
    private $email;

    /**
     * Create a new job instance.
     *
     * @var Email
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (app()->environment(['dev', 'demo', 'local*']) && !$this->email->incoming) {
            FakeRequest::postEmailStatuses($this->email);
        }
    }
}
