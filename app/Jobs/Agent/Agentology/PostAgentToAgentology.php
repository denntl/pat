<?php

namespace App\Jobs\Agent\Agentology;

use App\Agent;
use App\Services\Agentology\Exceptions\PostAgentToAgentologyException;
use App\Services\Api\Agentology\Facades\Agentology;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Response;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class PostAgentToAgentology
 * @package App\Jobs\Agent\Agentology
 */
class PostAgentToAgentology implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Max job attempts.
     */
    const MAX_ATTEMPTS = 10;

    /**
     * @var Agent
     */
    private $agent;

    /**
     * @var int
     */
    private $attempt;

    /**
     * Create a new job instance.
     *
     * @param Agent $agent
     * @param int $attempt
     *
     */
    public function __construct(Agent $agent, int $attempt = 1)
    {
        $this->agent = $agent;
        $this->attempt = $attempt;
    }

    /**
     * Execute the job.
     *
     * @throws PostAgentToAgentologyException
     *
     * @return void
     */
    public function handle()
    {
        $responseObj = Agentology::upsertAgent($this->agent);

        if (isset($responseObj->statusCode) && $responseObj->statusCode == Response::HTTP_CREATED) {
            $this->agent->agentologyUUID = $responseObj->response->user->uuid;
            $this->agent->save();
        } elseif (self::MAX_ATTEMPTS >= $this->attempt) {
            $nextAttempt = $this->attempt++;
            $job = new PostAgentToAgentology($this->agent, $nextAttempt);
            dispatch($job->delay(Carbon::now()->addHour()));
        } else {
            throw new PostAgentToAgentologyException($responseObj);
        }
    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     *
     * @return void
     */
    public function failed(Exception $exception)
    {
        logger()->error($exception->getMessage(), [
            'description' => 'Sending agent to Agentology failed',
            'trace' => $exception->getTraceAsString()
        ]);
    }
}
