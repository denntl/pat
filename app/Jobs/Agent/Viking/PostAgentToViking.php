<?php

namespace App\Jobs\Agent\Viking;

use App\Agent;
use App\Services\Api\Viking\Exceptions\UpdateAgentAtVikingException;
use App\Services\Api\Viking\Facades\Viking;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Response;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Exception;

class PostAgentToViking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Max attempt
     */
    const MAX_ATTEMPTS = 10;

    private $agent;

    private $attempt;

    /**
     * Create a new job instance.
     *
     * @param Agent $agent
     * @param int $attempt
     *
     */
    public function __construct(Agent $agent, int $attempt = 1)
    {
        $this->agent = $agent;
        $this->attempt = $attempt;
    }

    /**
     * Execute the job.
     *
     * @throws UpdateAgentAtVikingException
     *
     * @return void
     */
    public function handle()
    {
        $responseObj = Viking::sendNewAgent($this->agent);

        if (isset($responseObj->statusCode) && $responseObj->statusCode == Response::HTTP_OK) {
            $this->agent->createdAtViking = true;
            $this->agent->save();
        } elseif (self::MAX_ATTEMPTS >= $this->attempt) {
            $nextAttempt = $this->attempt++;
            $job = new PostAgentToViking($this->agent, $nextAttempt);
            dispatch($job->delay(Carbon::now()->addHour()));
        } else {
            throw new UpdateAgentAtVikingException($responseObj);
        }
    }

    public function failed(Exception $exception)
    {
        logger()->error($exception->getMessage(), [
            'description' => 'Sending agent to Viking failed',
            'trace' => $exception->getTraceAsString()
        ]);
    }
}
