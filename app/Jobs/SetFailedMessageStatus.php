<?php

namespace App\Jobs;

use App\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Message\MessageUpdateStatus;
use Illuminate\Foundation\Bus\Dispatchable;

class SetFailedMessageStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Message
     */
    private $message;

    /**
     * Create a new job instance.
     *
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->message->twilioMessageStatus == '' || $this->message->twilioMessageStatus == 'sending') {
            $this->message->twilioMessageStatus = 'failed';
            $this->message->saveOrFail();

            logger()->info(
                'Job CheckLeadMessageStatus: status of message id ' . $this->message->id . ' is changed to failed.'
            );

            event(new MessageUpdateStatus(
                $this->message->leadId,
                $this->message->twilioMessageSid,
                $this->message->twilioMessageStatus,
                $this->message->id
            ));
        }
    }

    /**
     * Fail job handler.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $message = 'Job CheckLeadMessageStatus is failed: ' . $exception->getMessage();
        logger()->error($message);
    }
}
