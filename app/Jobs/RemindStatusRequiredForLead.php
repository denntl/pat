<?php

namespace App\Jobs;

use App\Email;
use App\Facades\LogDb;
use App\Lead;
use App\LogsJobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class RemindStatusRequiredForLead
 * @package App\Jobs
 */
class RemindStatusRequiredForLead implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Lead instance.
     *
     * @var Lead
     */
    private $lead;

    /**
     * Create a new job instance.
     *
     * @param Lead $lead
     *
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        LogDb::setInstance(LogsJobs::class);
        LogDb::addParam('class', __CLASS__);

        // Check if there emails from this lead
        $emailFromLeadCount = Email::where('from', $this->lead->email)->count();
        if ($this->lead->tag == Lead::TAG_AWAITING_RESPONSE && !$emailFromLeadCount) {
            $this->lead->tag = Lead::TAG_ACTION_REQUIRED;
            $this->lead->save();
            LogDb::addParam('status', 'Set ' . Lead::TAG_ACTION_REQUIRED . ' tag for lead ' . $this->lead->id);
        } else {
            LogDb::addParam('status', 'Do nothing with lead ' . $this->lead->id);
        }

        LogDb::save();
    }

    public function failed(\Exception $exception)
    {
        LogDb::addParam('status', 'FAILED RemindStatusRequiredForLead JOB: ' . $exception->getMessage());
        LogDb::save();

        Log::error('FAILED RemindStatusRequiredForLead JOB: ' . $exception->getMessage());
    }
}
