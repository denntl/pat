<?php

namespace App\Jobs;

use App\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Email\EmailUpdateStatus;
use App\Services\SendGridService;
use Illuminate\Foundation\Bus\Dispatchable;

class SetFailedEmailStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Email
     */
    private $email;

    /**
     * Create a new job instance.
     *
     * @param Email $email
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->email->statusId == SendGridService::PENDING_EMAIL_STATUS_ID) {
            $this->email->statusId = SendGridService::INVALID_EMAIL_STATUS_ID;
            $this->email->saveOrFail();

            logger()->info(
                'Job SetFailedEmailStatus: status of email id ' . $this->email->id . ' is changed to failed.'
            );

            event(new EmailUpdateStatus($this->email->id, $this->email->lead->id, $this->email->statusId));
        }
    }

    /**
     * Fail job handler.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $message = 'Job SetFailedEmailStatus is failed: ' . $exception->getMessage();
        logger()->error($message);
    }
}
