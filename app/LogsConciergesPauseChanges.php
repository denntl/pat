<?php

namespace App;

use App\Models\AbstractModel as Model;
use App\Scopes\CreatedAtDescScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class LogsConciergesPauseChanges
 *
 * @property string $id
 * @property integer $conciergeId
 * @property integer $isPaused
 * @property Carbon $createdAt
 *
 * @package App
 */
class LogsConciergesPauseChanges extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logsConciergesPauseChanges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'conciergeId',
        'isPaused'
    ];

    /**
     * Get the concierge that owns the note.
     *
     * @return BelongsTo
     */
    public function concierge()
    {
        return $this->belongsTo('App\Concierge', 'conciergeId');
    }
}
