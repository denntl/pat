<?php

namespace App\Traits;

use App\Facades\LogDb;
use App\LogsWebsocket;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Exception\InvalidArgumentException;

/**
 * Trait WebSocket.
 *
 * @see Command
 * @package App\Traits
 */
trait WebSocket
{
    /**
     * @var Collection|null
     */
    private $socket;

    /**
     * WebSocket constructor.
     */
    public function __construct()
    {
        $this->signature .= ' {--S|socket=}';
        if (preg_match_all('(\{\w*[^?|^*]\})', $this->signature, $matches)) {
            throw new InvalidArgumentException(sprintf(
                'Missed optional parameter "?" or "*" at the end of %s signature in %s',
                $matches[0][0],
                static::class
            ));
        }

        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $socket = $this->option('socket');
        if ($socket === null) {
            $this->handleCommand();
            $this->logCommand();
            return;
        }

        $decoded = json_decode(base64_decode($socket), true);

        $this->socket = collect($decoded);
        $this->logCommand($socket);

        $this->handleSocketCommand();
        return;
    }

    /**
     * Handle command.
     * @return void
     */
    abstract public function handleCommand();

    /**
     * Handle socket command.
     * @return void
     */
    abstract public function handleSocketCommand();

    /**
     * Method for logging websocket data.
     *
     * @param string|null $socket
     *
     * @return void
     */
    private function logCommand(string $socket = null)
    {
        $signature = $this->arguments()['command'] ?? preg_replace('/(\s.*)/', '', $this->signature);
        $fillArray = [
            'command' => $signature,
            'class' => get_class($this)
        ];

        if (!$socket) {
            $argumentsArray = $this->arguments();

            if (isset($argumentsArray['command'])) {
                unset($argumentsArray['command']);
            }

            $fillArray['data'] = json_encode($argumentsArray);
        } else {
            $data = $this->socket->all();
            $fillArray['data'] = json_encode($data);
            $fillArray['socketData'] = $socket;
        }

        $logDb = LogDb::setInstance(LogsWebsocket::class);
        $logDb->setParams($fillArray);
        $logDb->save();
    }
}
