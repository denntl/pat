<?php

namespace App\Traits;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
use stdClass;

/**
 * Trait MakeHttpRequests
 * @package App\Traits
 */
trait MakeHttpRequests
{
    /**
     * GuzzleHttp client
     *
     * @var Client $client
     */
    protected $client;

    /**
     * Initialize GuzzleHttp client
     */
    protected function initHttpClient()
    {
        $this->client = new Client;
    }

    /**
     * Make request to agentology.
     *
     * @param string $method
     * @param string $url
     * @param array $json
     * @param string $token
     *
     * @return ResponseInterface|null
     */
    protected function makeRequest(string $method, string $url, array $json = [], string $token = '')
    {
        $res = null;
        $requestBody = [];
        $requestBody['headers']['content-Type'] = 'application/json';

        if ($token) {
            $requestBody['headers']['Authorization'] = 'Bearer ' . $token;
        }

        if (!empty($json)) {
            $requestBody['json'] = $json;
        }

        try {
            $res = $this->client->request($method, $url, $requestBody);
        } catch (ClientException $exception) {
            $res = $exception->getResponse();

            info('Failed request to ' . $url, [
                'method' => $method,
                'response_body' => $this->getFormattedResponseBody($res),
                'request_body' => $requestBody
            ]);
        }

        info('Response body for request ' . $url, [
            'method' => $method,
            'response' => $this->getFormattedResponseBody($res)
        ]);

        return $res;
    }

    /**
     * Format request data.
     *
     * @param ResponseInterface|null $res
     *
     * @return stdClass|null
     */
    protected function getFormattedResponseBody($res)
    {
        $result = null;

        if ($res) {
            $body = $res->getBody();
            $result = json_decode($body->getContents());
            // Solves problem that content is stream and clears after get content
            $body->rewind();
        }

        return $result;
    }
}
