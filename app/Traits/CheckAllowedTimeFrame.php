<?php

namespace App\Traits;

use Carbon\Carbon;

/**
 * CheckAllowedTimeFrame trait.
 * @package App\Traits
 */
trait CheckAllowedTimeFrame
{
    /**
     * Check date for FCC regulations.
     *
     * @param Carbon $date
     * @param string $timezone
     *
     * @return Carbon
     */
    public function checkAllowedTimeFrame(Carbon $date, $timezone)
    {
        $forbiddenFromDate = Carbon::today($timezone)->addHours(21);
        $allowedFromDate = Carbon::tomorrow($timezone)->addHours(8);

        if (Carbon::now($timezone)->between($forbiddenFromDate, $allowedFromDate)) {
            return $allowedFromDate;
        }

        return $date;
    }
}
