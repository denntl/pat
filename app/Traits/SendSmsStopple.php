<?php

namespace App\Traits;

use App\Facades\PhoneNumber;
use App\Lead;

/**
 * SendSmsStopple trait.
 * @package App\Traits
 */
trait SendSmsStopple
{
    /**
     * @inheritDoc
     */
    public function sendSms(string $to, string $from, string $body, bool $withCallback = true)
    {
        // Stopple to prevent sending sms in safe mode
        if (config('app.safe') && $this->stopple($to) || !$from) {
            return false;
        }

        return parent::sendSms($to, $from, $body, $withCallback);
    }

    /**
     * Stopple.
     *
     * @param string $phone
     *
     * @return bool
     */
    private function stopple(string $phone): bool
    {
        /** @var Lead $lead */
        $lead = Lead::where('phone', $phone)->first();
        if ($lead && $lead->phone !== PhoneNumber::getNationalPhone($lead->phoneSource)) {
            return false;
        }

        // Sms will not sending if `lead` with `phone` number exists and his `phone` the SAME as `phoneSource`.
        // Only `leads` with EQUALS `phone` & `phoneSource` are from Viking.
        // True means stopple is working
        return true;
    }
}
