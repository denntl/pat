<?php

namespace App\Traits;

use App\PortalPasswordResets;
use App\Agent;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

trait Validators
{
    /**
     * Adds custom validations to Validator
     *
     * @return void
     */
    private function initializeValidators()
    {
        Validator::extend('token_validation', function ($attribute, $currentToken, $parameters, $validator) {
            $user = PortalPasswordResets::where('email', $parameters[0])->first();
            if (empty($user)) {
                return false;
            }

            return Hash::check($currentToken, $user->token);
        });

        Validator::extend('same_as_old', function ($attribute, $newPassword, $parameters, $validator) {
            $agent = Agent::where('email', $parameters[0])->first();

            return !Hash::check($newPassword, $agent->password);
        });

        Validator::extend('uuid', function ($attribute, $value, $parameters, $validator) {
            return Uuid::isValid($value);
        });

// Adding price range rule because validator fails parse string after coma
        Validator::extend('price_range', function ($attribute, $value, $parameters, $validator) {
            $priceRange = [
                'Under $200,000',
                '$200,000 - $400,000',
                '$400,000 - $600,000',
                '$600,000 - $800,000',
                '$800,000 - $1,000,000',
                'Over $1,000,000'
            ];

            return in_array($value, $priceRange);
        });
    }
}
