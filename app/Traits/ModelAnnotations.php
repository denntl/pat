<?php

namespace App\Traits;

use \Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\QueriesRelationships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * ModelAnnotations trait.
 *
 * @method static static|Model create(array $attributes = [])
 * @method static bool chunk(int $count, callable $callback)
 * @method static static|Builder doesntHave(string $relation, string $boolean = 'and', Closure|null $callback = null)
 * @method static static|null|Model find(mixed $id, array $columns = ['*'])
 * @method static static|Collection findMany(array $ids, array $columns = ['*'])
 * @method static static|Model|Collection findOrFail(mixed $id, array $columns = ['*'])
 * @method static static|Model findOrNew(mixed $id, array $columns = ['*'])
 * @method static Builder has($relation, $operator = '>=', $count = 1, $boolean = 'and', Closure $callback = null)
 * @method static Builder onlyTrashed()
 * @method static static|Model updateOrCreate(array $attributes, array $values = [])
 * @method static Builder withGlobalScope(string $identifier, Scope|Closure $scope)
 * @method static Builder withoutGlobalScope(string|Scope $scope)
 * @method static Builder withoutGlobalScopes(array|null $scopes = null)
 * @method static Builder withTrashed()
 * @method static Builder where(string|Closure $column, string $operator = null, mixed $value = null, $boolean = 'and')
 * @method static static|Builder whereDoesntHave(string $relation, Closure|null $callback = null)
 * @method static static|Builder whereHas(string $relation, Closure $callback = null, $operator = '>=', $count = 1)
 * @method static Builder whereKey(mixed $id)
 *
 * @see Model
 * @see Builder
 * @see QueriesRelationships
 *
 * @package App\Traits
 */
trait ModelAnnotations
{
}
