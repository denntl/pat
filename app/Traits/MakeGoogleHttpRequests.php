<?php

namespace App\Traits;

use Google_Client;

/**
 * Trait MakeGoogleHttpRequests
 *
 * @package App\Traits
 *
 * @deprecated
 */
trait MakeGoogleHttpRequests
{
    private $googleClient;

    private function initGoogleHttpClient($config)
    {
        $this->googleClient = new Google_Client($config);
    }

    public function prepareAuthUrl($redirectUri, $scopes)
    {

        foreach ($scopes as $scope) {
            $this->googleClient->addScope($scope);
        }

        $this->googleClient->setRedirectUri($redirectUri);

        $authUrl = $this->googleClient->createAuthUrl();

        return $authUrl;
    }

    public function fetchGoogleTokenFromCode($code, $redirectUri)
    {
        $this->googleClient->setRedirectUri($redirectUri);
        $tokenData = $this->googleClient->fetchAccessTokenWithAuthCode($code);

        return $tokenData;
    }
}
