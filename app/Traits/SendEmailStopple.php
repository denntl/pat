<?php

namespace App\Traits;

use App\Email;
use App\Lead;

/**
 * SendEmailStopple trait
 * @package App\Traits
 */
trait SendEmailStopple
{
    /**
     * @inheritDoc
     */
    public function sendLeadEmail(Email $email)
    {
        // Stopple to prevent sending email in safe mode
        if (config('app.safe') && $this->stopple($email->to)) {
            return false;
        }

        return parent::sendLeadEmail($email);
    }

    /**
     * Stopple.
     *
     * @param string $email
     *
     * @return bool
     */
    private function stopple(string $email): bool
    {
        /** @var Lead $lead */
        $lead = Lead::where('email', $email)->first();
        if ($lead && $email !== $lead->emailSource) {
            return false;
        }

        // Email will not sending if `lead` with `email` exists and his `email` the SAME as `emailSource`.
        // Only `leads` with EQUALS `email` & `emailSource` are from Viking.
        // True means stopple is working
        return true;
    }
}
