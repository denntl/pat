<?php

namespace App\Traits;

use Ramsey\Uuid\Uuid as UuidGenerator;

/**
 * Trait Uuids.
 *
 * @package App\Traits
 */
trait Uuid
{
    /**
     * Boot function from laravel.
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            /** @var \Illuminate\Database\Eloquent\Model $model */
            if ($model->{$model->getKeyName()} === null) {
                $model->{$model->getKeyName()} = UuidGenerator::uuid1()->toString();
            }
        });
    }
}
