<?php

namespace App;

use App\Models\AbstractModel as Model;
use Carbon\Carbon;

/**
 * Class LogsLeadsVikingSync
 *
 * @property integer $id
 * @property string $requestUrl
 * @property string $responseData
 * @property integer $responseStatus
 * @property string $commandStatus
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 *
 * @package App
 */
class LogsLeadsVikingSync extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logsLeadsVikingSync';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requestUrl',
        'responseData',
        'responseStatus',
        'commandStatus'
    ];
}
