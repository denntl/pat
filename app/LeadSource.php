<?php

namespace App;

use App\Models\AbstractModel as Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class LeadSource
 *
 * @property integer $id
 * @property string $name
 *
 * @package App
 */
class LeadSource extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leadSources';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get the leads for the source.
     *
     * @return HasMany
     */
    public function leads()
    {
        return $this->hasMany('App\Lead');
    }
}
