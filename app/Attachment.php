<?php

namespace App;

use App\Models\AbstractUuidModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Attachment
 *
 * @property string $id
 * @property string $leadId
 * @property string $location
 * @property string $content
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 *
 * @property Lead $lead
 *
 * @package App
 */
class Attachment extends AbstractUuidModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leadVikingEmails';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'leadId',
        'location',
        'content',
        'createdAt',
        'updatedAt'
    ];

    /**
     * Get the lead that related to this email.
     *
     * @return BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo('App\Lead', 'leadId');
    }
}
