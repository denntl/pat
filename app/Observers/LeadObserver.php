<?php

namespace App\Observers;

use App\Lead;
use App\Facades\LogDb;
use App\Facades\PhoneNumber;
use App\Facades\Twilio;
use App\Jobs\Lead\Agentology\ReferLead;
use App\LogsLeadChanges;
use App\Services\Geo\Facades\GeoHelper;
use Illuminate\Support\Facades\Auth;

/**
 * Class LeadObserver
 *
 * @package App\Observers
 */
class LeadObserver
{
    /**
     * Listen to the Lead creating event.
     *
     * @param Lead $lead
     *
     * @return void
     */
    public function creating(Lead $lead)
    {
        $lead->emailSource = $lead->email;
        $lead->phoneSource = $lead->phone;

        if (PhoneNumber::checkIfPhoneValid($lead->phone)) {
            $lead->phoneCarrierType = Twilio::getPhoneCarrierType($lead->phone);
        }

        //$this->saveLog($lead, 'saving');
    }

    /**
     * Listen to the Lead created event.
     *
     * @param Lead $lead
     *
     * @return void
     */
    public function created(Lead $lead)
    {
        if (!app()->environment('testing')) {
            $lead->timezone = GeoHelper::getLeadTimezone($lead);
            $lead->save();
        }
    }

    /**
     * Listen to the Lead updating event
     *
     * @param Lead $lead
     *
     * @return void
     */
    public function updating(Lead $lead)
    {
        $this->saveLog($lead, 'updating');

        if ($lead->isDirty('phone') && $lead->phone && PhoneNumber::checkIfPhoneValid($lead->phone)) {
            $lead->phoneCarrierType = Twilio::getPhoneCarrierType($lead->phone);
        }
    }

    /**
     * Listen to the Lead deleting event
     *
     * @param Lead $lead
     *
     * @return void
     */
    public function deleting(Lead $lead)
    {
        $this->saveLog($lead, 'deleting');
    }

    /**
     * Method to save lead changes to db as logs.
     *
     * @param Lead $lead
     * @param string $action
     *
     * @return void
     */
    private function saveLog(Lead $lead, string $action)
    {
        LogDb::setInstance(LogsLeadChanges::class);
        LogDb::addParam('leadId', $lead->id);

        $this->checkOnUser();

        $newLeadData = $lead->toArray();

        unset($newLeadData['lastActivity']);
        unset($newLeadData['agent']);
        unset($newLeadData['source']);

        if ($action != 'deleting') {
            if ($action == 'updating') {
                $currentLeadData = Lead::find($lead->id)->toArray();

                unset($currentLeadData['lastActivity']);
                unset($currentLeadData['agent']);
                unset($currentLeadData['source']);

                $cleanNewData = array_diff($newLeadData, $currentLeadData);
                $cleanOldData = array_diff($currentLeadData, $newLeadData);

                LogDb::addParam('newData', json_encode($cleanNewData));
                LogDb::addParam('oldData', json_encode($cleanOldData));
            } else {
                LogDb::addParam('newData', json_encode($newLeadData));
            }
        } else {
            LogDb::addParam('oldData', json_encode($newLeadData));
        }

        LogDb::addParam('action', $action);
        LogDb::save();
    }

    /**
     * Check and add log data for user.
     *
     * @return void
     */
    private function checkOnUser()
    {
        $user = Auth::guard('web')->user();

        if (!$user) {
            $user = Auth::guard('portal')->user();
            $message = $user
                ? 'User ' . $user->firstName . ' with id ' . $user->id
                : 'through web api or something else';

            PHP_SAPI == 'cli'
                ? LogDb::addParam('user', 'from console command')
                : LogDb::addParam('user', $message);
        } else {
            LogDb::addParam('user', 'User ' . $user->firstName . ' with id ' . $user->id);
        }
    }
}
