<?php

namespace App\Observers;

use App\Email;
use App\Jobs\Email\MakeFakeUpdateStatusRequest;

/**
 * Class EmailObserver
 *
 * @package App\Observers
 */
class EmailObserver
{
    /**
     * Listen to the Email created event.
     *
     * @param Email $email
     *
     * @return void
     */
    public function created(Email $email)
    {
        if (app()->environment(['dev', 'demo', 'local*']) && !$email->incoming) {
            $job = new MakeFakeUpdateStatusRequest($email);
            dispatch($job->delay(1));
        }
    }
}
