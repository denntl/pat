<?php

namespace App\Observers;

use App\Agent;
use App\Jobs\Agent\Agentology\PostAgentToAgentology;
use App\Jobs\Agent\Viking\PostAgentToViking;
use App\Jobs\Agent\Viking\UpdateAgentAtViking;
use Illuminate\Support\Facades\App;
use Ramsey\Uuid\Uuid;

/**
 * Class AgentObserver
 *
 * @package App\Observers
 */
class AgentObserver
{
    /**
     * Listen to the Agent creating event. Creating uuid for vikingPatId.
     *
     * @param  Agent $agent
     *
     * @return void
     */
    public function creating(Agent $agent)
    {
        $agent->vikingPatId = Uuid::uuid1()->toString();
    }

    /**
     * Listen to the Agent created event. Sending data to Viking and Agentology.
     *
     * @param  Agent $agent
     *
     * @return void
     */
    public function created(Agent $agent)
    {
        if (!(App::environment('testing'))) {
            dispatch(new PostAgentToViking($agent));
            dispatch(new PostAgentToAgentology($agent));
        }
    }

    /**
     * Listen to the Agent updating event.
     *
     * @param Agent $agent
     *
     * @return void
     */
    public function updating(Agent $agent)
    {
        $this->updateRememberedToken($agent);
        $this->updateVikingAgent($agent);
    }

    /**
     * Update agent on viking if related fields updated.
     *
     * @param Agent $agent
     */
    private function updateVikingAgent(Agent $agent)
    {
        $fieldsOnViking = [
            'vikingGmailParse',
            'vikingPatId',
            'email',
            'emailForwarding',
            'salesForceId'
        ];

        if ($agent->isDirty($fieldsOnViking) && !(App::environment('testing'))) {
            dispatch(new UpdateAgentAtViking($agent));
        }
    }

    /**
     * Remove remembered token if email changed
     *
     * @param Agent $agent
     */
    private function updateRememberedToken(Agent $agent)
    {
        if ($agent->isDirty('email')) {
            $rememberTokenName = $agent->getRememberTokenName();
            $agent->{$rememberTokenName} = null;
            $agent->providerId = null;
        }
    }
}
