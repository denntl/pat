<?php

namespace App\Observers;

use App\Concierge;

/**
 * Class ConciergeObserver
 *
 * @package App\Observers
 */
class ConciergeObserver
{
    /**
     * Listen to the Concierge updating event. If new email and old email not equals
     * Remove remember token and provider id.
     *
     * @param Concierge $concierge
     *
     * @return void
     */
    public function updating(Concierge $concierge)
    {
        $currentConcierge = Concierge::find($concierge->id);
        if ($concierge->email != $currentConcierge->email) {
            $rememberTokenName = $concierge->getRememberTokenName();
            $concierge->{$rememberTokenName} = null;
            $concierge->providerId = null;
        }
    }
}
