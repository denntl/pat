<?php

namespace App;

use App\Models\AbstractAuthenticatable as Authenticatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

/**
 * Class Concierge
 *
 * @property integer $id
 * @property integer $role
 * @property string $name
 * @property string $email
 * @property string $provider
 * @property string $providerId
 * @property string $conciergeRole
 * @property string $companyName
 * @property string $teamName
 * @property string $teamLeader
 * @property Carbon $createdAt
 * @property Carbon $updatedAt
 * @property boolean $isDeleted
 * @property boolean $isPaused
 *
 * @method static Builder activeForAssign(integer $role = null)
 *
 * @package App
 */
class Concierge extends Authenticatable
{
    use HasApiTokens;

    const HASH = 'Pb3gOizm2Z';

    const ROLE_SUSPENDED = 0;
    const ROLE_TIER_I = 1;
    const ROLE_TIER_II = 2;
    const ROLE_ADMIN = 3;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The column name of the "remember me" token.
     *
     * @var string
     */
    protected $rememberTokenName = 'rememberToken';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'provider',
        'providerId',
        'role',
        'isDeleted',
        'isPaused'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'rememberToken'
    ];

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return md5($this->id . self::HASH);
    }

    /**
     * Check token.
     *
     * @param string $token
     *
     * @return bool
     */
    public function checkToken($token)
    {
        return $this->getToken() == $token;
    }

    /**
     * Check static token.
     *
     * @param mixed $id
     * @param string $token
     *
     * @return bool
     */
    public static function checkStaticToken($id, $token) : bool
    {
        return md5($id . self::HASH) == $token;
    }

    /**
     * Check concierge admin role.
     *
     * @param mixed $id
     *
     * @return bool
     */
    public static function checkAdminRole($id) : bool
    {
        $concierge = Concierge::find($id);

        return ($concierge->role == self::ROLE_ADMIN);
    }

    /**
     * Get the notes for the concierge.
     *
     * @return HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Note', 'conciergeId');
    }

    /**
     * Get the leads for the concierge.
     *
     * @return HasMany
     */
    public function leads()
    {
        return $this->hasMany('App\Lead', 'conciergeId');
    }

    /**
     * Get active concierges with the least amount of assigned leads.
     *
     * @example App\Concierge::activeForAssign(2)->get()->first();
     *
     * @param Builder $query
     * @param integer $role
     *
     * @return QueryBuilder
     */
    public function scopeActiveForAssign($query, int $role = null)
    {
        $query->leftJoin('leads', 'leads.conciergeId', '=', 'concierges.id')
            ->select('concierges.*');

        if ($role !== null) {
            $query->where('concierges.role', $role);
        }

        return $query->groupBy('concierges.id')
            ->orderBy(DB::raw('count(leads.id)'), 'ASC');
    }

    /**
     * Create a new concierge or recover an old one if he was safe deleted
     *
     * @param string $name
     * @param string $email
     * @param integer $role
     *
     * @return boolean|Concierge
     */
    public static function createOrRecoverConcierge($name, $email, $role)
    {
        $concierge = Concierge::firstOrNew(['email' => $email]);
        $concierge->name = $name;
        $concierge->email = $email;
        $concierge->role = $role;
        $concierge->isDeleted = false;
        $savedStatus = $concierge->save();

        if ($savedStatus) {
            return $concierge;
        } else {
            return false;
        }
    }

    /**
     * Check tier 2 role.
     *
     * @param mixed $id
     *
     * @return bool
     */
    public static function checkTierIIRole($id) : bool
    {
        if ($id === null) {
            return false;
        }

        $concierge = Concierge::find($id);

        return ($concierge->role == self::ROLE_TIER_II);
    }
}
