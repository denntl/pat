<?php

namespace App;

use App\Models\AbstractModel as Model;

/**
 * Class LogsLeadChanges
 *
 * @property integer $id
 * @property string $newData
 * @property string $oldData
 * @property string $user
 * @property string $action
 * @property string $leadId
 *
 * @package App
 */
class LogsLeadChanges extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logsLeadChanges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'newData',
        'oldData',
        'user',
        'action',
        'leadId',
    ];
}
