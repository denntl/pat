<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET')
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1'
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET')
    ],

    'stripe' => [
        'model' => App\Concierge::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET')
    ],

    'google' => [
        'client_id' => env('GOOGLE_ID'),
        'client_secret' => env('GOOGLE_SECRET'),
        'client_viking_id' => env('GOOGLE_VIKING_ID', ''),
        'client_viking_secret' => env('GOOGLE_VIKING_SECRET', ''),
        'redirect' => env('GOOGLE_URL'),
        'maps_key' => env('GOOGLE_STATIC_MAPS_KEY')
    ],

    'viking' => [
        'domain' => env('VIKING_DOMAIN'),
        'auth_bearer' => env('VIKING_POSTMAN_AUTH_BEARER'),
        'token' => env('VIKING_POSTMAN_TOKEN')
    ],

    'twilio' => [
        'app_sid' => env('TWILIO_APP_SID'),
        'account_sid' => env('TWILIO_ACCOUNT_SID'),
        'auth_token' => env('TWILIO_AUTH_TOKEN'),
        'test_account_sid' => env('TWILIO_TEST_ACCOUNT_SID'),
        'test_auth_token' => env('TWILIO_TEST_AUTH_TOKEN')
    ],

    'sendgrid' => [
        'id' => env('SEND_GRID_ID'),
        'key' => env('SEND_GRID_KEY'),
        'webhook_secret' => env('SEND_GRID_WEB_HOOK_SECRET'),
        'webhook_inbound_url' => env('SEND_GRID_INBOUND_WEB_HOOK_URL'),
        'email_domain' => env('SEND_GRID_EMAIL_DOMAIN')
    ],

    'rollbar' => [
        'access_token' => env('ROLLBAR_TOKEN'),
        'level' => env('ROLLBAR_LEVEL'),
        'front_token' => env('ROLL_BAR_FRONT_TOKEN')
    ],

    'intercom' => [
        'app_id' => env('INTERCOM_APP_ID'),
        'access_token' => env('INTERCOM_ACCESS_TOKEN'),
        'test_app_id' => env('INTERCOM_TEST_APP_ID'),
        'test_access_token' => env('INTERCOM_TEST_ACCESS_TOKEN')
    ],

    'pdf' => [
        'html_path' => env('PDF_TO_HTML_PATH', '/usr/bin/pdftohtml'),
        'info_path' => env('PDF_INFO_PATH', '/usr/bin/pdfinfo')
    ],

    'agentology' => [
        'url' => env('AGENTOLOGY_API_URL'),
        'key' => env('AGENTOLOGY_API_KEY'),
        'secret' => env('AGENTOLOGY_SECRET_KEY')
    ],

    'dblogs' => [
        'save' => env('LOG_DB', true)
    ]
];
