<?php
return [
    'leads' => [
        'fields' => [
            'id',
            'firstname',
            'lastname',
            'phone',
            'email',
            'leadtype',
            'bedrooms',
            'bathrooms',
            'timeframe',
            'city',
            'state',
            'channelwebsite',
            'leadcomment',
            'postalcode',
            'street',
            'gmail_id',
            'gmail_thread',
            'mortgage',
            'user_id',
            'pat_id',
            'duplicate_lead_id',
            'duplicate',
            'phone_carrier_type',
            'price',
            'attachments'
        ]
    ]
];