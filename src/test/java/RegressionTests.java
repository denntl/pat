import MainSettings.Settings;
import Pages.*;
import org.testng.annotations.Test;

import static Pages.ActiveChats.switchToChats;
import static Pages.ActiveChats.updateLeadStatus;
import static Pages.HomePage.signInGoogleConsole;

public class RegressionTests extends Settings
{
    @Test(groups = "regression tests", description = "Verification of ability to restore pass")
    public void restore_pass_test() {HomePage.restorePass();}

    @Test(groups = "regression tests", description = "Verification of ability to sign in")
    public void sign_in_test() {HomePage.signIn();}

    @Test(groups = "regression tests", description = "Verification of ability to sign in using google account")
    public void sign_in_using_google_account() {HomePage.signInUsingGoogle();}

    @Test(groups = "regression tests", description = "Verification of ability to sign in to console using google account")
    public void sign_in_into_console_using_google_account() {HomePage.signInGoogleConsole();}

    @Test(groups = "regression tests", description = "Verification of ability to create new user", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void create_new_user_test() {HomePage.createNewUser();}

    @Test(groups = "regression tests", description = "Verification of filters on Active Lead tab", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void active_lead_filters_test()
    {   signInGoogleConsole();
        driver.get(baseURL+"console/leads/active");
        Dashboard.lead_filters();
    }

    @Test(groups = "regression tests", description = "Verification of filters on Archive Lead tab", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void archive_lead_filters_test()
    {   signInGoogleConsole();
        driver.get(baseURL+"console/leads/archive");
        Dashboard.lead_filters();
    }

    @Test(groups = "regression tests", description = "Verification of filters by REP on Active leads tab", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void active_lead_filters_by_rep()
    {
        signInGoogleConsole();
        driver.get(baseURL+"console/leads/active");
        Dashboard.filter_by_REP();
    }

    @Test(groups = "regression tests", description = "Verification of filters by REP on Archive Lead tab", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void archive_lead_filters_by_rep()
    {
        signInGoogleConsole();
        driver.get(baseURL+"console/leads/archive");
        Dashboard.filter_by_REP();
    }

    @Test(groups = "regression tests", description = "Comparing of existing home page screen with expected")
    public void compare_home_page_screen() {HomePage.compareHomePageScreen();}

    @Test(groups = "regression tests", description = "Verification of CRUD actions for Suspended user ", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void  create_edit_remove_rep_account_suspended() {Dashboard.createRepAccountSuspended();}

    @Test(groups = "regression tests", description = "Verification of CRUD actions for Admin user", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void  create_edit_remove_rep_account_admin() {Dashboard.createRepAccountAdmin();}

    @Test(groups = "regression tests", description = "Verification of CRUD actions for Tier1 user", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void  create_edit_remove_rep_account_tier1() {Dashboard.createRepAccountTier1();}

    @Test(groups = "regression tests", description = "Verification of CRUD actions for Tier2 user", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void  create_edit_remove_rep_account_tier2() {Dashboard.createRepAccountTier2();}

    @Test(groups = "regression tests", description = "Verification of ability to assign/reassign leads ", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void  assign_and_reassign_lead()
    {Dashboard.assign_and_reassign_lead();}

    @Test(groups = "regression tests", description = "Verification of ability sign out from console", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void  sign_out_test()
    { HomePage.signInGoogleConsole();
    HomePage.signOut();}

    @Test(groups = "regression tests", description = "Verification of ability to add notes for lead ", dependsOnMethods = "assign_and_reassign_lead")
    public void  add_notes_for_lead_test()
    {ActiveChats.addNotesForLead();}

    @Test(groups = "regression tests", description = "Verification of ability to add admin notes for lead ", dependsOnMethods = "assign_and_reassign_lead")
    public void  add_admin_notes_for_lead_test()
    {ActiveChats.addAdminNotes();}

    @Test(groups = "regression tests", description = "Verification of ability to search users", dependsOnMethods = "sign_in_into_console_using_google_account")
    public void  user_search_test()
    {
        signInGoogleConsole();
        Dashboard.userSearch("Konstantin Ovramets");
        Dashboard.userSearch("Yuriy Golikov");
        Dashboard.userSearch("David Tal");
    }

    @Test(groups = "regression tests", description = "Verification of ability to edit basic lead info",dependsOnMethods = "assign_and_reassign_lead")
    public void  edit_basic_lead_info_test()
    {
        ActiveChats.editBasicLeadInfo();
    }

    @Test(groups = "regression tests", description = "Verification of ability to change lead status into qualified", dependsOnMethods = "assign_and_reassign_lead")
    public void  update_lead_status_to_qualified_test()
    {
        signInGoogleConsole();
        switchToChats();
        updateLeadStatus("update_lead_status_to_qualified_test","2");
    }

    @Test(groups = "regression tests", description = "Verification of ability to change lead status into unqualified", dependsOnMethods = "assign_and_reassign_lead")
    public void  update_lead_status_to_unqualified_test()
    {
        signInGoogleConsole();
        switchToChats();
        updateLeadStatus("update_lead_status_to_unqualified_test","3");
    }

    @Test(groups = "regression tests", description = "Verification of ability to change lead status into 'Not a lead' status", dependsOnMethods = "assign_and_reassign_lead")
    public void  update_lead_status_to_not_lead()
    {
        signInGoogleConsole();
        switchToChats();
        updateLeadStatus("update_lead_status_to_not_a_lead","4");
    }

    @Test(groups = "regression tests", description = "Verification of ability to change lead status into 'Referred' status", dependsOnMethods = "assign_and_reassign_lead")
    public void  update_lead_status_to_referred()
    {
        signInGoogleConsole();
        switchToChats();
        updateLeadStatus("update_lead_status_to_referred","5");
    }

    @Test(groups = "regression tests", description = "Verification of ability to change lead status into 'No Response Needed' status", dependsOnMethods = "assign_and_reassign_lead")
    public void  update_lead_status_to_mo_response_needed()
    {
        signInGoogleConsole();
        switchToChats();
        updateLeadStatus("update_lead_status_to_no_response_needed","6");
    }

    @Test(groups = "regression tests", description = "Verification of ability to send notes to agent", dependsOnMethods = "assign_and_reassign_lead")
    public void  send_note_to_agent_test()
    {ActiveChats.sendNotesToAgent();}

    @Test(groups = "regression tests", description = "Verification of ability to call to lead and verification of ability to cancel call", dependsOnMethods = "assign_and_reassign_lead")
    public void  call_to_lead_test()
    {
        ActiveChats.callToLead();
    }

    @Test(groups = "regression tests", description = "Verification of ability to send email to lead ", dependsOnMethods = "assign_and_reassign_lead")
    public void send_email_to_lead_test()
    {ActiveChats.sendEmailToLead();}

    @Test(groups = "regression tests", description = "Verification of ability to send sms to lead", dependsOnMethods = "assign_and_reassign_lead")
    public void  send_sms_to_lead_test()
    {ActiveChats.sendSmsToLead();}

    @Test(groups = "regression tests", description = "Verification of ability to view and hide emails", dependsOnMethods = "assign_and_reassign_lead")
    public void  view_hide_email_test()
    {ActiveChats.viewHideEmail();}
}
