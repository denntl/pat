package MainSettings;

import TrackReporting.CaptureScreenShotOnFailureListener;
import TrackReporting.LoggingEventListener;
import com.google.common.collect.ImmutableMap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;

@Listeners(CaptureScreenShotOnFailureListener.class)
public class Settings
{
        private static final WebDriverEventListener eventListener = new LoggingEventListener();
        protected static String testUserEmail="golikovplus3@gmail.com";
        protected static String testUserPass="N0th!ngN3ar";
        protected static WebDriver driver;
        protected static String baseURL = "https://pat.bl3ndlabs.com/";
        private static ChromeDriverService service;
        @BeforeMethod
        public static void createAndStartService()
        {
        service = new ChromeDriverService.Builder().usingDriverExecutable(new File("/opt/chromedriver-2.25")).usingAnyFreePort()
                //service = new ChromeDriverService.Builder().usingDriverExecutable(new File("/usr/local/share/chromedriver")).usingAnyFreePort()
                .withSilent(true)
                .withVerbose(false)
                .withEnvironment(ImmutableMap.of("DISPLAY",":10"))
                .build();
        try {
            service.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
        @BeforeMethod
        public void setUp() throws IOException
        {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--fast");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new EventFiringWebDriver(new RemoteWebDriver(service.getUrl(), capabilities)).register(eventListener);
        driver.get(baseURL);
        driver.manage().window().setSize(new org.openqa.selenium.Dimension(1920, 1080));
        }

        @AfterMethod
        public void tearDown()
        {
        driver.quit();
        service.stop();
        }

        public static WebDriver getDriver()
    {
        return driver;
    }

        public static void waitInSeconds (int seconds)
        {
        try {
            Thread.sleep(1000*seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        }

        protected static void waitUntilElementVisible(WebElement element)
        {
        WebDriverWait waiting = new WebDriverWait(driver, 10);
        waiting.until(ExpectedConditions.elementToBeClickable(element));
        }
}
