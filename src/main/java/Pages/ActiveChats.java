package Pages;

import Elements.Button;
import Elements.Element;
import Elements.TextField;
import Enums.Variables;
import MainSettings.Settings;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.time.LocalDateTime;

import static Pages.HomePage.signInGoogleConsole;

public class ActiveChats extends Settings
{
    public static ActiveChats addNotesForLead()
    {
        signInGoogleConsole();
        switchToNotes();
        TextField notesForAgent = new TextField(By.xpath(Variables.NOTES_FOR_AGENT.toString()));
        String testText = "Simple test of lead notes please ignore this record it was created just for test";
        notesForAgent.enterText(testText);
        WebElement saveLeadNotes = driver.findElement(By.xpath(Variables.SAVE_NOTES.toString()));
        Actions actions = new Actions(driver);
        actions.moveToElement(saveLeadNotes).click().build().perform();
        Element successNotes = new Element(By.xpath("//i[contains(text(),'"+testText+"')]"));
        if (successNotes.isPresent())
        {System.out.print("Test passed");}
        else {System.out.print("Test failed, notes not added");}
        return new ActiveChats();
    }

    public static ActiveChats addAdminNotes()
    {
        signInGoogleConsole();
        switchToNotes();
        TextField notesAdmin = new TextField(By.xpath(Variables.ADMIN_NOTES.toString()));
        String testText = "Simple test of admin notes please ignore this record it was created just for test";
        notesAdmin.enterText(testText);
        WebElement saveNotes = driver.findElement(By.xpath(Variables.SAVE_ADMIN_NOTES_BUTTON.toString()));
        Actions actions = new Actions(driver);
        actions.moveToElement(saveNotes).click();
        return new ActiveChats();
    }

    private static void switchToNotes()
    {
        switchToChats();
        Button notes = new Button(By.xpath(Variables.NOTES_TAB.toString()));
        notes.waitForElementIsPresent();
        notes.click();
    }

    public static void switchToChats()
    {
        Button switchToChats = new Button(By.xpath(Variables.SWITCH_TO_CHATS.toString()));
        switchToChats.waitForElementIsPresent();
        switchToChats.click();
    }

    public static ActiveChats updateLeadStatus(String notes, String statusId)
    {
        WebElement updateStatus = driver.findElement(By.xpath(Variables.UPDATE_STATUS.toString()));
        Actions actions = new Actions(driver);
        actions.moveToElement(updateStatus).click().build().perform();
        TextField notesArea = new TextField(By.xpath(Variables.UPDATE_NOTES_TEXT_FIELD.toString()));
        notesArea.enterText(notes);
        Button chooseStatus = new Button(By.xpath(Variables.CHOOSE_LEAD_STATUS_BUTTON.toString()));
        chooseStatus.waitForElementIsPresent();
        chooseStatus.click();
        Button status = new Button(By.xpath(".//*[@id='updateAdmin']/li["+statusId+"]/span"));
        status.waitForElementIsPresent();
        status.click();
        Button confirmUpdateStatus = new Button(By.xpath(Variables.CONFIRM_UPDATE_STATUS.toString()));
        confirmUpdateStatus.waitForElementIsPresent();
        confirmUpdateStatus.click();
        return new ActiveChats();
    }
    public static ActiveChats editBasicLeadInfo()
    {
        signInGoogleConsole();
        switchToChats();
        TextField leadEmail = new TextField(By.xpath(Variables.LEAD_EMAIL_INPUT.toString()));
        TextField leadFirstName = new TextField(By.xpath(Variables.LEAD_FIRST_NAME_INPUT.toString()));
        TextField leadLastName = new TextField(By.xpath(Variables.LEAD_LAST_NAME_INPUT.toString()));
        TextField leadPhone = new TextField(By.xpath(Variables.LEAD_PHONE_INPUT.toString()));
        leadEmail.waitForElementIsPresent();
        leadEmail.enterText("testuser@pat.com");
        leadFirstName.waitForElementIsPresent();
        leadFirstName.enterText("TestFirstNameField");
        leadLastName.waitForElementIsPresent();
        leadLastName.enterText("TestLastNameField");
        leadPhone.enterText(RandomStringUtils.randomNumeric(13));
        updateLeadStatus("test","1");
        return new ActiveChats();
    }

    public static ActiveChats sendNotesToAgent()
    {
        signInGoogleConsole();
        switchToNotes();
        WebElement notesForAgent = driver.findElement(By.xpath(Variables.NOTES_FOR_AGENT.toString()));
        String testText = "test message "+ LocalDateTime.now();
        notesForAgent.click();
        notesForAgent.sendKeys(testText);
        Button sendToAgent = new Button(By.xpath(".//*[@id='lead-notes']/div/div[1]/button[2]"));
        sendToAgent.waitForElementIsPresent();
        sendToAgent.click();
        Button confirmSend = new Button(By.xpath(".//*[@id='app']/div/div[5]/div[1]/div[2]/button[1]"));
        confirmSend.waitForElementIsPresent();
        confirmSend.click();
        Element success = new Element(By.xpath("//i[contains(text(),'"+testText+"')]"));
        if(success.isPresent())
        {System.out.print("\n Test passed");}
        else {System.out.print("\n Test failed, search results incorrect");}
        return new ActiveChats();
    }

    public static ActiveChats callToLead()
    {
        signInGoogleConsole();
        switchToChats();
        Button call = new Button(By.xpath(Variables.CALL_BUTTON.toString()));
        call.waitForElementIsPresent();
        call.click();
        Button cancelCall = new Button(By.xpath(Variables.CANCEL_CALL_BUTTON.toString()));
        Button confirmCall = new Button(By.xpath(Variables.CONFIRM_CALL_BUTTON.toString()));
        cancelCall.waitForElementIsPresent();
        cancelCall.click();
        call.waitForElementIsPresent();
        call.click();
        confirmCall.waitForElementIsPresent();
        confirmCall.click();
        return new ActiveChats();
    }

    public static ActiveChats sendSmsToLead()
    {
        signInGoogleConsole();
        switchToChats();
        sendTemplateMessage();
        return new ActiveChats();
    }

    private static void sendTemplateMessage()
    {
        Button openTemplates = new Button(By.xpath(Variables.OPEN_TEMPLATES_LIST_BUTTON.toString()));
        openTemplates.waitForElementIsPresent();
        openTemplates.click();
        WebElement chooseTemplate = driver.findElement(By.xpath(Variables.CHOOSE_TEMPLATE.toString()));
        Actions actions = new Actions(driver);
        actions.moveToElement(chooseTemplate).click().build().perform();
        Element send = new Element(By.xpath(Variables.SEND_MESSAGE.toString()));
        if (send.isPresent())
        {
            System.out.print("Test passed");
        }
        else
        {
            Assert.fail("Test failed");
        }
    }
    private static void switchToEmails()
    {
        Button switchEmail = new Button(By.xpath(Variables.SWITCH_TO_EMAILS_BUTTON.toString()));
        switchEmail.waitForElementIsPresent();
        switchEmail.click();
    }
    public static ActiveChats sendEmailToLead()
    {
        signInGoogleConsole();
        switchToChats();
        switchToEmails();
        sendTemplateMessage();
        return new ActiveChats();
    }

    public static ActiveChats viewHideEmail()
    {
        signInGoogleConsole();
        switchToChats();
        Button viewEmail = new Button(By.xpath(Variables.VIEW_EMAIL_BUTTON.toString()));
        viewEmail.waitForElementIsPresent();
        viewEmail.click();
        Button hideEmail = new Button(By.xpath(Variables.HIDE_EMAIL_BUTTON.toString()));
        hideEmail.waitForElementIsPresent();
        hideEmail.click();
        return new ActiveChats();
    }

    public static ActiveChats editExtendedLeadInfo()
    {
        signInGoogleConsole();
        switchToChats();
        return new ActiveChats();
    }
}
