package Pages;

import Elements.Button;
import Elements.Element;
import Elements.TextField;
import Enums.Variables;
import MainSettings.Settings;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HomePage extends Settings
{
    public static void signIn()
    {
       TextField email = new TextField(By.xpath(Variables.EMAIL_INPUT.toString()));
       TextField pass = new TextField(By.xpath(Variables.PASS_INPUT.toString()));
       Button signIn = new Button(By.xpath(Variables.SIGN_IN_BUTTON.toString()));
       email.enterText(testUserEmail);
       pass.enterText(testUserPass);
       signIn.click();
    }

    public static void signOut()
    {
        Button active = new Button(By.xpath(Variables.ACTIVE_MENU.toString()));
        active.click();
        Button signOut = new Button(By.xpath(".//*[@id='drop']/li[3]/a"));
        signOut.waitForElementIsPresent();
        signOut.click();
        verifyLinkStatus("console");
    }
    public static void signInUsingGoogle()
    {
        waitUntilElementVisible(driver.findElement(By.xpath(Variables.SIGN_IN_BUTTON_GOOGLE.toString())));
        WebElement signInWithGoogle = driver.findElement(By.xpath(Variables.SIGN_IN_BUTTON_GOOGLE.toString()));
        signInWithGoogle.click();
        signInGoogle();
        waitInSeconds(5);
        verifyLinkStatus("portal/setup#");
    }

    public static void createNewUser()
    {
        waitUntilElementVisible(driver.findElement(By.xpath(Variables.CREATE_ACCOUNT_LINK.toString())));
        String userEmail = RandomStringUtils.randomAlphabetic(10)+"@gmail.com";
        String userPass = RandomStringUtils.randomAlphabetic(10);
        WebElement createAccount = driver.findElement(By.xpath(Variables.CREATE_ACCOUNT_LINK.toString()));
        createAccount.click();
        TextField email = new TextField(By.xpath(Variables.EMAIL_INPUT.toString()));
        Button createAcc = new Button(By.xpath(Variables.CREATE_ACCOUNT_BUTTON.toString()));
        createAcc.click();
        verifyErrorMessage("The email field is required.");
        email.enterText(userEmail);
        TextField pass = new TextField(By.xpath(Variables.PASS_INPUT.toString()));
        verifyErrorMessage("Password should contain at least 6 characters");
        pass.enterText(userPass);
        TextField firstName = new TextField(By.xpath(Variables.FIRST_NAME_INPUT.toString()));
        TextField lastName = new TextField(By.xpath(Variables.LAST_NAME_INPUT.toString()));
        verifyErrorMessage("We need your first name");
        firstName.enterText("firstName "+ RandomStringUtils.randomAlphabetic(5));
        verifyErrorMessage("We need your last name");
        lastName.enterText("lastName "+ RandomStringUtils.randomAlphabetic(5));
        TextField phone = new TextField(By.xpath(Variables.PHONE_INPUT.toString()));
        verifyErrorMessage("The phone field is required.");
        phone.enterText("12312312321");
        createAcc.click();
        waitInSeconds(4);
        verifyLinkStatus("portal/setup");
    }

    private static void verifyErrorMessage(String errorText)
    {
        try
        {
            waitInSeconds(2);
            Element errorMessage = new Element(By.xpath("//label[contains(@data-error,'"+errorText+"')]"));
            errorMessage.waitForElementIsPresent();
            errorMessage.isPresent();
        }
        catch (NoSuchElementException ex)
        {
            org.junit.Assert.fail("Error not displayed");
        }

    }

    private static void signInGoogle()
    {
        TextField email = new TextField(By.xpath(Variables.EMAIL_INPUT_GOOGLE.toString()));
        waitUntilElementVisible(driver.findElement(By.xpath(Variables.NEXT_BUTTON_STEP1.toString())));
        WebElement next = driver.findElement(By.xpath(Variables.NEXT_BUTTON_STEP1.toString()));
        email.enterText(testUserEmail);
        Actions actions = new Actions(driver);
        actions.moveToElement(next).click().perform();
        waitInSeconds(3);
        WebElement nextStep = driver.findElement(By.xpath(Variables.NEXT_BUTTON_STEP2.toString()));
        TextField pass = new TextField(By.xpath(Variables.PASS_INPUT_GOOGLE.toString()));
        pass.enterText(testUserPass);
        actions.moveToElement(nextStep).click().perform();

    }

    public static void signInGoogleConsole()
    {

        driver.get(baseURL+"console");
        Button signInWithGoogle = new Button(By.xpath(Variables.SIGN_IN_BUTTON_CONSOLE.toString()));
        signInWithGoogle.waitForElementIsPresent();
        signInWithGoogle.click();
        signInGoogle();
        waitInSeconds(5);
        verifyLinkStatus("console/permission#");
    }

    private static void verifyLinkStatus(String link)
    {
        if (driver.getCurrentUrl().contains(baseURL + link)) {
            System.out.print("URL correct");
        } else {
            System.out.print(driver.getCurrentUrl());
            Assert.fail("URL is wrong");
        }
    }

    private static void verify_response()
    {
        URL url = null;
        try {
            url = new URL(driver.getCurrentUrl());
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }
        HttpURLConnection httpCon = null;
        try {
            assert url != null;
            httpCon = (HttpURLConnection)url.openConnection();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            assert httpCon != null;
            if (httpCon.getResponseMessage().contains("OK")) {
                System.out.println("\n Response Message is " + httpCon.getResponseMessage());
                httpCon.disconnect();
            } else {
                Assert.fail("\n Response Message isn't OK, response is " + httpCon.getResponseMessage());
                httpCon.disconnect();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static HomePage compareHomePageScreen()
    {
        waitInSeconds(5);
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("test_data/homePageActualScreen.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        CompareUtil.CompareImage("test_data/homePageScreen.png","test_data/homePageActualScreen.png");
        try {
            org.apache.commons.io.FileUtils.forceDelete(new File("test_data/homePageActualScreen.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HomePage();
    }

    public static void restorePass()
    {
        Button forgotPassword= new Button(By.xpath(Variables.FORGOT_PASSWORD_LINK.toString()));
        forgotPassword.waitForElementIsPresent();
        forgotPassword.click();
        TextField email = new TextField(By.xpath(Variables.EMAIL_INPUT.toString()));
        email.waitForElementIsPresent();
        email.enterText("test");
        Button sendLink = new Button(By.xpath(Variables.SEND_RESET_PASSWORD_LINK.toString()));
        sendLink.click();
        email.enterText(testUserEmail);
        sendLink.click();
        Element success = new Element(By.xpath(Variables.SEND_SUCCESSFULLY.toString()));
        success.waitForElementIsPresent();
        if(success.isPresent())
        {
            System.out.print("Test passed");
        }
        else
        {
            org.junit.Assert.fail("Test failed review it manually");
        }
    }
}





