package Pages;

import Elements.Button;
import Elements.Element;
import Elements.TextField;
import Enums.Variables;
import MainSettings.Settings;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static Pages.HomePage.signInGoogleConsole;

public class Dashboard extends Settings
{
    private static Button remove = new Button(By.xpath(Variables.REMOVE_USER_BUTTON.toString()));
    private static Button confirmRemove = new Button(By.xpath(Variables.CONFIRM_REMOVE.toString()));
    private static TextField email = new TextField(By.xpath(Variables.NEW_REP_EMAIL.toString()));
    private static String userEmail = RandomStringUtils.randomAlphabetic(10)+"@gmail.com";
    private static TextField name = new TextField(By.xpath(Variables.NEW_REP_NAME.toString()));
    public static Dashboard lead_filters()
    {
        Button filterSelect = new Button(By.xpath(Variables.FILTER_SELECT.toString()));
        filterSelect.waitForElementIsPresent();
        filterSelect.click();
        Button filterByNewLead = new Button(By.xpath(Variables.FILTER_NEW_LEAD.toString()));
        filterByNewLead.waitForElementIsPresent();
        filterByNewLead.click();
        waitInSeconds(2);
        Button filterByEscalated = new Button(By.xpath(Variables.FILTER_ESCALATED.toString()));
        filterSelect.click();
        filterByEscalated.waitForElementIsPresent();
        filterByEscalated.click();
        waitInSeconds(2);
        Button filterByCallRequested = new Button(By.xpath(Variables.FILTER_CALL_REQUIRED.toString()));
        filterSelect.click();
        filterByCallRequested.waitForElementIsPresent();
        filterByCallRequested.click();
        waitInSeconds(2);
        Button filterByUrgent = new Button(By.xpath(Variables.FILTER_URGENT.toString()));
        filterSelect.click();
        filterByUrgent.waitForElementIsPresent();
        filterByUrgent.click();
        waitInSeconds(2);
        Button filterByAwaitingResponse = new Button(By.xpath(Variables.FILTER_AWAITING_RESPONSE.toString()));
        filterSelect.click();
        filterByAwaitingResponse.waitForElementIsPresent();
        filterByAwaitingResponse.click();
        waitInSeconds(2);
        Button filterByActionRequired = new Button(By.xpath(Variables.FILTER_ACTIONS_REQUIRED.toString()));
        filterSelect.click();
        filterByActionRequired.waitForElementIsPresent();
        filterByActionRequired.click();
        waitInSeconds(2);
        return new Dashboard();
    }

    public static Dashboard filter_by_REP()
    {
        Button filterSelect = new Button(By.xpath(Variables.FILTER_SELECT2.toString()));
        filterSelect.waitForElementIsPresent();
        filterSelect.click();
        Button filterByREP = new Button(By.xpath(Variables.FILTER_BY_REP.toString()));
        filterByREP.waitForElementIsPresent();
        filterByREP.click();
        return new Dashboard();
    }

    private static void createRepAccount(String role, String id)
    {
        signInGoogleConsole();
        Button create = new Button(By.xpath(Variables.CREATE_REP_ACCOUNT_BUTTON.toString()));
        create.click();
        name.enterText(RandomStringUtils.randomAlphabetic(15));
        email.enterText(userEmail);
        Button selectPermission = new Button(By.xpath(Variables.SELECT_PERMISSION.toString()));
        selectPermission.click();
        waitInSeconds(2);
        Button permission = new Button(By.xpath("/descendant::span[contains(text(),'"+role+"')]["+id+"]"));
        permission.waitForElementIsPresent();
        permission.click();
        Button addUser = new Button(By.xpath(Variables.ADD_USER_BUTTON.toString()));
        addUser.waitForElementIsPresent();
        addUser.click();
        waitInSeconds(2);
    }

    public static void createRepAccountAdmin()
    {
        createRepAccount("Admin","1");
        editUser();
        removeUser();
    }

    public static void createRepAccountTier1()
    {
        createRepAccount("Tier 1","1");
        editUser();
        removeUser();
    }

    public static void createRepAccountTier2()
    {
        createRepAccount("Tier 2","1");
        editUser();
        removeUser();
    }

    public static void createRepAccountSuspended()
    {
        createRepAccount("Suspended","2");
        editUser();
        removeUser();
    }

    private static void removeUser()
    {
        remove.click();
        confirmRemove.waitForElementIsPresent();
        confirmRemove.click();
    }

    private static void editUser()
    {
        Button edit = new Button(By.xpath(Variables.EDIT_USER.toString()));
        edit.waitForElementIsPresent();
        edit.click();
        email.waitForElementIsPresent();
        email.enterText(userEmail);
        name.waitForElementIsPresent();
        name.enterText(RandomStringUtils.randomAlphabetic(20));
        Button save = new Button(By.xpath(Variables.SAVE_BUTTON.toString()));
        save.click();
    }

    public static void assign_and_reassign_lead()
    {
        signInGoogleConsole();
        driver.get(baseURL+"console/leads/active");
        waitInSeconds(2);
        Button selectRep = new Button(By.xpath(Variables.SELECT_REP.toString()));
        selectRep.waitForElementIsPresent();
        selectRep.click();
        WebElement testUser = driver.findElement(By.xpath("/descendant::span[contains(text(),'Konstantin Ovramets')][2]"));
        waitInSeconds(4);
        Actions actions = new Actions(driver);
        try
        {
            actions.moveToElement(testUser).click().build().perform();
        }
        catch (ElementNotVisibleException ex)
        {Button testUser3 = new Button(By.xpath("/descendant::span[contains(text(),'Blake Weis2')][2]"));
        testUser3.click();}
        selectRep.waitForElementIsPresent();
        selectRep.click();
        Button testUser2 = new Button(By.xpath("/descendant::span[contains(text(),'Yuriy Golikov')][3]"));
        testUser2.waitForElementIsPresent();
        testUser2.click();
    }

    public static Dashboard userSearch(String userName)
    {
        TextField search = new TextField(By.xpath(Variables.USER_SEARCH.toString()));
        search.enterText(userName);
        Element correctSearchResult = new Element(By.xpath("//span[contains(text(), '"+userName+"')]"));
        if(correctSearchResult.isPresent())
        {System.out.print("\n Test passed");}
        else {System.out.print("\n Test failed, search results incorrect");}
        return new Dashboard();
    }

}
