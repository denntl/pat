package Enums;

public enum Variables
{
    //Dashboard console
    SEND_MESSAGE(".//*[@id='app']/div/div[2]/div[2]/div/div/div[2]/div[3]/div[2]/form/button"),
    OPEN_TEMPLATES_LIST_BUTTON(".//*[@id='messages']/button"),
    CHOOSE_TEMPLATE(".//*[@id='scripts']/div[1]/div/div[2]/div[1]/p[2]"),
    VIEW_EMAIL_BUTTON(".//*[@id='app']/div/div[2]/div[2]/div/div/div[1]/button[1]"),
    HIDE_EMAIL_BUTTON(".//*[@id='app']/div/div[2]/div[2]/div/div/div[1]/button[2]"),
    SWITCH_TO_EMAILS_BUTTON(".//*[@id='app']/div/div[2]/div[2]/div/div/div[2]/div[1]/ul/li[3]"),
    CONFIRM_CALL_BUTTON(".//*[@id='app']/div/div[4]/div[1]/div[2]/button[1]"),
    CANCEL_CALL_BUTTON(".//*[@id='app']/div/div[4]/div[1]/div[2]/button[2]"),
    CALL_BUTTON(".//*[@id='app']/div/div[2]/div[2]/div/div/div[2]/div[1]/ul/li[1]/i"),
    CHOOSE_LEAD_STATUS_BUTTON(".//*[@id='app']/div/div[3]/div[1]/div[1]/div[1]"),
    UPDATE_NOTES_TEXT_FIELD(".//*[@id='app']/div/div[3]/div[1]/div[1]/div[2]/textarea"),
    CONFIRM_UPDATE_STATUS(".//*[@id='app']/div/div[3]/div[1]/div[2]/button[1]"),
    UPDATE_STATUS(".//*[@id='app']/div/div[2]/div[3]/div[3]/div/div/button"),
    LEAD_EMAIL_INPUT(".//*[@id='email']"),
    LEAD_PHONE_INPUT(".//*[@id='phone']"),
    LEAD_FIRST_NAME_INPUT(".//*[@id='first-name']"),
    LEAD_LAST_NAME_INPUT(".//*[@id='last-name']"),
    USER_SEARCH(".//*[@id='search']"),
    SAVE_NOTES(".//*[@id='lead-notes']/div/div[1]/button[1]"),
    NOTES_FOR_AGENT(".//*[@id='agent-note']"),
    ADMIN_NOTES(".//*[@id='admin-note']"),
    SAVE_ADMIN_NOTES_BUTTON(".//*[@id='lead-notes']/div/div[2]/button"),
    NOTES_TAB(".//*[@id='tabs-swipe-lead-info']/li[2]/a"),
    SWITCH_TO_CHATS(".//*[@id='app']/div/div[3]/nav/div/a"),
    SEND_SUCCESSFULLY("//span[contains(text(),'Reset link sent!')]"),
    SEND_RESET_PASSWORD_LINK(".//*[@id='portal-login']/div[3]/div/button"),
    FORGOT_PASSWORD_LINK(".//*[@id='portal-login']/div[2]/div/div/a"),
    ACTIVE_MENU(".//*[@id='app']/div/div[3]/nav/div/ul/li[1]/a"),
    SELECT_REP(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[2]/span/div/input"),
    SAVE_BUTTON(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[4]/span[3]"),
    EDIT_USER(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[4]/span[1]/i"),
    NEW_REP_NAME(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[1]/input"),
    NEW_REP_EMAIL(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[2]/input"),
    CONFIRM_REMOVE(".//*[@id='modalDelete']/div[2]/a[1]"),
    REMOVE_USER_BUTTON(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[4]/span[2]/i"),
    ADD_USER_BUTTON(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[4]/span[4]"),
    SELECT_PERMISSION(".//*[@id='app']/div/div[4]/div/div/table/tbody/tr[1]/td[3]/div/div/input"),
    CREATE_REP_ACCOUNT_BUTTON(".//*[@id='app']/div/div[4]/div/div/div[2]/a"),
    FILTER_BY_REP("/descendant::span[contains(text(),'Yuriy Golikov')][1]"),
    FILTER_SELECT(".//*[@id='app']/div/div[4]/div/div/div[1]/div[1]/input"),
    FILTER_SELECT2(".//*[@id='app']/div/div[4]/div/div/div[1]/div[2]/input"),
    FILTER_NEW_LEAD("//li[2]/span[contains(text(),'New lead')]"),
    FILTER_ESCALATED("//li[3]/span[contains(text(),'Escalated')]"),
    FILTER_CALL_REQUIRED("//li[4]/span[contains(text(),'Call Requested')]"),
    FILTER_URGENT("//li[5]/span[contains(text(),'Urgent')]"),
    FILTER_AWAITING_RESPONSE("//li[6]/span[contains(text(),'Awaiting Response')]"),
    FILTER_ACTIONS_REQUIRED("//li[7]/span[contains(text(),'Action Required')]"),
    //Sign in
    CREATE_ACCOUNT_LINK(".//*[@id='portal-login']/div[6]/div/a"),
    FIRST_NAME_INPUT(".//*[@id='first_name']"),
    LAST_NAME_INPUT(".//*[@id='last_name']"),
    PHONE_INPUT(".//*[@id='telephone']"),
    CREATE_ACCOUNT_BUTTON(".//*[@id='create-account']/div[6]/div/button"),
    SIGN_IN_BUTTON_GOOGLE(".//*[@id='portal-login']/div[5]/div/a/img"),
    SIGN_IN_BUTTON_CONSOLE(".//*[@id='app']/div/div/div/div[2]/p[1]/a/img"),
    EMAIL_INPUT_GOOGLE(".//*[@id='identifierId']"),
    PASS_INPUT_GOOGLE(".//*[@id='password']/div[1]/div/div[1]/input"),
    NEXT_BUTTON_STEP1(".//*[@id='identifierNext']/div[2]"),
    NEXT_BUTTON_STEP2(".//*[@id='passwordNext']/div[2]"),
    SIGN_IN_BUTTON(".//*[@id='portal-login']/div[3]/div/button"),
    PASS_INPUT(".//*[@id='password']"),
    EMAIL_INPUT(".//*[@id='email']");

    private String text;
    Variables(String text)
    {
        this.text=text;
    }

    public String toString()
    {

        return this.text;
    }
}