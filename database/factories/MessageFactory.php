<?php

use App\Concierge;
use App\Lead;
use App\Services\TwilioService;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Message::class, function (Faker\Generator $faker) {
    /** @var Lead $lead */
    $lead = factory(Lead::class)->create();

    $incoming = $faker->boolean(50);
    if ($incoming) {
        $lead->tag = Lead::TAG_AWAITING_RESPONSE;
        $lead->update();
    }

    $messageStatus = $faker->randomElement([
        TwilioService::MESSAGE_FAILED_STATUS,
        TwilioService::MESSAGE_DELIVERED_STATUS
    ]);

    return [
        'conciergeId' => function () {
            /** @var Concierge $concierge */
            $concierge = Concierge::inRandomOrder()->first();
            if (!$concierge) {
                factory(Concierge::class, 10)->create();
                $concierge = Concierge::inRandomOrder()->first();
            }

            return $concierge->id;
        },
        'leadId' => $lead->id,
        'to' => '+1' . $faker->randomNumber(2) . $faker->randomNumber(8),
        'from' => '+1' . $faker->randomNumber(2) . $faker->randomNumber(8),
        'text' => $faker->text(100),
        'incoming' => $incoming,
        'isRead' => $faker->boolean(50),
        'isAutomatic' => $faker->boolean(50),
        'twilioMessageSid' => $faker->uuid,
        'twilioMessageStatus' => $incoming ? TwilioService::MESSAGE_RECEIVED_STATUS : $messageStatus,
    ];
});
