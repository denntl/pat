<?php

use App\Lead;
use App\Concierge;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Note::class, function (Faker\Generator $faker) {
    $types = Lead::$activeStatuses + Lead::$archivedStatuses;

    return [
        'type' => $faker->randomElement($types),
        'note' => $faker->text(),
        'conciergeId' => function () {
            $concierge = factory(Concierge::class)->create();

            return $concierge->id;
        },
        'leadId' => function () {
            $lead = Lead::inRandomOrder()->first();
            if (!$lead) {
                factory(Lead::class, 10)->create();
                $lead = Lead::inRandomOrder()->first();
            }

            return $lead->id;
        }
    ];
});