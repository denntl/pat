<?php

use App\Agent;
use App\Concierge;
use App\Lead;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Lead::class, function (Faker\Generator $faker)
{
    $email = $faker->unique()->safeEmail;
    $phone = $faker->randomNumber(7);

    return [
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'phone' => $faker->tollFreePhoneNumber,
        'phoneSource' => '850'.$phone,
        'email' => $email,
        'emailSource' => $email,
        'agentId' => function () {
            // To prevent many agents and make more leads on one agent
            if (Agent::count() > 1) {
                return Agent::inRandomOrder()->first()->id;
            }

            return factory(Agent::class)->create()->id;
        },
        'conciergeId' => null,
        'sourceId' => function () {
            return App\LeadSource::where('name', 'Zillow')->first()->id;
        },
        'city' => $faker->city,
        'state' => $faker->state,
        'street' => $faker->streetAddress,
        'postalcode' => $faker->postcode,
        'leadType' => $faker->randomElement([
            'buyer',
            'seller'
        ]),
        'priceRange' => $faker->randomElement([
            'Under $200,000',
            '$200,000 - $400,000',
            '$400,000 - $600,000',
            '$600,000 - $800,000',
            '$800,000 - $1,000,000',
            'Over $1,000,000'
        ]),
        'mortgage' => $faker->randomElement([
            'Pre-Qualified',
            'Pre-Approved',
            'Haven`t Applied Yet',
            'Cash Buyer',
            'I don`t know',
            'Not Applicable'
        ]),
        'bedrooms' => $faker->randomDigit,
        'bathrooms' => $faker->randomDigit,
        'timeFrame' => $faker->randomElement([
            'Asap',
            'Within 3 months',
            '3-6 months',
            '6 months or longer'
        ]),
        'status' => Lead::STATUS_NEW,
        'tag' => Lead::TAG_URGENT,
        'originalEmail' => $faker->unique()->safeEmail,
        'agentAssignedAt' => Carbon::instance($faker->dateTimeThisMonth('now'))->toDateTimeString(),
        'createdAt' => Carbon::now()->subMonth()->toDateTimeString(),
        'fromViking' => false
    ];
});