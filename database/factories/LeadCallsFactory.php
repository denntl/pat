<?php

use App\Lead;
use App\LeadCall;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(LeadCall::class, function () {
    /** @var Lead $lead */
    $lead = factory(Lead::class)->create();

    return [
        'leadId' => $lead->id,
        'phone' => $lead->phone,
        'createdAt' => Carbon::now()->subMonth()->toDateTimeString(),
        'finishedAt' => Carbon::now()->addMinutes(5)->subMonth()->toDateTimeString()
    ];
});
