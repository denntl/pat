<?php

use App\Concierge;
use App\Lead;
use App\Agent;
use App\Services\SendGridService;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Email::class, function (Faker\Generator $faker) {
    return [
        'from' => $faker->safeEmail,
        'to' => $faker->safeEmail,
        'senderFullName' => $faker->name,
        'subject' => $faker->text(90),
        'statusId' => SendGridService::VALID_EMAIL_STATUS_ID,
        'leadId' => function () {
            $lead = Lead::inRandomOrder()->first();
            if (!$lead) {
                factory(Lead::class, 10)->create();
                $lead = Lead::inRandomOrder()->first();
            }

            return $lead->id;
        },
        'conciergeId' => function () {
            $concierge = Concierge::inRandomOrder()->first();
            if (!$concierge) {
                factory(Concierge::class, 10)->create();
                $concierge = Concierge::inRandomOrder()->first();
            }

            return $concierge->id;
        },
        'agentId' => function () {
            $agent = Agent::inRandomOrder()->first();
            if (!$agent) {
                factory(Agent::class, 10)->create();
                $agent = Agent::inRandomOrder()->first();
            }

            return $agent->id;
        },
        'text' => $faker->text(120),
        'UUID' => $faker->uuid,
        'isRead' => $faker->boolean(50),
        'incoming' => $faker->boolean(50)
    ];
});
