<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Agent::class, function (Faker\Generator $faker) {
    static $password;

    $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));

    return [
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'password' => $password ?? $password = bcrypt('secret'),
        'phone' => substr($faker->phoneNumber, 0, 12),
        'email' => $faker->unique()->safeEmail,
        'website' => $faker->domainName,
        'calendly' => $faker->domainName,
        'city' => $faker->city,
        'state' => $faker->word,
        'provider' => null,
        'providerId' => null,
        "company" => null,
        "twilioNumber" => null,
        "sendGridEmail" => null,
        "personaTeamName" => null,
        "personaName" => null,
        "personaTitle" => null,
        "gmailConnected" => false,
        "notificationPhone" => null,
        "notificationEmail" => null,
        "callForwardingNumber" => null,
        "emailForwarding" => null,
        "areaCode" => null,
        'extraNoteForRep' => null
    ];
});
