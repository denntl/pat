<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Concierge::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?? $password = bcrypt($password),
        'provider' => null,
        'providerId' => null,
        'rememberToken' => null,
        'role' => $faker->numberBetween(0, 3),
        'conciergeRole' => $faker->word,
        'companyName' => $faker->company,
        'teamName' => $faker->company,
        'teamLeader' => $faker->name
    ];
});
