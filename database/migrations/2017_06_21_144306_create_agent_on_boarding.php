<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentOnBoarding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentOnBoarding', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('step')->default(1);
            $table->tinyInteger('skip')->default(0);
            $table->integer('agentId')->unsigned();
            $table->foreign('agentId')->references('id')->on('agents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentOnBoarding');
    }
}
