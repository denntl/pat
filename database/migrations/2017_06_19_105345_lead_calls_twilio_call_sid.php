<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeadCallsTwilioCallSid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leadCalls', function (Blueprint $table) {
            $table->string('twilioCallSid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leadCalls', function (Blueprint $table) {
            $table->dropColumn('twilioCallSid');
        });
    }
}