<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeadRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function(Blueprint $table){
            $table->dropColumn('leadId');
        });
        Schema::table('messages', function(Blueprint $table){
            $table->uuid('leadId');
        });
        Schema::table('messages', function(Blueprint $table){
            $table->foreign('leadId')->references('id')->on('leads');
        });
        Schema::table('notes', function(Blueprint $table){
            $table->dropColumn('leadId');
        });
        Schema::table('notes', function(Blueprint $table){
            $table->uuid('leadId');
        });
        Schema::table('notes', function(Blueprint $table){
            $table->foreign('leadId')->references('id')->on('leads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('TRUNCATE notes CASCADE');
        DB::statement('TRUNCATE messages CASCADE');
        DB::statement('TRUNCATE leads CASCADE');

        Schema::table('messages', function(Blueprint $table){
            $table->dropColumn('leadId');
        });
        Schema::table('messages', function(Blueprint $table){
            $table->integer('leadId');
        });
        Schema::table('notes', function(Blueprint $table){
            $table->dropColumn('leadId');
        });
        Schema::table('notes', function(Blueprint $table){
            $table->integer('leadId');
        });
    }
}
