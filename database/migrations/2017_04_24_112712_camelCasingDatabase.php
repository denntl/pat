<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CamelCasingDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->changeCase();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->changeCase(false);
    }

    /**
     * Change table and column names to camel case or snake case
     * If camelCase is true to camel case else to snake case
     * @param bool $camelCase
     */
    private function changeCase($camelCase = true)
    {
        //Get all tables from database
        $tables = DB::select('SELECT * FROM pg_catalog.pg_tables');

        foreach($tables as $table) {
            //if table schemaname is public and table name is not migrations
            if ($table->schemaname == 'public' && $table->tablename != 'migrations') {
                //Get all columns from table
                $columnsFromTable = DB::select("SELECT *
                    FROM information_schema.columns
                    WHERE table_name ='" . $table->tablename . "'"
                );
                $tableName = $table->tablename;
                //Make table name camelCase if up() function is called or snake_case if down() function is called
                $changedCaseTableName = $this->changeCaseCallFunction($tableName, $camelCase);

                foreach ($columnsFromTable as $column) {
                    $columnName = $column->column_name;
                    //Make column name camelCase or snake_case like table name. See above
                    $changedCaseColumnName = $this->changeCaseCallFunction($columnName, $camelCase);
                    //If camelCase name or snake_case name of column not equal to original name
                    if ($columnName != $changedCaseColumnName) {
                        //Schema::rename will not work since not case sensitive
                        //Change column name in database
                        DB::insert('ALTER TABLE "' . $tableName . '"'
                            .' RENAME "' . $columnName . '"'
                            .' TO "' .$changedCaseColumnName . '"'
                        );
                    }
                }

                //The same as with columns see above
                if ($tableName != $changedCaseTableName) {
                    //Schema::rename will not work since not case sensitive
                    DB::insert('ALTER TABLE "' . $tableName . '" RENAME TO "' . $changedCaseTableName . '"');
                }
            }
        }
    }

    /**
     * @param string $value
     * @param bool $camelCase
     * @return string
     */
    private function changeCaseCallFunction($value, $camelCase = true)
    {
        if ($camelCase) {
            return camel_case($value);
        }

        return snake_case($value);
    }
}
