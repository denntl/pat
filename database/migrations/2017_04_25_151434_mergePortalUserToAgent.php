<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MergePortalUserToAgent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Alter table columns and change notes and admin notes to null default value
        Schema::table('agents', function (Blueprint $table) {
            $table->text('notes')->nullable()->change();
            $table->text('adminNotes')->nullable()->change();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('password');
            $table->string('provider')->nullable();
            $table->string('providerId', 155)->unique()->nullable();
            $table->string('rememberToken', 100)->nullable();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });

        //Get all users from table portalUsers
        $users = DB::table('portalUsers')->get();

        //Move all users from PortalUsers table to Agents table
        foreach ($users as $user) {
            DB::table('agents')->insert([
                'firstName' => $user->firstName,
                'lastName' => $user->lastName,
                'email' => $user->email,
                'password' => $user->password,
                'phone' => $user->phone,
                'provider' => $user->provider,
                'rememberToken' => $user->rememberToken,
                'createdAt' => $user->createdAt,
                'updatedAt' => $user->updatedAt,
            ]);
        }

        //Remove table portalUsers
        Schema::dropIfExists('portalUsers');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Create table portalUsers
        Schema::create('portalUsers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email', 155)->unique();
            $table->string('password');
            $table->string('phone');
            $table->string('provider')->nullable();
            $table->string('providerId', 155)->unique()->nullable();
            $table->string('rememberToken', 100)->nullable();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });

        //Get all users from agents table
        $users = DB::table('agents')->get();

        //Insert all users from agent to PortalUsers
        foreach ($users as $user) {
            DB::table('portalUsers')->insert([
                'firstName' => $user->firstName,
                'lastName' => $user->lastName,
                'email' => $user->email,
                'password' => $user->password,
                'phone' => $user->phone,
                'provider' => $user->provider,
                'rememberToken' => $user->rememberToken,
                'createdAt' => $user->createdAt,
                'updatedAt' => $user->updatedAt,
            ]);
        }

        //Clear agents table cascade
        DB::statement('TRUNCATE agents CASCADE');

        //Drop columns for users and back notes and admin notes to not null
        Schema::table('agents', function (Blueprint $table) {
            $table->dropColumn('firstName');
            $table->dropColumn('lastName');
            $table->dropColumn('password');
            $table->dropColumn('provider');
            $table->dropColumn('providerId');
            $table->dropColumn('rememberToken');
            $table->dropColumn('createdAt');
            $table->dropColumn('updatedAt');
            $table->text('notes')->nullable(false)->change();
            $table->text('adminNotes')->nullable(false)->change();
        });
    }
}
