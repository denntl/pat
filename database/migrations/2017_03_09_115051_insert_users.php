<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
            'email' => 'pronin.ihor.mail@gmail.com',
        ]);
        DB::table('users')->insert([
            'email' => 'elfanlide@gmail.com',
        ]);
        DB::table('users')->insert([
            'email' => 'blakeweis1986@gmail.com',
        ]);
        DB::table('users')->insert([
            'email' => 'maximdudnik.ua@gmail.com',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->where('email', 'LIKE', 'pronin.ihor.mail@gmail.com')->delete();
        DB::table('users')->where('email', 'LIKE', 'elfanlide@gmail.com')->delete();
        DB::table('users')->where('email', 'LIKE', 'blakeweis1986@gmail.com')->delete();
        DB::table('users')->where('email', 'LIKE', 'maximdudnik.ua@gmail.com')->delete();
    }
}
