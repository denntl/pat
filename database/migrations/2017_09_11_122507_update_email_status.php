<?php

use App\Email;
use Illuminate\Database\Migrations\Migration;

class UpdateEmailStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $emails = Email::where('statusId', 1)->update(['statusId' => 3]);
        logger()->info('Emails status updated: ' . $emails);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
