<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMessagesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->dropTimestamps();
            $table->dropColumn('is_read');
            $table->tinyInteger('isRead')->default(1);
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->timestamps();
            $table->tinyInteger('is_read')->default(1);
            $table->dropColumn('isRead');
            $table->dropColumn('createdAt');
            $table->dropColumn('updatedAt');
        });

    }
}