<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Agent;

class ChangeExistingForwarwardingEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try{
            Agent::where("emailForwarding", '!=', null)->chunk(10, function($agents){
                foreach ($agents as $agent) {
                    $emailForwarding = $agent->emailForwarding;
                    $changedEmail = preg_replace('/\.com$/i', '.me', $emailForwarding);
                    $agent->emailForwarding = $changedEmail;
                    $agent->save();
                }
            });
        } catch(Exception $exception) {
            info('migration with eloquent data failed');
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Agent::where("emailForwarding", '!=', null)->chunk(10, function($agents){
            foreach ($agents as $agent) {
                $emailForwarding = $agent->emailForwarding;
                $changedEmail = preg_replace('/\.me/i', '.com', $emailForwarding);
                $agent->emailForwarding = $changedEmail;
                $agent->save();
            }
        });
    }
}
