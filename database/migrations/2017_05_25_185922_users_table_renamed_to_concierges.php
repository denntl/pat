<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTableRenamedToConcierges extends Migration
{
    /**
     * @var string
     */
    private $from = 'users';

    /**
     * @var string
     */
    private $to = 'concierges';

    /**
     * @var string
     */
    private $fromColumn = 'userId';

    /**
     * @var string
     */
    private $toColumn = 'conciergeId';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename($this->from, $this->to);

        DB::insert('ALTER TABLE messages RENAME "' . $this->fromColumn . '" TO "' . $this->toColumn . '"');
        DB::insert('ALTER TABLE messages DROP CONSTRAINT messages_user_id_foreign');
        DB::insert(
            'ALTER TABLE messages 
             ADD CONSTRAINT messages_concierge_id_foreign 
             FOREIGN KEY ( "' . $this->toColumn . '" ) REFERENCES concierges(id)'
        );

        DB::insert('ALTER TABLE notes RENAME "' . $this->fromColumn . '" TO "' . $this->toColumn . '"');
        DB::insert('ALTER TABLE notes DROP CONSTRAINT notes_user_id_foreign');
        DB::insert(
            'ALTER TABLE notes 
             ADD CONSTRAINT notes_concierge_id_foreign 
             FOREIGN KEY ( "' . $this->toColumn . '" ) REFERENCES concierges(id)'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename($this->to, $this->from);

        DB::insert('ALTER TABLE messages RENAME "' . $this->toColumn. '" TO "' . $this->fromColumn. '"');
        DB::insert('ALTER TABLE messages DROP CONSTRAINT messages_concierge_id_foreign');
        DB::insert(
            'ALTER TABLE messages 
             ADD CONSTRAINT messages_user_id_foreign 
             FOREIGN KEY ( "' . $this->fromColumn . '" ) REFERENCES users(id)'
        );

        DB::insert('ALTER TABLE notes RENAME "' . $this->toColumn. '" TO "' . $this->fromColumn. '"');
        DB::insert('ALTER TABLE notes DROP CONSTRAINT notes_concierge_id_foreign');
        DB::insert(
            'ALTER TABLE notes 
             ADD CONSTRAINT notes_user_id_foreign 
             FOREIGN KEY ( "' . $this->fromColumn . '" ) REFERENCES users(id)'
        );
    }
}
