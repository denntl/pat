<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('from');
            $table->string('to');
            $table->string('senderFullName')->nullable();
            $table->string('subject');
            $table->integer('statusId')->unsigned()->default(1);
            $table->foreign('statusId')->references('id')
                ->on('letterDeliveryStatuses');
            $table->uuid('leadId')->unsigned();
            $table->foreign('leadId')->references('id')
                ->on('leads');
            $table->integer('conciergeId')->unsigned();
            $table->foreign('conciergeId')->references('id')
                ->on('concierges');
            $table->integer('agentId')->unsigned();
            $table->foreign('agentId')->references('id')
                ->on('agents');
            $table->text('text');
            $table->uuid('UUID')->nullable();
            $table->tinyInteger('isRead')->default(0);
            $table->tinyInteger('incoming')->default(0);
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
