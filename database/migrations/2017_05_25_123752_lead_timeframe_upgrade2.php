<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeadTimeframeUpgrade2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_time_frame_check');
        });

        $timeFrames = [
            'Asap',
            'Within 3 months',
            '3-6 months',
            '6 months or longer',
            'I\'\'m not sure',
            '0-1 months',
            '12+ months',
            '6-12 months'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value);
        }, $timeFrames) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_time_frame_check CHECK (\"timeFrame\"::text = ANY (ARRAY[$result]::text[]))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_time_frame_check');
        });

        $timeFrames = [
            'Asap',
            'Within 3 months',
            '3-6 months',
            '6 months or longer',
            'I\'\'m not sure',
            '0-1 months',
            '12+ months'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value);
        }, $timeFrames) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_time_frame_check CHECK (\"timeFrame\"::text = ANY (ARRAY[$result]::text[]))");
    }
}
