<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateLeadCalls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leadCalls', function (Blueprint $table) {
            DB::statement('ALTER TABLE "leadCalls" ALTER COLUMN "leadId" DROP NOT NULL;');
            DB::statement('ALTER TABLE "leadCalls" ALTER COLUMN "phone" DROP NOT NULL;');
            $table->tinyInteger('isForwarded')->default(0);
            $table->integer('agentId')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leadCalls', function (Blueprint $table) {
            DB::table("leadCalls")->truncate(); // To clear fields with null values
            DB::statement('ALTER TABLE "leadCalls" ALTER COLUMN "leadId" SET NOT NULL;');
            DB::statement('ALTER TABLE "leadCalls" ALTER COLUMN "phone" SET NOT NULL;');

            $table->dropColumn('isForwarded');
            $table->dropColumn('agentId');
        });
    }
}
