<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTagInLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_tag_check');
        });

        $tags = [
            'Urgent',
            'Awaiting Response',
            'Action Required',
            'Ongoing Chat',
            'Archive',
            'Automation'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value);
        }, $tags) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_tag_check CHECK (tag::text = ANY (ARRAY[$result]::text[]))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("UPDATE leads SET tag = null WHERE tag = 'Archive' OR tag = 'Automation';");

        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_tag_check');
        });

        $tags = [
            'Urgent',
            'Awaiting Response',
            'Action Required',
            'Ongoing Chat'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value);
        }, $tags) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_tag_check CHECK (tag::text = ANY (ARRAY[$result]::text[]))");
    }
}
