<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoggingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logsWebsocket', function (Blueprint $table) {
            $table->increments('id');
            $table->string('command');
            $table->text('data');
            $table->text('socketData')->default('console artisan command');
            $table->string('class');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
        Schema::create('logsLeadsVikingSync', function (Blueprint $table) {
            $table->increments('id');
            $table->text('requestUrl');
            $table->text('responseData')->nullable();
            $table->integer('responseStatus')->default(200);
            $table->string('commandStatus')->default('success');
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
        Schema::create('logsApi', function (Blueprint $table) {
            $table->increments('id');
            $table->text('requestUrl');
            $table->string('clientIp');
            $table->string('action');
            $table->text('requestData');
            $table->text('responseText')->nullable();
            $table->integer('responseStatus')->default(200);
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
        Schema::create('logsJobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class');
            $table->text('status')->default('Ok');
            $table->text('data')->nullable();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
        Schema::create('logsLeadChanges', function (Blueprint $table) {
            $table->increments('id');
            $table->text('newData')->nullable();
            $table->text('oldData')->nullable();
            $table->string('user')->nullable();
            $table->string('action')->nullable();
            $table->string('leadId')->nullable();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logsWebsocket');
        Schema::dropIfExists('logsLeadsVikingSync');
        Schema::dropIfExists('logsApi');
        Schema::dropIfExists('logsJobs');
        Schema::dropIfExists('logsLeadChanges');
    }
}
