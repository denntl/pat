<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VikingAttachmentsForLead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leadVikingEmails', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('leadId')->unsigned();
            $table->foreign('leadId')->references('id')
                ->on('leads');
            $table->uuid('vikingId')->unsigned();
            $table->string('location');
            $table->longText('content');
            $table->dateTime('createdAt')->nullable();
            $table->dateTime('updatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leadVikingEmails');
    }
}
