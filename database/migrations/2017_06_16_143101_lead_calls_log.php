<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeadCallsLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leadCalls', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('leadId');
            $table->foreign('leadId')->references('id')->on('leads');
            $table->string('phone');
            $table->dateTime('createdAt')->nullable();
            $table->dateTime('finishedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leadCalls');
    }
}
