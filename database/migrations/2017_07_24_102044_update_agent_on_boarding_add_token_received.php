<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAgentOnBoardingAddTokenReceived extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agentOnBoarding', function (Blueprint $table) {
            $table->boolean('tokenReceived')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agentOnBoarding', function (Blueprint $table) {
            $table->dropColumn('tokenReceived');
        });
    }
}
