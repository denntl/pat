<?php

use App\Message;
use Illuminate\Database\Migrations\Migration;

class UpdateMessageStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $messages = Message::whereIn('twilioMessageStatus', ['sending', ''])
            ->orWhereNull('twilioMessageStatus')
            ->update(['twilioMessageStatus' => 'delivered']);
        logger()->info('Messages status updated: ' . $messages);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
