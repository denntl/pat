<?php

use App\Lead;
use App\Facades\PhoneNumber;
use Illuminate\Database\Migrations\Migration;

class UpdateLeadPhoneFormat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Lead::chunk(10, function ($leads) {
            /** @var Lead $lead */
            foreach ($leads as $lead) {
                try {
                    $phone = PhoneNumber::getNationalPhone($lead->phoneSource);
                    $lead->update(['phone' => $phone]);
                } catch (Exception $exception) {
                    info($exception->getMessage());
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
