<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeadUuid extends Migration
{
    private static $currentKey = null;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->upRemoveKeys('notes', ['notes_leadid_foreign', 'notes_lead_id_foreign']);
        $this->upRemoveKeys('messages', ['messages_leadid_foreign', 'messages_lead_id_foreign']);
        $this->upLeadUpdateColumnsWithTruncate();
        $this->upTimeFrameUpgrade();
    }

    private function upRemoveKeys($table, $keys) {
        $results = DB::select(DB::raw('SELECT
                tc.constraint_name, tc.table_name, kcu.column_name, 
                ccu.table_name AS foreign_table_name,
                ccu.column_name AS foreign_column_name 
            FROM 
                information_schema.table_constraints AS tc 
                JOIN information_schema.key_column_usage AS kcu
                  ON tc.constraint_name = kcu.constraint_name
                JOIN information_schema.constraint_column_usage AS ccu
                  ON ccu.constraint_name = tc.constraint_name
            WHERE constraint_type = \'FOREIGN KEY\' AND tc.table_name=\''.$table.'\';'));
        foreach ($results as $result) {
            foreach ($keys as $key) {
                if ($result->constraint_name == $key) {
                    LeadUuid::$currentKey = $key;
                    Schema::table($table, function (Blueprint $table) {
                        $table->dropForeign(LeadUuid::$currentKey);
                    });
                }
            }
        }

    }

    private function upLeadUpdateColumnsWithTruncate() {
        Schema::table('leads', function (Blueprint $table) {
            $table->dropColumn('id');
        });

        DB::statement('TRUNCATE notes CASCADE');
        DB::statement('TRUNCATE messages CASCADE');
        DB::statement('TRUNCATE leads CASCADE');

        Schema::table('leads', function (Blueprint $table) {
            $table->string('firstName')->nullable()->change();
            $table->string('lastName')->nullable()->change();
            $table->float('bedrooms')->change();
            $table->string('channelwebsite')->nullable();
            $table->text('leadcomment')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('street')->nullable();
            $table->string('gmailId')->nullable();
            $table->string('gmailThread')->nullable();
            $table->uuid('vikingUserId')->nullable();
            $table->uuid('vikingPatId')->nullable();
            $table->uuid('duplicateLeadId')->nullable();
            $table->boolean('duplicate')->nullable();
            $table->string('phoneCarrierType')->nullable();
            $table->dropColumn('guid');
            $table->uuid('id');
            $table->primary('id');
        });
    }

    private function upTimeFrameUpgrade() {
        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_time_frame_check');
        });

        $timeFrames = [
            'Asap',
            'Within 3 months',
            '3-6 months',
            '6 months or longer',
            'I\'\'m not sure',
            '0-1 months'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value);
        }, $timeFrames) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_time_frame_check CHECK (\"timeFrame\"::text = ANY (ARRAY[$result]::text[]))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->downLeadUpdateColumnsWithTruncate();
        $this->downForeign();
        $this->downTimeFrameUpgrade();
    }

    private function downLeadUpdateColumnsWithTruncate() {
        DB::statement('TRUNCATE notes CASCADE');
        DB::statement('TRUNCATE messages CASCADE');
        DB::statement('TRUNCATE leads CASCADE');

        Schema::table('leads', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('channelwebsite');
            $table->dropColumn('leadcomment');
            $table->dropColumn('postalcode');
            $table->dropColumn('street');
            $table->dropColumn('gmailId');
            $table->dropColumn('gmailThread');
            $table->dropColumn('vikingUserId');
            $table->dropColumn('vikingPatId');
            $table->dropColumn('duplicateLeadId');
            $table->dropColumn('duplicate');
            $table->dropColumn('phoneCarrierType');
        });
        DB::statement('ALTER TABLE leads ADD COLUMN id BIGSERIAL PRIMARY KEY;');

        Schema::table('leads', function (Blueprint $table) {
            $table->string('firstName')->nullable(false)->change();
            $table->string('lastName')->nullable(false)->change();
            $table->integer('bedrooms')->change();
            $table->string('guid')->nullable();
        });
    }

    private function downForeign() {
        Schema::table('notes', function (Blueprint $table) {
            $table->foreign('leadId')->references('id')->on('leads');
        });
        Schema::table('messages', function (Blueprint $table) {
            $table->foreign('leadId')->references('id')->on('leads');
        });
    }

    private function downTimeFrameUpgrade() {
        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_time_frame_check');
        });

        $timeFrames = [
            'Asap',
            'Within 3 months',
            '3-6 months',
            '6 months or longer'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value);
        }, $timeFrames) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_time_frame_check CHECK (\"timeFrame\"::text = ANY (ARRAY[$result]::text[]))");
    }
}
