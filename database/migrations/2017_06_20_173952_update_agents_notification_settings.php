<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAgentsNotificationSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agents', function(Blueprint $table){
            $table->string('notificationPhone', 20)->nullable();
            $table->string('notificationEmail')->nullable();
            $table->string('callForwardingNumber', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents', function(Blueprint $table){
            $table->dropColumn('notificationPhone');
            $table->dropColumn('notificationEmail');
            $table->dropColumn('callForwardingNumber');
        });
    }
}
