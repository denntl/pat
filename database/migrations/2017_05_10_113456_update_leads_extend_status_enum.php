<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLeadsExtendStatusEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_status_check');
        });

        $statuses = [
            'New lead',
            'Escalated',
            'Call Requested',
            'Urgent',
            'Awaiting Response',
            'Action Required',
            'Qualified',
            'Unqualified',
            'Not a lead',
            'No Response Needed',
            'Referred'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value);
            }, $statuses) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_status_check CHECK (status::text = ANY (ARRAY[$result]::text[]))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("UPDATE leads SET status = null WHERE status = 'Referred';");

        Schema::table('leads', function(Blueprint $table){
            $table->dropForeign('leads_status_check');
        });

        $statuses = [
            'New lead',
            'Escalated',
            'Call Requested',
            'Urgent',
            'Awaiting Response',
            'Action Required',
            'Qualified',
            'Unqualified',
            'Not a lead',
            'No Response Needed'
        ];

        $result = join( ', ', array_map(function( $value ){
            return sprintf("'%s'::character varying", $value); },
            $statuses) );

        DB::statement("ALTER TABLE leads add CONSTRAINT leads_status_check CHECK (status::text = ANY (ARRAY[$result]::text[]))");
    }
}
