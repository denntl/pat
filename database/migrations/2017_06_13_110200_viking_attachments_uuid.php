<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VikingAttachmentsUuid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leadVikingEmails', function (Blueprint $table) {
            $table->dropColumn('id');
        });
        DB::statement('ALTER TABLE "leadVikingEmails" RENAME "vikingId" TO id');
        Schema::table('leadVikingEmails', function (Blueprint $table) {
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE "leadVikingEmails" RENAME id TO "vikingId"');
        Schema::table('leadVikingEmails', function (Blueprint $table) {
            $table->dropPrimary('leadVikingEmails_pkey');
        });
        DB::statement('ALTER TABLE "leadVikingEmails" ADD COLUMN id SERIAL PRIMARY KEY;');
    }
}
