<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('provider', 155)->nullable()->change();
            $table->string('provider_id', 155)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::table('users')->truncate(); // To clear fields with null values

            $table->string('name')->nullable(false)->change();
            $table->string('provider', 155)->nullable(false)->change();
            $table->string('provider_id', 155)->nullable(false)->change();
        });
    }
}
