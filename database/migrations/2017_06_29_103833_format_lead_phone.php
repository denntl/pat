<?php

use App\Facades\PhoneNumber;
use App\Lead;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class FormatLeadPhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Lead::chunk(10, function ($leads) {
            /** @var Lead $lead */
            foreach ($leads as $lead) {
                try {
                    $lead->update([
                        'phone' => PhoneNumber::getFormattedPhone($lead->phone)
                    ]);
                } catch (Exception $exception) {
                    Log::info($exception->getMessage());
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
