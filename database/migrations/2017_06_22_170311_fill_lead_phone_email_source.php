<?php

use App\Lead;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

class FillLeadPhoneEmailSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $id = null;
        $index = 0;

        do {
            /** @var Builder $query */
            if ($id) {
                $query = Lead::where('id', '>', $id);
            } else {
                $query = Lead::where('id', '!=', null);
            }

            /** @var Collection $leads */
            $leads = $query->orderBy('id', 'asc')->limit(1000)->get();
            if ($leads->count() === 0) {
                break;
            }

            $lastLead = $leads[$leads->count() - 1];
            $id = $lastLead->id;

            /** @var Lead $lead */
            foreach ($leads as $lead) {
                $lead->emailSource = $lead->emailSource ?? $lead->email;
                $lead->phoneSource = $lead->phoneSource ?? $lead->phone;

                $lead->saveOrFail();
            }

            $index++;
        } while ($index < 1000);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Not needed...
    }
}
