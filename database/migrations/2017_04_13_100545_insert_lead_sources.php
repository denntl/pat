<?php

use Illuminate\Database\Migrations\Migration;

class InsertLeadSources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('lead_sources')->insert([
            ['name' => 'Anchor Wave'],
            ['name' => 'Bold Leads'],
            ['name' => 'Bomb Bomb'],
            ['name' => 'Boomtown'],
            ['name' => 'Commissions Inc.'],
            ['name' => 'Diverse Solutions'],
            ['name' => 'Ez Lead Pages'],
            ['name' => 'Firepoint'],
            ['name' => 'FiveStreet'],
            ['name' => 'Follow Up Boss'],
            ['name' => 'Geographic Farm'],
            ['name' => 'Homes.com'],
            ['name' => 'House Hunt'],
            ['name' => 'HUDEx/USAHud/allhud'],
            ['name' => 'iHome Finder'],
            ['name' => 'Instapage'],
            ['name' => 'iVortex'],
            ['name' => 'Kunversion Zapier'],
            ['name' => 'KWKLY'],
            ['name' => 'Listing Booster'],
            ['name' => 'Listings to Leads'],
            ['name' => 'Market Leader'],
            ['name' => 'Perfect Storm'],
            ['name' => 'Precise Home Pricing'],
            ['name' => 'Prime Seller Leads'],
            ['name' => 'Pro Agent Websites'],
            ['name' => 'Properties Online'],
            ['name' => 'Real Esales'],
            ['name' => 'Real Estate Pals'],
            ['name' => 'Real Estate Webmasters'],
            ['name' => 'RealGeeks'],
            ['name' => 'Realtor.com'],
            ['name' => 'Realty Now'],
            ['name' => 'Realty Store'],
            ['name' => 'Smart Zip'],
            ['name' => 'Success Website'],
            ['name' => 'Tiger Leads'],
            ['name' => 'Top Agent Connection'],
            ['name' => 'Top Producer'],
            ['name' => 'TORCHx'],
            ['name' => 'Trulia'],
            ['name' => 'Zillow']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('lead_sources')->where('name', 'Anchor Wave')->delete();
        DB::table('lead_sources')->where('name', 'Bold Leads')->delete();
        DB::table('lead_sources')->where('name', 'Bomb Bomb')->delete();
        DB::table('lead_sources')->where('name', 'Boomtown')->delete();
        DB::table('lead_sources')->where('name', 'Commissions Inc.')->delete();
        DB::table('lead_sources')->where('name', 'Diverse Solutions')->delete();
        DB::table('lead_sources')->where('name', 'Ez Lead Pages')->delete();
        DB::table('lead_sources')->where('name', 'Firepoint')->delete();
        DB::table('lead_sources')->where('name', 'FiveStreet')->delete();
        DB::table('lead_sources')->where('name', 'Follow Up Boss')->delete();
        DB::table('lead_sources')->where('name', 'Geographic Farm')->delete();
        DB::table('lead_sources')->where('name', 'Homes.com')->delete();
        DB::table('lead_sources')->where('name', 'House Hunt')->delete();
        DB::table('lead_sources')->where('name', 'HUDEx/USAHud/allhud')->delete();
        DB::table('lead_sources')->where('name', 'iHome Finder')->delete();
        DB::table('lead_sources')->where('name', 'Instapage')->delete();
        DB::table('lead_sources')->where('name', 'iVortex')->delete();
        DB::table('lead_sources')->where('name', 'Kunversion Zapier')->delete();
        DB::table('lead_sources')->where('name', 'KWKLY')->delete();
        DB::table('lead_sources')->where('name', 'Listing Booster')->delete();
        DB::table('lead_sources')->where('name', 'Listings to Leads')->delete();
        DB::table('lead_sources')->where('name', 'Market Leader')->delete();
        DB::table('lead_sources')->where('name', 'Perfect Storm')->delete();
        DB::table('lead_sources')->where('name', 'Precise Home Pricing')->delete();
        DB::table('lead_sources')->where('name', 'Prime Seller Leads')->delete();
        DB::table('lead_sources')->where('name', 'Pro Agent Websites')->delete();
        DB::table('lead_sources')->where('name', 'Properties Online')->delete();
        DB::table('lead_sources')->where('name', 'Real Esales')->delete();
        DB::table('lead_sources')->where('name', 'Real Estate Pals')->delete();
        DB::table('lead_sources')->where('name', 'Real Estate Webmasters')->delete();
        DB::table('lead_sources')->where('name', 'RealGeeks')->delete();
        DB::table('lead_sources')->where('name', 'Realtor.com')->delete();
        DB::table('lead_sources')->where('name', 'Realty Now')->delete();
        DB::table('lead_sources')->where('name', 'Realty Store')->delete();
        DB::table('lead_sources')->where('name', 'Smart Zip')->delete();
        DB::table('lead_sources')->where('name', 'Success Website')->delete();
        DB::table('lead_sources')->where('name', 'Tiger Leads')->delete();
        DB::table('lead_sources')->where('name', 'Top Agent Connection')->delete();
        DB::table('lead_sources')->where('name', 'Top Producer')->delete();
        DB::table('lead_sources')->where('name', 'TORCHx')->delete();
        DB::table('lead_sources')->where('name', 'Trulia')->delete();
        DB::table('lead_sources')->where('name', 'Zillow')->delete();
    }
}
