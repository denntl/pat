<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('leads');
        Schema::dropIfExists('agents');
        Schema::dropIfExists('concierges');
        Schema::dropIfExists('scripts');
        Schema::create('scripts', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['greeting', 'questions', 'conclusion']);
            $table->text('text');
        });
        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('website', 255)->nullable();
            $table->text('calendly')->nullable();
            $table->string('city', 255)->nullable();
            $table->text('state')->nullable();
            $table->text('notes');
            $table->text('admin_notes');
        });
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->integer('agent_id')->unsigned()->nullable();
            $table->foreign('agent_id')->references('id')->on('agents');
            $table->integer('concierge_id')->unsigned()->nullable();
            $table->foreign('concierge_id')->references('id')->on('users');
            $table->string('address')->nullable();
            $table->enum('lead_type', ['buyer', 'seller'])->nullable();
            $table->enum('price_range', ['Under $200,000', '$200,000 - $400,000', '$400,000 - $600,000', '$600,000 - $800,000', '$800,000 - $1,000,000', 'Over $1,000,000'])->nullable();
            $table->enum('mortgage', ['Pre-Qualified', 'Pre-Approved', 'Haven`t Applied Yet', 'Cash Buyer', 'I don`t know', 'Not Applicable'])->nullable();
            $table->integer('bedrooms')->nullable();
            $table->double('bathrooms')->nullable();
            $table->enum('time_frame', ['Asap', 'Within 3 months', '3-6 months', '6 months or longer'])->nullable();
            $table->enum('status', ['New lead', 'Escalated', 'Call Requested', 'Urgent', 'Awaiting Response', 'Action Required', 'Qualified', 'Unqualified', 'Not a lead', 'No Response Needed'])->nullable();
            $table->text('original_email')->nullable();
            $table->timestamps();
        });
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id')->unsigned();
            $table->foreign('lead_id')->references('id')->on('leads');
            $table->timestamp('time');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('leads');
        Schema::dropIfExists('agents');
        Schema::dropIfExists('concierges');
        Schema::dropIfExists('scripts');
    }
}