<?php

use App\Lead;
use App\LeadSource;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class FillLeadSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Lead::chunk(10, function ($leads) {
            /** @var Lead $lead */
            foreach ($leads as $lead) {
                if (!$lead->channelwebsite) {
                    continue;
                }

                try {
                    $source = LeadSource::create(['name' => $lead->channelwebsite]);

                    $lead->update(['sourceId' => $source->id]);
                } catch (Exception $exception) {
                    Log::info($exception->getMessage());
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
