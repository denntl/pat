<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('concierge_role')->nullable();
            $table->string('company_name')->nullable();
            $table->string('team_name')->nullable();
            $table->string('team_leader')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('concierge_role');
            $table->dropColumn('company_name');
            $table->dropColumn('team_name');
            $table->dropColumn('team_leader');
        });
    }
}
