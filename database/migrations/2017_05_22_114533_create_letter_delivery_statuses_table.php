<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\LetterDeliveryStatus;

class CreateLetterDeliveryStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letterDeliveryStatuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Eloquent::unguard();

        $deliveryStatuses = [
            ['id' => 1, 'name' => 'pending'],
            ['id' => 2, 'name' => 'invalid'],
            ['id' => 3, 'name' => 'delivered'],
            ['id' => 4, 'name' => 'opened'],
            ['id' => 5, 'name' => 'spam'],
        ];

        LetterDeliveryStatus::insert($deliveryStatuses);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letterDeliveryStatuses');
    }
}
