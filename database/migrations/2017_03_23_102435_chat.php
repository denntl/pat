<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Chat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scripts', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['greeting', 'questions', 'conclusion']);
            $table->text('text');
        });
        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone', 255);
            $table->string('email', 255);
            $table->string('website', 255);
            $table->text('calendly');
            $table->string('city', 255);
            $table->text('state');
            $table->text('notes');
            $table->text('admin_notes');
        });
        Schema::create('concierges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('role');
            $table->string('company_name');
            $table->string('team_name');
            $table->string('team_leader');
        });
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email');
            $table->integer('agent_id')->unsigned();
            $table->foreign('agent_id')->references('id')->on('agents');
            $table->integer('concierge_id')->unsigned();
            $table->foreign('concierge_id')->references('id')->on('concierges');
            $table->string('address');
            $table->enum('lead_type', ['buyer', 'seller']);
            $table->enum('price_range', ['Under $200,000', '$200,000 - $400,000', '$400,000 - $600,000', '$600,000 - $800,000', '$800,000 - $1,000,000', 'Over $1,000,000']);
            $table->enum('mortgage', ['Pre-Qualified', 'Pre-Approved', 'Haven`t Applied Yet', 'Cash Buyer', 'I don`t know', 'Not Applicable']);
            $table->integer('bedrooms');
            $table->double('bathrooms');
            $table->enum('time_frame', ['Asap', 'Within 3 months', '3-6 months', '6 months or longer']);
            $table->enum('status', ['Qualify', 'Unqualify', 'Request Call', 'Escalate', 'No response needed']);
            $table->text('original_email');
        });
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id')->unsigned();
            $table->foreign('lead_id')->references('id')->on('leads');
            $table->timestamp('time');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('leads');
        Schema::dropIfExists('agents');
        Schema::dropIfExists('concierges');
        Schema::dropIfExists('scripts');
    }
}
