<?php

use App\Lead;
use App\LeadCall;
use App\Message;
use App\Email;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('production')) {
            return;
        }

        factory(Lead::class, 10)->create();
        factory(Message::class, 50)->create();
        factory(LeadCall::class, 20)->create();
        factory(Email::class, 50)->create();
    }
}
